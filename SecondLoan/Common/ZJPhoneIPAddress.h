//
//  ZJPhoneIPAddress.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/10/11.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJPhoneIPAddress : NSObject
- (NSString *)getIPType;     //获取IP类型
- (NSString *)getIPAddress;    //获取IP地址
@end

NS_ASSUME_NONNULL_END
