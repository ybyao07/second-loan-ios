//
//  CommonModelTypeDIc.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonModelTypeDIc : NSObject
@property (strong, nonatomic) NSArray *arraySalaryType;
@property (strong, nonatomic) NSArray *arrayWorkPost;
@property (strong, nonatomic) NSArray *arrayMarriageType;
@property (strong, nonatomic) NSArray *arrayHighEduType;
@property (strong, nonatomic) NSArray *arrayAddressType;
@property (strong, nonatomic) NSArray *arrayCompanyScale;
@property (strong, nonatomic) NSArray *arrayRelationType;
@property (strong, nonatomic) NSArray *arrayMortgage;  //抵押方式
@property (strong, nonatomic) NSArray *arrayWorkOccupation;
@property (strong, nonatomic) NSArray *arrayPaybackType; //还款方式
@property (strong, nonatomic) NSArray *arrayLoanTerm;
@property (nonatomic, strong) NSArray *arrayWorkType;
@property (nonatomic, strong) NSArray *arrayLoanPurpos;

@property (nonatomic, strong) NSArray *arrayHavaChild;


+ (CommonModelTypeDIc *)shareManager;

//+ (NSArray *)arrayWorkType;
//+ (NSArray *)arrayWorkPost;
//+ (NSArray *)arrayLoanTerm;
//+ (NSArray *)arrayPaybackType;
//+ (NSArray *)arrayMarriageType;
//+ (NSArray *)arrayHighEduType;
//+ (NSArray *)arrayCompanyScale;
//- (NSArray *)arraySalaryType;
//+ (NSArray *)arrayRelationType;
+ (NSArray *)arrayFamilyNumber;
+ (NSArray *)arrayYearList;
+ (NSArray *)arrayGraduateYearList;
//+ (NSArray *)arrayAddressType;

+ (NSDictionary *)dicWorkType;
+ (NSDictionary *)dicWorkPostType;
+ (NSDictionary *)dicWorkOccupationType;
+ (NSDictionary *)dicLaonTerm;
+ (NSDictionary *)dicPaybackType;
+ (NSDictionary *)dicEdu;
+ (NSDictionary *)dicCompanyScale;
+ (NSDictionary *)dicSalaryType;
+ (NSDictionary *)dicRelationType;
+ (NSDictionary *)dicAddressType;
+ (NSDictionary *)dicMarState;
+ (NSDictionary *)dicType:(NSArray *)array;
+ (NSDictionary *)dicMortgage;
+ (NSDictionary *)dicLoanPurpos;
+ (NSDictionary *)dicHaveChild;



@end

NS_ASSUME_NONNULL_END
