//
//  NSString+Tools.h
//  danbai_client_ios
//
//  Created by dbjyz on 15/6/13.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Tools)

+ (NSString *)getMoneyStringWithMoneyNumber:(long)money;
//验证手机号码
- (BOOL)validateMobile;

// 验证邮政编码
+ (BOOL)validateZipCode:(NSString *)zipCode;

//验证固话
- (BOOL)checkSolidPhone:(NSString *)strExpress;

- (NSString *)hideMiddle4PhoneNumber;

//验证身份证号
- (BOOL) validateIdentityCard;

//验证邮箱格式
- (BOOL) isValidateEmail;

- (BOOL)isValidateWX;
//真实姓名
- (BOOL) validateTrueName;

//判断是否有特殊符号
- (BOOL)effectivePassword;

- (BOOL)validateUsual;
//匹配首尾空白字符
-(BOOL) validateBlankString;


//判断 是否是8个中文内的字符
- (BOOL)validateEightChineseCharater;


//判断手机型号
+ (NSString *)deviceString;

- (BOOL)judgePassWordLegal;

/*
 *
 *提供城市名字，返回城市代码
 */
+ (NSString *)cityDictionary:(NSString *)cityKey valueForKey:(NSString *)value;


//提供城市代码，返回城市名字
+ (NSString *)cityDictionary2:(NSString *)cityKey valueForKey:(NSString *)value;


//- (CGSize)sizeWithFont:(UIFont *)font;


- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;

//毫秒转日期时间
+ (NSString *)getShowDateWithTime:(NSString *)time;




+ (NSString *)getShowDateHourMinuteSecondWithTime:(NSString *)time;

//今天，昨天 显示
+ (NSString *)getShowDateTodayYesterdayWithTime:(NSString *)time;


//当前时间
+ (NSString *)getCurrentTime;

+ (NSString *)getCurrentTimeWithFormat:(NSString *)strFormat;

//判断是否为整形：
- (BOOL)isPureInt;
//判断是否为浮点形：
- (BOOL)isPureFloat;
//判断仅输入字母或数字
- (BOOL)checkInputShouldAlphaNum;



- (CGSize)wkb_sizeWithFont:(UIFont *)font;



@end
