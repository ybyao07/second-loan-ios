//
//  CommanTool.m
//  WKCarInsurance
//
//  Created by ybyao07 on 16/10/26.
//  Copyright © 2016年 wkbins. All rights reserved.
//

#import "CommanTool.h"
@implementation CommanTool

//+ (void)showAllTextDialog:(NSString *)str inView:(UIView *)view
//{
//    [CommanTool showAllTextDialog:str inView:view withYOffset:0 ];
//}
//
//+ (void)showAllTextDialog:(NSString *)str inView:(UIView *)view withYOffset:(CGFloat)yOffset
//{
//    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
//    [hud setUserInteractionEnabled:NO];
//    [view addSubview:hud];
//    hud.label.text = str;
//    hud.label.numberOfLines = 0;
//    hud.mode = MBProgressHUDModeText;
//    CGPoint offset = hud.offset;
//    offset.y = yOffset;
//    hud.offset = offset;
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:1.0];
//}

#pragma mark 字典转化字符串
+(NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+ (void)setShadowOnView:(UIView *)view
{
    view.layer.shadowColor   = ColorFromHex(0x000000).CGColor;
    view.layer.shadowOffset  = CGSizeMake(0, 0);
    view.layer.shadowOpacity = 0.08;
    view.layer.shadowRadius  = 3;
}

+ (void)setShadowOnView1:(UIView *)view
{
    view.layer.shadowColor   = ColorFromHex(0x000000).CGColor;
    view.layer.shadowOffset  = CGSizeMake(0, 0);
    view.layer.shadowOpacity = 0.1;
    view.layer.shadowRadius  = 5;
    view.layer.cornerRadius = 15;
}


+ (UIViewController *)getCurrentViewController
{
    UIViewController *result = nil;
    // 获取默认的window
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    // app默认windowLevel是UIWindowLevelNormal，如果不是，找到它。
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    
    // 获取window的rootViewController
    result = window.rootViewController;
    while (result.presentedViewController) {
        result = result.presentedViewController;
    }
    if ([result isKindOfClass:[UITabBarController class]]) {
        result = [(UITabBarController *)result selectedViewController];
    }
    if ([result isKindOfClass:[UINavigationController class]]) {
        result = [(UINavigationController *)result visibleViewController];
    }
    return result;
}
+ (NSArray *)sortedWithDict:(NSDictionary*)dict
{
    NSArray*sortedKeys=[[dict allKeys]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableArray *sortedValues = [NSMutableArray array];
    for(NSString *key in sortedKeys){
        [sortedValues addObject:[dict objectForKey:key]];
    }
    return sortedValues;
}
@end
