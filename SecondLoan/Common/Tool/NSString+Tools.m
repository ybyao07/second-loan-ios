//
//  NSString+Tools.m
//  danbai_client_ios
//
//  Created by dbjyz on 15/6/13.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import "NSString+Tools.h"
#import "sys/utsname.h"

@implementation NSString (Tools)

+ (NSString *)getMoneyStringWithMoneyNumber:(long)money{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###,##0"];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:money]];
    return formattedNumberString;
}

- (CGSize)wkb_sizeWithFont:(UIFont *)font{
    NSDictionary *att = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    return [self sizeWithAttributes:att];
}


- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    NSDictionary *att = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    return [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:att context:nil].size;
}

//手机号码验证
- (BOOL)validateMobile
{
    // 由于手机号码的规则一直在变，所以我这边只需要判断输入的内容为以1开头的11位数字
//    NSRange numberRange = [self rangeOfString:@"^1\\d{10}$" options:NSRegularExpressionSearch];
    NSRange numberRange = [self rangeOfString:@"^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$" options:NSRegularExpressionSearch];
    return numberRange.location != NSNotFound;
}

+ (BOOL) validateZipCode:(NSString *)zipCode
{
    NSString *regex = @"[0-9]\\d{5}(?!\\d)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:zipCode];
}

/**
 *  验证手机号以及固话方法
 *  @param  电话号
 *  @return BOOL yes格式正确 no格式错误
 */
- (BOOL)checkSolidPhone:(NSString *)strExpress{
    NSPredicate *checktest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strExpress];
    return [checktest evaluateWithObject:self];
}

- (NSString *)hideMiddle4PhoneNumber
{
    if (self.validateMobile) {
        NSMutableString *numberStr = [NSMutableString new];
        [numberStr appendString:[self substringToIndex:3]];
        [numberStr appendString:@"****"];
        [numberStr appendString:[self substringFromIndex:7]];
        return numberStr;
    }else{
        return @"";
    }
}

- (BOOL)judgePassWordLegal{
    BOOL result = NO;
    if ([self length] >= 8 && [self length] <= 16){
        //数字条件
        NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:NSRegularExpressionCaseInsensitive error:nil];
        //符合数字条件的有几个
        NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:self
                                                                           options:NSMatchingReportProgress
                                                                             range:NSMakeRange(0, self.length)];
        //英文字条件
        NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
        
        //符合英文字条件的有几个
        NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:self
                                                                                 options:NSMatchingReportProgress
                                                                                   range:NSMakeRange(0, self.length)];
       
        //特殊字符
        NSRegularExpression *tSpecialRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[~!@&$%^*()_#]" options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger tSpecialMatchCount = [tSpecialRegularExpression numberOfMatchesInString:self
                                                                                 options:NSMatchingReportProgress
                                                                                   range:NSMakeRange(0, self.length)];
        if((tNumMatchCount >= 1 && tLetterMatchCount >= 1) ||(tNumMatchCount >= 1 && tSpecialMatchCount >= 1) || (tLetterMatchCount >= 1 && tSpecialMatchCount >= 1) ){
            result = YES;
        }
        
    }
    return result;
}


-(BOOL)isValidateEmail {
//    if (isEmptyString(self)) {
//        self = @"";
//    }
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL)isValidateWX{
    NSString *WXRegx = @"^[a-zA-Z]([-_a-zA-Z0-9]{5,19})+$";
    NSPredicate *wxTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", WXRegx];
    return [wxTest evaluateWithObject:self] || [self validateMobile];
}

//真实姓名
- (BOOL) validateTrueName
{
    //    NSString *nameRegex = @"^([\u4e00-\u9fa5]+|([a-zA-Z]+\\s?)+)$";
    NSString *nameRegex = @"^[\u4E00-\u9FFF|A-Za-z|\\.|\\s]*$";
    NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nameRegex];
    return [namePredicate evaluateWithObject:self];
    
}


- (BOOL)validateEightChineseCharater
{
    NSString *chinese = @"[\u4e00-\u9fa5]{2,}";
    NSPredicate *chinesePre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",chinese];
    return [chinesePre evaluateWithObject:self];
}
//身份证号
//- (BOOL) validateIdentityCard
//{
//    BOOL flag;
//    if (self.length <= 0) {
//        flag = NO;
//        return flag;
//    }
//    NSString *regex2 = @"^(\\d{15}$)|(^\\d{17}([0-9]|x|X))$";
////    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
//    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
//    return [identityCardPredicate evaluateWithObject:self];
//}

- (BOOL)validateIdentityCard
{
    NSMutableArray *IDArray = [NSMutableArray array];
    if (self.length <18) {
        return NO;
    }
    // 遍历身份证字符串,存入数组中
    for (int i = 0; i < 18; i++) {
        NSRange range = NSMakeRange(i, 1);
        NSString *subString = [self substringWithRange:range];
        [IDArray addObject:subString];
    }
    // 系数数组
    NSArray *coefficientArray = [NSArray arrayWithObjects:@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2", nil];
    // 余数数组
    NSArray *remainderArray = [NSArray arrayWithObjects:@"1", @"0", @"X", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2", nil];
    // 每一位身份证号码和对应系数相乘之后相加所得的和
    int sum = 0;
    for (int i = 0; i < 17; i++) {
        int coefficient = [coefficientArray[i] intValue];
        int ID = [IDArray[i] intValue];
        sum += coefficient * ID;
    }
    // 这个和除以11的余数对应的数
    NSString *str = remainderArray[(sum % 11)];
    // 身份证号码最后一位
    NSString *string = [self substringFromIndex:17];
    // 如果这个数字和身份证最后一位相同,则符合国家标准,返回YES
    if ([str isEqualToString:string]) {
        return YES;
    } else {
        return NO;
    }
}


//判断是否有特殊符号
- (BOOL)effectivePassword
{
    //    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"&\"< >\\/*/null & /* /NULL"];
    //    NSString *trimmedString = [self stringByTrimmingCharactersInSet:set];
    //    return trimmedString;
    NSString *regex = @"[a-zA-Z0-9]{6,20}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:self];
}
- (BOOL)validateUsual{
    if (!self.length)
        return YES;
    NSArray *strings = [self convertToArray];
    BOOL isValidate = YES;
    for (NSString *singleCharString in strings) {
        NSString *regex = @"[a-zA-Z0-9]";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        isValidate = [predicate evaluateWithObject:singleCharString];
        if (!isValidate) {
            return isValidate;
        }
    }
    
    return isValidate;
}

-(NSArray *)convertToArray
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i=0; i < self.length; i++) {
        NSString *tmp_str = [self substringWithRange:NSMakeRange(i, 1)];
        [arr addObject:[tmp_str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    return arr;
}

-(BOOL) validateBlankString {
    NSString *regex = @"^\\s*|\\s*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:self];
}


//判断手机型号
+ (NSString *)deviceString
{
    // 需要#import "sys/utsname.h"
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if ([deviceString isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([deviceString isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([deviceString isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([deviceString isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([deviceString isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([deviceString isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([deviceString isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([deviceString isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([deviceString isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([deviceString isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([deviceString isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([deviceString isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([deviceString isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([deviceString isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceString isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([deviceString isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([deviceString isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([deviceString isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([deviceString isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([deviceString isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([deviceString isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([deviceString isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([deviceString isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([deviceString isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([deviceString isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([deviceString isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([deviceString isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([deviceString isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([deviceString isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([deviceString isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([deviceString isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([deviceString isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([deviceString isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([deviceString isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([deviceString isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceString isEqualToString:@"x86_64"])       return @"Simulator";
    
    debugLog(@"NOTE: Unknown device type: %@", deviceString);
    return deviceString;
}

+ (NSString *)cityDictionary:(NSString *)cityKey valueForKey:(NSString *)value
{
    
    NSDictionary *cityDic = @{
                              @"中国" : @"1000000",
                              @"安徽省": @"1010000",
                              @"六安市": @"1010100",
                              @"合肥市": @"1010200",
                              @"安庆市": @"1010300",
                              @"宣城市": @"1010400",
                              @"宿州市": @"1010500",
                              @"毫州市": @"1010700",
                              @"池州市": @"1010800",
                              @"淮北市": @"1010900",
                              @"淮南市": @"1011000",
                              @"滁州市": @"1011100",
                              @"芜湖市": @"1011200",
                              @"蚌埠市": @"1011300",
                              @"铜陵市": @"1011400",
                              @"阜阳市": @"1011500",
                              @"马鞍山市": @"1011600",
                              @"黄山市": @"1011700",
                              @"北京": @"1020000",
                              @"北京市": @"1020100",
                              @"重庆": @"1030000",
                              @"重庆市": @"1030100",
                              @"福建省": @"1040000",
                              @"三明市": @"1040100",
                              @"南平市": @"1040200",
                              @"厦门市": @"1040300",
                              @"宁德市": @"1040400",
                              @"泉州市": @"1040500",
                              @"漳州市": @"1040600",
                              @"福州市": @"1040700",
                              @"莆田市": @"1040800",
                              @"龙岩市": @"1040900",
                              @"广东省": @"1050000",
                              @"东莞市": @"1050100",
                              @"中山市": @"1050200",
                              @"云浮市": @"1050300",
                              @"佛山市": @"1050400",
                              @"广州市": @"1050500",
                              @"惠州市": @"1050600",
                              @"揭阳市": @"1050700",
                              @"梅州市": @"1050800",
                              @"汕头市": @"1050900",
                              @"汕尾市": @"1051000",
                              @"江门市": @"1051100",
                              @"河源市": @"1051200",
                              @"深圳市": @"1051300",
                              @"清远市": @"1051400",
                              @"湛江市": @"1051500",
                              @"潮州市": @"1051600",
                              @"珠海市": @"1051700",
                              @"肇庆市": @"1051800",
                              @"茂名市": @"1051900",
                              @"阳江市": @"1052000",
                              @"韶关市": @"1052100",
                              @"广西省": @"1060000",
                              @"北海市": @"1060100",
                              @"南宁市": @"1060200",
                              @"崇左市": @"1060300",
                              @"来宾市": @"1060400",
                              @"柳州市": @"1060500",
                              @"梧州市": @"1060600",
                              @"河池市": @"1060700",
                              @"玉林市": @"1060800",
                              @"百色市": @"1060900",
                              @"贵港市": @"1061000",
                              @"贺州市": @"1061100",
                              @"钦州市": @"1061200",
                              @"防城港市": @"1061300",
                              @"桂林市": @"1061400",
                              @"甘肃省": @"1070000",
                              @"临夏回族自治州": @"1070100",
                              @"兰州市": @"1070200",
                              @"嘉峪关市": @"1070300",
                              @"天水市": @"1070400",
                              @"定西市": @"1070500",
                              @"平凉市": @"1070600",
                              @"庆阳市": @"1070700",
                              @"张掖市": @"1070800",
                              @"武威市": @"1070900",
                              @"甘南藏族自治州": @"1071000",
                              @"白银市": @"1071100",
                              @"酒泉市": @"1071200",
                              @"金昌市": @"1071300",
                              @"陇南市": @"1071400",
                              @"贵州省": @"1080000",
                              @"六盘水市": @"1080100",
                              @"安顺市": @"1080200",
                              @"毕节市": @"1080300",
                              @"贵阳市": @"1080400",
                              @"遵义市": @"1080500",
                              @"铜仁市": @"1080600",
                              @"黔东南苗族侗族自治州": @"1080700",
                              @"黔南布依族苗族自治州": @"1080800",
                              @"黔西南布依族苗族自治州": @"1080900",
                              @"河北省": @"1090000",
                              @"保定市": @"1090100",
                              @"唐山市": @"1090200",
                              @"廊坊市": @"1090300",
                              @"张家口市": @"1090400",
                              @"承德市": @"1090500",
                              @"沧州市": @"1090600",
                              @"石家庄市": @"1090700",
                              @"秦皇岛市": @"1090800",
                              @"衡水市": @"1090900",
                              @"邢台市": @"1091000",
                              @"邯郸市": @"1091100",
                              @"河南省": @"1100000",
                              @"三门峡市": @"1100100",
                              @"信阳市": @"1100200",
                              @"南阳市": @"1100300",
                              @"周口市": @"1100400",
                              @"商丘市": @"1100500",
                              @"安阳市": @"1100600",
                              @"平顶山市": @"1100700",
                              @"开封市": @"1100800",
                              @"新乡市": @"1100900",
                              @"洛阳市": @"1101000",
                              @"漯河市": @"1101100",
                              @"濮阳市": @"1101200",
                              @"焦作市": @"1101300",
                              @"许昌市": @"1101400",
                              @"郑州市": @"1101500",
                              @"驻马店市": @"1101600",
                              @"鹤壁市": @"1101700",
                              @"海南省": @"1110000",
                              @"海南省直辖县级行政单位": @"1110100",
                              @"三亚市": @"1110200",
                              @"海口市": @"1111300",
                              @"湖北省": @"1120000",
                              @"湖北省直辖县级行政单位": @"1120100",
                              @"十堰市": @"1120200",
                              @"咸宁市": @"1120300",
                              @"天门市": @"1120400",
                              @"孝感市": @"1120500",
                              @"宜昌市": @"1120600",
                              @"恩施土家族苗族自治州": @"1120700",
                              @"武汉市": @"1120800",
                              @"荆州市": @"1121100",
                              @"荆门市": @"1121200",
                              @"襄阳市": @"1121300",
                              @"鄂州市": @"1121400",
                              @"随州市": @"1121500",
                              @"黄冈市": @"1121600",
                              @"黄石市": @"1121700",
                              @"湖南省": @"1130000",
                              @"娄底市": @"1130100",
                              @"岳阳市": @"1130200",
                              @"常德市": @"1130300",
                              @"张家界市": @"1130400",
                              @"怀化市": @"1130500",
                              @"株洲市": @"1130600",
                              @"永州市": @"1130700",
                              @"湘潭市": @"1130800",
                              @"湘西土家族苗族自治州": @"1130900",
                              @"益阳市": @"1131000",
                              @"衡阳市": @"1131100",
                              @"邵阳市": @"1131200",
                              @"郴州市": @"1131300",
                              @"长沙市": @"1131400",
                              @"黑龙江省": @"1140000",
                              @"七台河市": @"1140100",
                              @"伊春市": @"1140200",
                              @"佳木斯市": @"1140300",
                              @"双鸭山市": @"1140400",
                              @"哈尔滨市": @"1140500",
                              @"大兴安岭地区": @"1140600",
                              @"大庆市": @"1140700",
                              @"牡丹江市": @"1140800",
                              @"绥化市": @"1140900",
                              @"鸡西市": @"1141000",
                              @"鹤岗市": @"1141100",
                              @"黑河市": @"1141200",
                              @"齐齐哈尔市": @"1141300",
                              @"吉林省": @"1150000",
                              @"吉林市": @"1150100",
                              @"四平市": @"1150200",
                              @"延边朝鲜族自治州": @"1150300",
                              @"松原市": @"1150400",
                              @"白城市": @"1150500",
                              @"白山市": @"1150600",
                              @"辽源市": @"1150700",
                              @"通化市": @"1150800",
                              @"长春市": @"1150900",
                              @"江苏省": @"1160000",
                              @"南京市": @"1160100",
                              @"南通市": @"1160200",
                              @"宿迁市": @"1160300",
                              @"常州市": @"1160400",
                              @"徐州市": @"1160500",
                              @"扬州市": @"1160600",
                              @"无锡市": @"1160700",
                              @"泰州市": @"1160800",
                              @"淮安市": @"1160900",
                              @"盐城市": @"1161000",
                              @"苏州市": @"1161100",
                              @"连云港市": @"1161200",
                              @"镇江市": @"1161300",
                              @"江西省": @"1170000",
                              @"上饶市": @"1170100",
                              @"九江市": @"1170200",
                              @"南昌市": @"1170300",
                              @"吉安市": @"1170400",
                              @"宜春市": @"1170500",
                              @"抚州市": @"1170600",
                              @"新余市": @"1170700",
                              @"景德镇市": @"1170800",
                              @"萍乡市": @"1170900",
                              @"赣州市": @"1171000",
                              @"鹰潭市": @"1171100",
                              @"辽宁省": @"1180000",
                              @"丹东市": @"1180100",
                              @"大连市": @"1180200",
                              @"抚顺市": @"1180300",
                              @"朝阳市": @"1180400",
                              @"本溪市": @"1180500",
                              @"沈阳市": @"1180600",
                              @"盘锦市": @"1180700",
                              @"营口市": @"1180800",
                              @"葫芦岛市": @"1180900",
                              @"辽阳市": @"1181000",
                              @"铁岭市": @"1181100",
                              @"锦州市": @"1181200",
                              @"阜新市": @"1181300",
                              @"鞍山市": @"1181400",
                              @"内蒙古": @"1190000",
                              @"乌兰察布市": @"1190100",
                              @"乌海市": @"1190200",
                              @"兴安盟": @"1190300",
                              @"包头市": @"1190400",
                              @"呼和浩特市": @"1190500",
                              @"巴彦淖尔市": @"1190600",
                              @"赤峰市": @"1190700",
                              @"通辽市": @"1190800",
                              @"鄂尔多斯市": @"1190900",
                              @"锡林郭勒盟": @"1191000",
                              @"阿拉善盟": @"1191100",
                              @"呼伦贝尔市": @"1191200",
                              @"宁夏": @"1200000",
                              @"中卫市": @"1200100",
                              @"吴忠市": @"1200200",
                              @"固原市": @"1200300",
                              @"石嘴山市": @"1200400",
                              @"银川市": @"1200500",
                              @"青海省": @"1210000",
                              @"果洛藏族自治州": @"1210100",
                              @"海东地区": @"1210200",
                              @"海北藏族自治州": @"1210300",
                              @"海南藏族自治州": @"1210400",
                              @"海西蒙古族藏族自治州": @"1210500",
                              @"玉树藏族自治州": @"1210600",
                              @"西宁市": @"1210700",
                              @"黄南藏族自治州": @"1210800",
                              @"上海": @"1220000",
                              @"上海市": @"1220100",
                              @"四川省": @"1230000",
                              @"乐山市": @"1230100",
                              @"内江市": @"1230200",
                              @"凉山彝族自治州": @"1230300",
                              @"南充市": @"1230400",
                              @"宜宾市": @"1230500",
                              @"巴中市": @"1230600",
                              @"广元市": @"1230700",
                              @"广安市": @"1230800",
                              @"德阳市": @"1230900",
                              @"成都市": @"1231000",
                              @"攀枝花市": @"1231100",
                              @"泸州市": @"1231200",
                              @"甘孜藏族自治州": @"1231300",
                              @"眉山市": @"1231400",
                              @"绵阳市": @"1231500",
                              @"自贡市": @"1231600",
                              @"资阳市": @"1231700",
                              @"达州市": @"1231800",
                              @"遂宁市": @"1231900",
                              @"阿坝藏族羌族自治州": @"1232000",
                              @"雅安市": @"1232100",
                              @"山东省": @"1240000",
                              @"东营市": @"1240100",
                              @"临沂市": @"1240200",
                              @"威海市": @"1240300",
                              @"德州市": @"1240400",
                              @"日照市": @"1240500",
                              @"枣庄市": @"1240600",
                              @"泰安市": @"1240700",
                              @"济南市": @"1240800",
                              @"济宁市": @"1240900",
                              @"淄博市": @"1241000",
                              @"滨州市": @"1241100",
                              @"潍坊市": @"1241200",
                              @"烟台市": @"1241300",
                              @"聊城市": @"1241400",
                              @"莱芜市": @"1241500",
                              @"青岛市": @"1241600",
                              @"菏泽市": @"1241700",
                              @"山西省": @"1250000",
                              @"临汾市": @"1250100",
                              @"吕梁市": @"1250200",
                              @"大同市": @"1250300",
                              @"太原市": @"1250400",
                              @"忻州市": @"1250500",
                              @"晋中市": @"1250600",
                              @"晋城市": @"1250700",
                              @"朔州市": @"1250800",
                              @"运城市": @"1250900",
                              @"长治市": @"1251000",
                              @"阳泉市": @"1251100",
                              @"陕西省": @"1260000",
                              @"咸阳市": @"1260100",
                              @"商洛市": @"1260200",
                              @"安康市": @"1260300",
                              @"宝鸡市": @"1260400",
                              @"延安市": @"1260500",
                              @"榆林市": @"1260600",
                              @"汉中市": @"1260700",
                              @"渭南市": @"1260800",
                              @"西安市": @"1260900",
                              @"铜川市": @"1261000",
                              @"天津": @"1270000",
                              @"天津市": @"1270100",
                              @"新疆": @"1280000",
                              @"乌鲁木齐市": @"1280100",
                              @"新疆维吾尔自治区直辖县级行政单位": @"1280200",
                              @"伊犁哈萨克自治州": @"1280300",
                              @"克孜勒苏柯尔克孜自治州": @"1280400",
                              @"克拉玛依市": @"1280500",
                              @"博尔塔拉蒙古自治州": @"1280600",
                              @"吐鲁番地区": @"1280700",
                              @"和田地区": @"1280800",
                              @"哈密地区": @"1280900",
                              @"喀什地区": @"1281000",
                              @"塔城地区": @"1281200",
                              @"巴音郭楞蒙古自治州": @"1281300",
                              @"昌吉回族自治州": @"1281400",
                              @"阿克苏地区": @"1281600",
                              @"阿勒泰地区": @"1281700",
                              @"西藏": @"1290000",
                              @"山南地区": @"1290100",
                              @"拉萨市": @"1290200",
                              @"日喀则地区": @"1290300",
                              @"昌都地区": @"1290400",
                              @"林芝地区": @"1290500",
                              @"那曲地区": @"1290600",
                              @"阿里地区": @"1290700",
                              @"云南省": @"1300000",
                              @"临沧市": @"1300100",
                              @"丽江市": @"1300200",
                              @"保山市": @"1300300",
                              @"大理白族自治州": @"1300400",
                              @"德宏州市": @"1300500",
                              @"怒江傈僳族自治州": @"1300600",
                              @"文山壮族苗族自治州": @"1300700",
                              @"昆明市": @"1300800",
                              @"昭通市": @"1300900",
                              @"普洱市": @"1301000",
                              @"曲靖市": @"1301100",
                              @"楚雄彝族自治州": @"1301200",
                              @"玉溪市": @"1301300",
                              @"红河哈尼族彝族自治州": @"1301400",
                              @"西双版纳傣族自治州": @"1301500",
                              @"迪庆藏族自治州": @"1301600",
                              @"浙江省": @"1310000",
                              @"丽水市": @"1310100",
                              @"台州市": @"1310200",
                              @"嘉兴市": @"1310300",
                              @"宁波市": @"1310400",
                              @"杭州市": @"1310500",
                              @"温州市": @"1310600",
                              @"湖州市": @"1310700",
                              @"绍兴市": @"1310800",
                              @"舟山市": @"1310900",
                              @"衢州市": @"1311000",
                              @"金华市": @"1311100",
                              @"台湾省": @"1320000",
                              @"台北市": @"1320100",
                              @"澳门": @"1330000",
                              @"澳门特别行政区": @"1330100",
                              @"香港": @"1340000",
                              @"香港特别行政区": @"1340100",
                              };


    if (cityKey) {
        NSString *citycode = [cityDic objectForKey:cityKey];
        return citycode;
    }else {
        NSString *cityValue = [cityDic valueForKey:value];
        if (cityValue == nil) {
            return @"浙江省 杭州市";
        }else {
            return cityValue;
        }
    }
   
}


+ (NSString *)cityDictionary2:(NSString *)cityKey valueForKey:(NSString *)value
{
    
    NSDictionary *cityDic = @{
                              @"1000000" : @"中国",
                              @"1010000": @"安徽省",
                              @"1010100": @"六安市",
                              @"1010200": @"合肥市",
                              @"1010300": @"安庆市",
                              @"1010400": @"宣城市",
                              @"1010500": @"宿州市",
                              @"1010700": @"毫州市",
                              @"1010800": @"池州市",
                              @"1010900": @"淮北市",
                              @"1011000": @"淮南市",
                              @"1011100": @"滁州市",
                              @"1011200": @"芜湖市",
                              @"1011300": @"蚌埠市",
                              @"1011400": @"铜陵市",
                              @"1011500": @"阜阳市",
                              @"1011600": @"马鞍山市",
                              @"1011700": @"黄山市",
                              @"1020000": @"北京",
                              @"1020100": @"北京市",
                              @"1030000": @"重庆",
                              @"1030100": @"重庆市",
                              @"1040000": @"福建省",
                              @"1040100": @"三明市",
                              @"1040200": @"南平市",
                              @"1040300": @"厦门市",
                              @"1040400": @"宁德市",
                              @"1040500": @"泉州市",
                              @"1040600": @"漳州市",
                              @"1040700": @"福州市",
                              @"1040800": @"莆田市",
                              @"1040900": @"龙岩市",
                              @"1050000": @"广东省",
                              @"1050100": @"东莞市",
                              @"1050200": @"中山市",
                              @"1050300": @"云浮市",
                              @"1050400": @"佛山市",
                              @"1050500": @"广州市",
                              @"1050600": @"惠州市",
                              @"1050700": @"揭阳市",
                              @"1050800": @"梅州市",
                              @"1050900": @"汕头市",
                              @"1051000": @"汕尾市",
                              @"1051100": @"江门市",
                              @"1051200": @"河源市",
                              @"1051300": @"深圳市",
                              @"1051400": @"清远市",
                              @"1051500": @"湛江市",
                              @"1051600": @"潮州市",
                              @"1051700": @"珠海市",
                              @"1051800": @"肇庆市",
                              @"1051900": @"茂名市",
                              @"1052000": @"阳江市",
                              @"1052100": @"韶关市",
                              @"1060000": @"广西省",
                              @"1060100": @"北海市",
                              @"1060200": @"南宁市",
                              @"1060300": @"崇左市",
                              @"1060400": @"来宾市",
                              @"1060500": @"柳州市",
                              @"1060600": @"梧州市",
                              @"1060700": @"河池市",
                              @"1060800": @"玉林市",
                              @"1060900": @"百色市",
                              @"1061000": @"贵港市",
                              @"1061100": @"贺州市",
                              @"1061200": @"钦州市",
                              @"1061300": @"防城港市",
                              @"1061400": @"桂林市",
                              @"1070000": @"甘肃省",
                              @"1070100": @"临夏回族自治州",
                              @"1070200": @"兰州市",
                              @"1070300": @"嘉峪关市",
                              @"1070400": @"天水市",
                              @"1070500": @"定西市",
                              @"1070600": @"平凉市",
                              @"1070700": @"庆阳市",
                              @"1070800": @"张掖市",
                              @"1070900": @"武威市",
                              @"1071000": @"甘南藏族自治州",
                              @"1071100": @"白银市",
                              @"1071200": @"酒泉市",
                              @"1071300": @"金昌市",
                              @"1071400": @"陇南市",
                              @"1080000": @"贵州省",
                              @"1080100": @"六盘水市",
                              @"1080200": @"安顺市",
                              @"1080300": @"毕节市",
                              @"1080400": @"贵阳市",
                              @"1080500": @"遵义市",
                              @"1080600": @"铜仁市",
                              @"1080700": @"黔东南苗族侗族自治州",
                              @"1080800": @"黔南布依族苗族自治州",
                              @"1080900": @"黔西南布依族苗族自治州",
                              @"1090000": @"河北省",
                              @"1090100": @"保定市",
                              @"1090200": @"唐山市",
                              @"1090300": @"廊坊市",
                              @"1090400": @"张家口市",
                              @"1090500": @"承德市",
                              @"1090600": @"沧州市",
                              @"1090700": @"石家庄市",
                              @"1090800": @"秦皇岛市",
                              @"1090900": @"衡水市",
                              @"1091000": @"邢台市",
                              @"1091100": @"邯郸市",
                              @"1100000": @"河南省",
                              @"1100100": @"三门峡市",
                              @"1100200": @"信阳市",
                              @"1100300": @"南阳市",
                              @"1100400": @"周口市",
                              @"1100500": @"商丘市",
                              @"1100600": @"安阳市",
                              @"1100700": @"平顶山市",
                              @"1100800": @"开封市",
                              @"1100900": @"新乡市",
                              @"1101000": @"洛阳市",
                              @"1101100": @"漯河市",
                              @"1101200": @"濮阳市",
                              @"1101300": @"焦作市",
                              @"1101400": @"许昌市",
                              @"1101500": @"郑州市",
                              @"1101600": @"驻马店市",
                              @"1101700": @"鹤壁市",
                              @"1110000": @"海南省",
                              @"1110100": @"海南省直辖县级行政单位",
                              @"1110200": @"三亚市",
                              @"1111300": @"海口市",
                              @"1120000": @"湖北省",
                              @"1120100": @"湖北省直辖县级行政单位",
                              @"1120200": @"十堰市",
                              @"1120300": @"咸宁市",
                              @"1120400": @"天门市",
                              @"1120500": @"孝感市",
                              @"1120600": @"宜昌市",
                              @"1120700": @"恩施土家族苗族自治州",
                              @"1120800": @"武汉市",
                              @"1121100": @"荆州市",
                              @"1121200": @"荆门市",
                              @"1121300": @"襄阳市",
                              @"1121400": @"鄂州市",
                              @"1121500": @"随州市",
                              @"1121600": @"黄冈市",
                              @"1121700": @"黄石市",
                              @"1130000": @"湖南省",
                              @"1130100": @"娄底市",
                              @"1130200": @"岳阳市",
                              @"1130300": @"常德市",
                              @"1130400": @"张家界市",
                              @"1130500": @"怀化市",
                              @"1130600": @"株洲市",
                              @"1130700": @"永州市",
                              @"1130800": @"湘潭市",
                              @"1130900": @"湘西土家族苗族自治州",
                              @"1131000": @"益阳市",
                              @"1131100": @"衡阳市",
                              @"1131200": @"邵阳市",
                              @"1131300": @"郴州市",
                              @"1131400": @"长沙市",
                              @"1140000": @"黑龙江省",
                              @"1140100": @"七台河市",
                              @"1140200": @"伊春市",
                              @"1140300": @"佳木斯市",
                              @"1140400": @"双鸭山市",
                              @"1140500": @"哈尔滨市",
                              @"1140600": @"大兴安岭地区",
                              @"1140700": @"大庆市",
                              @"1140800": @"牡丹江市",
                              @"1140900": @"绥化市",
                              @"1141000": @"鸡西市",
                              @"1141100": @"鹤岗市",
                              @"1141200": @"黑河市",
                              @"1141300": @"齐齐哈尔市",
                              @"1150000": @"吉林省",
                              @"1150100": @"吉林市",
                              @"1150200": @"四平市",
                              @"1150300": @"延边朝鲜族自治州",
                              @"1150400": @"松原市",
                              @"1150500": @"白城市",
                              @"1150600": @"白山市",
                              @"1150700": @"辽源市",
                              @"1150800": @"通化市",
                              @"1150900": @"长春市",
                              @"1160000": @"江苏省",
                              @"1160100": @"南京市",
                              @"1160200": @"南通市",
                              @"1160300": @"宿迁市",
                              @"1160400": @"常州市",
                              @"1160500": @"徐州市",
                              @"1160600": @"扬州市",
                              @"1160700": @"无锡市",
                              @"1160800": @"泰州市",
                              @"1160900": @"淮安市",
                              @"1161000": @"盐城市",
                              @"1161100": @"苏州市",
                              @"1161200": @"连云港市",
                              @"1161300": @"镇江市",
                              @"1170000": @"江西省",
                              @"1170100": @"上饶市",
                              @"1170200": @"九江市",
                              @"1170300": @"南昌市",
                              @"1170400": @"吉安市",
                              @"1170500": @"宜春市",
                              @"1170600": @"抚州市",
                              @"1170700": @"新余市",
                              @"1170800": @"景德镇市",
                              @"1170900": @"萍乡市",
                              @"1171000": @"赣州市",
                              @"1171100": @"鹰潭市",
                              @"1180000": @"辽宁省",
                              @"1180100": @"丹东市",
                              @"1180200": @"大连市",
                              @"1180300": @"抚顺市",
                              @"1180400": @"朝阳市",
                              @"1180500": @"本溪市",
                              @"1180600": @"沈阳市",
                              @"1180700": @"盘锦市",
                              @"1180800": @"营口市",
                              @"1180900": @"葫芦岛市",
                              @"1181000": @"辽阳市",
                              @"1181100": @"铁岭市",
                              @"1181200": @"锦州市",
                              @"1181300": @"阜新市",
                              @"1181400": @"鞍山市",
                              @"1190000": @"内蒙古",
                              @"1190100": @"乌兰察布市",
                              @"1190200": @"乌海市",
                              @"1190300": @"兴安盟",
                              @"1190400": @"包头市",
                              @"1190500": @"呼和浩特市",
                              @"1190600": @"巴彦淖尔市",
                              @"1190700": @"赤峰市",
                              @"1190800": @"通辽市",
                              @"1190900": @"鄂尔多斯市",
                              @"1191000": @"锡林郭勒盟",
                              @"1191100": @"阿拉善盟",
                              @"1191200": @"呼伦贝尔市",
                              @"1200000": @"宁夏",
                              @"1200100": @"中卫市",
                              @"1200200": @"吴忠市",
                              @"1200300": @"固原市",
                              @"1200400": @"石嘴山市",
                              @"1200500": @"银川市",
                              @"1210000": @"青海省",
                              @"1210100": @"果洛藏族自治州",
                              @"1210200": @"海东地区",
                              @"1210300": @"海北藏族自治州",
                              @"1210400": @"海南藏族自治州",
                              @"1210500": @"海西蒙古族藏族自治州",
                              @"1210600": @"玉树藏族自治州",
                              @"1210700": @"西宁市",
                              @"1210800": @"黄南藏族自治州",
                              @"1220000": @"上海",
                              @"1220100": @"上海市",
                              @"1230000": @"四川省",
                              @"1230100": @"乐山市",
                              @"1230200": @"内江市",
                              @"1230300": @"凉山彝族自治州",
                              @"1230400": @"南充市",
                              @"1230500": @"宜宾市",
                              @"1230600": @"巴中市",
                              @"1230700": @"广元市",
                              @"1230800": @"广安市",
                              @"1230900": @"德阳市",
                              @"1231000": @"成都市",
                              @"1231100": @"攀枝花市",
                              @"1231200": @"泸州市",
                              @"1231300": @"甘孜藏族自治州",
                              @"1231400": @"眉山市",
                              @"1231500": @"绵阳市",
                              @"1231600": @"自贡市",
                              @"1231700": @"资阳市",
                              @"1231800": @"达州市",
                              @"1231900": @"遂宁市",
                              @"1232000": @"阿坝藏族羌族自治州",
                              @"1232100": @"雅安市",
                              @"1240000": @"山东省",
                              @"1240100": @"东营市",
                              @"1240200": @"临沂市",
                              @"1240300": @"威海市",
                              @"1240400": @"德州市",
                              @"1240500": @"日照市",
                              @"1240600": @"枣庄市",
                              @"1240700": @"泰安市",
                              @"1240800": @"济南市",
                              @"1240900": @"济宁市",
                              @"1241000": @"淄博市",
                              @"1241100": @"滨州市",
                              @"1241200": @"潍坊市",
                              @"1241300": @"烟台市",
                              @"1241400": @"聊城市",
                              @"1241500": @"莱芜市",
                              @"1241600": @"青岛市",
                              @"1241700": @"菏泽市",
                              @"1250000": @"山西省",
                              @"1250100": @"临汾市",
                              @"1250200": @"吕梁市",
                              @"1250300": @"大同市",
                              @"1250400": @"太原市",
                              @"1250500": @"忻州市",
                              @"1250600": @"晋中市",
                              @"1250700": @"晋城市",
                              @"1250800": @"朔州市",
                              @"1250900": @"运城市",
                              @"1251000": @"长治市",
                              @"1251100": @"阳泉市",
                              @"1260000": @"陕西省",
                              @"1260100": @"咸阳市",
                              @"1260200": @"商洛市",
                              @"1260300": @"安康市",
                              @"1260400": @"宝鸡市",
                              @"1260500": @"延安市",
                              @"1260600": @"榆林市",
                              @"1260700": @"汉中市",
                              @"1260800": @"渭南市",
                              @"1260900": @"西安市",
                              @"1261000": @"铜川市",
                              @"1270000": @"天津",
                              @"1270100": @"天津市",
                              @"1280000": @"新疆",
                              @"1280100": @"乌鲁木齐市",
                              @"1280200": @"新疆维吾尔自治区直辖县级行政单位",
                              @"1280300": @"伊犁哈萨克自治州",
                              @"1280400": @"克孜勒苏柯尔克孜自治州",
                              @"1280500": @"克拉玛依市",
                              @"1280600": @"博尔塔拉蒙古自治州",
                              @"1280700": @"吐鲁番地区",
                              @"1280800": @"和田地区",
                              @"1280900": @"哈密地区",
                              @"1281000": @"喀什地区",
                              @"1281200": @"塔城地区",
                              @"1281300": @"巴音郭楞蒙古自治州",
                              @"1281400": @"昌吉回族自治州",
                              @"1281600": @"阿克苏地区",
                              @"1281700": @"阿勒泰地区",
                              @"1290000": @"西藏",
                              @"1290100": @"山南地区",
                              @"1290200": @"拉萨市",
                              @"1290300": @"日喀则地区",
                              @"1290400": @"昌都地区",
                              @"1290500": @"林芝地区",
                              @"1290600": @"那曲地区",
                              @"1290700": @"阿里地区",
                              @"1300000": @"云南省",
                              @"1300100": @"临沧市",
                              @"1300200": @"丽江市",
                              @"1300300": @"保山市",
                              @"1300400": @"大理白族自治州",
                              @"1300500": @"德宏州市",
                              @"1300600": @"怒江傈僳族自治州",
                              @"1300700": @"文山壮族苗族自治州",
                              @"1300800": @"昆明市",
                              @"1300900": @"昭通市",
                              @"1301000": @"普洱市",
                              @"1301100": @"曲靖市",
                              @"1301200": @"楚雄彝族自治州",
                              @"1301300": @"玉溪市",
                              @"1301400": @"红河哈尼族彝族自治州",
                              @"1301500": @"西双版纳傣族自治州",
                              @"1301600": @"迪庆藏族自治州",
                              @"1310000": @"浙江省",
                              @"1310100": @"丽水市",
                              @"1310200": @"台州市",
                              @"1310300": @"嘉兴市",
                              @"1310400": @"宁波市",
                              @"1310500": @"杭州市",
                              @"1310600": @"温州市",
                              @"1310700": @"湖州市",
                              @"1310800": @"绍兴市",
                              @"1310900": @"舟山市",
                              @"1311000": @"衢州市",
                              @"1311100": @"金华市",
                              @"1320000": @"台湾省",
                              @"1320100": @"台北市",
                              @"1330000": @"澳门",
                              @"1330100": @"澳门特别行政区",
                              @"1340000": @"香港",
                              @"1340100": @"香港特别行政区",
                              };
    
    
    if (cityKey) {
        NSString *citycode = [cityDic objectForKey:cityKey];
        return citycode;
    }else {
        NSString *cityValue = [cityDic valueForKey:value];
        if (cityValue == nil) {
            return @"杭州市";
        }else {
            return cityValue;
        }
    }
}

+ (NSString *)getCurrentTime
{
//    @"YYYY/MM/dd hh:mm:ss"
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)getCurrentTimeWithFormat:(NSString *)strFormat
{
    //    @"YYYY/MM/dd hh:mm:ss"
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:strFormat];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}



+ (NSString *)getShowDateWithTime:(NSString *)time{
    /**
     传入时间转NSDate类型
     */
    NSDate *timeDate = [[NSDate alloc]initWithTimeIntervalSince1970:[time longLongValue]/1000.0];
    /**
     初始化并定义Formatter
     
     :returns: NSDate
     */
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    /**
     *  使用Formatter格式化时间（传入时间和当前时间）为NSString
     */
    NSString *timeStr = [dateFormatter stringFromDate:timeDate];
    return timeStr;
//    /**
//     *  判断前四位是不是本年，不是本年直接返回完整时间
//     */
//    if ([[timeStr substringWithRange:NSMakeRange(0, 4)] rangeOfString:[nowDateStr substringWithRange:NSMakeRange(0, 4)]].location == NSNotFound) {
//        return timeStr;
//    }else{
//        /**
//         *  判断是不是本天，是本天返回HH:mm,不是返回MM-dd HH:mm
//         */
//        if ([[timeStr substringWithRange:NSMakeRange(5, 5)] rangeOfString:[nowDateStr substringWithRange:NSMakeRange(5, 5)]].location != NSNotFound) {
//            return [timeStr substringWithRange:NSMakeRange(11, 5)];
//        }else{
//            return [timeStr substringWithRange:NSMakeRange(5, 11)];
//        }
//    }
}


+ (NSString *)getShowDateHourMinuteSecondWithTime:(NSString *)time
{
    /**
     传入时间转NSDate类型
     */
    NSDate *timeDate = [[NSDate alloc]initWithTimeIntervalSince1970:[time longLongValue]/1000.0];
    /**
     初始化并定义Formatter
     :returns: NSDate
     */
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *timeStr = [dateFormatter stringFromDate:timeDate];
    return timeStr;
}

+ (NSString *)getShowDateTodayYesterdayWithTime:(NSString *)time
{
    /**
     传入时间转NSDate类型
     */
    NSDate*inputDate = [[NSDate alloc]initWithTimeIntervalSince1970:[time longLongValue]/1000.0];
    //NSLog(@"startDate= %@", inputDate);
    NSDateFormatter *outputFormatter= [[NSDateFormatter alloc] init];
    [outputFormatter setLocale:[NSLocale currentLocale]];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //get date str
    NSString *str= [outputFormatter stringFromDate:inputDate];
    //str to nsdate
    NSDate *strDate = [outputFormatter dateFromString:str];
    //修正8小时的差时
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: strDate];
    NSDate *endDate = [strDate  dateByAddingTimeInterval: interval];
    //NSLog(@"endDate:%@",endDate);
    NSString *lastTime = [NSString compareDate:endDate];
    NSLog(@"lastTime = %@",lastTime);
    return lastTime;
}

+(NSString *)compareDate:(NSDate *)date{
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    //修正8小时之差
    NSDate *date1 = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date1];
    NSDate *localeDate = [date1  dateByAddingTimeInterval: interval];
    //NSLog(@"nowdate=%@\nolddate = %@",localeDate,date);
    NSDate *today = localeDate;
    NSDate *yesterday,*beforeOfYesterday;
    //今年
    NSString *toYears;
    toYears = [[today description] substringToIndex:4];
    yesterday = [today dateByAddingTimeInterval: -secondsPerDay];
    beforeOfYesterday = [yesterday dateByAddingTimeInterval: -secondsPerDay];
    // 10 first characters of description is the calendar date:
    NSString *todayString = [[today description] substringToIndex:10];
    NSString *yesterdayString = [[yesterday description] substringToIndex:10];
    NSString *beforeOfYesterdayString = [[beforeOfYesterday description] substringToIndex:10];
    
    NSString *dateString = [[date description] substringToIndex:10];
    NSString *dateYears = [[date description] substringToIndex:4];
    
    NSString *dateContent;
    if ([dateYears isEqualToString:toYears]) {//同一年
        //今 昨 前天的时间
        NSString *time = [[date description] substringWithRange:(NSRange){11,5}];
        //其他时间
        NSString *time2 = [[date description] substringWithRange:(NSRange){5,11}];
        if ([dateString isEqualToString:todayString]){
            dateContent = [NSString stringWithFormat:@"%@",time];
            return dateContent;
        } else if ([dateString isEqualToString:yesterdayString]){
            dateContent = [NSString stringWithFormat:@"昨天 %@",time];
            return dateContent;
        }else if ([dateString isEqualToString:beforeOfYesterdayString]){
            dateContent = [NSString stringWithFormat:@"前天 %@",time];
            return dateContent;
        }else{
            return time2;
        }
    }else{
        return dateString;
    }
}

//判断是否为整形：
- (BOOL)isPureInt{
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

//判断是否为浮点形：

- (BOOL)isPureFloat{
    NSScanner* scan = [NSScanner scannerWithString:self];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

- (BOOL)checkInputShouldAlphaNum
{
    NSString *regex =@"[a-zA-Z0-9]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    if (![pred evaluateWithObject:self]) {
        return YES;
    }
    return NO;
}




@end
