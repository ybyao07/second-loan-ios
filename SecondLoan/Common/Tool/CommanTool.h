//
//  CommanTool.h
//  WKCarInsurance
//
//  Created by ybyao07 on 16/10/26.
//  Copyright © 2016年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommanTool : NSObject

// 显示 ToastView 
//+ (void)showAllTextDialog:(NSString *)str inView:(UIView *)view;
//
//
//+ (void)showAllTextDialog:(NSString *)str inView:(UIView *)view withYOffset:(CGFloat)yOffset;


+(NSString*)dictionaryToJson:(NSDictionary *)dic;


+ (void)setShadowOnView:(UIView *)view;

+ (void)setShadowOnView1:(UIView *)view;

+ (UIViewController *)getCurrentViewController;

+ (NSArray *)sortedWithDict:(NSDictionary*)dict;


@end
