//
//  CommonModelTypeDIc.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "CommonModelTypeDIc.h"

@implementation CommonModelTypeDIc
+ (CommonModelTypeDIc *)shareManager {
    static CommonModelTypeDIc *globalData = nil;
    static dispatch_once_t onceYoken;
    dispatch_once(&onceYoken, ^{
        globalData = [[CommonModelTypeDIc alloc] init];
    });
    return globalData;
}

- (void)setArraySalaryType:(NSArray *)arraySalaryType{
    _arraySalaryType = [self arrayFromData:arraySalaryType];
}

- (NSArray *)arrayFromData:(NSArray *)data{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in data) {
        if ([dic[@"status"] isEqualToString:@"0"]) {
            [arr addObject: @{
                              @"id":dic[@"key"],
                              @"name":dic[@"value"]
                              }];
        }
    }
    return arr;
}

- (NSArray *)arrayFromIndustryData:(NSArray *)data{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in data) {
//        if ([dic[@"status"] isEqualToString:@"0"]) {
            [arr addObject: @{
                              @"id":dic[@"code"],
                              @"name":dic[@"codeText"]
                              }];
//        }
    }
    return arr;
}


- (void)setArrayWorkPost:(NSArray *)arrayWorkPost{
    _arrayWorkPost = [self arrayFromData:arrayWorkPost];;
}
- (void)setArrayMarriageType:(NSArray *)arrayMarriageType
{
    _arrayMarriageType = [self arrayFromData:arrayMarriageType];
}
- (void)setArrayHighEduType:(NSArray *)arrayHighEduType
{
    _arrayHighEduType = [self arrayFromData:arrayHighEduType];
}
- (void)setArrayAddressType:(NSArray *)arrayAddressType{
    _arrayAddressType = [self arrayFromData:arrayAddressType];
}
- (void)setArrayCompanyScale:(NSArray *)arrayCompanyScale{
    _arrayCompanyScale = [self arrayFromData:arrayCompanyScale];
}
- (void)setArrayRelationType:(NSArray *)arrayRelationType{
    _arrayRelationType = [self arrayFromData:arrayRelationType];
}
- (void)setArrayMortgage:(NSArray *)arrayMortgage{
    _arrayMortgage = [self arrayFromData:arrayMortgage];
}
- (void)setArrayWorkOccupation:(NSArray *)arrayWorkOccupation{
    _arrayWorkOccupation = [self arrayFromData:arrayWorkOccupation];
}
- (void)setArrayPaybackType:(NSArray *)arrayPaybackType{
    _arrayPaybackType = [self arrayFromData:arrayPaybackType];
}
- (void)setArrayLoanPurpos:(NSArray *)arrayLoanPurpos
{
    _arrayLoanPurpos = [self arrayFromData:arrayLoanPurpos];
}
- (void)setArrayHavaChild:(NSArray *)arrayHavaChild
{
    _arrayHavaChild = [self arrayFromData:arrayHavaChild];
}
- (void)setArrayLoanTerm:(NSArray *)arrayLoanTerm{
    _arrayLoanTerm = [self arrayFromData:arrayLoanTerm];
}
- (void)setArrayWorkType:(NSArray *)arrayWorkType{
    _arrayWorkType = [self arrayFromIndustryData:arrayWorkType];
}
+ (NSDictionary *)dicType:(NSArray *)arr
{
    NSMutableDictionary *dicType = [NSMutableDictionary new];
    for (int i = 0; i < 1000; i++) {
        dicType[@(++i)] = @"";
    }
    
    for (int i = 0; i < [arr count]; i++) {
        NSDictionary *dic = arr[i];
        if ([dic[@"id"] isKindOfClass:[NSString class]]) {
            dicType[@([dic[@"id"] integerValue])] = dic[@"name"];
        }else{
            dicType[dic[@"id"]] = dic[@"name"];
        }
//        dicType[@(++i)] = dic[@"name"];
    }
    return dicType;
}
+ (NSDictionary *)dicWorkType:(NSArray *)arr
{
    NSMutableDictionary *dicType = [NSMutableDictionary new];
    for (int i = 0; i < [arr count]; i++) {
        NSDictionary *dic = arr[i];
        if ([dic[@"id"] isKindOfClass:[NSString class]]) {
            dicType[dic[@"id"]] = dic[@"name"];
        }
    }
    return dicType;
}


+ (NSDictionary *)dicWorkType{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayWorkType;
    return [CommonModelTypeDIc dicWorkType:arr];
}

+ (NSDictionary *)dicWorkPostType{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayWorkPost;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicWorkOccupationType
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayWorkOccupation;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicLaonTerm
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayLoanTerm;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicLoanPurpos
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayLoanPurpos;
    return [CommonModelTypeDIc dicIdNameType:arr];
}
+ (NSDictionary *)dicIdNameType:(NSArray *)arr{
    NSMutableDictionary *dicType = [NSMutableDictionary new];
    for (int i = 0; i < [arr count]; i++) {
        NSDictionary *dic = arr[i];
        dicType[dic[dic.allKeys[0]]] = dic[dic.allKeys[1]];
    }
    return dicType;
}

+ (NSDictionary *)dicPaybackType
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayPaybackType;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicEdu
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayHighEduType;
    return [CommonModelTypeDIc dicType:arr];
}

+ (NSDictionary *)dicCompanyScale
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayCompanyScale;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicSalaryType
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arraySalaryType;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicRelationType
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayRelationType;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicAddressType
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayAddressType;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicMarState{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayMarriageType;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicMortgage{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayMortgage;
    return [CommonModelTypeDIc dicType:arr];
}
+ (NSDictionary *)dicHaveChild
{
    NSArray *arr = [CommonModelTypeDIc shareManager].arrayHavaChild;
    return [CommonModelTypeDIc dicIdNameType:arr];
}

+ (NSArray *)arrayFamilyNumber{
    NSMutableArray *array = [NSMutableArray new];
    for (int i = 0; i < 10; i++) {
        [array addObject:@{
                           @"name":[NSString stringWithFormat:@"%d",i],
                           @"id":@(i),
                           }];
    }
    return array;
}
+ (NSArray *)arrayYearList
{
    NSMutableArray *array = [NSMutableArray new];
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy"];
    NSString *thisYearString=[dateformatter stringFromDate:senddate];
    for (int i = [thisYearString integerValue]; i >= 1950 ; i--) {
        [array addObject:@{
                           @"name":[NSString stringWithFormat:@"%d",i],
                           @"id":@(i),
                           }];
    }
    return array;
}


+ (NSArray *)arrayGraduateYearList
{
    NSMutableArray *array = [NSMutableArray new];
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy"];
    NSString *thisYearString=[dateformatter stringFromDate:senddate];
    int i = [thisYearString integerValue];
    for (; i >= 1981 ; i--) {
        [array addObject:@{
                           @"name":[NSString stringWithFormat:@"%d",i],
                           @"id":@(i),
                           }];
    }
    [array addObject:@{
                       @"name":@"更早",
                       @"id":@(i++),
                       }];
    
    [array addObject:@{
                       @"name":@"无",
                       @"id":@(i),
                       }];
    return array;
}
//+ (NSArray *)arrayWorkOccupation
//{
//    return @[
//             @{
//                 @"name":@"国家公务员",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"公办学校在编教师",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"公立医院在编医护人员",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"其他事业单位在编人员",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"通讯、电力、烟草、石化、交通、金融、新闻出版社等行业企业正式员工",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"具有律师、注册会计师等从业资格证的高级专业人才",
//                 @"id":@"6"
//                 },
//             @{
//                 @"name":@"其他优质企业中层以上管理人员",
//                 @"id":@"7"
//                 },
//             @{
//                 @"name":@"其他优质企业员工",
//                 @"id":@"8"
//                 },
//             @{
//                 @"name":@"一般企业中层以上管理人员",
//                 @"id":@"9"
//                 },
//             @{
//                 @"name":@"一般企业员工",
//                 @"id":@"10"
//                 },
//             @{
//                 @"name":@"下岗职工",
//                 @"id":@"11"
//                 },
//             @{
//                 @"name":@"个体经营",
//                 @"id":@"12"
//                 },
//             @{
//                 @"name":@"自由职业",
//                 @"id":@"13",
//                 },
//             @{
//                 @"name":@"其他",
//                 @"id":@"14"
//                 },
//             ];
//}
//+ (NSArray *)arrayAddressType{
//    return @[
//             @{
//                 @"name":@"自置",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"按揭",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"租房",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"集体宿舍",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"亲属楼宇",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"共有住宅",
//                 @"id":@"6"
//                 },
//             @{
//                 @"name":@"其他",
//                 @"id":@"7"
//                 },
//             ];
//}
//+ (NSArray *)arrayMarriageType{
//    return @[
//             @{
//                 @"name":@"未婚",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"已婚",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"丧偶",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"离婚",
//                 @"id":@"4"
//                 },
//             ];
//}
//+ (NSArray *)arrayHighEduType{
//    return @[
//             @{
//                 @"name":@"博士研究生",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"硕士研究生",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"大学本科",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"大学专科",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"中等专业或技术学校",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"高中",
//                 @"id":@"6"
//                 },
//             @{
//                 @"name":@"初中",
//                 @"id":@"7"
//                 },
//             @{
//                 @"name":@"小学",
//                 @"id":@"8"
//                 },
//             @{
//                 @"name":@"文盲或半文盲",
//                 @"id":@"9"
//                 }
//             ];
//}
//- (NSArray *)arrayWorkPost
//{
//    return @[
//             @{
//                 @"name":@"总经理",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"总监",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"部门经理",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"组长",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"职员",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"其他",
//                 @"id":@"6"
//                 }
//             ];
//}
//+ (NSArray *)arrayRelationType{
//    return @[
//             @{
//                 @"name":@"朋友",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"夫妻",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"兄弟姐妹",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"父亲",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"母亲",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"同事",
//                 @"id":@"6"
//                 },
//
//             ];
//}
//- (void)arraySalaryType:(NSArray *)arrDic
//{
//    NSMutableArray *arr = [NSMutableArray array];
//    for (NSDictionary *dic in arrDic) {
//        [arr addObject: @{
//            @"id":dic[@"key"],
//            @"name":dic[@"name"]
//        }];
//    }
//    self.arraySalaryType = arr;
//}
//- (NSArray *)arraySalaryType
//{
//
//    return @[
//             @{
//                 @"name":@"5000元以下",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"5001-8000元",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"80001-10000元",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"10001-15000元",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"15001-20000元",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"20000元以上",
//                 @"id":@"6"
//                 }
//             ];
//}

//+ (NSArray *)arrayCompanyScale{
//    return @[
//             @{
//                 @"name":@"0-100人",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"101-200人",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"201-500人",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"501-1000人",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"1001-2000人",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"2000人以上",
//                 @"id":@"6"
//                 }
//             ];
//}

//+ (NSArray *)arrayPaybackType{
//    return @[
//             @{
//                 @"name":@"等额本息",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"等额本金",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"到期还本付息",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"按季（月）付息，到期还款",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"按约定还款",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"其他",
//                 @"id":@"6"
//                 }
//             ];
//}

//+ (NSArray *)arrayLoanTerm{
//    return @[
//             @{
//                 @"name":@"3个月",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"6个月",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"12个月",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"24个月",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"36个月",
//                 @"id":@"5"
//                 }
//             ];
//}
//+ (NSArray *)arrayWorkType
//{
//    return @[
//             @{
//                 @"name":@"农、林、牧、渔业",
//                 @"id":@"1"
//                 },
//             @{
//                 @"name":@"采矿业",
//                 @"id":@"2"
//                 },
//             @{
//                 @"name":@"制造业",
//                 @"id":@"3"
//                 },
//             @{
//                 @"name":@"电力、热力、燃气及水生产和供应业",
//                 @"id":@"4"
//                 },
//             @{
//                 @"name":@"建筑业",
//                 @"id":@"5"
//                 },
//             @{
//                 @"name":@"批发和零售业",
//                 @"id":@"6"
//                 },
//             @{
//                 @"name":@"交通运输、仓储和邮政业",
//                 @"id":@"7"
//                 },
//             @{
//                 @"name":@"住宿和餐饮业",
//                 @"id":@"8"
//                 },
//             @{
//                 @"name":@"信息传输、软件和信息技术服务业",
//                 @"id":@"9"
//                 },
//             @{
//                 @"name":@"金融业",
//                 @"id":@"10"
//                 },
//             @{
//                 @"name":@"房地产业",
//                 @"id":@"11"
//                 },
//             @{
//                 @"name":@"租赁和商务服务业",
//                 @"id":@"12"
//                 },
//             @{
//                 @"name":@"科学研究和技术服务业",
//                 @"id":@"13"
//                 },
//             @{
//                 @"name":@"水利、环境和公共设施管理业",
//                 @"id":@"14"
//                 },
//             @{
//                 @"name":@"居民服务、修理和其他服务业",
//                 @"id":@"15"
//                 },
//             @{
//                 @"name":@"教育",
//                 @"id":@"16"
//                 },
//             @{
//                 @"name":@"卫生和社会工作",
//                 @"id":@"17"
//                 },
//             @{
//                 @"name":@"文化、体育和娱乐业",
//                 @"id":@"18"
//                 },
//             @{
//                 @"name":@"政府融资平台类公司",
//                 @"id":@"19"
//                 }
//             ];
//}
@end
