//
//  DataController+Cache.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "DataController.h"

@interface DataController (Cache)

- (void)addCache:(NSData *)data
     andCacheKey:(NSString *)cacheKey;

- (void)addCache:(NSData *)data andCacheKey:(NSString *)cacheKey
        andCachedNSURLResponse:(NSURLResponse *)response;


- (NSData *)loadCacheDataWithKey:(NSString *)cacheKey;

- (NSURLResponse *)loadCacheResponseWithKey:(NSString *)cacheKey;


- (BOOL)findCachedWithKey:(NSString *)cacheKey;

- (BOOL)deleteCacheWithKey:(NSString *)cacheKey;


@end
