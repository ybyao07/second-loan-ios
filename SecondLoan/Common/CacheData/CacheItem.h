//
//  CacheItem.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheItem : NSObject<NSCoding>

@property (nonatomic, strong) NSData *dataCache;
@property (nonatomic, strong) NSString *cacheKey;
@property (nonatomic, strong) NSURLResponse *cacheResponse;

@end
