//
//  DataController.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheData.h"


@interface DataController : NSObject
{
    CacheData *cacheData;
}
+ (DataController *)getInstance;

- (void)destory;

@end
