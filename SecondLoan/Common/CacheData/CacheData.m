//
//  CacheData.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "CacheData.h"

@implementation CacheData

- (id)init
{
    if (self = [super init]) {
        _arrayCacheData = nil;
        return self;
    }
    return nil;
}

- (void)destory
{
    _arrayCacheData = nil;
}
@end
