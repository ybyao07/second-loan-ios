//
//  CacheData.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheData : NSObject

@property (nonnull, strong) NSMutableArray *arrayCacheData; //缓存数据

//初始化函数
- (id)init;

- (void)destory;

@end
