//
//  CacheItem.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "CacheItem.h"

@implementation CacheItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_dataCache forKey:@"kDataCache"];
    [encoder encodeObject:_cacheKey forKey:@"kCacheKey"];
    [encoder encodeObject:_cacheResponse forKey:@"kCacheResponse"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    _dataCache = [decoder decodeObjectForKey:@"kDataCache"];
    _cacheKey = [decoder decodeObjectForKey:@"kCacheKey"];
    _cacheResponse = [decoder decodeObjectForKey:@"kCacheResponse"];
    return self;
}

@end
