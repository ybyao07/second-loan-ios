//
//  DataController.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "DataController.h"
#import "DataController+Cache.h"
@implementation DataController

+ (DataController *)getInstance
{
    static DataController *globalDataController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        globalDataController = [[DataController alloc] init];
    });
    return globalDataController;
}

- (void)destory
{
    [cacheData destory];
}
@end
