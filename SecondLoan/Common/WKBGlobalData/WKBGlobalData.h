//
//  WKBGlobalData.h
//  WKCarInsurance
//
//  Created by sunyang on 17/8/1.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyMoneyRangeModel.h"
#import "ProductModel.h"
#import "UserMineInfo.h"

@interface WKBGlobalData : NSObject

+ (WKBGlobalData *)shareManager;

/***********主流程缓存数据********/
@property (copy, nonatomic) NSString *productIdPuhui;

@property (strong, nonatomic) MyMoneyRangeModel *homeMoneyModel;
@property (strong, nonatomic) ProductModel *homeProduct;

@property (strong, nonatomic) UserMineInfo *mineInfo;
@end
