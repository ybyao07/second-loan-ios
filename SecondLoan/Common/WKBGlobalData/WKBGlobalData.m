//
//  WKBGlobalData.m
//  WKCarInsurance
//
//  Created by sunyang on 17/8/1.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "WKBGlobalData.h"

@implementation WKBGlobalData
+ (WKBGlobalData *)shareManager {
    static WKBGlobalData *globalData = nil;
    static dispatch_once_t onceYoken;
    dispatch_once(&onceYoken, ^{
        globalData = [[WKBGlobalData alloc] init];
    });
    return globalData;
}

@end
