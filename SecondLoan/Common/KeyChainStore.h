//
//  KeyChainStore.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/14.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KeyChainStore : NSObject

+ (void)save:(NSString*)service data:(id)data;
+ (id)load:(NSString*)service;
+ (void)deleteKeyData:(NSString*)service;

@end

NS_ASSUME_NONNULL_END
