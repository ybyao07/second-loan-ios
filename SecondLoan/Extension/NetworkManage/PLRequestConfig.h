//
//  PLRequestConfig.h
//  PL
//
//  Created by kimi on 15/7/22.
//  Copyright (c) 2015年 kimi. All rights reserved.
//

#define URLTEST

// 登录注册
#define url_send_code @"/select/sendMessage"    //发送验证码
#define url_check_code @"/select/getCheckcode"              //图像验证码获取
#define url_login @"/person/loginIn"        //登录
#define url_register @"/person/register"    //注册
#define url_get_img_code @"/select/getCheckcode"    //图像验证码
#define url_login @"/person/loginIn"    //登录
#define url_register @"/person/register"    //注册
#define url_forgetPwd @"/person/forgetPwd"    // 忘记密码
#define url_updatePwd @"/person/updatePwd"   //修改密码



#define usl_upload_face @"/person/updateFace"   //上传头像
#define url_get_auth   @"/person/auth"         //是否已认证
#define url_getCardInfo @"/person/getCardInfo" //身份证验证信息
#define url_face_compare @"/person/comparison"  //人脸对比
#define url_bank_info @"/person/getBankPersonInfoByPhone"  //银行信息查询

#define url_select_list @"/select/selectdic"    // 获取枚举列表
#define url_select_industry  @"/select/selectindustry" // 获取行业类型

// 产品
#define url_product_info  @"/person/productInfo"  // 优选产品详情
//申请
#define url_person_borrow_apply  @"/person/borrowApply"  // 借款申请
#define url_loan_apply_puhui  @"/person/loan"  // 普惠贷贷款申请
#define url_loan_apply_normal  @"/person/loanOpt"  // 优选产品贷款产品申请

// 首页
#define url_home_product_list @"/person/productList" //
#define url_product_puhui @"/person/product"  // 普惠快贷产品详情
#define url_my_quota @"/person/viewQuota"   // 获取我的额度

// 个人信息
#define url_user_info_update @"/person/savePersonInfo"  //个人信息新增
#define url_user_bank_update @"/person/saveBankPersonInfo"
#define url_user_info @"/person/getPersonInfoByPhone"
#define url_user_home_info @"/person/selectAllPersonInfo" //个人中心首页


#define url_myloan_list @"/person/loanRecordList"  // 我的贷款记录
//#define url_loan_record_list @"/person/loanRecordList"  // 贷款记录
#define url_borrow_record_list @"/person/borrowRecordList"  // 借款记录
#define url_apply_record_list @"/person/applyRecordList"  // 申请记录

#define url_message_list @"/person/messageList"    // 消息列表
#define url_get_new_message @"/person/getIsNewMessage" //是否有新消息
#define url_update_status @"/person/updateStatus"    // 更新阅读状态

// other --- websocket and location
//#define WEB_SOCKET_BASE_URL @"47.95.203.73:9190"
#define WEB_SOCKET_BASE_URL @"120.220.43.57:8009"

#define url_upload_device @"/person/app/api/machine"    // 设备配置信息接口
#define url_upload_phone  @"/person/app/api/contract"        //联系人记录
#define url_loginWebsocket @"/person/app/api/loginstatus"  //用户登陆信息上传
#define url_upload_location @"/person/app/api/location"  //上传地理信息位置

#define url_discover_home @"http://180.76.249.68:8090/person/app/view/findus"
#define url_about_us @"http://180.76.249.68:8090/person/app/view/aboutus"
#define url_contect_us @"http://180.76.249.68:8090/person/app/view/aboutus"
#define url_private_protocal @"http://180.76.249.68:8090/person/app/view/syxy"
#define url_private_polity @"http://180.76.249.68:8090/person/app/view/yszc"
//#define url_discover_home @"http://120.220.43.57:8009/static/app/view/findUs.html"
//#define url_about_us @"http://120.220.43.57:8009/static/app/view/aboutUs.html"
//#define url_contect_us @"http://120.220.43.57:8009/static/app/view/aboutUs.html"
//#define url_private_protocal @"http://120.220.43.57:8009/static/app/view/syxy.html"
//#define url_private_polity @"http://120.220.43.57:8009/static/app/view/yszc.html"

#define url_download @"https://mbc.sdrcu.com/mbcper/download.html"
//设置请求超时
#define TIME_OUT_SET 30

#ifdef URLTEST
//#define  HOST_BASE   @"47.95.203.73:9190"  // 测试
#define  HOST_BASE  @"120.220.43.113:8009"
//#define  HOST_BASE   @"120.220.43.57:8009"  // appStore审核环境
#define  HTTP_BASE_URL   HTTP_CUSURL(@"http://",HOST_BASE)
#define  QiNiuBaseUrl @"http://qiniu.magictechco.cn/"

#ifndef DEBUG
#endif
#else
#endif

#define HTTPAPIWITH(n)     [NSString stringWithFormat:@"http://%@%@",HOST_BASE,n]
#define WEBSOCKETWITH(n)     [NSString stringWithFormat:@"http://%@%@",WEB_SOCKET_BASE_URL,n]

#define HTTP_CUSURL(a,b)        [NSString stringWithFormat:@"%@%@",a,b]

typedef NS_ENUM(NSInteger, RequestType) {
    RequestPost = 2222,
    RequestGet,
};
