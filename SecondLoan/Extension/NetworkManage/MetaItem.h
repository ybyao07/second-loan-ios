//
//  MetaItem.h
//  jz_ios
//
//  Created by junlei on 2017/3/5.
//  Copyright © 2017年 hppower. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MetaItemType) {
    MetaItemTypePhoto = 2222,
    MetaItemTypeVideo,
};

@interface MetaItem : NSObject
@property (nonatomic,assign) MetaItemType type;
@property (nonatomic,strong) NSData * thumbImage;
@property (nonatomic,strong) NSData * object;
@end
