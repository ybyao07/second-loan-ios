//
//  PLRequestManage.h
//  PL
//
//  Created by kimi on 15/7/21.
//  Copyright (c) 2015年 kimi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLRequestConfig.h"

@interface PLRequestManage : NSObject

@property (strong,nonatomic) AFHTTPSessionManager * _Nonnull httpManager;

+ (id _Nonnull)sharedInstance;

- (NSURLSessionDataTask *_Nonnull)httpRequestWithUrl:(NSString* _Nonnull)urlStr Type:(RequestType)type parameters:(NSDictionary *_Nonnull)params success:(void (^_Nonnull)(NSURLSessionDataTask * _Nonnull, id _Nullable))success failure:(void (^_Nonnull)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

- (void)httpRequestWithUrl:(NSString*)urlStr Type:(RequestType)type parameters:(NSDictionary *)params completeHandle:(void (^)(id _Nullable responseObject, NSError * error))completeHandle;


- (void)httpRequestWithUrl:(NSString*)urlStr Type:(RequestType)type parameters:(NSDictionary *)params completeHandle:(void (^)(id _Nullable responseObject, NSError * error))completeHandle offlineCache:(NSString *)cacheKey;

@end
