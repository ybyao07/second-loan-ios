//
//  SYNotiAutoPage.m
//  SuYue
//
//  Created by Win10 on 2018/12/20.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "SYNotiAutoPage.h"
#import "BaseViewController.h"

@implementation SYNotiAutoPage

+ (SYNotiAutoPage *)shareInstance{
    static dispatch_once_t pred = 0;
    __strong static SYNotiAutoPage *_sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (BaseViewController *)getCurrentController
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    return (BaseViewController *)currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    if ([rootVC presentedViewController]) {
        rootVC = [rootVC presentedViewController];        // 视图是被presented出来的
    }
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
    } else {
        // 根视图为非导航类
        currentVC = rootVC;
    }
    return currentVC;
}

- (void)reciveRemoteNoti:(NSDictionary *)userinfo
{
    NSString * type = userinfo[@"usertype"];
    NSString * typeinfo = S(userinfo[@"userinfo"]);
    if ([type isKindOfClass:[NSString self]] && [type isEqualToString:@"chat"]) {
        [self requestChatWithId:typeinfo];
    }
    if ([type isKindOfClass:[NSString self]] && [type isEqualToString:@"siyue"]) {
        [self requestSingleIfNeed:typeinfo];
    }
}


-(void)requestChatWithId:(id)chatid{
    if (![UserLocal isLogin]){
        return;
    }
}

- (void)requestSingleIfNeed:(NSString *)orderid
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:orderid forKey:@"orderid"];
    
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(@"/mobile2/singleyue/is_order_canchoice") Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        if (!error) {
            if ([responseObject[@"canapply"] integerValue]) {
                [self singleYueChoiseWithOrderId:orderid nickName:S(responseObject[@"user_nickname"]) block:^(NSInteger isAgree) {
                    if(isAgree){ // 同意跳转到订单列表
                    }else{
                    }
                }];
            }
        } else {
            [[self getCurrentController] HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

- (void)singleYueChoiseWithOrderId:(NSString *)orderId nickName:(NSString *)nickname block:(void (^)(NSInteger isAgree))block
{
}

- (void)suYueApplyWithOrderId:(NSString *)orderId block:(void (^)(NSInteger success))block
{
}
@end
