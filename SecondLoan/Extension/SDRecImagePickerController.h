//
//  SDRecImagePickerController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SDRecImagePickerController : UIImagePickerController
@property (nonatomic, assign) BOOL idFront;
@property (nonatomic, strong) UIImageView *backImg;
@property (nonatomic, strong) UIImageView *fronImg;
@end

NS_ASSUME_NONNULL_END
