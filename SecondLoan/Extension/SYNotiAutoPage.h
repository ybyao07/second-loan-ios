//
//  SYNotiAutoPage.h
//  SuYue
//
//  Created by Win10 on 2018/12/20.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SYNotiAutoPage : NSObject

+ (SYNotiAutoPage*)shareInstance;
- (void)reciveRemoteNoti:(NSDictionary *)userinfo;
@property(nonatomic,weak) UITabBarController * tabbarController;
- (BaseViewController *)getCurrentController;

- (void)singleYueChoiseWithOrderId:(NSString *)orderId nickName:(NSString *)nickname block:(void (^)(NSInteger isAgree))block;

- (void)suYueApplyWithOrderId:(NSString *)orderId block:(void (^)(NSInteger success))block;

@end

NS_ASSUME_NONNULL_END
