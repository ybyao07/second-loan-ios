//
//  GDSNSManager.h
//  kuaiji
//
//  Created by gaodun on 15/11/18.
//  Copyright © 2015年 idea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDSNSShareModel.h"


@protocol TencentSessionDelegate;
typedef NS_ENUM(NSInteger,GDSNSLoginResult){
    GDSNSLoginResultSuccess = 0,
};
typedef enum _GDSNSLoginType{
    GDSNSLoginTypeQQ = 2,
    GDSNSLoginTypeWX = 3,
    GDSNSLoginTypeWeibo = 1
}GDSNSLoginType;

typedef enum _GDShareType{
    GDShareTypeWeixiSession = 1111,
    GDShareTypeWeixiTimeline,
    GDShareTypeQQ,
    GDShareTypeQQSpace,
    GDShareTypeSinaWeibo,
}GDShareType;

typedef void(^loginHandler)(NSString * code,NSInteger errCode);

@interface GDSNSManager : NSObject
+ (instancetype)shareGDSNSManager;

+ (void)registerApp;

+(BOOL)isWXAppInstalled;
+(BOOL)isQQInstalled;

+(BOOL)handleOpenURL:(NSURL *)url;

- (void)share:(id)object type:(GDShareType)type;

- (void)loginWithPlatform:(GDSNSLoginType)type completionHandler:(loginHandler)handler;
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock;

@end

@protocol GDSNSManagerDelegate <NSObject>
- (void)loginresult:(NSString*)str errCode:(NSInteger)code enter:(GDSNSLoginType)type;
@end
