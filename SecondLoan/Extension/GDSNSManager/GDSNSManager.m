//
//  GDSNSManager.m
//  kuaiji
//
//  Created by gaodun on 15/11/18.
//  Copyright © 2015年 idea. All rights reserved.
//


#import "GDSNSManager.h"
#import "WeiboManger.h"
#import "TencentManager.h"

@interface GDSNSManager()<GDSNSManagerDelegate>

@property (nonatomic,copy) loginHandler loginBlock;
@property (nonatomic,strong) TencentManager * tencentMgr;
@property (nonatomic,strong) WeiboManger * weiboMgr;

@property (nonatomic,assign) GDSNSLoginType currentLoginType;

@end
@implementation GDSNSManager
+ (instancetype)shareGDSNSManager{
    static GDSNSManager *gdsnsmanager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gdsnsmanager = [[self alloc] init];
        gdsnsmanager.tencentMgr = [[TencentManager alloc] initWithDelegate:gdsnsmanager];
        gdsnsmanager.weiboMgr = [[WeiboManger alloc] initWithDelegate:gdsnsmanager];
    });
    return gdsnsmanager;
}

+ (void)registerApp
{
    GDSNSManager *gdsnsmanager = [GDSNSManager shareGDSNSManager];
    [gdsnsmanager.tencentMgr registerApp];
    [gdsnsmanager.weiboMgr registerApp];
}

#pragma mark --handleURL

+(BOOL)handleOpenURL:(NSURL *)url{
    GDSNSManager*  __self = [GDSNSManager shareGDSNSManager];
    
    NSString *urlString = url.absoluteString;
    if ([urlString rangeOfString:@"wb"].length>0) {
//        return [ WeiboSDK handleOpenURL:url delegate:__self.weiboMgr];
        return NO;
    }else if([urlString rangeOfString:@"tencent"].length>0) {
//        return [TencentOAuth HandleOpenURL:url];
        return NO;
    }else if([urlString rangeOfString:@"QQ"].length>0) {
//        return [TencentOAuth HandleOpenURL:url];
        return NO;
    }
    return YES;
}



#pragma mark - Share
- (void)share:(id)object type:(GDShareType)type
{
    if (type == GDShareTypeSinaWeibo) {
        [self.weiboMgr share:object];
    }
    if (type == GDShareTypeQQ || type == GDShareTypeQQSpace) {
        [self.tencentMgr share:object type:type];
    }
    if (type == GDShareTypeWeixiSession || type == GDShareTypeWeixiTimeline){
    }
}

- (void)loginWithPlatform:(GDSNSLoginType)type completionHandler:(loginHandler)handler
{
    self.loginBlock = handler;
    if (type == GDSNSLoginTypeQQ) {
        [self.tencentMgr login];
    }
    if (type == GDSNSLoginTypeWeibo) {
        [self.weiboMgr login];
    }
}
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock
{
    if (self.currentLoginType == GDSNSLoginTypeQQ) {
        [self.tencentMgr requestSnsInformationCompletion:comBlock];
    }
    if (self.currentLoginType == GDSNSLoginTypeWX) {
    }
    if (self.currentLoginType == GDSNSLoginTypeWeibo) {
        [self.weiboMgr requestSnsInformationCompletion:comBlock];
    }
}


#pragma delegate
- (void)loginresult:(NSString*)str errCode:(NSInteger)code enter:(GDSNSLoginType)type
{
    if (code == 0) {
        self.currentLoginType = type;
    }
    if (self.loginBlock)
    {
        self.loginBlock(str,code);
        self.loginBlock = nil;
    }
}
+(BOOL)isQQInstalled
{
    return [TencentOAuth iphoneQQInstalled];
}
@end
