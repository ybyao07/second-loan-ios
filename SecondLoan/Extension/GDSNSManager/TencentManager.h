//
//  TencentManager.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/12/15.
//  Copyright © 2015年 JET. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "GDSNSManager.h"

@interface TencentManager : NSObject<TencentSessionDelegate,QQApiInterfaceDelegate>
- (instancetype)initWithDelegate:(id)delegate;
- (void)registerApp;
- (void)login;
- (void)share:(id)object type:(GDShareType)type;
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock;
@end
