//
//  WeiboManger.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/12/15.
//  Copyright © 2015年 JET. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <WeiboSDK.h>
#import "GDSNSManager.h"

//@interface WeiboManger : NSObject<WeiboSDKDelegate>
@interface WeiboManger : NSObject
- (instancetype)initWithDelegate:(id)delegate;
- (void)registerApp;
- (void)login;
- (void)share:(id)object;
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock;
@end
