//
//  GDSNSShareModel.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/11/25.
//  Copyright © 2015年 JET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDSNSShareModel : NSObject
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * describtion;
@property (nonatomic,strong) NSString * webTitle;
@property (nonatomic,strong) NSString * webDescribtion;
@property (nonatomic,strong) NSString * webUrl;
@property (nonatomic,strong) UIImage * webImg;
@end
