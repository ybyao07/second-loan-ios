//
//  WeiboManger.m
//  easyride_kuaiji
//
//  Created by gaodun on 15/12/15.
//  Copyright © 2015年 JET. All rights reserved.
//
#define kSinaRedirectURL @"http://www.gaodun.com/oauth/weibo/callback.php"

#import "WeiboManger.h"
//#import <WBHttpRequest+WeiboUser.h>
@interface WeiboManger()

@property (nonatomic,weak) id<GDSNSManagerDelegate> delegate;

//@property (nonatomic,strong) WBAuthorizeResponse * authResponse;
@end
@implementation WeiboManger
- (instancetype)initWithDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}
- (void)registerApp
{
    //微博注册
//    [WeiboSDK enableDebugMode:YES];
//    [WeiboSDK registerApp:@"3995371278"];
}

- (void)login{
//    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
//    request.redirectURI = kSinaRedirectURL;
//    request.scope = @"email";
//    //request.userInfo = @{};
//    [WeiboSDK sendRequest:request];
}
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock
{
//   [WBHttpRequest requestForUserProfile:self.authResponse.userID withAccessToken:self.authResponse.userInfo[@"access_token"] andOtherProperties:nil queue:[[NSOperationQueue alloc]init] withCompletionHandler:^(WBHttpRequest *httpRequest, id result, NSError *error) {
//       NSDictionary * response = ((NSObject *)result).keyValues;
//       NSMutableDictionary * dic = [NSMutableDictionary dictionary];
//       [dic Key:@"headurl" object:[response TValForKey:@"avatarLargeUrl"]];//avatarHDUrl,avatarLargeUrl
//       [dic Key:@"name" object:[response TValForKey:@"name"]];
//       if (comBlock){
//          comBlock(dic);
//       }
//   }];
}
- (void)share:(id)object
{
//    GDSNSShareModel * shareModel = object;
//
//    WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
//    //authRequest.redirectURI = kSinaRedirectURL;
//    authRequest.scope = @"all";
//
//    WBMessageObject *message = [WBMessageObject message];
//    message.text = shareModel.describtion;
//
//    WBWebpageObject *webpage = [WBWebpageObject object];
//    webpage.objectID = @"identifier1";
//    webpage.title = shareModel.webTitle;
//    webpage.description = shareModel.webDescribtion;
//    webpage.thumbnailData = UIImagePNGRepresentation(shareModel.webImg);
//    webpage.webpageUrl = shareModel.webUrl;
//    message.mediaObject = webpage;
//
//    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message authInfo:nil access_token:nil];
//    request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
//                         @"Other_Info_1": [NSNumber numberWithInt:123],
//                         @"Other_Info_2": @[@"obj1", @"obj2"],
//                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
//
//    [WeiboSDK sendRequest:request];
}
#pragma mark - SinaWeibo
#pragma mark -
//- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
//{
//
//}

//- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
//{
//    if ([response isKindOfClass:WBAuthorizeResponse.class])
//    {
//        WBAuthorizeResponse * authResponse = (WBAuthorizeResponse *)response;
//        if (authResponse.statusCode == 0) {
//            self.authResponse = authResponse;
//            NSLog(@"%@",self.authResponse.userInfo);
//        }
//
//        [self.delegate loginresult:authResponse.userID errCode:authResponse.statusCode enter:GDSNSLoginTypeWeibo];
//
//    } else if ([response isKindOfClass:WBSendMessageToWeiboResponse.class]){
//
//        if (response.statusCode == 0) {
//
//        } else {
//
//        }
//
//    }
//}
@end
