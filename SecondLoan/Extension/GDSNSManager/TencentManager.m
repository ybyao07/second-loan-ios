//
//  TencentManager.m
//  easyride_kuaiji
//
//  Created by gaodun on 15/12/15.
//  Copyright © 2015年 JET. All rights reserved.
//
#define QQAPPID @"1107960323"
#define QQAPPKEY @"Fv5we4BZhOPwpPZy"

#import "TencentManager.h"
@interface TencentManager()

@property (nonatomic,strong) TencentOAuth *tencentOAuth;

@property (nonatomic,weak) id<GDSNSManagerDelegate> delegate;
@end
@implementation TencentManager
- (instancetype)initWithDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}
- (void)registerApp
{
    
    //qq注册
    self.tencentOAuth  = [[TencentOAuth alloc]initWithAppId:QQAPPID andDelegate:self];
    self.tencentOAuth.authShareType = AuthShareType_QQ;
    self.tencentOAuth.redirectURI = @"www.qq.com";
    self.tencentOAuth.sessionDelegate = self;
    
}
- (void)login{
    [self.tencentOAuth authorize:@[kOPEN_PERMISSION_GET_USER_INFO,
                                   kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                                   kOPEN_PERMISSION_GET_INFO,
                                   ]];
}
- (void)requestSnsInformationCompletion:(void (^)(id response))comBlock
{
    NSString * url = [NSString stringWithFormat:@"https://graph.qq.com/user/get_simple_userinfo?access_token=%@&oauth_consumer_key=%@&openid=%@",self.tencentOAuth.accessToken,QQAPPID,self.tencentOAuth.openId];
    NSURL * nsurl = [NSURL URLWithString:url];
    NSURLRequest * request = [NSURLRequest requestWithURL:nsurl];

    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (connectionError == nil) {
            NSError *error = nil;
            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];

            NSString * headurl;
            headurl = [jsonObject valueForKey:@"figureurl_qq_2"];
            if (headurl.length == 0) {
                headurl = [jsonObject valueForKey:@"figureurl_qq_1"];
            }

            NSMutableDictionary * dic = [NSMutableDictionary dictionary];
            [dic setObject:headurl forKey:@"headurl"];
            [dic setObject:jsonObject[@"nickname"] forKey:@"name"];

            if (comBlock) {
                comBlock(dic);
            }

        } else {

        }
    }];
    
}
- (void)share:(id)object type:(GDShareType)type
{
    GDSNSShareModel * shareModel = object;

    QQApiNewsObject *newsObj = [QQApiNewsObject objectWithURL:[NSURL URLWithString:shareModel.webUrl] title:shareModel.title description:shareModel.describtion previewImageData:UIImagePNGRepresentation(shareModel.webImg)];
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
    //将内容分享到qzone
    if (type == GDShareTypeQQ) {
        [QQApiInterface sendReq:req];
    }
    if (type == GDShareTypeQQSpace) {
        [QQApiInterface SendReqToQZone:req];
    }
}
#pragma mark - tecent delegate
- (void)tencentDidLogin
{
    if ([self.tencentOAuth getUserInfo]){
        NSString * userid = self.tencentOAuth.openId;
        [self.delegate loginresult:userid errCode:0 enter:GDSNSLoginTypeQQ];
    }
}
- (void)tencentDidNotLogin:(BOOL)cancelled
{
    [self.delegate loginresult:nil errCode:-6 enter:GDSNSLoginTypeQQ];
}
- (void)tencentDidNotNetWork
{
    [self.delegate loginresult:nil errCode:-7 enter:GDSNSLoginTypeQQ];
}
- (void)isOnlineResponse:(NSDictionary *)response
{
    
}
@end
