//
//  CustomCameraViewController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/21.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CustomCameraViewController;

@protocol CustomCameraDelegate <NSObject>
- (void)imageCameraController:(CustomCameraViewController *)picker didFinishPickingMediaWithImage:(UIImage *)image;
@end

@interface CustomCameraViewController : UIViewController

@property (nonatomic, assign) BOOL isCard;
@property (nonatomic, weak) id<CustomCameraDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
