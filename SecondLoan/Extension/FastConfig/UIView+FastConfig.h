//
//  UIView+FastConfig.h
//  SuYue
//
//  Created by junlei on 2018/11/17.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFactory.h"


#define fQuery(a,b) ((a).query(b))
#define fQueryBtn(a,b) ((UIButton *)(a).query(b))
#define fQueryImgv(a,b) ((UIImageView *)(a).query(b))
#define fQueryLbl(a,b) ((UILabel *)(a).query(b))
#define fQueryField(a,b) ((UITextField *)(a).query(b))
#define fQueryTable(a,b) ((UITableView *)(a).query(b))
#define fQueryScroll(a,b) ((UIScrollView *)(a).query(b))

#define fQQuery(a,b,c) ((a).query(b).query(c))

#define fsBind(a) fsConfig(^(id view){a = view;})

NS_ASSUME_NONNULL_BEGIN

@interface UIView(FastConfig)
@property (nonatomic,strong) NSString * fvid;
@property (nonatomic,assign) BOOL fislistTemplate;
@property (nonatomic,strong) NSString * flistTemplateEntitySignerHash;
@property (nonatomic,assign) BOOL fissignPlaceholderView;
@property (nonatomic,assign,readonly) NSMutableDictionary * flistdataSorceDic;
@property (nonatomic,assign,readonly) NSMutableDictionary * flisttemplateDic;
@property (nonatomic,strong) void (^ftemplateConstructBlock)(FFactory * factory);


@property (nonatomic,assign) NSString * ftemplateSign;

- (void)renderList;

- (UIView * (^)(NSString *))query;
- (UIView * (^)(NSString *))fsFvid;
- (UIView * (^)(void (^)(FFactory * factory)))develop;

- (UIView * (^)(NSInteger))fsBackGroundColor;
- (UIView * (^)(BOOL))fsHidden;

- (UIView * (^)(BOOL))fsClip;
- (UIView * (^)(CGFloat))fsRadius;
- (UIView * (^)(CGFloat,NSInteger))fsBorder ;
- (UIView * (^)(CGSize,CGFloat,NSInteger))fsShadow;

- (UIView * (^)(void (^)(UIView * __self)))fsConfig;
- (UIView *)queryViewByFvid:(NSString *)fvid;
@end

NS_ASSUME_NONNULL_END
