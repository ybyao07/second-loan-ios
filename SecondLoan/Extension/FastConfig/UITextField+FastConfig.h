//
//  UITextField+FastConfig.h
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField(FastConfig)


- (UITextField * (^)(UIFont *))fsFont;

- (UITextField * (^)(CGFloat))fsFontSize;

- (UITextField * (^)(NSInteger))fsTextColor;

- (UITextField * (^)(NSString *))fsText;

- (UITextField * (^)(NSString *))fsPlaceholder;

- (UITextField * (^)(UIKeyboardType))fsKeyboard;

- (UITextField * (^)(NSString*,NSInteger,CGFloat))fsPText;

- (UITextField * (^)(NSString *limitCount))fsLimit;

- (UITextField * (^)(CGFloat))fsLeftPad;

- (UITextField * (^) (UITextFieldViewMode))fsClearButtonMode;
@end

NS_ASSUME_NONNULL_END
