//
//  UIView+FastConfig.m
//  SuYue
//
//  Created by junlei on 2018/11/17.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UIView+FastConfig.h"

@implementation UIView(FastConfig)

- (UIView *)queryViewByFvid:(NSString *)fvid
{
    if ([fvid isEqualToString:self.fvid]) {
        return self;
    }
    for (int i = 0; i < self.subviews.count; i++) {
        UIView * v = self.subviews[i];
        UIView * rv = [v queryViewByFvid:fvid];
        if (rv) {
            return rv;
        }
    }
    return nil;
}

- (UIView * (^)(NSString *))query{
    return ^id(NSString * fvid) {
        return [self queryViewByFvid:fvid];
    };
}
- (UIView * (^)(NSString *))fsFvid{
    return ^id(NSString * fvid) {
        [self setFvid:fvid];
        return self;
    };
}
- (UIView * (^)(void (^)(FFactory * factory)))develop
{
    return ^id(void (^develop)(FFactory * factory)) {
        if (self.fislistTemplate) {
            self.ftemplateConstructBlock = develop;
        } else {
            FFactory * fact = [[FFactory alloc] initWithSuperView:self];
            develop(fact);
        }
        return self;
    };
}


- (UIView * (^)(NSInteger))fsBackGroundColor {
    return ^id(NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        self.backgroundColor = color;
        return self;
    };
}

- (UIView * (^)(BOOL))fsHidden {
    return ^id(BOOL hidden) {
        self.hidden = hidden;
        return self;
    };
}

- (UIView * (^)(BOOL))fsClip {
    return ^id(BOOL clip) {
        self.clipsToBounds = clip;
        return self;
    };
}
- (UIView * (^)(CGFloat))fsRadius {
    return ^id(CGFloat radius) {
        self.layer.cornerRadius = radius;
        return self;
    };
}
- (UIView * (^)(CGSize,CGFloat,NSInteger))fsShadow {
    return ^id(CGSize offset,CGFloat opacity,NSInteger color) {
        self.layer.shadowOffset = offset;
        self.layer.shadowOpacity = opacity;
        self.layer.shadowColor = UIColorFromHex(color).CGColor;
        return self;
    };
}
- (UIView * (^)(CGFloat,NSInteger))fsBorder {
    return ^id(CGFloat width,NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        self.layer.borderColor = color.CGColor;
        self.layer.borderWidth = width;
        return self;
    };
}
- (UIView * (^)(void (^)(UIView * __self)))fsConfig{
    return ^id(void (^bind)(UIView * __self)) {
        bind(self);
        return self;
    };
}

//定义常量 必须是C语言字符串
static char *FvidKey = "CloudoxKey";

-(void)setFvid:(NSString *)fvid{
    /*
     objc_AssociationPolicy参数使用的策略：
     OBJC_ASSOCIATION_ASSIGN;            //assign策略
     OBJC_ASSOCIATION_COPY_NONATOMIC;    //copy策略
     OBJC_ASSOCIATION_RETAIN_NONATOMIC;  // retain策略
     
     OBJC_ASSOCIATION_RETAIN;
     OBJC_ASSOCIATION_COPY;
     */
    /*
     关联方法：
     objc_setAssociatedObject(id object, const void *key, id value, objc_AssociationPolicy policy);
     
     参数：
     * id object 给哪个对象的属性赋值
     const void *key 属性对应的key
     id value  设置属性值为value
     objc_AssociationPolicy policy  使用的策略，是一个枚举值，和copy，retain，assign是一样的，手机开发一般都选择NONATOMIC
     */
    
    objc_setAssociatedObject(self, FvidKey, fvid, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)fvid{
    return objc_getAssociatedObject(self, FvidKey);
}
static char *fissignPlaceholderViewKey = "fissignPlaceholderViewKey";
- (void)setFissignPlaceholderView:(BOOL)fissignPlaceholderView{
    objc_setAssociatedObject(self, fissignPlaceholderViewKey, @(fissignPlaceholderView), OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (BOOL)fissignPlaceholderView
{
    NSNumber * n =  objc_getAssociatedObject(self, fissignPlaceholderViewKey);
    return [n boolValue];
}
static char *isListTemplateKey = "isListTemplateKey";
- (void)setFislistTemplate:(BOOL)fislistTemplate{
    objc_setAssociatedObject(self, isListTemplateKey, @(fislistTemplate), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(BOOL)fislistTemplate{
    NSNumber * n =  objc_getAssociatedObject(self, isListTemplateKey);
    return [n boolValue];
}

static char *flistTemplateEntitySignerHashKey = "flistTemplateEntitySignerHash";
- (void)setFlistTemplateEntitySignerHash:(NSString *)flistTemplateEntitySignerHash
{
    objc_setAssociatedObject(self, flistTemplateEntitySignerHashKey, flistTemplateEntitySignerHash, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSString *)flistTemplateEntitySignerHash
{
    return objc_getAssociatedObject(self, flistTemplateEntitySignerHashKey);
}

static char *flistdataSorceDicKey = "flistdataSorceDicKey";
-(NSMutableDictionary *)flistdataSorceDic{
    NSMutableDictionary * n =  objc_getAssociatedObject(self, flistdataSorceDicKey);
    if (n == nil) {
        n = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, flistdataSorceDicKey, n, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return n;
}

static char *flisttemplateDicKey = "flisttemplateDicKey";
-(NSMutableDictionary *)flisttemplateDic{
    NSMutableDictionary * n =  objc_getAssociatedObject(self, flisttemplateDicKey);
    if (n == nil) {
        n = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, flisttemplateDicKey, n, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return n;
}
static char *ftemplateSignKey = "ftemplateSignKey";
- (void)setFtemplateSign:(NSString *)ftemplateSign{
    objc_setAssociatedObject(self, ftemplateSignKey,ftemplateSign,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(NSString *)ftemplateSign{
    NSString * n =  objc_getAssociatedObject(self, ftemplateSignKey);
    return n;
}
static char *ftemplateConstructBlockKey = "ftemplateConstructBlockKey";
- (void)setFtemplateConstructBlock:(void (^)(FFactory * _Nonnull))ftemplateConstructBlock
{
    objc_setAssociatedObject(self, ftemplateConstructBlockKey,ftemplateConstructBlock,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (void (^)(FFactory * _Nonnull))ftemplateConstructBlock
{
    return objc_getAssociatedObject(self, ftemplateConstructBlockKey);
}

@end
