//
//  UIPickerView+FastConfig.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/10/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIPickerView (FastConfig)

- (UIPickerView * (^)(NSObject<UIPickerViewDelegate,UIPickerViewDataSource> *))fsHandler;

@end

NS_ASSUME_NONNULL_END
