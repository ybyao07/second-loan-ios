//
//  UIPickerView+FastConfig.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/10/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "UIPickerView+FastConfig.h"

@implementation UIPickerView (FastConfig)

- (UIPickerView * (^)(NSObject<UIPickerViewDelegate,UIPickerViewDataSource> *))fsHandler
{
    return ^id(NSObject<UIPickerViewDelegate,UIPickerViewDataSource> * handler) {
        self.delegate = handler;
        self.dataSource = handler;
        return self;
    };
}
@end
