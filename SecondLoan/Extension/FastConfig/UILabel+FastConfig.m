//
//  UILabel+FastConfig.m
//  SuYue
//
//  Created by junlei on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UILabel+FastConfig.h"

@implementation UILabel(FastConfig)
- (UILabel * (^)(UIFont *))fsFont{
    return ^id(UIFont *font) {
        self.font = font;
        return self;
    };
}
- (UILabel * (^)(CGFloat))fsFontSize {
    return ^id(CGFloat size) {
        self.font = [UIFont systemFontOfSize:SC(size)];
        return self;
    };
}
- (UILabel * (^)(NSString *))fsText {
    return ^id(NSString * content) {
        self.text = [NSString stringWithFormat:@"%@",content];
        return self;
    };
}
- (UILabel * (^)(NSInteger))fsTextColor {
    return ^id(NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        self.textColor = color;
        return self;
    };
}
- (UILabel * (^)(NSTextAlignment))fsAlign{
    return ^id(NSTextAlignment a) {
        self.textAlignment = a;
        return self;
    };
}
- (UILabel * (^)(NSInteger))fsLines {
    return ^id(NSInteger num) {
        self.numberOfLines = num;
        return self;
    };
}
@end
