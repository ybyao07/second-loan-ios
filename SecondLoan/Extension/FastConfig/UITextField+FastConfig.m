//
//  UITextField+FastConfig.m
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UITextField+FastConfig.h"
#import "LimitInput.h"

@implementation UITextField(FastConfig)

- (UITextField * (^)(UIFont *))fsFont{
    return ^id(UIFont * font) {
        self.font = font;
        return self;
    };
}
- (UITextField * (^)(CGFloat))fsFontSize {
    return ^id(CGFloat size) {
        self.font = [UIFont systemFontOfSize:SC(size)];
        return self;
    };
}
- (UITextField * (^)(NSInteger))fsTextColor {
    return ^id(NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        self.textColor = color;
        return self;
    };
}
- (UITextField * (^)(NSString *))fsPlaceholder
{
    return ^id(NSString * content) {
        self.placeholder = content;
        return self;
    };
}

- (UITextField * (^) (UITextFieldViewMode))fsClearButtonMode
{
    return ^id(UITextFieldViewMode mode) {
        self.clearButtonMode = mode;
        return self;
    };
}

- (UITextField * (^)(NSString *))fsText {
    return ^id(NSString * content) {
        self.text = content;
        return self;
    };
}

- (UITextField * (^)(UIKeyboardType))fsKeyboard
{
    return ^id(UIKeyboardType content) {
        self.keyboardType = content;
        return self;
    };
}

- (UITextField * (^)(NSString*,NSInteger,CGFloat))fsPText {
    return ^id(NSString* text, NSInteger hexValue,CGFloat fontsize) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary]; // 创建属性字典
        attrs[NSFontAttributeName] = [UIFont systemFontOfSize:SC(fontsize)]; // 设置font
        attrs[NSForegroundColorAttributeName] = color; // 设置颜色
        NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:text attributes:attrs]; // 初始化富文本占位字符串
        self.attributedPlaceholder = attStr;
        return self;
    };
}
- (UITextField * (^)(NSString *limitCount))fsLimit {
    return ^id(NSString *limitCount) {
        [self setValue:limitCount forKey:@"limit"];
        return self;
    };
}

- (UITextField * (^)(CGFloat))fsLeftPad {
    return ^id(CGFloat l) {
        UIView * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, l, l)];
        self.leftView = v;
        self.leftViewMode = UITextFieldViewModeAlways;
        return self;
    };
}
@end
