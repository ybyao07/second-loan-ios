//
//  UIButton+FastConfig.h
//  SuYue
//
//  Created by junlei on 2018/11/17.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>



#define fsOnClick(n) fsOnTouchUpInside(self,@selector(n))

NS_ASSUME_NONNULL_BEGIN

@interface UIButton(FastConfig)

- (UIButton * (^)(UIFont *))fsFont;
- (UIButton * (^)(CGFloat))fsFontSize;
- (UIButton * (^)(NSString *))fsText;
- (UIButton * (^)(NSString *))fsImage;
- (UIButton * (^)(NSString *))fsWebImage;
- (UIButton * (^)(NSString *))fsTextSelect;
- (UIButton * (^)(NSString *))fsImageSelect;
- (UIButton * (^)(BOOL))fsStaSel;
- (UIButton * (^)(BOOL))fsEnable;
- (UIButton * (^)(NSInteger))fsTextColor;
- (UIButton * (^)(NSInteger))fsTextColorSel;
- (UIButton * (^)(NSObject *,SEL))fsOnTouchUpInside;
- (UIButton * (^)(NSString *))fsBackGroundImage;
- (UIButton * (^)(NSTextAlignment))fsAlign;
@end

NS_ASSUME_NONNULL_END
