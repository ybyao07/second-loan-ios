//
//  UITableView+FastConfig.m
//  SuYue
//
//  Created by junlei on 2018/11/21.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UITableView+FastConfig.h"

@implementation UITableView(FastConfig)

- (UITableView * (^)(NSObject<UITableViewDelegate,UITableViewDataSource> *))fsHandler
{
    return ^id(NSObject<UITableViewDelegate,UITableViewDataSource> * handler) {
        self.delegate = handler;
        self.dataSource = handler;
        return self;
    };
}
- (UITableView * (^)(UITableViewCellSeparatorStyle))fsSeparator
{
    return ^id(UITableViewCellSeparatorStyle style) {
        self.separatorStyle = style;
        return self;
    };
}
- (UITableView * (^)(BOOL))fsShowIndicator
{
    return ^id(BOOL isShow) {
        [self setShowsVerticalScrollIndicator:isShow];
        [self setShowsHorizontalScrollIndicator:isShow];
        return self;
    };
}
- (UITableView * (^)(Class ))fsRegisterCellClass
{
    return ^id(Class cellClass) {
        [self registerClass:cellClass forCellReuseIdentifier:NSStringFromClass(cellClass)];
        return self;
    };
}
- (UITableView * (^)(Class ))fsRegisterCellNib
{
    return ^id(Class cellClass) {
        [self registerNib:[UINib nibWithNibName:NSStringFromClass(cellClass) bundle:nil] forCellReuseIdentifier:NSStringFromClass(cellClass)];
        return self;
    };
}

@end
