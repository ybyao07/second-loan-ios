//
//  UIScrollView+FastConfig.h
//  SuYue
//
//  Created by junlei on 2019/3/9.
//  Copyright © 2019 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView(FastConfig)

- (UITableView * (^)(BOOL))fsShowIndicator;

@end

NS_ASSUME_NONNULL_END
