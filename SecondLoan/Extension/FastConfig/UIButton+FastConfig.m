//
//  UIButton+FastConfig.m
//  SuYue
//
//  Created by junlei on 2018/11/17.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UIButton+FastConfig.h"

@implementation UIButton(FastConfig)

- (UIButton * (^)(UIFont *))fsFont{
    return ^id(UIFont *font) {
        self.titleLabel.font = font;
        return self;
    };
}
- (UIButton * (^)(CGFloat))fsFontSize {
    return ^id(CGFloat size) {
        self.titleLabel.font = [UIFont systemFontOfSize:SC(size)];
        return self;
    };
}
- (UIButton * (^)(NSString *))fsText {
    return ^id(NSString * content) {
        [self setTitle:content forState:UIControlStateNormal];
        return self;
    };
}
- (UIButton * (^)(NSString *))fsImage{
    return ^id(NSString * content) {
        [self setImage:[UIImage imageNamed:content] forState:UIControlStateNormal];
        return self;
    };
}
- (UIButton * (^)(NSString *))fsWebImage{
    return ^id(NSString * content) {
        [self sd_setImageWithURL:[CommonTools transQiniuKeyToURL:content] forState:UIControlStateNormal];
        return self;
    };
}

- (UIButton * (^)(NSString *))fsTextSelect{
    return ^id(NSString * content) {
        [self setTitle:content forState:UIControlStateSelected];
        return self;
    };
}

- (UIButton * (^)(NSString *))fsImageSelect{
    return ^id(NSString * content) {
        [self setImage:[UIImage imageNamed:content] forState:UIControlStateSelected];
        return self;
    };
}
- (UIButton * (^)(BOOL))fsStaSel
{
    return ^id(BOOL sel) {
        self.selected = sel;
        return self;
    };
}
- (UIButton * (^)(BOOL))fsEnable
{
    return ^id(BOOL ene) {
        self.enabled = ene;
        return self;
    };
}
- (UIButton * (^)(NSInteger))fsTextColor {
    return ^id(NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        UIColor * color2 = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:0.5];
        
        [self setTitleColor:color forState:UIControlStateNormal];
        [self setTitleColor:color2 forState:UIControlStateHighlighted];
        return self;
    };
}
- (UIButton * (^)(NSInteger))fsTextColorSel
{
    return ^id(NSInteger hexValue) {
        UIColor * color = [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0];
        
        [self setTitleColor:color forState:UIControlStateSelected];
        return self;
    };
}
- (UIButton * (^)(NSObject *,SEL))fsOnTouchUpInside
{
    return ^id(NSObject * target,SEL selector) {
        [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        return self;
    };
}

- (UIButton * (^)(NSString *))fsBackGroundImage {
    return ^id(NSString * filename) {
        [self setBackgroundImage:[UIImage imageNamed:filename] forState:UIControlStateNormal];
        return self;
    };
}

- (UIButton * (^)(NSTextAlignment))fsAlign{
    return ^id(NSTextAlignment a) {
        self.titleLabel.textAlignment = a;
        return self;
    };
}


@end
