//
//  UIImageView+FastConfig.m
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UIImageView+FastConfig.h"

@implementation UIImageView(FastConfig)
- (UIImageView * (^)(NSString*))fsImage
{
    return ^id(NSString * content) {
        [self setImage:[UIImage imageNamed:content]];
        return self;
    };
}
- (UIImageView * (^)(NSString*))fsHigImage
{
    return ^id(NSString * content) {
        [self setHighlightedImage:[UIImage imageNamed:content]];
        return self;
    };
}
- (UIImageView * (^)(NSString*))fsWebImage
  {
    return ^id(NSString * content) {
      [self sd_setImageWithURL:[CommonTools transQiniuKeyToURL:content]];
      return self;
    };
  }

- (UIImageView * (^)(UIViewContentMode))fsImgContentMode
{
    return ^id(UIViewContentMode mode) {
      [self setContentMode:mode];
      return self;
    };
}


@end
