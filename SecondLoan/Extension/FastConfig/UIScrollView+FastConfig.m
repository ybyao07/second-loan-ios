//
//  UIScrollView+FastConfig.m
//  SuYue
//
//  Created by junlei on 2019/3/9.
//  Copyright © 2019 Win10. All rights reserved.
//

#import "UIScrollView+FastConfig.h"

@implementation UIScrollView(FastConfig)

- (UITableView * (^)(BOOL))fsShowIndicator
{
    return ^id(BOOL isShow) {
        [self setShowsVerticalScrollIndicator:isShow];
        [self setShowsHorizontalScrollIndicator:isShow];
        return self;
    };
}

@end
