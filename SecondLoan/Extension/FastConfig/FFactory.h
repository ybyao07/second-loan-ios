//
//  FastFactory.h
//  SuYue
//
//  Created by junlei on 2018/11/21.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+FastConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface FFactory : NSObject
@property (nonatomic,strong) UIView * sView;

@property (nonatomic,strong) NSDictionary * needData;

- (instancetype)initWithSuperView:(UIView *)sView;
- (instancetype)initWithSuperView:(UIView *)sView data:(NSDictionary *)data;

- (UIView * (^)(UIView *))cView;
- (UIButton * (^)(UIView *))cButton;
- (UIView * (^)(void))view;

- (UIButton * (^)(void))button;

- (UILabel * (^)(void))label;

- (UIImageView * (^)(void))imageView;

- (UITextField * (^)(void))textField;

- (UITableView * (^)(void))tableView;
- (UIPickerView * (^)(void))pickerView;
- (UITableView * (^)(void))tableViewG;

- (UIScrollView * (^)(void))scrollView;

- (UIView * (^)(NSArray *))list;
@end

NS_ASSUME_NONNULL_END
