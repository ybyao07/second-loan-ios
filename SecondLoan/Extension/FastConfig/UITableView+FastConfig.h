//
//  UITableView+FastConfig.h
//  SuYue
//
//  Created by junlei on 2018/11/21.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView(FastConfig)

- (UITableView * (^)(NSObject<UITableViewDelegate,UITableViewDataSource> *))fsHandler;
- (UITableView * (^)(UITableViewCellSeparatorStyle))fsSeparator;
- (UITableView * (^)(BOOL))fsShowIndicator;
- (UITableView * (^)(Class ))fsRegisterCellClass;
- (UITableView * (^)(Class ))fsRegisterCellNib;
@end

NS_ASSUME_NONNULL_END
