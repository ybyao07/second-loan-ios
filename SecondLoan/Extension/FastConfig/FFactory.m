//
//  FastFactory.m
//  SuYue
//
//  Created by junlei on 2018/11/21.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "FFactory.h"

@implementation FFactory

- (instancetype)initWithSuperView:(UIView *)sView
{
    self = [super init];
    if (self) {
        self.sView = sView;
    }
    return self;
}
- (instancetype)initWithSuperView:(UIView *)sView data:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self.sView = sView;
        self.needData = data;
    }
    return self;
}
- (UIView * (^)(UIView *))cView
{
    return ^id(UIView * view) {
        [self.sView addSubview:view];
        return view;
    };
}
- (UIButton * (^)(UIView *))cButton
{
    return ^id(UIView * view) {
        [self.sView addSubview:view];
        return view;
    };
}
- (UIView * (^)(void))view
{
    return ^id() {
        UIView * widget = [[UIView alloc] init];
        [self.sView addSubview:widget];
        return widget;
    };
}

- (UIButton * (^)(void))button{
    return ^id() {
        UIButton * widget = [[UIButton alloc] init];
        [self.sView addSubview:widget];
        return widget;
    };
}

- (UILabel * (^)(void))label
{
    return ^id() {
        UILabel * widget = [[UILabel alloc] init];
        [self.sView addSubview:widget];
        return widget;
    };
}
- (UITextField * (^)(void))textField
{
    return ^id() {
        UITextField * widget = [[UITextField alloc] init];
        widget.borderStyle = UITextBorderStyleNone;
        [self.sView addSubview:widget];
        return widget;
    };
}
- (UIImageView * (^)(void))imageView
{
    return ^id() {
        UIImageView * widget = [[UIImageView alloc] init];
        widget.contentMode = UIViewContentModeScaleAspectFill;
        [self.sView addSubview:widget];
        return widget;
    };
}

- (UITableView * (^)(void))tableView
{
    return ^id() {
        UITableView * widget = [[UITableView alloc] initWithFrame:CGRectZero];
        [self.sView addSubview:widget];
        return widget;
    };
}
- (UIPickerView * (^)(void))pickerView
{
    return ^id() {
        UIPickerView * widget = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [self.sView addSubview:widget];
        return widget;
    };
}

- (UITableView * (^)(void))tableViewG
{
    return ^id() {
        UITableView * widget = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [self.sView addSubview:widget];
        return widget;
    };
}


- (UIScrollView * (^)(void))scrollView
{
    return ^id() {
        UIScrollView * widget = [[UIScrollView alloc] initWithFrame:CGRectZero];
        [widget setAlwaysBounceVertical:YES];
        [self.sView addSubview:widget];
        return widget;
    };
}

- (UIView * (^)(NSArray *))list
{
    return ^id(NSArray * dataSource) {
        UIView * signView = [[UIView alloc] init];
        signView.fissignPlaceholderView = YES;
        
        NSString * key = [@(signView.hash) stringValue];
        
        UIView * widgetTemplate = [[UIView alloc] init];
        widgetTemplate.fislistTemplate = YES;
        
        [[self.sView flistdataSorceDic] setObject:dataSource forKey:key];
        [[self.sView flisttemplateDic] setObject:widgetTemplate forKey:key];
        
        [self.sView addSubview:signView];
        return widgetTemplate;
    };
}
@end
