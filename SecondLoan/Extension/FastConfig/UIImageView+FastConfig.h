//
//  UIImageView+FastConfig.h
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView(FastConfig)
- (UIImageView * (^)(NSString*))fsImage;
- (UIImageView * (^)(NSString*))fsHigImage;
- (UIImageView * (^)(NSString*))fsWebImage;
- (UIImageView * (^)(UIViewContentMode))fsImgContentMode;

@end

NS_ASSUME_NONNULL_END
