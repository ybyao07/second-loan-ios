//
//  UILabel+FastConfig.h
//  SuYue
//
//  Created by junlei on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel(FastConfig)
- (UILabel * (^)(UIFont *))fsFont;
- (UILabel * (^)(CGFloat))fsFontSize;
- (UILabel * (^)(NSString *))fsText;
- (UILabel * (^)(NSInteger))fsTextColor;
- (UILabel * (^)(NSTextAlignment))fsAlign;
- (UILabel * (^)(NSInteger))fsLines;
@end

NS_ASSUME_NONNULL_END
