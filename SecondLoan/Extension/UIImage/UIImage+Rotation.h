//
//  UIImage+Rotation.h
//  AFNetworking
//
//  Created by Echo Zhangjie on 2019/9/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Rotation)
+(UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation;
@end

NS_ASSUME_NONNULL_END
