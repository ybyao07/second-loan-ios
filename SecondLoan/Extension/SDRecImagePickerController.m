//
//  SDRecImagePickerController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SDRecImagePickerController.h"

@interface SDRecImagePickerController ()

@property (nonatomic, strong) UIImageView *cardBg;
@end

@implementation SDRecImagePickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    if (self.idFront) {
//        [self.view addSubview:self.fronImg];
//    }else{
//        [self.view addSubview:self.backImg];
//    }
    [self.view addSubview:self.cardBg];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (UIImageView *)cardBg{
    if (!_cardBg) {
        _cardBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_c"]];
        CGSize size = _cardBg.size;
        _cardBg.frame = CGRectMake(20, SCREEN_HEIGHT/2.0 - 160, SCREEN_WIDTH - 40, size.height);
    }
    return _cardBg;
}

- (UIImageView *)backImg{
    if (!_backImg) {
        _backImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Page 1"]];
        CGSize size = _backImg.size;
        _backImg.frame = CGRectMake(20, SCREEN_HEIGHT/2.0 - 60, size.width, size.height);
    }
    return _backImg;
}
- (UIImageView *)fronImg{
    if (!_fronImg) {
        _fronImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xuxian"]];
        CGSize size = _fronImg.size;
        _fronImg.frame = CGRectMake(SCREEN_WIDTH - size.width - 30, SCREEN_HEIGHT/2.0 - size.height - 20, size.width, size.height);
    }
    return _fronImg;
}




@end
