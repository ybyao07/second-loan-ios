//
//  LocationManager.h
//  SuYue
//
//  Created by Win10 on 2018/11/23.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationManager : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic,strong) CLLocation * currentLocation;
@property (nonatomic,assign) NSInteger nextTime;
@property (nonatomic,assign) BOOL isUpdating;
- (void)updateLocation;
@end

NS_ASSUME_NONNULL_END
