//
//  WebSocketManager.h
//  滴滴卡
//
//  Created by powerlong on 15-9-14.
//  Copyright (c) 2015年 kimi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@protocol WebSocketManagerReciver <NSObject>

- (void)reciveMessage:(NSArray *)message;

@end


@interface WebSocketManager : NSObject<SRWebSocketDelegate>

@property(nonatomic ,strong) SRWebSocket * webSocket;

+ (WebSocketManager*)shareInstance;

//- (void)sendMessage:(NSString *)message;

- (void)setSocketClose;
- (void)setSocketOpen;

- (SRReadyState)getSocketStatue;

@property (assign , nonatomic) BOOL needRepConnect;
@property (strong , nonatomic) NSMutableArray<WebSocketManagerReciver> * recivers;   //消息接收者

@end
