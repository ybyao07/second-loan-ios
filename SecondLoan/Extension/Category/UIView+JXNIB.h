//
//  UIView+JXNIB.h
//  jiuxian
//
//  Created by tianzhenzi on 15/8/12.
//  Copyright (c) 2015年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 *  NIB分类
 */
@interface UIView (JXNIB)

/**
 *  获取NIB
 *
 *  @return nib
 */
+ (UINib *)nib;

/**
 *  通过名称获取nib
 *
 *  @param name 名称
 *
 *  @return nib
 */
+ (UINib *)nibWithName:(NSString *)name;

/**
 *  通过NIb名称获取内部View视图集合
 *
 *  @param name nib名称
 *
 *  @return View集合
 */
+ (NSArray *)viewsWithNibName:(NSString *)name;

@end
NS_ASSUME_NONNULL_END
