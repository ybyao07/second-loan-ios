//
//  UIView+JXTapEvent.m
//  jiuxian
//
//  Created by tianzhenzi on 15/8/24.
//  Copyright (c) 2015年 jiuxian.com. All rights reserved.
//

#import "UIView+JXTapEvent.h"

@implementation UIView (JXTapEvent)

/**
 增加单手指点击事件
 */
- (void)addTapOneTarget:(id)target action:(SEL)action {
    UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc] initWithTarget:target
                                                                                    action:action];
    [tapGestureTel setNumberOfTapsRequired:1];
    [tapGestureTel setNumberOfTouchesRequired:1];
    [self setUserInteractionEnabled:YES];
    [self addGestureRecognizer:tapGestureTel];
}

- (void)removeAllTapTarget {
    [[self gestureRecognizers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIGestureRecognizer class]]) {
            UIGestureRecognizer *recognizer = (UIGestureRecognizer *) obj;
            [self removeGestureRecognizer:recognizer];
        }
    }];
}

@end
