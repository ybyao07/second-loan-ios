//
//  UIView+JXNIB.m
//  jiuxian
//
//  Created by tianzhenzi on 15/8/12.
//  Copyright (c) 2015年 jiuxian.com. All rights reserved.
//
#import "UIView+JXNIB.h"

@implementation UIView (JXNIB)

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (UINib *)nibWithName:(NSString *)name {
    return [UINib nibWithNibName:name bundle:nil];
}

+ (NSArray *)viewsWithNibName:(NSString *)name {
    return [[NSBundle mainBundle] loadNibNamed:name owner:nil options:nil];
}

@end
