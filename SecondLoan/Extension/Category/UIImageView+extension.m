//
//  UIImageView+extension.m
//  jiuxian
//
//  Created by 张正超 on 16/4/25.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import "UIImageView+extension.h"
#import <objc/runtime.h>

@interface UIImageView ()

@end

@implementation UIImageView (extension)

- (void)enableTouchEvent{
     self.userInteractionEnabled = YES;
     [self addGestureRecognizer:self.pg_tapGestureRecognizer];
}

- (void)setPg_object:(NSDictionary *)pg_object{
    objc_setAssociatedObject(self, _cmd, pg_object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)pg_object{
    return objc_getAssociatedObject(self, @selector(setPg_object:));
}

- (void)setPg_block:(void (^)(NSDictionary *))pg_block{
    objc_setAssociatedObject(self, _cmd, pg_block, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void (^)(NSDictionary *))pg_block{
    return objc_getAssociatedObject(self, @selector(setPg_block:));
}

- (UITapGestureRecognizer *)pg_tapGestureRecognizer{
    
    UITapGestureRecognizer *tapGestureRecognizer = objc_getAssociatedObject(self, _cmd);
    if (!tapGestureRecognizer) {
        tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pg_handleTapGesture:)];
        objc_setAssociatedObject(self, _cmd, tapGestureRecognizer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return tapGestureRecognizer;
}

- (void)pg_handleTapGesture:(UIGestureRecognizer *)ges{
    if (self.pg_block) {
        self.pg_block(self.pg_object);
    }
}

@end
