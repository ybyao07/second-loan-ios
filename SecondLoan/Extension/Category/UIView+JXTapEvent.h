//
//  UIView+JXTapEvent.h
//  jiuxian
//
//  Created by tianzhenzi on 15/8/24.
//  Copyright (c) 2015年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 *  tap事件
 */
@interface UIView (JXTapEvent)

/**
 *  添加单手指点击事件
 *
 *  @param target 对象
 *  @param action 触发方法
 */
- (void)addTapOneTarget:(id)target action:(SEL)action;

/**
 *  移除所有单手指点击事件
 */
- (void)removeAllTapTarget;
@end
NS_ASSUME_NONNULL_END
