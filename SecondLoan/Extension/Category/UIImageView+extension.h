//
//  UIImageView+extension.h
//  jiuxian
//
//  Created by 张正超 on 16/4/25.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface UIImageView (extension)

//UIImageView真正的点击手势，如需定制化操作，可以拿到这个之后，自由使用
@property(nonatomic, strong, readonly) UITapGestureRecognizer *pg_tapGestureRecognizer;


//如果需要传参，请给object赋值
@property(nonatomic, copy) NSDictionary *pg_object;

@property (nonatomic , strong) void (^pg_block)(NSDictionary *params);


//开启event点击事件
- (void)enableTouchEvent;

@end
NS_ASSUME_NONNULL_END
