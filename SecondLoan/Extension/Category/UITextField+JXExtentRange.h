//
//  UITextField+JXExtentRange.h
//  jiuxian
//
//  Created by Dely on 15/10/14.
//  Copyright © 2015年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface UITextField (JXExtentRange)

- (NSRange)selectedRange;

- (void)setSelectedRange:(NSRange)range;

@end
NS_ASSUME_NONNULL_END
