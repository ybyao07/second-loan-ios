//
//  NSString+JXCommon.h
//  jiuxian
//
//  Created by tianzhenzi on 15/8/5.
//  Copyright (c) 2015年 jiuxian.com. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 *  字符串处理分类
 */
@interface NSString (JXCommon)

/**
 URL编码
 */
- (NSString *)urlEncoding;

/**
 URL解码
 */
- (NSString *)urlDecoding;

- (NSString *)stringByURLDecode;

-(NSString *)URLDecodedString;

/**
 判断字符串是否是条码
 */
- (BOOL)isBarcode;

/**
 判断是否是url
 */
- (BOOL)isUrl;

/**
 *  拼接大图url
 *
 *  @return 大图url
 */
- (NSString *)bigImageUrl;

/**
 判断是否是email
 */
- (BOOL)isEmail;

/**
 判断是否是手机号
 */
- (BOOL)isMobile;

/**
 判断是否是电话号码
 
 匹配格式：
 11位手机号码
 3-4位区号，7-8位直播号码，1－4位分机号
 如：12345678901、1234-12345678-1234
 */
- (BOOL)isPhone;

/**
 判断是否是身份证
 */
- (BOOL)isIdCard;

/**
 根据身份证号获取生日:YYYY-MM-dd字符串
 */
- (NSString *)birthdayWithIdCard;

/**
 验证密码格式，数字，字母，符号组合验证
 */
- (BOOL)isPassWord;

/**
 判断日期格式：YYYY-MM-dd
 */
- (BOOL)isDateForYYYY_MM_dd;

- (CGSize)adjustHeightWithFont:(UIFont *)font width:(CGFloat)width;

/**
 分类中方法
 */
- (CGSize)jkdSizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size
            lineBreakMode:(NSLineBreakMode)lineBreakMode;

- (CGSize)jx_SizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size
                lineSpace:(CGFloat)lineSpace
            numberOfLines:(NSInteger)lines;

- (CGSize)jx_SizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size
                lineSpace:(CGFloat)lineSpace
            numberOfLines:(NSInteger)lines
               attributes:(NSDictionary * _Nullable)attributes;

- (CGSize)jx_SizeWithFont:(UIFont *)font
        constrainedToSize:(CGSize)size;

/**
 过滤返回值为nil的情况
 */
- (NSString *)replaceNil;

/**
 *  价格显示规范:最多两位小数，最后一位小数是0的情况，显示一位小数
 *
 *  @return 规范后的价格
 */
+ (NSString *)priceDisplayWithStr:(double)price;

/**
 输入值是空格的
 */
- (BOOL)isEmpty;

/**
 字符串是否为空的 空：YES 否：NO
 */
+ (BOOL)isEmptyWithString:(NSString *)string;



- (BOOL)isValidString;

- (UIImage *)forumMyLevelImage;

- (UIImage *)forumOtherLevelImage;

/**
 *特殊字过滤
 *  @ string  字符串
 *  @ character  特殊字符
 *  @ return  过滤后侧字符串
 */
-(NSString *)stringOfSpecialCharacter:(NSString *)string character:(nonnull NSString *)character;

/**
 *  字体所占size大小
 *
 *  @param font          字体大小
 *  @param constrainSize 最大长宽
 *
 *  @return size
 */
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)constrainSize;

@end
NS_ASSUME_NONNULL_END
