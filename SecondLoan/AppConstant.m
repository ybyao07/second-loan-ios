//
//  AppConstant.m
//  mobilePD
//
//  Created by wangyan on 2016－05－12.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "AppConstant.h"
#import <Foundation/Foundation.h>

// 登录成功
NSString *const NOTIFICATION_LOGINSUCCESS = @"notification_loginSuccess";
// 退出登录
NSString *const NOTIFICATION_LOGOUT = @"notification_loginout";
// 登录过期
NSString *const NOTIFICATION_LOGIN_EXPIRED = @"notification_loginExpired";
// 弹出登录提示
NSString *const NOTIFICATION_NEED_ALERT_LOGINVIEWCONTROLLER = @"notification_needAlertLoginViewController";

NSString * const NOTIFICATION_GROUP_LIST_NEW = @"NOTIFICATION_GROUP_LIST_NEW";

NSString * const NOTIFICATION_MESSAGE = @"NOTIFICATION_MESSAGE";

NSString * const NOTIFICATION_LOAD_BADGE = @"NOTIFICATION_LOAD_BADGE";

NSString * const HomeCityCode = @"HOMECITYCODE";
NSString * const HomeCityName = @"HOMECITYNAME";

NSString * const kDeviceTokens = @"device_tokens";  //友盟单推需要记录

NSString * const kNoNetworkMsg = @"没有网络请确认后重试";
NSString * const kNetworkTimeoutMsg = @"网络异常，请稍后重试";



NSString * const MsgEmpty               = @"输入内容不能为空";
NSString * const MsgAddAddressCityEmpty = @"请选择地址";
NSString * const MsgAddAddressEmpty     = @"详细地址不能为空";

NSString * const MsgHintPhoneEmpty      = @"请填写手机号";
NSString * const MsgHintPhoneNoCorrect  = @"请输入正确手机号";
NSString * const MsgHintContactPhoneNoCorrect  = @"请输入联系人正确手机号";
NSString * const MsgHintCompanyPhoneNoCorrect  = @"请输入单位正确电话";
NSString * const MsgHintSpousePhoneNoCorrect  = @"请输入配偶正确手机号";
NSString * const MsgHintErrorEMail      = @"个人邮箱输入不正确";
NSString * const MsgHintErrorWX      = @"请输入正确的微信号";
NSString * const MsgHintNameEmpty       = @"请输入姓名";
NSString * const MsgHintNameNoCorrect   = @"请正确输入姓名";
NSString * const MsgHintIDCardEmpty     = @"请填写身份证号";
//NSString * const MsgHintIDCardNoCorrect = @"请正确填写身份证号";
NSString * const MsgHintIDCardNoCorrect = @"请正确上传个人身份信息";


NSString * const MsgHintVerifyCodeNoCorrect = @"请输入正确的验证码";
NSString * const MsgHintVerifyCodeEmpty = @"请输入短信验证码";
NSString * const MsgHintVerifyImgCodeEmpty = @"请输入图像验证码";
NSString * const MsgHintPasswordNeed  = @"请输入密码";
NSString * const MsgHintPasswordSureNeed  = @"请输入确认密码";
NSString * const MsgHintNewPasswordSureNeed  = @"请输入确认密码";

NSString * const MsgHintNewPasswordNeed  = @"请输入新密码";
NSString * const MsgHintNewEqualOld = @"为保障账户安全，新密码设置不能与旧密码一致";
NSString * const MsgHintPasswordValid  = @"密码8-16位且必须包含英文字母,特殊字符和数字中的2种";


NSString * const MsgHintNoFaceData = @"请刷脸";
NSString * const MsgHintFaceFailureData = @"认证失败，请重新认证";


NSString * const kMsgHintBaiduyunNotFull = @"请核对信息是否正确";
NSString * const kBaiduyunNoInfo = @"未能识别信息，请手动输入";


void JLAsyncRun(JLRun run) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), run);
}

void JLAsyncRunInMain(JLRun run) {
    dispatch_async(dispatch_get_main_queue(), run);
}

void JLAsyncRunInMainDelayed(CGFloat afterTime ,JLRun run) {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(afterTime * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), run);
}
