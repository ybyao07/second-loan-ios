//
//  MineHomeItemView.m
//  SuYue
//
//  Created by Win10 on 2018/12/11.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "MineHomeItemView.h"

@implementation MineHomeItemView

- (instancetype)initWithIcon:(NSString *)icon name:(NSString *)name{
    return [self initWithIcon:icon name:name showline:YES];
}
- (instancetype)initWithNBIcon:(NSString *)icon name:(NSString *)name{
    return [self initWithIcon:icon name:name showline:NO];
}

- (instancetype)initWithIcon:(NSString *)icon name:(NSString *)name showline:(BOOL)showl
{
    self = [super init];
    self.iconView.image = [UIImage imageNamed:icon];
    self.mainLable.text = name;
    self.bottomLine.hidden = !showl;
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.develop(^(FFactory * factory){
            factory.imageView().fsClip(YES).fsFvid(@"hjiedddwr_icon");
            factory.label().fsFontSize(15).fsText(@"--").fsTextColor(0x23242A).fsFvid(@"hjiedddwr_title");
            factory.imageView().fsImage(@"arrow").fsFvid(@"hjiedddwr_arrow");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"hjiedddwr_bottomline");
        });
    }
    
    self.mainLable = fQueryLbl(self, @"hjiedddwr_title");
    self.iconView = fQueryImgv(self, @"hjiedddwr_icon");
    self.arrow = fQueryImgv(self, @"hjiedddwr_arrow");
    self.bottomLine = fQuery(self, @"hjiedddwr_bottomline");
    [fQuery(self, @"hjiedddwr_icon") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
        make.centerY.equalTo(self);
        make.left.equalTo(self).with.offset(15);
    }];
    [fQuery(self, @"hjiedddwr_title") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fQuery(self, @"hjiedddwr_icon").mas_right).with.offset(10);
        make.centerY.equalTo(self);
    }];
    [fQuery(self, @"hjiedddwr_arrow") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-15);
        make.centerY.equalTo(self);
    }];
    [fQuery(self, @"hjiedddwr_bottomline") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(22);
        make.right.equalTo(self).with.offset(-22);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    return self;
}

@end
