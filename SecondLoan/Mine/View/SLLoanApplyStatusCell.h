//
//  SLApplyStatusCell.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanApplyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLLoanApplyStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dingdanNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblApplyTime;
@property (weak, nonatomic) IBOutlet UILabel *lblApplyMoney;
@property (weak, nonatomic) IBOutlet UILabel *lblApplyLimit;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;

@property (nonatomic, strong) LoanApplyModel *model;

+ (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
