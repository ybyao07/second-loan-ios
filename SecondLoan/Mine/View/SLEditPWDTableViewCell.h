//
//  SLEditPWDTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/29.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLEditPWDTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
- (void)setTitle:(NSString *)title;
- (NSString *)getPwd;
- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
