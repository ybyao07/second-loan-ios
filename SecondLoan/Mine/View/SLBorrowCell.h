//
//  SLFinishedCell.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BorrowApplyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SLBorrowCell : UITableViewCell
@property (nonatomic, strong) BorrowApplyModel *model;

+ (CGFloat)cellHeight;
@end

NS_ASSUME_NONNULL_END
