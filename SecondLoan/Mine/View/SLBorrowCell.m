//
//  SLFinishedCell.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLBorrowCell.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

@interface SLBorrowCell()
@property (weak, nonatomic) IBOutlet UILabel *dingdanNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *jineLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueMoneyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *borrowStatusImg;

@end
//
//@property (nonatomic, copy) NSString *dingdanNum;
//    //订单号
//@property (nonatomic, copy) NSString *borrowMoney;          //借款金额
//@property (nonatomic, copy) NSString *borrowTime;           //借款时间
//@property (nonatomic, copy) NSString *borrowTerm;           //借款期限
//@property (nonatomic, assign) NSInteger borrowStatus;         //借款状态
//@property (nonatomic, copy) NSString *settleTime;           //结清时间
//@property (nonatomic, copy) NSString *surplusMoney;         //7日待还
//
//@end

@implementation SLBorrowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [CommanTool setShadowOnView:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(BorrowApplyModel *)model
{
    _model = model;
    _dingdanNumLabel.text = model.borrowTitle;
    _jineLabel.text = [NSMutableString stringWithFormat:@"%@元", [NSString getMoneyStringWithMoneyNumber:model.borrowMoney]];//借款金额
    _dateLabel.text = model.borrowTime;
    _timeLabel.text = [NSMutableString stringWithFormat:@"%@", [BorrowApplyModel getBorrowTime:model.borrowTerm] ];//借款期限
    _dueDateLabel.text = model.settleTime;
    _dueMoneyLabel.text = [NSMutableString stringWithFormat:@"%@元", [NSString getMoneyStringWithMoneyNumber:model.surplusMoney]];//七日应还
  switch (model.borrowStatus) {
    case CodeBorrowing:
      _borrowStatusImg.image = [UIImage imageNamed:@"loan_status_middle"];
      break;
    case CodeBorrowNotFinish:
      _borrowStatusImg.image = [UIImage imageNamed:@"loan_status_weijie"];
      break;
    case CodeForrowFinished:
      _borrowStatusImg.image = [UIImage imageNamed:@"loan_status_finished"];
      break;
    default:
      break;
  }
}

+ (CGFloat)cellHeight
{
    return 215;
}

@end
