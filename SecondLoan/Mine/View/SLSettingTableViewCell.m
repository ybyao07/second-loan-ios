//
//  SLSettingTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLSettingTableViewCell.h"

@interface SLSettingTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
  @property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation SLSettingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bindTitle: (NSString *)title {
//  self.imgView.image = [UIImage imageNamed:imgName];
//  self.imgView.contentMode = UIViewContentModeScaleAspectFit;
  self.titleLabel.text = title;
}

@end
