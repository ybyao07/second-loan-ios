//
//  SLMyAddressView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankPersonAddressInfo.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SLMyAddressTag) {
    MyAddressViewTypeTag,
    MyAddressAreaTag,
    MyAddressDetailTag,
    MyAddressTimeTag
};

@protocol SLMyAddressViewDelegate
- (void)chooseMyAddressView:(InfoChooseView *)view;
@end

@interface SLMyAddressView : UIView

@property (strong, nonatomic) InfoChooseView *addressHouseType;   // 住房类型
@property (strong, nonatomic) InfoTextFieldView *addressArea;   // 所在地区
@property (strong, nonatomic) InfoTextFieldView *addressDetail;   // 详细地址
//@property (strong, nonatomic) InfoTextFieldView *addressTime;   // 入住时长
@property (strong, nonatomic) InfoChooseView *addressTime;   // 入住时长

@property (nonatomic, weak) id<SLMyAddressViewDelegate> delegate;

@property (strong, nonatomic) BankPersonAddressInfo *model;

@end

NS_ASSUME_NONNULL_END
