//
//  InfoTextFieldView.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/7/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoTextFieldView : UIView

@property (strong, nonatomic) UILabel *lblHint;
@property (strong, nonatomic) UITextField *textFieldView;

@property (nonatomic, strong) UIView *seperTopLine;
@property (nonatomic, strong) UIView *seperBottomLine;
@end
