//
//  InfoWorkView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "InfoChooseView.h"

@interface InfoChooseView()
@property (nonatomic, strong) UIImageView *arrow;

@end

@implementation InfoChooseView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.lblHint];
        [self addSubview:self.lblContent];
        [self addSubview:self.arrow];
        _seperTopLine = [UIView new];
        _seperTopLine.backgroundColor = CCellSeperLineColor;
        _seperTopLine.hidden = YES;
        _seperBottomLine = [UIView new];
        _seperBottomLine.backgroundColor = CCellSeperLineColor;
        [self addSubview:_seperTopLine];
        [self addSubview:_seperBottomLine];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withHintTitle:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.lblHint];
        [self addSubview:self.lblContent];
        [self addSubview:self.arrow];
        _seperTopLine = [UIView new];
        _seperTopLine.backgroundColor = CCellSeperLineColor;
        _seperTopLine.hidden = YES;
        _seperBottomLine = [UIView new];
        _seperBottomLine.backgroundColor = CCellSeperLineColor;
        [self addSubview:_seperTopLine];
        [self addSubview:_seperBottomLine];
        self.lblHint.text = title;
    }
    return self;
}

- (UILabel *)lblHint{
    if (!_lblHint) {
       _lblHint = [[UILabel alloc] initWithFrame:CGRectZero];
        _lblHint.font = Font15Medium;
        _lblHint.textColor = COLOR_Gary_66;
    }
    return _lblHint;
}

- (UILabel *)lblContent{
    if (!_lblContent) {
        _lblContent = [[UILabel alloc] initWithFrame:CGRectZero];
        _lblContent.font = Font15;
        _lblContent.textColor = COLOR_BlackText_33;
    }
    return _lblContent;
}
- (UIImageView *)arrow{
    if (!_arrow) {
        _arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down"]];
    }
    return _arrow;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _lblHint.frame = CGRectMake(20, 0, 140, kInfoEditHeiht);
    _lblHint.centerY = self.bounds.size.height/2.0;
//    _lblContent.frame = CGRectMake(SCREEN_WIDTH - 200, 0, 180, kInfoEditHeiht);
    _lblContent.frame = CGRectMake(120, 0, SCREEN_WIDTH - 120 - 20, kInfoEditHeiht);
    _lblContent.centerY = self.bounds.size.height/2.0;
    _seperTopLine.frame = CGRectMake(20, 0, SCREEN_WIDTH-40 , 0.5);
    _seperBottomLine.frame = CGRectMake(20, self.bounds.size.height-0.5, SCREEN_WIDTH-40, 0.5);
    _arrow.frame = CGRectMake(SCREEN_WIDTH - 30, (kInfoEditHeiht - CGRectGetHeight(_arrow.bounds))/2.0,  CGRectGetWidth(_arrow.bounds),  CGRectGetHeight(_arrow.bounds));
}



@end
