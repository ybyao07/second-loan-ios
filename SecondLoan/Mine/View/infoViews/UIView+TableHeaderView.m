//
//  UIView+TableHeaderView.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/5/26.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "UIView+TableHeaderView.h"

@implementation UIView (TableHeaderView)

+ (UIView *)tableSectionHeaderViewImg:(NSString *)imgStr title:(NSString *)titleStr
{
    return  [[self class] tableSectionHeaderViewImg:imgStr title:titleStr Height:44];
}

+ (UIView *)tableSectionHeaderViewImg:(NSString *)imgStr title:(NSString *)titleStr Height:(CGFloat)height
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgStr]];
    imgView.frame = CGRectMake(16, (view.bounds.size.height - imgView.size.height)/2, imgView.size.width, imgView.size.height);
    [view addSubview:imgView];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame) + 10, 0, 200, 30)];
    title.textColor = COLOR_BlackText_33;
    title.font = Font15Bold;
    title.text = titleStr;
    [view addSubview:title];
    title.centerY = view.centerY;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15, height-0.5, SCREEN_WIDTH - 15, 0.5)];
    line.backgroundColor = CCellSeperLineColor;
    [view addSubview:line];
    return view;
}

@end
