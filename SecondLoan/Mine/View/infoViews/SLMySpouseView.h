//
//  SLMySpouseView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "SpouseInfo.h"
NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger, SLMySpouseViewTag) {
    MySpouseViewNameTag,
    MySpouseViewIDTag,
    MySpouseViewPhoneTag,
    MySpouseViewWorkCompanyName,
    MySpouseViewWorkAddressTag,
    MySpouseViewWoryTypeTag,
    MySpouseViewDutyTag,
    MySpouseViewCompanyPhoneTag
};

@protocol SLMySpouseViewDelegate
- (void)chooseMySpouseView:(InfoChooseView *)view;
@end

@interface SLMySpouseView : UIView

@property (strong, nonatomic) InfoTextFieldView *spouseName;   // 配偶姓名
@property (strong, nonatomic) InfoTextFieldView *spouseId;   // 身份证号
@property (strong, nonatomic) InfoTextFieldView *spousePhone;   // 手机号
@property (strong, nonatomic) InfoTextFieldView *spouseWorkCompany;   // 单位
@property (strong, nonatomic) InfoTextFieldView *spouseWorkAddress;   // 地点
@property (strong, nonatomic) InfoChooseView *spouseWorktype;   // 所属行业
@property (strong, nonatomic) InfoChooseView *spouseWorkDuty;  // 工作职位
@property (strong, nonatomic) InfoTextFieldView *spouseCompanyPhone;  // 工作单位电话

@property (nonatomic, weak) id<SLMySpouseViewDelegate> delegate;

@property (strong, nonatomic) SpouseInfo *model;

@end

NS_ASSUME_NONNULL_END
