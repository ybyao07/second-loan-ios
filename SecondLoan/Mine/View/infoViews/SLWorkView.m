//
//  SLWorkView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLWorkView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"


@interface SLWorkView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;


@end
@implementation SLWorkView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.industryView];
        [self addSubview:self.jobViewView];
        [self addSubview:self.companyName];
        [self addSubview:self.positionView];
        [self addSubview:self.annualIncome];
        [self addSubview:self.workHourView];
    }
    return self;
}
- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_work" title:@"工作信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}
- (InfoChooseView *)industryView{
    if (!_industryView) {
        _industryView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"所属行业"];
        _industryView.tag = WorkIndustryTag;
        [_industryView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _industryView;
}

- (InfoChooseView *)jobViewView{
    if (!_jobViewView) {
        _jobViewView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"职业"];
        _jobViewView.tag = WorkJobTag;
        [_jobViewView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _jobViewView;
}
- (InfoTextFieldView *)companyName
{
    if (!_companyName) {
        _companyName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _companyName.lblHint.text = @"单位名称";
        _companyName.textFieldView.placeholder = @"请输入公司名称";
        _companyName.textFieldView.tag = WorkCompanyTag;
//        [_companyName.textFieldView setValue:@"18" forKey:@"limit"];
        _companyName.textFieldView.delegate = self;
//        _companyName.textFieldView.keyboardType = UIKeyboardTypeASCIICapable;
//        _companyName.textFieldView.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    }
    return _companyName;
}

- (InfoChooseView *)positionView{
    if (!_positionView) {
        _positionView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"职务"];
        _positionView.tag = WorkPositionTag;
        [_positionView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _positionView;
}

- (InfoTextFieldView *)annualIncome
{
    if (!_annualIncome) {
        _annualIncome = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _annualIncome.lblHint.text = @"年收入(万元)";
        _annualIncome.textFieldView.placeholder = @"请输入数字，单位a万元";
        _annualIncome.textFieldView.tag = WorkIncomeTag;
        _annualIncome.textFieldView.keyboardType = UIKeyboardTypePhonePad;
        [_annualIncome.textFieldView setValue:@"18" forKey:@"limit"];
        _annualIncome.textFieldView.delegate = self;
    }
    return _annualIncome;
}

- (InfoChooseView *)workHourView{
    if (!_workHourView) {
        _workHourView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"工作时间"];
        _workHourView.tag = WorkHourTag;
        [_workHourView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _workHourView;
}

- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseWorkView:chooseView];
    }
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _industryView.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _jobViewView.frame = CGRectMake(0, CGRectGetMaxY(_industryView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _companyName.frame = CGRectMake(0, CGRectGetMaxY(_jobViewView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _positionView.frame = CGRectMake(0, CGRectGetMaxY(_companyName.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _annualIncome.frame = CGRectMake(0, CGRectGetMaxY(_positionView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workHourView.frame = CGRectMake(0, CGRectGetMaxY(_annualIncome.frame), SCREEN_WIDTH, kInfoEditHeiht);
}
- (void)setModel:(BankInfoObject *)model{
    _model = model;
    if (model) {
        _industryView.lblContent.text = [CommonModelTypeDIc dicWorkType][model.industryType] ;
        // [NSString stringWithFormat:@"%ld", model.industryType]
        _jobViewView.lblContent.text =  [CommonModelTypeDIc dicWorkOccupationType][@(model.occupation)] ;
        _companyName.textFieldView.text = model.companyName;
        _positionView.lblContent.text = [CommonModelTypeDIc dicWorkPostType][@(model.workPost)];
        _annualIncome.textFieldView.text = [NSString stringWithFormat:@"%ld",model.yearSalary];
        _workHourView.lblContent.text = model.workTime;
    }
    if (model == nil) {
        _model = [[BankInfoObject alloc] init];
    }
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case WorkCompanyTag:
        {
            _model.companyName = textField.text;
        }
            break;
        case WorkIncomeTag:
        {
            _model.yearSalary = [textField.text integerValue];
        }
            break;
        default:
            break;
    }
}


@end
