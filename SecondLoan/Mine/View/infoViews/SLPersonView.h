//
//  SLPersonView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankPersonInfo.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SLPersonViewTag) {
    PersonViewMarrigeTag,
    PersonViewEmailTag,
    PersonViewWXTag
};

@protocol SLPersonViewDelegate
- (void)choosePersonView:(InfoChooseView *)view;
@end

@interface SLPersonView : UIView

@property (strong, nonatomic) InfoChooseView *marrigeView;   // 婚姻状况
@property (strong, nonatomic) InfoTextFieldView *email;
@property (strong, nonatomic) InfoTextFieldView *wxNumber;
@property (strong, nonatomic) BankPersonInfo *model;

@property (nonatomic, weak) id<SLPersonViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
