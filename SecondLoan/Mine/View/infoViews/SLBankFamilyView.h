//
//  SLBankFamilyView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankInfoObject.h"

NS_ASSUME_NONNULL_BEGIN


@protocol SLBankFamilyViewDelegate
- (void)chooseBankFamilyView:(InfoChooseView *)view;
@end

typedef NS_ENUM(NSInteger, BankFamilyViewTag) {
    BankFamilyViewPersonNumberTag,
    BankFamilyViewMarTag,
    BankFamilyHaveChildTag,
    BankFamilyViewChildTag,
    BankLoanViewPurposeTag
};

@interface SLBankFamilyView : UIView

@property (strong, nonatomic) InfoChooseView *personNumberView;   // 家庭人数
@property (strong, nonatomic) InfoChooseView *marView;   // 婚姻状况
@property (strong, nonatomic) InfoChooseView *childHaveView;   // 是否有子女
@property (strong, nonatomic) InfoChooseView *childNumberView;   // 子女人数

@property (strong, nonatomic) InfoChooseView *loanPurposeView;   // 贷款用途

@property (nonatomic, weak) id<SLBankFamilyViewDelegate> delegate;
@property (strong, nonatomic) BankInfoObject *model;

@end

NS_ASSUME_NONNULL_END
