//
//  SLMyConnectorView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "ContactInfo.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SLMyConnectorViewTag) {
    MyConnectorViewRelationTag,
    MyConnectorViewNameTag,
    MyConnectorViewPhoneTag,
    MyConnectorViewARelationTag,
    MyConnectorViewANameTag,
    MyConnectorViewAPhoneTag
};


@protocol SLMyConnectorViewDelegate
- (void)chooseMyConnectorView:(InfoChooseView *)view;
@end

@interface SLMyConnectorView : UIView

@property (strong, nonatomic) InfoChooseView *connectRelation;   // 与联系人关系
@property (strong, nonatomic) InfoTextFieldView *connectName;   // 姓名
@property (strong, nonatomic) InfoTextFieldView *connectPhone;   // 电话

@property (strong, nonatomic) InfoChooseView *connectAnontherRelation;   // 与联系人关系
@property (strong, nonatomic) InfoTextFieldView *connecAnonthertName;   // 姓名
@property (strong, nonatomic) InfoTextFieldView *connectAnontherPhone;   // 电话

@property (nonatomic, weak) id<SLMyConnectorViewDelegate> delegate;


- (void)setTwoModel:(NSArray <ContactInfo *> *)models;

@property (strong, nonatomic) ContactInfo *model;
@property (strong, nonatomic) ContactInfo *anotherModel;



@end

NS_ASSUME_NONNULL_END
