//
//  SLAddressView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankInfoObject.h"

NS_ASSUME_NONNULL_BEGIN

@class SLAddressView;
@protocol SLAddressViewDelegate
- (void)chooseAddresTypeView:(InfoChooseView *)view;
@end

@interface SLAddressView : UIView

@property (strong, nonatomic) InfoTextFieldView *address;
@property (strong, nonatomic) InfoTextFieldView *location;
@property (strong, nonatomic) InfoChooseView *addressHouseType;   // 住房类型
@property (strong, nonatomic) InfoTextFieldView *postCode;

@property (nonatomic, strong) BankInfoObject *model;

@property (nonatomic, weak) id<SLAddressViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
