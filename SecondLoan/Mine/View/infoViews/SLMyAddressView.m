//
//  SLMyAddressView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMyAddressView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLMyAddressView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;
@end

@implementation SLMyAddressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.addressHouseType];
        [self addSubview:self.addressArea];
        [self addSubview:self.addressDetail];
        [self addSubview:self.addressTime];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_address" title:@"住址信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoChooseView *)addressHouseType{
    if (!_addressHouseType) {
        _addressHouseType = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"住房类型"];
        _addressHouseType.tag = MyAddressViewTypeTag;
        [_addressHouseType addTapOneTarget:self action:@selector(onClick:)];
    }
    return _addressHouseType;
}

- (InfoTextFieldView *)addressArea{
    if (!_addressArea) {
        _addressArea = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _addressArea.lblHint.text = @"所在地区";
        _addressArea.textFieldView.placeholder = @"请输入所在地区";
        _addressArea.textFieldView.tag = MyAddressAreaTag;
        _addressArea.textFieldView.delegate = self;
    }
    return _addressArea;
}

- (InfoTextFieldView *)addressDetail
{
    if (!_addressDetail) {
        _addressDetail = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _addressDetail.lblHint.text = @"详细地址";
        _addressDetail.textFieldView.placeholder = @"请输入详细地址";
        _addressDetail.textFieldView.tag = MyAddressDetailTag;
        _addressDetail.textFieldView.delegate = self;
    }
    return _addressDetail;
}

- (InfoChooseView *)addressTime{
    if (!_addressTime) {
        _addressTime = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"入住年份"];
        _addressTime.tag = MyAddressTimeTag;
        [_addressTime addTapOneTarget:self action:@selector(onClick:)];
    }
    return _addressTime;
}


- (void)setModel:(BankPersonAddressInfo *)model
{
    _model = model;
    if (model) {
        _addressHouseType.lblContent.text = [CommonModelTypeDIc dicAddressType][@(model.addressType)] ;
        _addressArea.textFieldView.text = model.city;
        _addressDetail.textFieldView.text = model.address;
        _addressTime.lblContent.text = model.entryTime;
    }
    if (model == nil) {
        _model = [[BankPersonAddressInfo alloc] init];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _addressHouseType.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _addressArea.frame = CGRectMake(0, CGRectGetMaxY(_addressHouseType.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _addressDetail.frame = CGRectMake(0, CGRectGetMaxY(_addressArea.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _addressTime.frame = CGRectMake(0, CGRectGetMaxY(_addressDetail.frame), SCREEN_WIDTH, kInfoEditHeiht);
}
- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseMyAddressView:chooseView];
    }
}
#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case MyAddressDetailTag:
        {
            _model.address = textField.text;
        }
            break;
        case MyAddressAreaTag:
        {
            _model.city = textField.text;
        }
            break;
//        case MyAddressTimeTag:
//        {
//            _model.entryTime = textField.text;
//        }
//            break;
        default:
            break;
    }
}

@end
