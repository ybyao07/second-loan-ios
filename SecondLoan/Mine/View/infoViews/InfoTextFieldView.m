//
//  InfoTextFieldView.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/7/14.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "InfoTextFieldView.h"


@implementation InfoTextFieldView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _lblHint = [[UILabel alloc] initWithFrame:CGRectZero];
        _lblHint.font = Font15Medium;
        _lblHint.textColor = COLOR_Gary_66;
        [self addSubview:_lblHint];
        _textFieldView = [[UITextField alloc] initWithFrame:CGRectZero];
        _textFieldView.textAlignment = NSTextAlignmentRight;
        _textFieldView.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textFieldView.font = Font15;
        _textFieldView.textColor = COLOR_BlackText_33;
        [self addSubview:_textFieldView];
        _seperTopLine = [UIView new];
        _seperTopLine.backgroundColor = CCellSeperLineColor;
        _seperTopLine.hidden = YES;
        _seperBottomLine = [UIView new];
        _seperBottomLine.backgroundColor = CCellSeperLineColor;
        [self addSubview:_seperTopLine];
        [self addSubview:_seperBottomLine];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _lblHint.frame = CGRectMake(20, 0, 140, kInfoEditHeiht);
    _lblHint.centerY = self.bounds.size.height/2.0;
//    _textFieldView.frame = CGRectMake(SCREEN_WIDTH - 200, 0, 180, kInfoEditHeiht);
    _textFieldView.frame = CGRectMake(120, 0, SCREEN_WIDTH - 120 - 20, kInfoEditHeiht);
    _textFieldView.textAlignment = NSTextAlignmentLeft;
    _textFieldView.centerY = self.bounds.size.height/2.0;
    _seperTopLine.frame = CGRectMake(20, 0, SCREEN_WIDTH-40 , 0.5);
    _seperBottomLine.frame = CGRectMake(20, self.bounds.size.height-0.5,SCREEN_WIDTH-40, 0.5);
    [_textFieldView setValue:COLOR_Gary_99 forKeyPath:@"_placeholderLabel.textColor"];
    [_textFieldView setValue:Font13 forKeyPath:@"_placeholderLabel.font"];
}

@end
