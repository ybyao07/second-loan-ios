//
//  UIView+TableHeaderView.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/5/26.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TableHeaderView)


+ (UIView *)tableSectionHeaderViewImg:(NSString *)imgStr title:(NSString *)titleStr;

+ (UIView *)tableSectionHeaderViewImg:(NSString *)imgStr title:(NSString *)titleStr Height:(CGFloat)height;

@end
