//
//  SLMyConnectorView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMyConnectorView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLMyConnectorView()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *titleView;

@end
@implementation SLMyConnectorView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.connectRelation];
        [self addSubview:self.connectName];
        [self addSubview:self.connectPhone];
        [self addSubview:self.connectAnontherRelation];
        [self addSubview:self.connecAnonthertName];
        [self addSubview:self.connectAnontherPhone];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_contect" title:@"联系人信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoChooseView *)connectRelation{
    if (!_connectRelation) {
        _connectRelation = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"联系人关系"];
        _connectRelation.tag = MyConnectorViewRelationTag;
        [_connectRelation addTapOneTarget:self action:@selector(onClick:)];
    }
    return _connectRelation;
}
- (InfoTextFieldView *)connectName
{
    if (!_connectName) {
        _connectName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _connectName.lblHint.text = @"姓名";
        _connectName.textFieldView.placeholder = @"请输入联系人姓名";
        _connectName.textFieldView.tag = MyConnectorViewNameTag;
//        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _connectName.textFieldView.delegate = self;
    }
    return _connectName;
}
- (InfoTextFieldView *)connectPhone
{
    if (!_connectPhone) {
        _connectPhone = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _connectPhone.lblHint.text = @"手机号";
        _connectPhone.textFieldView.placeholder = @"请输入联系人手机号";
        _connectPhone.textFieldView.tag = MyConnectorViewPhoneTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _connectPhone.textFieldView.delegate = self;
    }
    return _connectPhone;
}

- (InfoChooseView *)connectAnontherRelation{
    if (!_connectAnontherRelation) {
        _connectAnontherRelation = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"其它联系人"];
        _connectAnontherRelation.tag = MyConnectorViewARelationTag;
        [_connectAnontherRelation addTapOneTarget:self action:@selector(onClick:)];
    }
    return _connectAnontherRelation;
}
- (InfoTextFieldView *)connecAnonthertName
{
    if (!_connecAnonthertName) {
        _connecAnonthertName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _connecAnonthertName.lblHint.text = @"姓名";
        _connecAnonthertName.textFieldView.placeholder = @"请输入联系人姓名";
        _connecAnonthertName.textFieldView.tag = MyConnectorViewANameTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _connecAnonthertName.textFieldView.delegate = self;
    }
    return _connecAnonthertName;
}
- (InfoTextFieldView *)connectAnontherPhone
{
    if (!_connectAnontherPhone) {
        _connectAnontherPhone = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _connectAnontherPhone.lblHint.text = @"手机号";
        _connectAnontherPhone.textFieldView.placeholder = @"请输入联系人手机号";
        _connectAnontherPhone.textFieldView.tag = MyConnectorViewAPhoneTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _connectAnontherPhone.textFieldView.delegate = self;
    }
    return _connectAnontherPhone;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _connectRelation.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _connectName.frame = CGRectMake(0, CGRectGetMaxY(_connectRelation.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _connectPhone.frame = CGRectMake(0, CGRectGetMaxY(_connectName.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _connectAnontherRelation.frame = CGRectMake(0, CGRectGetMaxY(_connectPhone.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _connecAnonthertName.frame = CGRectMake(0, CGRectGetMaxY(_connectAnontherRelation.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _connectAnontherPhone.frame = CGRectMake(0, CGRectGetMaxY(_connecAnonthertName.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)setTwoModel:(NSArray <ContactInfo *> *)models{
    if (models) {
        if ([models count] > 1) {
            self.model = models[0];
            self.anotherModel = models[1];
        }else if([models count] > 0){
            self.model = models[0];
            _anotherModel = [[ContactInfo alloc] init];
        }else{
            _model = [[ContactInfo alloc] init];
            _anotherModel = [[ContactInfo alloc] init];
        }
    }else{
        _model = [[ContactInfo alloc] init];
        _anotherModel = [[ContactInfo alloc] init];
    }
}
- (void)setModel:(ContactInfo *)model
{
    _model = model;
    if (model) {
        _connectRelation.lblContent.text = [CommonModelTypeDIc dicRelationType][@(model.contactsRelation)] ;
        _connectName.textFieldView.text = model.contactsName;
        _connectPhone.textFieldView.text = model.contactsPhone;
    }
}

- (void)setAnotherModel:(ContactInfo *)anotherModel{
    _anotherModel = anotherModel;
    if (anotherModel) {
        _connectAnontherRelation.lblContent.text = [CommonModelTypeDIc dicRelationType][@(anotherModel.contactsRelation)];
        _connecAnonthertName.textFieldView.text = anotherModel.contactsName;
        _connectAnontherPhone.textFieldView.text = anotherModel.contactsPhone;
    }
}


- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseMyConnectorView:chooseView];
    }
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case MyConnectorViewNameTag:
        {
            _model.contactsName = textField.text;
        }
            break;
        case MyConnectorViewPhoneTag:
        {
            _model.contactsPhone = textField.text;
        }
            break;
        case  MyConnectorViewANameTag:
        {
            _anotherModel.contactsName = textField.text;
        }
            break;
        case MyConnectorViewAPhoneTag:
        {
            _anotherModel.contactsPhone = textField.text;
        }
            break;
        default:
            break;
    }
}

@end
