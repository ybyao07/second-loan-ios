//
//  SLPersonView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLPersonView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLPersonView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;
@end
@implementation SLPersonView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.marrigeView];
        [self addSubview:self.email];
        [self addSubview:self.wxNumber];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_person" title:@"个人信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoChooseView *)marrigeView{
    if (!_marrigeView) {
        _marrigeView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"婚姻状况"];
        _marrigeView.tag = PersonViewMarrigeTag;
        [_marrigeView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _marrigeView;
}

- (InfoTextFieldView *)email
{
    if (!_email) {
        _email = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _email.lblHint.text = @"个人邮箱";
        _email.textFieldView.placeholder = @"请输入您的邮箱地址";
        _email.textFieldView.tag = PersonViewEmailTag;
        _email.textFieldView.delegate = self;
    }
    return _email;
}

- (InfoTextFieldView *)wxNumber
{
    if (!_wxNumber) {
        _wxNumber = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _wxNumber.lblHint.text = @"微信号";
        _wxNumber.textFieldView.placeholder = @"请输入您的微信号";
        _wxNumber.textFieldView.tag = PersonViewWXTag;
        [_wxNumber.textFieldView setValue:@"20" forKey:@"limit"];
        _wxNumber.textFieldView.delegate = self;
        _wxNumber.textFieldView.keyboardType = UIKeyboardTypeASCIICapable;
    }
    return _wxNumber;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _marrigeView.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _email.frame = CGRectMake(0, CGRectGetMaxY(_marrigeView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _wxNumber.frame = CGRectMake(0, CGRectGetMaxY(_email.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)setModel:(BankPersonInfo *)model
{
    _model = model;
    if (model) {
        _marrigeView.lblContent.text = [CommonModelTypeDIc dicMarState][@(_model.marStatus)];
        _email.textFieldView.text = _model.email;
        _wxNumber.textFieldView.text = _model.wetNum;
    }
    if (model == nil) {
        _model = [[BankPersonInfo alloc] init];
    }
}
- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate choosePersonView:chooseView];
    }
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case PersonViewEmailTag:
        {
            _model.email = textField.text;
        }
            break;
        case PersonViewWXTag:
        {
            _model.wetNum = textField.text;
        }
            break;
        default:
            break;
    }
}

@end
