//
//  SLBankEduView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLBankEduView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLBankEduView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;
@end

@implementation SLBankEduView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.highEduView];
        [self addSubview:self.schoolNameView];
        [self addSubview:self.graduateTimeView];
    }
    return self;
}
- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_edu" title:@"学历信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}
- (InfoChooseView *)highEduView{
    if (!_highEduView) {
        _highEduView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"学历"];
        _highEduView.tag = BankEduViewHighTag;
        [_highEduView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _highEduView;
}
- (InfoTextFieldView *)schoolNameView{
    if (!_schoolNameView) {
        _schoolNameView = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _schoolNameView.lblHint.text = @"学校";
        _schoolNameView.textFieldView.placeholder  = @"请输入学校名称";
        _schoolNameView.textFieldView.tag = BankEduViewNameTag;
        _schoolNameView.tag = BankEduViewNameTag;
        _schoolNameView.textFieldView.delegate = self;
    }
    return _schoolNameView;
}
- (InfoChooseView *)graduateTimeView{
    if (!_graduateTimeView) {
        _graduateTimeView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"毕业时间"];
        _graduateTimeView.tag = BankEduViewTimeTag;
        [_graduateTimeView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _graduateTimeView;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _highEduView.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _schoolNameView.frame = CGRectMake(0, CGRectGetMaxY(_highEduView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _graduateTimeView.frame = CGRectMake(0, CGRectGetMaxY(_schoolNameView.frame), SCREEN_WIDTH, kInfoEditHeiht);
}
- (void)onClick:(UITapGestureRecognizer *)recoginer{
    InfoChooseView *chooseView = (InfoChooseView *)recoginer.view;
    if (self.delegate) {
        [self.delegate chooseBankEduView:chooseView];
    }
}

- (void)setModel:(BankInfoObject *)model{
    _model = model;
    if (model) {
        _highEduView.lblContent.text = [CommonModelTypeDIc dicEdu][@(model.highEdu)] ;
        _schoolNameView.textFieldView.text = model.schoolName;
        _graduateTimeView.lblContent.text = model.graduationTime;
    }
    if (model == nil) {
        _model = [[BankInfoObject alloc] init];
    }
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case BankEduViewNameTag:
        {
            _model.schoolName = textField.text;
        }
            break;
        default:
            break;
    }
}

@end
