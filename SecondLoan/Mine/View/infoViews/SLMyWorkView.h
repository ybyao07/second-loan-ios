//
//  SLMyWorkView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankPersonWorkInfo.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SLMyWorkViewTag) {
    MyWorkViewNameTag,
    MyWorkViewTypeTag,
    MyWorkViewAddressTag,
    MyWorkViewPhoneTag,
    MyWorkViewScaleTag,
    MyWorkViewDutyTag,
    MyWorkViewSaleryTag
};


@protocol SLMyWorkViewDelegate
- (void)chooseMyWorkView:(InfoChooseView *)view;
@end

@interface SLMyWorkView : UIView

@property (strong, nonatomic) InfoTextFieldView *workplaceName;   // 工作单位名称
@property (strong, nonatomic) InfoChooseView *workType;   // 行业类别
@property (strong, nonatomic) InfoTextFieldView *workAddress;   // 详细地址
@property (strong, nonatomic) InfoTextFieldView *workPhone;   // 单位电话
@property (strong, nonatomic) InfoChooseView *workScale;   // 单位规模
@property (strong, nonatomic) InfoChooseView *workDuty;   // 工作职务
@property (strong, nonatomic) InfoChooseView *workSalery;  // 月薪范围

@property (nonatomic, weak) id<SLMyWorkViewDelegate> delegate;

@property (strong, nonatomic) BankPersonWorkInfo *model;

@end

NS_ASSUME_NONNULL_END
