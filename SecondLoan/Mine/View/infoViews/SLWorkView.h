//
//  SLWorkView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonInfoObject.h"
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankInfoObject.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, WorkViewTag) {
    WorkIndustryTag,
    WorkJobTag,
    WorkCompanyTag,
    WorkPositionTag,
    WorkIncomeTag,
    WorkHourTag
};

@class SLWorkView;
@protocol SLWorkViewDelegate
- (void)chooseWorkView:(InfoChooseView *)view;
@end

@interface SLWorkView : UIView
@property (strong, nonatomic) InfoChooseView *industryView;   // 行业
@property (strong, nonatomic) InfoChooseView *jobViewView;  //职业
@property (strong, nonatomic) InfoTextFieldView *companyName;
@property (strong, nonatomic) InfoChooseView *positionView; // 职务
@property (strong, nonatomic) InfoTextFieldView *annualIncome;  //年收入
@property (strong, nonatomic) InfoChooseView *workHourView; // 工作时间

@property (nonatomic, weak) id<SLWorkViewDelegate> delegate;

@property (nonatomic, strong) BankInfoObject *model;

@end

NS_ASSUME_NONNULL_END
