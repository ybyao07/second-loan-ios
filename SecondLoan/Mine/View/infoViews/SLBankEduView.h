//
//  SLBankEduView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "BankInfoObject.h"
NS_ASSUME_NONNULL_BEGIN

@protocol SLBankEduViewDelegate
- (void)chooseBankEduView:(InfoChooseView *)view;
@end

typedef NS_ENUM(NSInteger, BankEduViewTag) {
    BankEduViewHighTag,
    BankEduViewNameTag,
    BankEduViewTimeTag
};

@interface SLBankEduView : UIView

@property (strong, nonatomic) InfoChooseView *highEduView;   // 最高学历
@property (strong, nonatomic) InfoTextFieldView *schoolNameView;   // 学校名称
@property (strong, nonatomic) InfoChooseView *graduateTimeView;   // 毕业时间
@property (nonatomic, weak) id<SLBankEduViewDelegate> delegate;


@property (nonatomic, strong) BankInfoObject *model;
@end

NS_ASSUME_NONNULL_END
