//
//  SLBankFamilyView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLBankFamilyView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLBankFamilyView()
@property (nonatomic, strong) UIView *titleView;

@end
@implementation SLBankFamilyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.personNumberView];
        [self addSubview:self.marView];
        [self addSubview:self.childHaveView];
        [self addSubview:self.childNumberView];
        [self addSubview:self.loanPurposeView];
    }
    return self;
}
- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_family" title:@"家庭信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}
- (InfoChooseView *)personNumberView{
    if (!_personNumberView) {
        _personNumberView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"家庭人数"];
        _personNumberView.tag = BankFamilyViewPersonNumberTag;
        [_personNumberView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _personNumberView;
}
- (InfoChooseView *)marView{
    if (!_marView) {
        _marView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"婚姻状况"];
        _marView.tag = BankFamilyViewMarTag;
        [_marView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _marView;
}
- (InfoChooseView *)childHaveView{
    if (!_childHaveView) {
        _childHaveView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"是否有子女"];
        _childHaveView.tag = BankFamilyHaveChildTag;
        [_childHaveView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _childHaveView;
}
- (InfoChooseView *)childNumberView{
    if (!_childNumberView) {
        _childNumberView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"子女人数"];
        _childNumberView.tag = BankFamilyViewChildTag;
        [_childNumberView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _childNumberView;
}
- (InfoChooseView *)loanPurposeView{
    if (!_loanPurposeView) {
        _loanPurposeView = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"贷款用途"];
        _loanPurposeView.tag = BankLoanViewPurposeTag;
        [_loanPurposeView addTapOneTarget:self action:@selector(onClick:)];
    }
    return _loanPurposeView;
}

- (void)setModel:(BankInfoObject *)model{
    _model = model;
    if (model) {
        _personNumberView.lblContent.text = [NSString stringWithFormat:@"%ld", model.personNum];
        _marView.lblContent.text = [CommonModelTypeDIc dicMarState][@(model.marStatus)];
        _childNumberView.lblContent.text = [NSString stringWithFormat:@"%ld", model.childNum];
        _childHaveView.lblContent.text = [CommonModelTypeDIc dicHaveChild][model.haveChild];
        _loanPurposeView.lblContent.text = [CommonModelTypeDIc dicLoanPurpos][model.loanPurpose];
    }
    if (model == nil) {
        _model = [[BankInfoObject alloc] init];
    }
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _personNumberView.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _marView.frame = CGRectMake(0, CGRectGetMaxY(_personNumberView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _childHaveView.frame = CGRectMake(0, CGRectGetMaxY(_marView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _childNumberView.frame = CGRectMake(0, CGRectGetMaxY(_childHaveView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _loanPurposeView.frame = CGRectMake(0, CGRectGetMaxY(_childNumberView.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)onClick:(UITapGestureRecognizer *)recoginer{
    InfoChooseView *chooseView = (InfoChooseView *)recoginer.view;
    if (self.delegate) {
        [self.delegate chooseBankFamilyView:chooseView];
    }
}
@end
