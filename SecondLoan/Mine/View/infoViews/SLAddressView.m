//
//  SLAddressView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLAddressView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

typedef NS_ENUM(NSInteger, AddressViewTag) {
    AddressTag,
    LocationTag,
    PostCodeTag,
    MyAddressViewTypeTag
};

@interface SLAddressView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;

@end

@implementation SLAddressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.address];
        [self addSubview:self.location];
        [self addSubview:self.addressHouseType];
        [self addSubview:self.postCode];
    }
    return self;
}
- (void)setModel:(BankInfoObject *)model
{
    _model = model;
    if (model.familyAddress.length) {
        _address.textFieldView.text = model.familyAddress;
    }
    if (model.address.length) {
        _location.textFieldView.text = model.address;
    }
    if (model.postCode.length) {
        _postCode.textFieldView.text = model.postCode;
    }
    _addressHouseType.lblContent.text = [CommonModelTypeDIc dicAddressType][@(model.reside)] ;
    if (model == nil) {
        _model = [[BankInfoObject alloc] init];
    }
}
- (InfoChooseView *)addressHouseType{
    if (!_addressHouseType) {
        _addressHouseType = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"住房类型"];
        _addressHouseType.tag = MyAddressViewTypeTag;
        [_addressHouseType addTapOneTarget:self action:@selector(onClick:)];
    }
    return _addressHouseType;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _address.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _location.frame = CGRectMake(0, CGRectGetMaxY(_address.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _addressHouseType.frame = CGRectMake(0, CGRectGetMaxY(_location.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _postCode.frame = CGRectMake(0, CGRectGetMaxY(_addressHouseType.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseAddresTypeView:chooseView];
    }
}
- (InfoTextFieldView *)address
{
    if (!_address) {
        _address = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _address.lblHint.text = @"家庭住址";
        _address.textFieldView.placeholder = @"请输入详细地址";
        _address.textFieldView.tag = AddressTag;
//        [_address.textFieldView setValue:@"20" forKey:@"limit"];
        _address.textFieldView.delegate = self;
    }
    return _address;
}
- (InfoTextFieldView *)location
{
    if (!_location) {
        _location = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _location.lblHint.text = @"居住地址";
        _location.textFieldView.placeholder = @"请输入详细地址";
        _location.textFieldView.tag = LocationTag;
        _location.textFieldView.delegate = self;
    }
    return _location;
}
- (InfoTextFieldView *)postCode
{
    if (!_postCode) {
        _postCode = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _postCode.lblHint.text = @"城市邮编";
        _postCode.textFieldView.placeholder = @"请输入城市邮编";
        _postCode.textFieldView.tag = PostCodeTag;
        _postCode.textFieldView.delegate = self;
    }
    return _postCode;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_address" title:@"居住信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case AddressTag:
        {
            _model.familyAddress = textField.text;
        }
            break;
        case LocationTag:
        {
            _model.address = textField.text;
        }
            break;
        case PostCodeTag:
        {
            _model.postCode = textField.text;
        }
            break;
        default:
            break;
    }
}

@end
