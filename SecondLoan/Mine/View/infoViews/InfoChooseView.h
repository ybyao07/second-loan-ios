//
//  InfoWorkView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoChooseView : UIView

@property (strong, nonatomic) UILabel *lblHint;
@property (strong, nonatomic) UILabel *lblContent;
@property (nonatomic, strong) UIView *seperTopLine;
@property (nonatomic, strong) UIView *seperBottomLine;


- (instancetype)initWithFrame:(CGRect)frame withHintTitle:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
