//
//  SLEduView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLEduView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"
@interface SLEduView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;

@end
@implementation SLEduView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.eduHighest];
        [self addSubview:self.eduLocation];
        [self addSubview:self.eduSchoolName];
        [self addSubview:self.eduGraduateYear];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_edu" title:@"学历信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoChooseView *)eduHighest{
    if (!_eduHighest) {
        _eduHighest = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"最高学历"];
        _eduHighest.tag = EduViewHighestTag;
        [_eduHighest addTapOneTarget:self action:@selector(onClick:)];
    }
    return _eduHighest;
}
- (InfoTextFieldView *)eduLocation{
    if (!_eduLocation) {
        _eduLocation = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _eduLocation.lblHint.text = @"学校所在地";
        _eduLocation.textFieldView.placeholder  = @"请输入学校所在地";
        _eduLocation.textFieldView.tag = EduViewLocationTag;
        _eduLocation.textFieldView.delegate = self;
    }
    return _eduLocation;
}
- (InfoTextFieldView *)eduSchoolName{
    if (!_eduSchoolName) {
        _eduSchoolName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _eduSchoolName.lblHint.text = @"学校名称";
        _eduSchoolName.textFieldView.placeholder  = @"请输入学校名称";
        _eduSchoolName.textFieldView.tag = EduViewNameTag;
        _eduSchoolName.textFieldView.delegate = self;
    }
    return _eduSchoolName;
}
- (InfoChooseView *)eduGraduateYear{
    if (!_eduGraduateYear) {
        _eduGraduateYear = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"毕业年份"];
        _eduGraduateYear.tag = EduViewYearTag;
        [_eduGraduateYear addTapOneTarget:self action:@selector(onClick:)];
    }
    return _eduGraduateYear;
}

- (void)setModel:(EduInfoModel *)model{
    _model = model;
    if (model) {
        _eduHighest.lblContent.text = [CommonModelTypeDIc dicEdu][@(model.highEdu)] ;
        _eduSchoolName.textFieldView.text = model.schoolName;
        _eduLocation.textFieldView.text = model.schoolAddr;
        _eduGraduateYear.lblContent.text = model.graduationTime;
    }
    if (model == nil) {
        _model = [[EduInfoModel alloc] init];
    }
}
- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseEduView:chooseView];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _eduHighest.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _eduLocation.frame = CGRectMake(0, CGRectGetMaxY(_eduHighest.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _eduSchoolName.frame = CGRectMake(0, CGRectGetMaxY(_eduLocation.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _eduGraduateYear.frame = CGRectMake(0, CGRectGetMaxY(_eduSchoolName.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case EduViewLocationTag:
        {
            _model.schoolAddr = textField.text;
        }
            break;
        case EduViewNameTag:
        {
            _model.schoolName = textField.text;
        }
            break;
        default:
            break;
    }
}

@end
