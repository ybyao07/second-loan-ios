//
//  SLMyWorkView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMyWorkView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLMyWorkView()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *titleView;

@end

@implementation SLMyWorkView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.workplaceName];
        [self addSubview:self.workType];
        [self addSubview:self.workAddress];
        [self addSubview:self.workPhone];
        [self addSubview:self.workScale];
        [self addSubview:self.workDuty];
        [self addSubview:self.workSalery];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_work" title:@"工作信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoTextFieldView *)workplaceName
{
    if (!_workplaceName) {
        _workplaceName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _workplaceName.lblHint.text = @"单位名称";
        _workplaceName.textFieldView.placeholder = @"请输入单位名称";
        _workplaceName.textFieldView.tag = MyWorkViewNameTag;
//        [_workplaceName.textFieldView setValue:@"20" forKey:@"limit"];
        _workplaceName.textFieldView.delegate = self;
    }
    return _workplaceName;
}

- (InfoChooseView *)workType{
    if (!_workType) {
        _workType = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"所属行业"];
        _workType.tag = MyWorkViewTypeTag;
        [_workType addTapOneTarget:self action:@selector(onClick:)];
    }
    return _workType;
}

- (InfoTextFieldView *)workAddress
{
    if (!_workAddress) {
        _workAddress = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _workAddress.lblHint.text = @"详细地址";
        _workAddress.textFieldView.placeholder = @"请输入详细地址";
        _workAddress.textFieldView.tag = MyWorkViewAddressTag;
//        [_workAddress.textFieldView setValue:@"20" forKey:@"limit"];
        _workAddress.textFieldView.delegate = self;
    }
    return _workAddress;
}
- (InfoTextFieldView *)workPhone
{
    if (!_workPhone) {
        _workPhone = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _workPhone.lblHint.text = @"单位电话";
        _workPhone.textFieldView.placeholder = @"请输入单位电话，格式010-88888888";
        _workPhone.textFieldView.tag = MyWorkViewPhoneTag;
        //        [_workAddress.textFieldView setValue:@"20" forKey:@"limit"];
        _workPhone.textFieldView.delegate = self;
    }
    return _workPhone;
}

- (InfoChooseView *)workScale{
    if (!_workScale) {
        _workScale = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"单位规模"];
        _workScale.tag = MyWorkViewScaleTag;
        [_workScale addTapOneTarget:self action:@selector(onClick:)];
    }
    return _workScale;
}

- (InfoChooseView *)workDuty{
    if (!_workDuty) {
        _workDuty = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"职务"];
        _workDuty.tag = MyWorkViewDutyTag;
        [_workDuty addTapOneTarget:self action:@selector(onClick:)];
    }
    return _workDuty;
}

- (InfoChooseView *)workSalery{
    if (!_workSalery) {
        _workSalery = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"月薪范围"];
        _workSalery.tag = MyWorkViewSaleryTag;
        [_workSalery addTapOneTarget:self action:@selector(onClick:)];
    }
    return _workSalery;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _workplaceName.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workType.frame = CGRectMake(0, CGRectGetMaxY(_workplaceName.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workAddress.frame = CGRectMake(0, CGRectGetMaxY(_workType.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workPhone.frame = CGRectMake(0, CGRectGetMaxY(_workAddress.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workScale.frame = CGRectMake(0, CGRectGetMaxY(_workPhone.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workDuty.frame = CGRectMake(0, CGRectGetMaxY(_workScale.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _workSalery.frame = CGRectMake(0, CGRectGetMaxY(_workDuty.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)setModel:(BankPersonWorkInfo *)model{
    _model = model;
    if (model) {
        _workplaceName.textFieldView.text = model.companyName;
        _workType.lblContent.text =  [CommonModelTypeDIc dicWorkType][model.industryType];
        _workAddress.textFieldView.text = model.companyAddress;
        _workPhone.textFieldView.text = model.companyTel;
        _workScale.lblContent.text = [CommonModelTypeDIc dicCompanyScale][@(model.companyScale)];
        _workDuty.lblContent.text = [CommonModelTypeDIc dicWorkPostType][@(model.workPost)];
        _workSalery.lblContent.text = [CommonModelTypeDIc dicSalaryType][@(model.salary)];
    }
    if (model == nil) {
        _model = [[BankPersonWorkInfo alloc] init];
    }
}
- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseMyWorkView:chooseView];
    }
}
#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case MyWorkViewNameTag:
        {
            _model.companyName = textField.text;
        }
            break;
        case MyWorkViewAddressTag:
        {
            _model.companyAddress = textField.text;
        }
            break;
        case MyWorkViewPhoneTag:
        {
            _model.companyTel = textField.text;
        }
            break;
//        case MyWorkViewScaleTag:
//        {
//            _model.companyScale = textField.text;
//        }
//            break;
//        case MyWorkViewDutyTag:
//        {
//            _model.workPost = textField.text;
//        }
//            break;
        default:
            break;
    }
    
}
@end
