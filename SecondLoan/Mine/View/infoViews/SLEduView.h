//
//  SLEduView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoChooseView.h"
#import "InfoTextFieldView.h"
#import "EduInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SLEduViewTag) {
    EduViewHighestTag,
    EduViewLocationTag,
    EduViewNameTag,
    EduViewYearTag
};

@protocol SLEduViewDelegate
- (void)chooseEduView:(InfoChooseView *)view;
@end

@interface SLEduView : UIView

@property (strong, nonatomic) InfoChooseView *eduHighest;   // 最高学历
@property (strong, nonatomic) InfoTextFieldView *eduLocation;   // 学校所在地
@property (strong, nonatomic) InfoTextFieldView *eduSchoolName;   // 学校名称
@property (strong, nonatomic) InfoChooseView *eduGraduateYear;   // 毕业时间

@property (nonatomic, weak) id<SLEduViewDelegate> delegate;

@property (strong, nonatomic) EduInfoModel *model;





@end

NS_ASSUME_NONNULL_END
