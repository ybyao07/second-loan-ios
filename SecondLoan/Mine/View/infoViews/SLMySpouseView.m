//
//  SLMySpouseView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMySpouseView.h"
#import "UIView+TableHeaderView.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"

@interface SLMySpouseView()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleView;

@end

@implementation SLMySpouseView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_White;
        [self addSubview:self.titleView];
        [self addSubview:self.spouseName];
        [self addSubview:self.spouseId];
        [self addSubview:self.spousePhone];
        [self addSubview:self.spouseWorkCompany];
        [self addSubview:self.spouseWorkAddress];
        [self addSubview:self.spouseWorktype];
        [self addSubview:self.spouseWorkDuty];
        [self addSubview:self.spouseCompanyPhone];
    }
    return self;
}

- (UIView *)titleView
{
    if (!_titleView) {
        _titleView = [UIView tableSectionHeaderViewImg:@"icon_spouse" title:@"配偶信息" Height:kInfoEditHeiht];
    }
    return _titleView;
}

- (InfoTextFieldView *)spouseName
{
    if (!_spouseName) {
        _spouseName = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spouseName.lblHint.text = @"配偶姓名";
        _spouseName.textFieldView.placeholder = @"请输入配偶姓名";
        _spouseName.textFieldView.tag = MySpouseViewNameTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _spouseName.textFieldView.delegate = self;
    }
    return _spouseName;
}
- (InfoTextFieldView *)spouseId
{
    if (!_spouseId) {
        _spouseId = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spouseId.lblHint.text = @"身份证号";
        _spouseId.textFieldView.placeholder = @"请输入配偶身份证号";
        _spouseId.textFieldView.tag = MySpouseViewIDTag;
        [_spouseId.textFieldView setValue:@"18" forKey:@"limit"];
        _spouseId.textFieldView.delegate = self;
    }
    return _spouseId;
}
- (InfoTextFieldView *)spousePhone
{
    if (!_spousePhone) {
        _spousePhone = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spousePhone.lblHint.text = @"手机号";
        _spousePhone.textFieldView.placeholder = @"请输入配偶手机号";
        _spousePhone.textFieldView.tag = MySpouseViewPhoneTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _spousePhone.textFieldView.delegate = self;
    }
    return _spousePhone;
}
- (InfoTextFieldView *)spouseWorkCompany
{
    if (!_spouseWorkCompany) {
        _spouseWorkCompany = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spouseWorkCompany.lblHint.text = @"工作单位";
        _spouseWorkCompany.textFieldView.placeholder = @"请输入配偶工作单位";
        _spouseWorkCompany.textFieldView.tag = MySpouseViewWorkCompanyName;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _spouseWorkCompany.textFieldView.delegate = self;
    }
    return _spouseWorkCompany;
}
- (InfoTextFieldView *)spouseWorkAddress
{
    if (!_spouseWorkAddress) {
        _spouseWorkAddress = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spouseWorkAddress.lblHint.text = @"工作地点";
        _spouseWorkAddress.textFieldView.placeholder = @"请输入配偶工作地点";
        _spouseWorkAddress.textFieldView.tag = MySpouseViewWorkAddressTag;
        //        [_connectName.textFieldView setValue:@"20" forKey:@"limit"];
        _spouseWorkAddress.textFieldView.delegate = self;
    }
    return _spouseWorkAddress;
}
- (InfoChooseView *)spouseWorktype
{
    if (!_spouseWorktype) {
        _spouseWorktype = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"所属行业"];
        _spouseWorktype.tag = MySpouseViewWoryTypeTag;
        [_spouseWorktype addTapOneTarget:self action:@selector(onClick:)];
    }
    return _spouseWorktype;
}
- (InfoChooseView *)spouseWorkDuty
{
    if (!_spouseWorkDuty) {
        _spouseWorkDuty = [[InfoChooseView alloc] initWithFrame:CGRectZero withHintTitle:@"工作职位"];
        _spouseWorkDuty.tag = MySpouseViewDutyTag;
        [_spouseWorkDuty addTapOneTarget:self action:@selector(onClick:)];
    }
    return _spouseWorkDuty;
}
- (InfoTextFieldView *)spouseCompanyPhone
{
    if (!_spouseCompanyPhone) {
        _spouseCompanyPhone = [[InfoTextFieldView alloc] initWithFrame:CGRectZero];
        _spouseCompanyPhone.lblHint.text = @"单位电话";
        _spouseCompanyPhone.textFieldView.placeholder = @"请输入配偶单位电话";
        _spouseCompanyPhone.textFieldView.tag = MySpouseViewCompanyPhoneTag;
        _spouseCompanyPhone.textFieldView.delegate = self;
    }
    return _spouseCompanyPhone;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    _titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht);
    _spouseName.frame = CGRectMake(0, CGRectGetMaxY(_titleView.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseId.frame = CGRectMake(0, CGRectGetMaxY(_spouseName.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spousePhone.frame = CGRectMake(0, CGRectGetMaxY(_spouseId.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseWorkCompany.frame = CGRectMake(0, CGRectGetMaxY(_spousePhone.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseWorkAddress.frame = CGRectMake(0, CGRectGetMaxY(_spouseWorkCompany.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseWorktype.frame = CGRectMake(0, CGRectGetMaxY(_spouseWorkAddress.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseWorkDuty.frame = CGRectMake(0, CGRectGetMaxY(_spouseWorktype.frame), SCREEN_WIDTH, kInfoEditHeiht);
    _spouseCompanyPhone.frame = CGRectMake(0, CGRectGetMaxY(_spouseWorkDuty.frame), SCREEN_WIDTH, kInfoEditHeiht);
}

- (void)setModel:(SpouseInfo *)model
{
    _model = model;
    if (model) {
        _spouseName.textFieldView.text = model.spouseName;
        _spouseId.textFieldView.text = model.spouseCard;
        _spousePhone.textFieldView.text = model.spousePhone;
        _spouseWorkCompany.textFieldView.text = model.spouseCompany;
        _spouseWorkAddress.textFieldView.text = model.spouseWork;
        _spouseWorktype.lblContent.text = [CommonModelTypeDIc dicWorkType][model.industryType] ;
        _spouseWorkDuty.lblContent.text = [CommonModelTypeDIc dicWorkPostType][@(model.workPost)] ;
        _spouseCompanyPhone.textFieldView.text = model.spouseTel;
    }
    if (model == nil) {
        _model = [[SpouseInfo alloc] init];
    }
}
- (void)onClick:(UITapGestureRecognizer *)recognizer{
    InfoChooseView *chooseView = (InfoChooseView *)recognizer.view;
    if (self.delegate) {
        [self.delegate chooseMySpouseView:chooseView];
    }
}
#pragma mark
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case MySpouseViewNameTag:
        {
            _model.spouseName = textField.text;
        }
            break;
        case MySpouseViewIDTag:
        {
            _model.spouseCard = textField.text;
        }
            break;
        case MySpouseViewPhoneTag:
        {
            _model.spousePhone = textField.text;
        }
            break;
        case MySpouseViewWorkCompanyName:
        {
            _model.spouseCompany = textField.text;
        }
            break;
        case MySpouseViewWorkAddressTag:
        {
            _model.spouseWork = textField.text;
        }
            break;
//        case MySpouseViewWoryTypeTag:
//        {
//            _model.spouseIndustry = textField.text;
//        }
//            break;
//        case MySpouseViewDutyTag:
//        {
//            _model.spousePost = textField.text;
//        }
//            break;
        case MySpouseViewCompanyPhoneTag:
        {
            _model.spouseTel = textField.text;
        }
            break;
            
        default:
            break;
    }
}
@end
