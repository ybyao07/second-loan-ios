//
//  SLApplyStatusCell.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLLoanApplyStatusCell.h"
#import "CommanTool.h"
#import "NSString+Tools.h"
@interface SLLoanApplyStatusCell ()
@property (weak, nonatomic) IBOutlet UIImageView *applyStatusImg;

@end

@implementation SLLoanApplyStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [CommanTool setShadowOnView:self];
}

- (void)setApplyStatus:(NSString *)status {
  

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(LoanApplyModel *)model
{
    _model = model;
    _dingdanNumLabel.text = model.title;
    _lblApplyTime.text = model.applyTime;
    _lblApplyMoney.text = [NSString  stringWithFormat:@"%@元",[NSString getMoneyStringWithMoneyNumber:model.loanMoney]];
    _lblApplyLimit.text = [LoanApplyModel getBorrowTime:model.applyTerm];
//    if(model.status == CodeApplyWait){
//        [_imgStatus setImage:[UIImage imageNamed:@"apply_status_wait"]];
//    }else
    if(model.status == CodeApplying){
        [_imgStatus setImage:[UIImage imageNamed:@"apply_status_doing"]];
    }else if(model.status == CodeApplyPassed){
      [_imgStatus setImage:[UIImage imageNamed:@"apply_status_fangkuan"]];
    }else if(model.status == CodeApplyFailed){
      [_imgStatus setImage:[UIImage imageNamed:@"apply_status_failed"]];
    }
}


+ (CGFloat)cellHeight
{
    return 160;
}
@end
