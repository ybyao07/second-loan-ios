//
//  SLSettingTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLSettingTableViewCell : UITableViewCell


- (void)bindTitle: (NSString *)title;

@end

NS_ASSUME_NONNULL_END
