//
//  SLEditPWDTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/29.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLEditPWDTableViewCell.h"

@interface SLEditPWDTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SLEditPWDTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title {
  self.titleLabel.text = title;
}

- (NSString *)getPwd {
  return _pwdTextField.text;
}

- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate {
    self.pwdTextField.delegate = delegate;
}


@end
