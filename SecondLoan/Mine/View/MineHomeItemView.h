//
//  MineHomeItemView.h
//  SuYue
//
//  Created by Win10 on 2018/12/11.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineHomeItemView : UIButton
- (instancetype)initWithNBIcon:(NSString *)icon name:(NSString *)name;//无底线
- (instancetype)initWithIcon:(NSString *)icon name:(NSString *)name;
- (instancetype)initWithIcon:(NSString *)icon name:(NSString *)name showline:(BOOL)showl;


@property (nonatomic,weak) UIImageView * iconView;
@property (nonatomic,weak) UILabel * mainLable;
@property (nonatomic,weak) UIImageView * arrow;
@property (nonatomic,weak) UIView * bottomLine;
@end

NS_ASSUME_NONNULL_END
