//
//  MyLoanModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/4.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyLoanModel : NSObject

@property (assign, nonatomic) NSInteger modelId;
@property (assign, nonatomic) NSInteger paybackTYPE; // 抵押方式 - -1：不限  1：房产  2：土地使用权  3：厂房  4：机器设备  5：存货  6：其他
@property (nonatomic, copy) NSString *loanTime;  // 贷款时间
@property (nonatomic, assign) NSInteger loanMoney;  // 贷款金额
@property (nonatomic, copy) NSString *bankName;     // 银行名称
@property (assign, nonatomic) NSInteger status;  // 1：待审核  2：审核中  3：已放款  4：审核失败
//+ (NSDictionary *)dicMortgage;

@end


NS_ASSUME_NONNULL_END
