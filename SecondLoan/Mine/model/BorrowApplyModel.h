//
//  BorrowApplyModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BorrowApplyModel : NSObject

@property (nonatomic, copy) NSString *borrowTitle;          //订单号
@property (nonatomic, assign) NSInteger borrowMoney;          //借款金额
@property (nonatomic, copy) NSString *borrowTime;           //借款时间
@property (nonatomic, assign) NSInteger borrowTerm;           //借款期限
@property (nonatomic, assign) NSInteger borrowStatus;         //借款状态 1.借款中  2. 未结清  3. 已结清
@property (nonatomic, copy) NSString *settleTime;           //结清时间
@property (nonatomic, assign) NSInteger surplusMoney;         //7日待还

+ (NSString *)getBorrowTime:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
