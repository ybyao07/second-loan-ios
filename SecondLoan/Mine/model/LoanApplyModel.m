//
//  LoanApplyModel.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "LoanApplyModel.h"

@implementation LoanApplyModel
+ (NSDictionary *)borrowTimeDic
{
    return @{
             @(1):@"3个月",
             @(2):@"6个月",
             @(3):@"12个月",
             @(4):@"24个月",
             @(5):@"36个月",
             };
}

+ (NSString *)getBorrowTime:(NSInteger)num
{
    NSString *borrowTime = @"";
    borrowTime =  [LoanApplyModel borrowTimeDic][@(num)];
    if (borrowTime == nil) return @"";
    return borrowTime;
}
@end
