//
//  LoanApplyModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoanApplyModel : NSObject
@property (nonatomic, copy) NSString *applyTime;  // 申请时间
@property (nonatomic, assign) NSInteger loanMoney; // 借款金额
@property (nonatomic, copy) NSString *bankName; //银行名称
@property (nonatomic, assign) NSInteger status; // 1:待审核 2：审核中 3：已放款  （循环数据）4. 审核失败
@property (nonatomic, assign) NSInteger applyTerm;// 申请期限
@property (nonatomic, copy) NSString *title;// 产品名称
@property (nonatomic, copy) NSString *updateTime;// 更新时间
+ (NSString *)getBorrowTime:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
