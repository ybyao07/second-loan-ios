//
//  ContactInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactInfo : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, assign) NSInteger contactsRelation;
@property (nonatomic, copy) NSString *contactsName;
@property (nonatomic, copy) NSString *personPhone;
@property (nonatomic, copy) NSString *contactsPhone;

@end

NS_ASSUME_NONNULL_END
