//
//  BankPersonAddressInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankPersonAddressInfo : NSObject

@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *entryTime;
@property (nonatomic, copy) NSString *personPhone;
@property (nonatomic, copy) NSString *familyAddress;  // 家庭住址
@property (nonatomic, assign) NSInteger addressType;     // 住房类型

@end

NS_ASSUME_NONNULL_END
