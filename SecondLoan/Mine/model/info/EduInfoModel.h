//
//  EduInfoModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EduInfoModel : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (assign, nonatomic) NSInteger highEdu;
@property (nonatomic, copy) NSString *schoolAddr;
@property (nonatomic, copy) NSString *schoolName;
@property (nonatomic, copy) NSString *personPhone;
@property (nonatomic, copy) NSString *graduationTime;

@end

NS_ASSUME_NONNULL_END
