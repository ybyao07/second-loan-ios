//
//  BankInfoObject.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/21.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankInfoObject : NSObject

@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, copy) NSString *phone;        // 手机号
@property (nonatomic, copy) NSString *familyAddress; //家庭住址
@property (nonatomic, assign) NSInteger reside;  // 住房类型
@property (nonatomic, copy) NSString *address;      //居住地址
@property (nonatomic, copy) NSString *postCode;
@property (nonatomic, copy) NSString *industryType; //所属行业
@property (nonatomic, assign) NSInteger occupation;       //职业
@property (nonatomic, copy) NSString *companyName;      //单位名称
@property (nonatomic, assign) NSInteger workPost;         //职务
@property (nonatomic, assign) NSInteger yearSalary;        //年收入
@property (nonatomic, copy) NSString *workTime;         //工作时间
@property (nonatomic, assign) NSInteger highEdu;        //学历
@property (nonatomic, copy) NSString *schoolName;       //学校
@property (nonatomic, copy) NSString *graduationTime;   //毕业时间
@property (nonatomic, assign) NSInteger personNum;      //家庭人数
@property (nonatomic, assign) NSInteger childNum;       //子女人数
@property (nonatomic, assign) NSInteger marStatus;      //婚姻状况

//@property (nonatomic, copy) NSString *ifHaveChild; //是否有子女

@property (nonatomic, copy) NSString *haveChild;
@property (nonatomic, assign) NSString *loanPurpose;

@end

NS_ASSUME_NONNULL_END
