//
//  PersonInfoObject.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "PersonInfoObject.h"

@implementation PersonInfoObject
MJCodingImplementation


+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"appEduInfoList" : [EduInfoModel class],
             @"appContactsInfoList" : [ContactInfo class]
             };
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    NSDictionary *dict = @{
                           @"peronModel":@"appPersonInfo",
                           @"addressModel":@"appAddressInfo",
                           @"workModel":@"appWorkInfo",
                           @"spouseModel":@"appSpouseInfo",
                           };
    return dict;
}

@end
