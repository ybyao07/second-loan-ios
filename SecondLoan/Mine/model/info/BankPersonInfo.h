//
//  BankPersonInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankPersonInfo : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *wetNum;    //微信号
@property (nonatomic, assign) NSInteger childNum;
@property (nonatomic, assign) NSInteger marStatus; //婚姻状况  2已婚
@property (nonatomic, copy) NSString *email;
@property (nonatomic, assign) NSInteger personNum; //家庭人数

@end

NS_ASSUME_NONNULL_END
