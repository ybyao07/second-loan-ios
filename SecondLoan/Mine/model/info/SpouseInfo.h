//
//  SpouseInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpouseInfo : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, copy) NSString *spouseTel;            //配偶公司电话
@property (nonatomic, copy) NSString *spouseCompany;
@property (nonatomic, copy) NSString *spousePhone;      //配偶手机号
@property (nonatomic, copy) NSString *personPhone;
@property (nonatomic, copy) NSString *spouseCard;
@property (nonatomic, copy) NSString *spouseWork;       //配偶工作地点
@property (nonatomic, copy) NSString *spouseName;
@property (nonatomic, assign) NSInteger workPost;      //配偶职务
@property (nonatomic, copy) NSString *industryType; // 配偶所属行业
@property (nonatomic, copy) NSString *cardUrl;
@end
NS_ASSUME_NONNULL_END
