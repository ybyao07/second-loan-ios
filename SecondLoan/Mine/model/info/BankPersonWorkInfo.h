//
//  BankPersonWorkInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankPersonWorkInfo : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, copy) NSString *industryType;
@property (nonatomic, copy) NSString *occupation;
@property (nonatomic, assign) NSInteger companyScale;
@property (nonatomic, copy) NSString *companyTel;
@property (nonatomic, assign) NSInteger salary;
@property (nonatomic, copy) NSString *personPhone;
@property (nonatomic, copy) NSString *yearSalary;
@property (nonatomic, copy) NSString *companyAddress;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, assign) NSInteger workPost;     //职务
@property (nonatomic, copy) NSString *workTime;



@end

NS_ASSUME_NONNULL_END
