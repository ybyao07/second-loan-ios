//
//  BankEduModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankEduModel : NSObject
@property (assign, nonatomic) NSInteger modelId;
@property (nonatomic, assign) NSInteger highEdu;
@property (nonatomic, copy) NSString *schoolName;
@property (nonatomic, copy) NSString *graduationTime;  

@end

NS_ASSUME_NONNULL_END
