//
//  PersonInfoObject.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EduInfoModel.h"
#import "BankPersonInfo.h"
#import "BankPersonWorkInfo.h"
#import "BankPersonAddressInfo.h"
#import "ContactInfo.h"
#import "SpouseInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonInfoObject : NSObject<NSCoding>

@property (strong, nonatomic) NSMutableArray<EduInfoModel *> *appEduInfoList; //教育信息
@property (strong, nonatomic) BankPersonInfo *peronModel; // 个人信息
@property (strong, nonatomic) BankPersonWorkInfo *workModel;  //工作信息
@property (strong, nonatomic) BankPersonAddressInfo *addressModel; // 地址信息
@property (strong, nonatomic) NSMutableArray<ContactInfo *>  *appContactsInfoList; //联系人信息
@property (strong, nonatomic) SpouseInfo *spouseModel; // 配偶信息


@end

NS_ASSUME_NONNULL_END
