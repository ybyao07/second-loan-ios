//
//  SLFinishRecordController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLBorrowRecordController.h"
#import "SLBorrowCell.h"
#import "BorrowApplyModel.h"

@interface SLBorrowRecordController () 
@property (assign,nonatomic) NSInteger cPage;
@end

@implementation SLBorrowRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = @"借款记录";
    self.view.backgroundColor = UIColorHex(0xf3f3f3);
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(6, 0, 0, 0);
    [self.tableView registerNib:[UINib nibWithNibName:@"SLBorrowCell" bundle:nil] forCellReuseIdentifier:@"SLBorrowCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)loadData
{
    self.cPage = 1;
    [self loadCurrentPageData];
}

- (void)loadMoreData
{
    [self loadCurrentPageData];
}

- (void)loadCurrentPageData
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:@(self.cPage) forKey:@"pageNum"];
    [param setObject:@(20) forKey:@"pageSize"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_borrow_record_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        UITableView * tableView = self.tableView;
        NSDictionary *resultData = responseObject[@"data"];
        AUTO_TABLE_PLACEHOLDER(tableView,self.dataSource,resultData,error)
        if (!error) {
            NSArray * list = resultData[@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            for (int i = 0; i< list.count; i ++) {
                BorrowApplyModel * data = [BorrowApplyModel mj_objectWithKeyValues:list[i]];
                [mlist addObject:data];
            }
            if (self.cPage == 1){
                self.dataSource = mlist;
            } else {
                self.dataSource =  [self.dataSource arrayByAddingObjectsFromArray:mlist];
            }
            self.cPage++;
            tableView.mj_footer.hidden = (self.dataSource.count >= [resultData[@"total"] integerValue]);
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        [tableView reloadData];
    }];
}
#pragma mark ====== UItableview ======

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SLBorrowCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLBorrowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SLBorrowCell"];
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


@end
