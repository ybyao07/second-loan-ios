//
//  SLSettingViewController.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "BaseViewController.h"
#import "UserMineInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLSettingViewController : BaseViewController
@property (strong, nonatomic) UserMineInfo *mineModel;
@end

NS_ASSUME_NONNULL_END
