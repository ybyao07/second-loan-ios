//
//  SLSettingViewController.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLSettingViewController.h"
#import "SLSettingTableViewCell.h"
#import "SLEditPWDViewController.h"
#import "IDCardViewController.h"
#import "SystemDeviceTool.h"
#import "WebSocketManager.h"
#import "WKBGlobalData.h"

@interface SLSettingViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *rowDataArray;
@property (nonatomic, strong) UIButton *logoutBtn;
@end

@implementation SLSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupTableView];
    [self setupLogoutBtn];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)setupTableView {
  self.tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, KStatusBarAndNavigationBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT - KStatusBarAndNavigationBarHeight) style:UITableViewStylePlain];
  [self.view addSubview:self.tableView];
  [self.tableView registerNib:[UINib nibWithNibName:@"SLSettingTableViewCell" bundle:nil] forCellReuseIdentifier:@"SLSettingTableViewCell"];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
  self.tableView.scrollEnabled = NO;
  self.tableView.tableFooterView = [[UIView alloc] init];
//  self.rowDataArray = [NSMutableArray arrayWithObjects:@"忘记 / 修改密码", @"实名认证", @"合同查看", @"求助投诉", nil];
  self.rowDataArray = [NSMutableArray arrayWithObjects:@"密码管理", @"认证管理", nil];
}

- (void)setupLogoutBtn {
  self.view.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
    factory.button().fsBackGroundImage(@"btn_bg").fsFontSize(20.0).fsText(@"退出登录").fsTextColor(0xffffff).fsOnClick(logout).fsClip(YES).fsRadius(3.3).fsFvid(@"logoutBtn");
  });
  [fQuery(self.view, @"logoutBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(self.view.mas_bottom).with.offset(-(10 + KMagrinBottom ));
      make.left.equalTo(self.view).with.offset(10);
      make.right.equalTo(self.view).with.offset(-10);
    make.height.equalTo(@50);
  }];
}

#pragma mark ==== UITableView ======
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 54;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  SLSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SLSettingTableViewCell" forIndexPath:indexPath];
  [cell bindTitle:self.rowDataArray[indexPath.row]];
  return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.rowDataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
  {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) { //忘记/修改密码
            SLEditPWDViewController *editPWDVC = [[SLEditPWDViewController alloc] init];
            [self.navigationController pushViewController:editPWDVC animated:YES];
    }else if (indexPath.row == 1){//实名认证
        if (self.mineModel.isAuthorized) {
            [self HUDshowSuccessWithStatus:@"您已认证通过了"];
        }else{
            IDCardViewController *cardVC = [[IDCardViewController alloc] initWithNibName:@"IDCardViewController" bundle:nil];
            cardVC.isOnlyAuth = YES;
            [self.navigationController pushViewController:cardVC animated:YES];
        }
    }else if (indexPath.row == 2){//合同查看

    }else if (indexPath.row == 3){ //求助投诉

    }
  }

  //MARK: 退出登录
- (void)logout {
    if ([UserLocal isLogin]) {
        // ---- socket 相关
        NSMutableDictionary * paramSocket = [NSMutableDictionary dictionary];
        paramSocket[@"macId"] = [SystemDeviceTool getUUID];
        paramSocket[@"userId"] = [[UserLocal sharedInstance] currentUserInfo].phone;
        paramSocket[@"flag"] = @(1);
        [[PLRequestManage sharedInstance] httpRequestWithUrl:WEBSOCKETWITH(url_loginWebsocket) Type:RequestPost parameters:paramSocket completeHandle:^(id  _Nullable responseObject, NSError *error) {
            if (!error) {
            }else{
                
            }
        }];
//        [[WebSocketManager shareInstance] setSocketClose];
        [[UserLocal sharedInstance] setCurrentUserInfo:nil];
        [WKBGlobalData shareManager].mineInfo = nil;
        [self.navigationController popViewControllerAnimated: YES];
    } else {
        [self HUDshowSuccessWithStatus:@"请先登录"];
    }
}

@end
