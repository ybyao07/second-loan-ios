//
//  SLLoanApplyViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLLoanApplyViewController.h"
#import "SLLoanApplyStatusCell.h"

@interface SLLoanApplyViewController ()

@property (assign,nonatomic) NSInteger cPage;

@end

@implementation SLLoanApplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = @"申请记录";
    self.view.backgroundColor = UIColorHex(0xf3f3f3);
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(6, 0, 0, 0);
    [self.tableView registerNib:[UINib nibWithNibName:@"SLLoanApplyStatusCell" bundle:nil] forCellReuseIdentifier:@"SLLoanApplyStatusCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)loadData
{
    self.cPage = 1;
    [self loadCurrentPageData];
}
- (void)loadMoreData
{
    [self loadCurrentPageData];
}
- (void)loadCurrentPageData
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
//    [param setObject:[UserLocal uId] forKey:@"userId"];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:@(self.cPage) forKey:@"pageNum"];
    [param setObject:@(20) forKey:@"pageSize"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_apply_record_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        NSDictionary *resultData = responseObject[@"data"];
        UITableView * tableView = self.tableView;
        AUTO_TABLE_PLACEHOLDER(tableView,self.dataSource,resultData,error)
        if (!error) {
            NSArray * list = responseObject[@"data"][@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            for (int i = 0; i< list.count; i++) {
                LoanApplyModel * data = [LoanApplyModel mj_objectWithKeyValues:list[i]];
                [mlist addObject:data];
            }
            if (self.cPage == 1){
                self.dataSource = mlist;
            } else {
                self.dataSource =  [self.dataSource arrayByAddingObjectsFromArray:mlist];
            }
            self.cPage++;
            tableView.mj_footer.hidden = (self.dataSource.count >= [resultData[@"total"] integerValue]);
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        [tableView reloadData];
    }];
}
#pragma mark ====== UItableview ======
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SLLoanApplyStatusCell cellHeight];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLLoanApplyStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SLLoanApplyStatusCell"];
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
