//
//  SLMyInfoManagerVC.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMyInfoManagerVC.h"
#import "SLPersonView.h"
#import "SLEduView.h"
#import "SLMyAddressView.h"
#import "SLMyWorkView.h"
#import "SLMyConnectorView.h"
#import "SLMySpouseView.h"
#import "PersonInfoObject.h"
#import "SLSingleChoseView.h"
#import "CommonModelTypeDIc.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

const int row = 4 + 5 + 5 + 8 + 7 + 9;
const int rowNoSpouse = 4 + 5 + 5 + 8 + 7;

#define kVertiacalMarge 10

@interface SLMyInfoManagerVC ()<SLPersonViewDelegate,SLEduViewDelegate,SLMyAddressViewDelegate,SLMyWorkViewDelegate,SLMyConnectorViewDelegate,SLMySpouseViewDelegate>

@property (strong, nonatomic) UIScrollView *containScrollView;
@property (strong, nonatomic) SLPersonView *personView;
@property (strong, nonatomic) SLEduView *eduView;
@property (strong, nonatomic) SLMyAddressView *addressView;
@property (strong, nonatomic) SLMyWorkView *workView;
@property (strong, nonatomic) SLMyConnectorView *connectorView;
@property (strong, nonatomic) SLMySpouseView *spouseView;
@property (strong, nonatomic) PersonInfoObject *personInfo;
@property (strong, nonatomic) UIButton *btnSave;
@property (strong, nonatomic) NSString *firstRelation;
@property (strong, nonatomic) NSString *secondRelation;

@end


@implementation SLMyInfoManagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"基础信息管理";
    self.view.backgroundColor = COLOR_White;
    _firstRelation = @"";
    _secondRelation = @"";
    [self.view addSubview:self.containScrollView];
    [self.containScrollView addSubview:self.personView];
    [self.containScrollView addSubview:self.eduView];
    [self.containScrollView addSubview:self.addressView];
    [self.containScrollView addSubview:self.workView];
    [self.containScrollView addSubview:self.connectorView];
    [self.containScrollView addSubview:self.spouseView];
//    [self.containScrollView addSubview:self.btnSave];
    [self.view addSubview:self.btnSave];
    [self loadData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)loadData{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_user_info) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        if (!error) {
            self.personInfo = [PersonInfoObject mj_objectWithKeyValues:responseObject[@"data"]];
            self.personView.model = self.personInfo.peronModel;
            if (self.personView.model.marStatus == 2) {
                self.spouseView.hidden = NO;
                _containScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, row * kInfoEditHeiht + 5 * kVertiacalMarge);
            }else{
                self.spouseView.hidden = YES;
                _containScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, rowNoSpouse * kInfoEditHeiht + 4 * kVertiacalMarge);
            }
            self.eduView.model = self.personInfo.appEduInfoList[[self.personInfo.appEduInfoList count] - 1];
            self.addressView.model = self.personInfo.addressModel;
            self.workView.model= self.personInfo.workModel;
            [self.connectorView setTwoModel:self.personInfo.appContactsInfoList];
            self.spouseView.model = self.personInfo.spouseModel;
        } else {
            self.personView.model = self.personInfo.peronModel;
            self.eduView.model = self.personInfo.appEduInfoList[[self.personInfo.appEduInfoList count] - 1];
            self.addressView.model = self.personInfo.addressModel;
            self.workView.model= self.personInfo.workModel;
            [self.connectorView setTwoModel:self.personInfo.appContactsInfoList];
            self.spouseView.model = self.personInfo.spouseModel;
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

#pragma mark ======= SLPersonViewDelegate ====
- (void)choosePersonView:(InfoChooseView *)view{
    SLSingleChoseView *chooseView = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayMarriageType title:@"婚姻状况"] ;
    [chooseView setEnterRowBlock:^(NSString * _Nonnull iterm, NSString * _Nonnull rowId) {
        self.personView.marrigeView.lblContent.text = iterm;
        self.personView.model.marStatus = [rowId integerValue];
        // 如果是是非结婚状态，隐藏配偶信息
        if (self.personView.model.marStatus == 2) {
            self.spouseView.hidden = NO;
            _containScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, row * kInfoEditHeiht + 5 * kVertiacalMarge);
        }else{
            self.spouseView.hidden = YES;
            _containScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, rowNoSpouse * kInfoEditHeiht + 4 * kVertiacalMarge);
        }
    }];
    [chooseView popTo:nil];
}
#pragma ======= SLEduViewDelegate =======
- (void)chooseEduView:(InfoChooseView *)view{
    switch (view.tag) {
        case EduViewHighestTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayHighEduType title:@"学历"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.eduView.eduHighest.lblContent.text = iterm;
                self.eduView.model.highEdu = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        case EduViewYearTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc arrayGraduateYearList] title:@"毕业时间"] ;
            [view setEnterBlock:^(NSString * iterm) {
                self.eduView.eduGraduateYear.lblContent.text = iterm;
                self.eduView.model.graduationTime = iterm;
            }];
            [view popTo:nil];
        }
        default:
            break;
    }
}
#pragma ===== SLMyAddressViewDelegate ====
- (void)chooseMyAddressView:(InfoChooseView *)view{
    switch (view.tag) {
        case MyAddressViewTypeTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayAddressType title:@"住房类型"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.addressView.addressHouseType.lblContent.text = iterm;
                self.addressView.model.addressType = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        case MyAddressTimeTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc arrayYearList] title:@"入住年份"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.addressView.addressTime.lblContent.text = iterm;
                self.addressView.model.entryTime = iterm;
            }];
            [view popTo:nil];
        }
            break;
            
        default:
            break;
    }
}
#pragma mark ======= SLMyWorkViewDelegate =====
- (void)chooseMyWorkView:(InfoChooseView *)view
{
    switch (view.tag) {
        case MyWorkViewTypeTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayWorkType title:@"行业类型"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.workView.workType.lblContent.text = iterm;
                self.workView.model.industryType = rowId;
            }];
            [view popTo:nil];
        }
            break;
        case MyWorkViewDutyTag:{
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayWorkPost title:@"工作职务"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.workView.workDuty.lblContent.text = iterm;
                self.workView.model.workPost = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        case MyWorkViewScaleTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayCompanyScale title:@"单位规模"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.workView.workScale.lblContent.text = iterm;
                self.workView.model.companyScale = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        case MyWorkViewSaleryTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arraySalaryType title:@"月薪"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId){
                self.workView.workSalery.lblContent.text = iterm;
                self.workView.model.salary = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark ====== SLMyConnectorViewDelegate ======
- (void)chooseMyConnectorView:(InfoChooseView *)view{
    WeakSelf
    switch (view.tag) {
        case MyConnectorViewRelationTag:
        {
            // 如果非已婚
            NSMutableArray *arrRelation = [NSMutableArray array];
            if (self.personView.model.marStatus != 2){
                for (NSDictionary *dic in [CommonModelTypeDIc shareManager].arrayRelationType) {
                    if (![dic[@"name"] isEqualToString:@"夫妻"] && ![dic[@"id"] isEqualToString:_secondRelation]) {
                        [arrRelation addObject:dic];
                    }
                }
            }else{
                arrRelation = [CommonModelTypeDIc shareManager].arrayRelationType;
            }
            
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:arrRelation title:@"联系人关系"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId){
                weakSelf.firstRelation = rowId;
                self.connectorView.connectRelation.lblContent.text = iterm;
                self.connectorView.model.contactsRelation = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        case MyConnectorViewARelationTag:
        {
            // 如果非已婚
            NSMutableArray *arrRelation = [NSMutableArray array];
            if (self.personView.model.marStatus != 2){
                for (NSDictionary *dic in [CommonModelTypeDIc shareManager].arrayRelationType) {
                    if (![dic[@"name"] isEqualToString:@"夫妻"] && ![dic[@"id"] isEqualToString:_firstRelation]) {
                        [arrRelation addObject:dic];
                    }
                }
            }else{
                arrRelation = [CommonModelTypeDIc shareManager].arrayRelationType;
            }
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:arrRelation title:@"联系人关系"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                weakSelf.secondRelation = rowId;
                self.connectorView.connectAnontherRelation.lblContent.text = iterm;
                self.connectorView.anotherModel.contactsRelation = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark ====== SLMySpouseViewDelegate ====
- (void)chooseMySpouseView:(InfoChooseView *)view{
    switch (view.tag) {
        case MySpouseViewWoryTypeTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayWorkType title:@"所属行业"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.spouseView.spouseWorktype.lblContent.text = iterm;
                self.spouseView.model.industryType = rowId ;
            }];
            [view popTo:nil];
        }
            break;
        case MySpouseViewDutyTag:
        {
            SLSingleChoseView * view = [[SLSingleChoseView alloc] initWithFrame:CGRectZero data:[CommonModelTypeDIc shareManager].arrayWorkPost title:@"工作职位"] ;
            [view setEnterRowBlock:^(NSString * iterm, NSString * _Nonnull rowId) {
                self.spouseView.spouseWorkDuty.lblContent.text = iterm;
                self.spouseView.model.workPost = [rowId integerValue];
            }];
            [view popTo:nil];
        }
            break;
        default:
            break;
    }
    
}
- (void)save:(UIButton *)sender{
    // 个人信息
    if (self.personView.model.marStatus == 0) {
        [self HUDshowErrorWithStatus:@"请选择婚姻状况"];
        return;
    }
    if (self.personView.model.email.length > 0) {
        if (![self.personView.model.email isValidateEmail] ) {
            [self HUDshowErrorWithStatus:MsgHintErrorEMail];
            return;
        }
    }else{
        [self HUDshowErrorWithStatus:@"请填写个人邮箱"];
        return;
    }
    if (self.personView.model.wetNum.length > 0) {
        if (![self.personView.model.wetNum isValidateWX] ) {
            [self HUDshowErrorWithStatus:MsgHintErrorWX];
            return;
        }
    }else{
        [self HUDshowErrorWithStatus:@"请填写微信号"];
        return;
    }
    // 学历信息
    if (self.eduView.model.highEdu == 0 || isEmptyString(self.eduView.eduHighest.lblContent.text) ) {
        [self HUDshowErrorWithStatus:@"请选择最高学历"];
        return;
    }
    if (isEmptyString(self.eduView.model.schoolAddr) ) {
        [self HUDshowErrorWithStatus:@"请输入学校所在地"];
        return;
    }
    if (isEmptyString(self.eduView.model.schoolName) ) {
        [self HUDshowErrorWithStatus:@"请输入学校名称"];
        return;
    }
    // 住址信息
    if (self.addressView.model.addressType == 0 || isEmptyString(self.addressView.addressHouseType.lblContent.text)) {
        [self HUDshowErrorWithStatus:@"请选择住房类型"];
        return;
    }
    if (isEmptyString(self.addressView.model.city) ) {
        [self HUDshowErrorWithStatus:@"请输入所在地区"];
        return;
    }
    if (isEmptyString(self.addressView.model.address) ) {
        [self HUDshowErrorWithStatus:@"请输入详细地址"];
        return;
    }
    if (isEmptyString(self.addressView.model.entryTime) ) {
        [self HUDshowErrorWithStatus:@"请选择入学年份"];
        return;
    }
    // 联系人信息
    if (self.connectorView.model.contactsRelation == 0 || isEmptyString(self.connectorView.connectRelation.lblContent.text)) {
        [self HUDshowErrorWithStatus:@"请选择第一位联系人的关系"];
        return;
    }
    if (isEmptyString(self.connectorView.model.contactsName) ) {
        [self HUDshowErrorWithStatus:@"请输入第一位联系人姓名"];
        return;
    }
    if (self.connectorView.model.contactsPhone.length > 0) {
        if (![self.connectorView.model.contactsPhone validateMobile]) {
            [self HUDshowErrorWithStatus:@"请输入第一位联系人正确手机号"];
            return;
        }
    }else{
        [self HUDshowErrorWithStatus:@"请输入第一位联系人手机号"];
        return;
    }
    if (self.connectorView.anotherModel.contactsRelation == 0 ||isEmptyString(self.connectorView.connectAnontherRelation.lblContent.text)) {
        [self HUDshowErrorWithStatus:@"请选择第二位联系人的关系"];
        return;
    }
    if (isEmptyString(self.connectorView.anotherModel.contactsName) ) {
        [self HUDshowErrorWithStatus:@"请输入第二位联系人姓名"];
        return;
    }
    if (self.connectorView.anotherModel.contactsPhone.length > 0) {
        if (![self.connectorView.anotherModel.contactsPhone validateMobile]) {
            [self HUDshowErrorWithStatus:@"请输入第二位联系人正确手机号"];
            return;
        }
    }else{
        [self HUDshowErrorWithStatus:@"请输入第二位联系人手机号"];
        return;
    }
    // 第二位 联系人不能与第一位联系人相同 （手机号)
    if ([self.connectorView.anotherModel.contactsPhone isEqualToString:self.connectorView.model.contactsPhone]) {
        [self HUDshowErrorWithStatus:@"两位联系人手机号不能相同"];
        return;
    }

    //工作信息
    if (self.workView.model.companyTel.length > 0) { // 如果是固话
        if (![self.workView.model.companyTel hasPrefix:@"1"]) { //手机号
            NSString * strNum = @"";
            if ([self.workView.model.companyTel containsString:@"-"]) {
                //验证输入的固话中带 "-"符号
                strNum = @"^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\\d{8}$)";
            }else{
                //验证输入的固话中不带 "-"符号
                strNum = @"^(0[0-9]{2,3})?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\\d{8}$)";
            }
            if (![self.workView.model.companyTel checkSolidPhone:strNum] ) {
                [self HUDshowErrorWithStatus:MsgHintCompanyPhoneNoCorrect];
                return;
            }
        }else{
            if (![self.workView.model.companyTel validateMobile] ) {
                [self HUDshowErrorWithStatus:MsgHintCompanyPhoneNoCorrect];
                return;
            }
        }
    }
    // 配偶信息
    if (self.personView.model.marStatus == 2) {
        if (isEmptyString(self.spouseView.model.spouseName) ) {
            [self HUDshowErrorWithStatus:@"请输入配偶姓名"];
            return;
        }
        if (isEmptyString(self.spouseView.model.spouseCard) ) {
            [self HUDshowErrorWithStatus:@"请输入配偶身份证号"];
            return;
        }
        if (![self.spouseView.model.spouseCard validateIdentityCard]) {
            [self HUDshowErrorWithStatus:@"请输入配偶正确身份证号"];
            return;
        }
        if (self.spouseView.model.spousePhone.length > 0) {
            if (![self.spouseView.model.spousePhone validateMobile]) {
                [self HUDshowErrorWithStatus:MsgHintSpousePhoneNoCorrect];
                return;
            }
        }else{
            [self HUDshowErrorWithStatus:@"请输入配偶手机号"];
            return;
        }
        if (isEmptyString(self.spouseView.model.spouseCompany) ) {
            [self HUDshowErrorWithStatus:@"请输入配偶工作单位"];
            return;
        }
        if (isEmptyString(self.spouseView.model.spouseWork) ) {
            [self HUDshowErrorWithStatus:@"请输入配偶工作地点"];
            return;
        }
        if (isEmptyString(self.spouseView.model.industryType)) {
            [self HUDshowErrorWithStatus:@"请选择配偶所属行业"];
            return;
        }
        if (self.spouseView.model.workPost == 0) {
            [self HUDshowErrorWithStatus:@"请选择配偶工作职位"];
            return;
        }
        
        if (self.spouseView.model.spouseTel.length > 0) { // 如果是固话
            if (![self.spouseView.model.spouseTel hasPrefix:@"1"]) { //手机号
                NSString * strNum = @"";
                if ([self.spouseView.model.spouseTel containsString:@"-"]) {
                    //验证输入的固话中带 "-"符号
                    strNum = @"^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\\d{8}$)";
                }else{
                    //验证输入的固话中不带 "-"符号
                    strNum = @"^(0[0-9]{2,3})?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\\d{8}$)";
                }
                if (![self.spouseView.model.spouseTel checkSolidPhone:strNum] ) {
                    [self HUDshowErrorWithStatus:@"请输入配偶正确的单位电话"];
                    return;
                }
            }else{
                if (![self.spouseView.model.spouseTel validateMobile] ) {
                    [self HUDshowErrorWithStatus:@"请输入配偶正确的单位电话"];
                    return;
                }
            }
        }else{
            [self HUDshowErrorWithStatus:@"请选择配偶单位电话"];
            return;
        }
    }
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    NSMutableDictionary *dicPerson = [NSMutableDictionary new];
    dicPerson[@"id"] = self.personView.model.modelId > 0 ? @(self.personView.model.modelId):nil;
    dicPerson[@"marStatus"] = @(self.personView.model.marStatus);
    dicPerson[@"email"] = isEmptyString(self.personView.model.email)?@"":self.personView.model.email;
    dicPerson[@"wetNum"] = isEmptyString(self.personView.model.wetNum)?@"":self.personView.model.wetNum;
    dicPerson[@"phone"] = [[UserLocal sharedInstance] currentUserInfo].phone;
    param[@"appPersonInfo"] = dicPerson;
    NSMutableDictionary *dicEdu = [NSMutableDictionary dictionary];
    dicEdu[@"id"] = self.eduView.model.modelId > 0 ? @(self.eduView.model.modelId):nil;
    dicEdu[@"highEdu"] = @(self.eduView.model.highEdu);
    dicEdu[@"schoolAddr"] = isEmptyString(self.eduView.model.schoolAddr)?@"":self.eduView.model.schoolAddr;
    dicEdu[@"schoolName"] = isEmptyString(self.eduView.model.schoolName)?@"":self.eduView.model.schoolName;
    dicEdu[@"graduationTime"]  = isEmptyString(self.eduView.model.graduationTime)?@"":self.eduView.model.graduationTime;
    param[@"appEduInfoList"] = @[dicEdu];
    NSMutableDictionary *dicAddress = [NSMutableDictionary new];
    dicAddress[@"id"] = self.addressView.model.modelId > 0 ? @(self.addressView.model.modelId):nil;
    dicAddress[@"city"] = isEmptyString(self.addressView.model.city)?@"":self.addressView.model.city;
    dicAddress[@"address"] = isEmptyString(self.addressView.model.address)?@"":self.addressView.model.address;
    dicAddress[@"entryTime"] = isEmptyString(self.addressView.model.entryTime)?@"":self.addressView.model.entryTime;
    dicAddress[@"addressType"] = @(self.addressView.model.addressType);
    param[@"appAddressInfo"] = dicAddress;
    NSMutableDictionary *dicWork = [NSMutableDictionary new];
    dicWork[@"id"] = self.workView.model.modelId > 0 ? @(self.workView.model.modelId):nil;
    dicWork[@"companyName"] = isEmptyString(self.workView.model.companyName)?@"":self.workView.model.companyName;
    dicWork[@"industryType"] = isEmptyString(self.workView.model.industryType)?@"":self.workView.model.industryType;
    dicWork[@"companyAddress"] = isEmptyString(self.workView.model.companyAddress)?@"":self.workView.model.companyAddress;
    dicWork[@"companyTel"] = isEmptyString(self.workView.model.companyTel)?@"":self.workView.model.companyTel;
    dicWork[@"companyScale"] = @(self.workView.model.companyScale);
    dicWork[@"workPost"] = @(self.workView.model.workPost);
    dicWork[@"salary"] = @(self.workView.model.salary);
    param[@"appWorkInfo"] = dicWork;
    NSMutableDictionary *dicConn = [NSMutableDictionary new];
    dicConn[@"id"] = self.connectorView.model.modelId > 0 ? @(self.connectorView.model.modelId):nil;
    dicConn[@"contactsRelation"] = @(self.connectorView.model.contactsRelation);
    dicConn[@"contactsPhone"] = isEmptyString(self.connectorView.model.contactsPhone)?@"":self.connectorView.model.contactsPhone;
    dicConn[@"contactsName"] = isEmptyString(self.connectorView.model.contactsName)?@"":self.connectorView.model.contactsName;
    
    NSMutableDictionary *dicAnoConn = [NSMutableDictionary new];
    dicAnoConn[@"id"] = self.connectorView.anotherModel.modelId > 0 ? @(self.connectorView.anotherModel.modelId):nil;
    dicAnoConn[@"contactsRelation"] = @(self.connectorView.anotherModel.contactsRelation);
    dicAnoConn[@"contactsPhone"] = isEmptyString(self.connectorView.anotherModel.contactsPhone)?@"":self.connectorView.anotherModel.contactsPhone;
    dicAnoConn[@"contactsName"] = isEmptyString(self.connectorView.anotherModel.contactsName)?@"":self.connectorView.anotherModel.contactsName;
    param[@"appContactsInfoList"] = @[dicConn, dicAnoConn];
    NSMutableDictionary *dicSpouse = [NSMutableDictionary new];
    dicSpouse[@"id"] = self.spouseView.model.modelId > 0 ? @(self.spouseView.model.modelId) : nil;
    dicSpouse[@"spouseName"] = isEmptyString(self.spouseView.model.spouseName)?@"":self.spouseView.model.spouseName;
    dicSpouse[@"spouseCard"] = isEmptyString(self.spouseView.model.spouseCard)?@"":self.spouseView.model.spouseCard;
    dicSpouse[@"spousePhone"] = isEmptyString(self.spouseView.model.spousePhone)?@"":self.spouseView.model.spousePhone;
    dicSpouse[@"spouseCompany"] = isEmptyString(self.spouseView.model.spouseCompany)?@"":self.spouseView.model.spouseCompany;
    dicSpouse[@"spouseWork"] = isEmptyString(self.spouseView.model.spouseWork)?@"":self.spouseView.model.spouseWork;
    dicSpouse[@"industryType"] =isEmptyString(self.spouseView.model.industryType)?@"":self.spouseView.model.industryType;
    dicSpouse[@"workPost"] = @(self.spouseView.model.workPost);
    dicSpouse[@"spouseTel"] = isEmptyString(self.spouseView.model.spouseTel)?@"":self.spouseView.model.spouseTel;
    dicSpouse[@"cardUrl"] = @"";
//    if (self.personView.model.marStatus == 2) {
        param[@"appSpouseInfo"] = dicSpouse;
//    }
    sender.enabled = NO;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//    });
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_user_info_update) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        sender.enabled = YES;
        if (!error) {
            [self HUDshowWithStatus:@"保存成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

#pragma mark ---
- (UIScrollView *)containScrollView
{
    if (!_containScrollView) {
        _containScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake (0, KStatusBarAndNavigationBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT - KStatusBarAndNavigationBarHeight - 60 - KMagrinBottom )];
        _containScrollView.backgroundColor = ColorFromHex(0xF3F3F3);
        _containScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, row * kInfoEditHeiht + 5 * kVertiacalMarge);
    }
    return _containScrollView;
}

- (SLPersonView *)personView
{
    if (!_personView) {
        _personView = [[SLPersonView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kInfoEditHeiht * 4)];
        _personView.delegate = self;
    }
    return _personView;
}

- (SLEduView *)eduView{
    if (!_eduView) {
        _eduView = [[SLEduView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.personView.frame) + kVertiacalMarge, SCREEN_WIDTH, kInfoEditHeiht * 5)];
        _eduView.delegate = self;
    }
    return _eduView;
}

- (SLMyAddressView *)addressView{
    if (!_addressView) {
        _addressView = [[SLMyAddressView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.eduView.frame) + kVertiacalMarge, SCREEN_WIDTH, kInfoEditHeiht * 5)];
        _addressView.delegate = self;
    }
    return _addressView;
}
- (SLMyWorkView *)workView{
    if (!_workView) {
        _workView = [[SLMyWorkView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.addressView.frame) + kVertiacalMarge, SCREEN_WIDTH, kInfoEditHeiht * 8)];
        _workView.delegate = self;
    }
    return _workView;
}
- (SLMyConnectorView *)connectorView{
    if (!_connectorView) {
        _connectorView =  [[SLMyConnectorView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.workView.frame) + kVertiacalMarge, SCREEN_WIDTH, kInfoEditHeiht * 7)];
        _connectorView.delegate = self;
    }
    return _connectorView;
}
- (SLMySpouseView *)spouseView{
    if (!_spouseView) {
        _spouseView = [[SLMySpouseView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.connectorView.frame) + kVertiacalMarge, SCREEN_WIDTH, kInfoEditHeiht * 9)];
        _spouseView.delegate = self;
    }
    return _spouseView;
}
- (UIButton *)btnSave{
    if (!_btnSave) {
        _btnSave = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnSave.frame = CGRectMake(10, SCREEN_HEIGHT - 55 - KMagrinBottom, SCREEN_WIDTH - 20, 50);
        [_btnSave setBackgroundImage:[UIImage imageNamed:@"btn_bg"] forState:UIControlStateNormal];
        [_btnSave addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
        [_btnSave setTitle:@"保存" forState:UIControlStateNormal];
        [_btnSave setTitleColor:COLOR_White forState:UIControlStateNormal];
        _btnSave.titleLabel.font = Font18Bold;
        _btnSave.titleLabel.textColor = COLOR_White;
    }
    return _btnSave;
}
@end
