//
//  IDFaceViewController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IDFaceViewController : BaseViewController
@property (nonatomic, copy) NSString *idCard;
@property (nonatomic, copy) NSString *name;
@property (weak, nonatomic) IBOutlet UIButton *btnFace;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;


@property (nonatomic, copy) NSString *productId;
@property (assign, nonatomic) BOOL isOnlyAuth; //只是为了做认证
@property (assign, nonatomic) BOOL isForPuhuiProduct; // 是不是申请普惠产品

@end

NS_ASSUME_NONNULL_END
