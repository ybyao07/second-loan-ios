//
//  SLAmountViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLAmountViewController.h"
#import "HomeHeaderView.h"
#import "SLProductInfoViewController.h"
#import "HomeBannerModel.h"
#import "PDWebViewController.h"
#import "MyMoneyRangeModel.h"
#import "UserMineInfo.h"
#import "SLBorrowRecordController.h"
#import "SLVerifySubmitViewController.h"
#import "SLVerifyFailedViewController.h"
#import "SLPHApplyLoanViewController.h"
#import "WKBGlobalData.h"
#import "SLVerifyViewController.h"
#import "NSString+Tools.h"

@interface SLAmountViewController ()<SDCycleScrollViewDelegate>

@property (nonatomic,strong) HomeHeaderView *headerView;
@property (strong, nonatomic) NSArray *bannerList;//banner列表
@property (strong, nonatomic) MyMoneyRangeModel *myMoneyRange;
@property (strong, nonatomic) ProductModel *productPuhui;

@end

@implementation SLAmountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的额度";
    self.view.backgroundColor = COLOR_White;
    [self.view addSubview:self.headerView];
    self.headerView.headTopCreditView.hidden = NO;
    self.headerView.headTopView.hidden = YES;
    [self addApplyView];
    [self loadProductPuhui];
    self.bannerList = [HomeBannerModel mj_objectArrayWithKeyValuesArray:@[
                                                                          @{
                                                                              @"picture":@"product_detail_top1.jpg",
                                                                             
                                                                              },
                                                                          @{
                                                                              @"picture":@"product_detail_top2.jpg",
                                                                              @"jumpURL":@""
                                                                              },
                                                                          @{
                                                                              @"picture":@"product_detail_top3.jpg",
                                                                              @"jumpURL":@""
                                                                              }]];
    [self setupTopScrollViewWithImageURLArray];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)loadProductPuhui{
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_product_puhui) Type:RequestPost parameters:[NSDictionary new] completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        if (!error) {
            self.productPuhui = [ProductModel mj_objectWithKeyValues:responseObject[@"data"]];
            self.headerView.headTopView.model = self.productPuhui;
            self.headerView.headTopCreditView.hidden = YES;
            self.headerView.headTopView.hidden = NO;
            if([UserLocal isLogin]){ // 如果登录，则调用是否有额度接口获取我的额度
                NSMutableDictionary * param = [NSMutableDictionary dictionary];
                [param setObject:[UserLocal token] forKey:@"token"];
                [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_my_quota) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
                    [self HUDdismiss];
                    if (!error) {
                        self.myMoneyRange = [MyMoneyRangeModel mj_objectWithKeyValues:responseObject[@"data"]];
                        if (self.myMoneyRange) { //
                            if (self.myMoneyRange.status == CodeCreditPassed) {
                                self.headerView.headTopCreditView.model = self.myMoneyRange;
                                [self.headerView.headTopCreditView.imgBank sd_setImageWithURL:[NSURL URLWithString:self.productPuhui.bankUrl]];
                                self.headerView.headTopCreditView.lblName.text = self.productPuhui.bankName;
                                [self.headerView.headTopCreditView setProductName:self.productPuhui.productName];
                                self.headerView.headTopCreditView.hidden = NO;
                                self.headerView.headTopView.hidden = YES;
                            }else{
                                self.headerView.headTopView.model = self.productPuhui;
                                self.headerView.headTopCreditView.hidden = YES;
                                self.headerView.headTopView.hidden = NO;
                            }
                        }
                    }else{
//                        [self HUDshowErrorWithStatus:error.localizedDescription];
                    }
                }];
            }
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}


#pragma mark - ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ 循环滚动 ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
- (void)setupTopScrollViewWithImageURLArray {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSMutableArray *imgBanners = [NSMutableArray new];
        [self.bannerList enumerateObjectsUsingBlock:^(HomeBannerModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [imgBanners addObject:obj.picture];
//            [imgBanners addObject:@"product_detail_top"];
        }];
        self.headerView.homeScrollView.imageURLStringsGroup = imgBanners;
    });
    self.headerView.homeScrollView.delegate = self;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    HomeBannerModel *banner = self.bannerList[index];
    if (banner.jumpURL.length) {
        PDWebViewController * vc = [[PDWebViewController alloc] init];
        vc.webUrl =  banner.jumpURL;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (HomeHeaderView *)headerView
{
    if (!_headerView) {
//        _headerView = [[HomeHeaderView alloc] initWithFrame:CGRectMake(0, KStatusBarAndNavigationBarHeight, SCREEN_WIDTH, [HomeHeaderView height])];
        _headerView = [[HomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, [HomeHeaderView height])];
        WeakSelf
        _headerView.lblLogo.hidden = YES;
        _headerView.btnAboutUs.hidden = YES;
        // 查看额度
        _headerView.checkMyCreditBlock = ^(){
            //重新调用获取额度接口 ------- 如果成果则跳转到审核通过的界面 ----- SLVerifyViewController
            NSMutableDictionary * param = [NSMutableDictionary dictionary];
            [param setObject:[UserLocal token] forKey:@"token"];
            [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_my_quota) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
                if (!error) {
                    weakSelf.myMoneyRange = [MyMoneyRangeModel mj_objectWithKeyValues:responseObject[@"data"]];
                    [WKBGlobalData shareManager].homeMoneyModel = weakSelf.myMoneyRange;
                    if (weakSelf.myMoneyRange) {
                        if (weakSelf.myMoneyRange.status == CodeCreditDoing) {
                            SLVerifySubmitViewController *doingVC = [[SLVerifySubmitViewController alloc] initWithNibName:@"SLVerifySubmitViewController" bundle:nil];
                            doingVC.isPuhui = YES;
                            [weakSelf.navigationController pushViewController:doingVC animated:YES];
                        }else if (weakSelf.myMoneyRange.status == CodeCreditNotPassed){
                            SLVerifyFailedViewController *failedVC = [[SLVerifyFailedViewController alloc] initWithNibName:@"SLVerifyFailedViewController" bundle:nil];
                            [weakSelf.navigationController pushViewController:failedVC animated:YES];
                        }else if (weakSelf.myMoneyRange.status == CodeCreditPassed ){
                            SLVerifyViewController *vcController = [[SLVerifyViewController alloc] init];
                            vcController.money = [NSString stringWithFormat:@"%@", [NSString getMoneyStringWithMoneyNumber:weakSelf.myMoneyRange.viewQuota]];
                            [weakSelf.navigationController pushViewController:vcController animated:YES];
                        }
                    }else{
                        if (weakSelf.productPuhui) {
                            SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
                            productInfo.productId = weakSelf.productPuhui.productId;
                            productInfo.isPuhui = YES;
                            [weakSelf.navigationController pushViewController:productInfo animated:YES];
                        }
                    }
                }else{
                    if (weakSelf.productPuhui) {
                        SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
                        productInfo.productId = weakSelf.productPuhui.productId;
                        productInfo.isPuhui = YES;
                        [weakSelf.navigationController pushViewController:productInfo animated:YES];
                    }
                }
            }];
//            if (weakSelf.myMoneyRange) {
//               if (weakSelf.myMoneyRange.status == CodeCreditDoing) {
//                    SLVerifySubmitViewController *doingVC = [[SLVerifySubmitViewController alloc] initWithNibName:@"SLVerifySubmitViewController" bundle:nil];
//                    [weakSelf.navigationController pushViewController:doingVC animated:YES];
//                }else if (weakSelf.myMoneyRange.status == CodeCreditNotPassed){
//                    SLVerifyFailedViewController *failedVC = [[SLVerifyFailedViewController alloc] initWithNibName:@"SLVerifyFailedViewController" bundle:nil];
//                    [weakSelf.navigationController pushViewController:failedVC animated:YES];
//                }
//            }else{
//                if (weakSelf.productPuhui) {
//                    SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
//                    productInfo.productId = weakSelf.productPuhui.productId;
//                    productInfo.isPuhui = YES;
//                    [weakSelf.navigationController pushViewController:productInfo animated:YES];
//                }
//            }
        };
        // 立即借款
        _headerView.goBorrowBlock = ^(){
            SLPHApplyLoanViewController *applyVC = [[SLPHApplyLoanViewController alloc] init];
            applyVC.productId = weakSelf.productPuhui.productId;
            [weakSelf.navigationController pushViewController:applyVC animated:YES];
        };
        // 查看借款历史
        _headerView.checkMyBorrowBlock = ^(){
            SLBorrowRecordController *finishVC = [[SLBorrowRecordController alloc] init];
            [weakSelf.navigationController pushViewController:finishVC animated:YES];
        };
    }
    return _headerView;
}


- (void)addApplyView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), SCREEN_WIDTH, 30.0f)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_table_sec_hint"]];
    imgView.frame = CGRectMake(10, 6, 7, 30.0f - 2*6);
    [view addSubview:imgView];
    UILabel *lblRecommend = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame) + 10, 0, 120, 30.0f)];
    lblRecommend.text = @"提额申请";
    lblRecommend.font = Font15Bold;
    [view addSubview:lblRecommend];
    view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view];
    
    UIImageView *waitForView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_waiting"]];
    waitForView.frame = CGRectMake(10, CGRectGetMaxY(view.frame) + 6, (SCREEN_WIDTH - 20), CGRectGetHeight(waitForView.frame) / CGRectGetWidth(waitForView.frame) * (SCREEN_WIDTH - 20));
    [self.view addSubview:waitForView];
}

@end
