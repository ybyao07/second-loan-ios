//
//  IDCardViewController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface IDCardViewController : BaseViewController

@property (assign, nonatomic) BOOL isOnlyAuth; //只是为了做认证
@property (assign, nonatomic) BOOL isForPuhuiProduct; // 是不是申请普惠产品
@property (nonatomic, copy) NSString *productId;;

@end

NS_ASSUME_NONNULL_END
