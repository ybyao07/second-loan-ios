//
//  GlobalColor.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#ifndef GlobalColor_h
#define GlobalColor_h

#define UIColorFromHex(hexValue) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0]
#define UIGrayColorWithAlpha(alpha) [UIColor colorWithRed:0 green:0 blue:0 alpha:alpha]
#define COLOR(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

/**
 *  颜色宏
 */
#define COLOR_RANDOM [UIColor colorWithRGB:arc4random()%(256*256*256)];
#define COLOR_ThemeRed UIColorFromHex(0xff4c2b)
#define COLOR_White [UIColor whiteColor]
#define COLOR_White_FD UIColorFromHex(0xFDFDFD)
#define COLOR_Background UIColorFromHex(0xeaeaea)
#define COLOR_Gary_66 UIColorFromHex(0x666666)
#define COLOR_Gary_99 UIColorFromHex(0x999999)
#define COLOR_BlackText_33 UIColorFromHex(0x333333)
#define COLOR_LightBlue UIColorFromHex(0x1ab2ff)
#define CCellSeperLineColor UIColorFromHex(0xe8e8e8)
#define COLOR_Gary_F3 UIColorFromHex(0xF3F3F3)
#define COLOR_Gary_FA UIColorFromHex(0xFAFAFA)

//#define COLOR_Gary_F3 UIColorFromHex(0xFF0000)


#endif /* GlobalColor_h */
