//
//  LoginController.h
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginController : BaseViewController

@property (nonatomic, assign) BOOL isRemoteLogin;

@end

NS_ASSUME_NONNULL_END
