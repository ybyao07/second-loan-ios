//
//  LoginController.m
//  SuYue
//
//  Created by Win10 on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "LoginController.h"
#import "GDSNSManager.h"
#import "MainTabViewController.h"
#import "LocationManager.h"
#import "WebSocketManager.h"
#import "PDWebViewController.h"
#import "RegisterViewController.h"
#import "ForgetViewController.h"
#import "SystemDeviceTool.h"

@interface LoginController ()
@property (nonatomic, assign) CGFloat lastSendcodeTime;
@property (strong, nonatomic) UITextField *txtPhone;
@property (strong, nonatomic) UITextField *txtPwd;
@end

static CGFloat const lineSpace = 82.0f;

@implementation LoginController

- (void)setupViewForSignin {
  self.view.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
    factory.imageView().fsImage(@"loginbg").fsImgContentMode(UIViewContentModeScaleAspectFill).fsFvid(@"bg_img");
    factory.imageView().fsImage(@"top_log").fsFvid(@"topLog");
    factory.label().fsFontSize(15.0).fsText(@"手机号").fsTextColor(0x23242A).fsFvid(@"phoneLabel");
  factory.textField().fsPlaceholder(@"请输入手机号").fsClearButtonMode(UITextFieldViewModeWhileEditing).fsLimit(@"11").fsFontSize(15.0).fsTextColor(0x23242A).fsKeyboard(UIKeyboardTypePhonePad).fsFvid(@"phoneField");
    factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"bl1");
    factory.label().fsFontSize(15.0).fsText(@"密码").fsTextColor(0x23242A).fsFvid(@"pwdLabel");
    factory.textField().fsPlaceholder(@"请输入密码").fsClearButtonMode(UITextFieldViewModeWhileEditing).fsLimit(@"16").fsFontSize(15.0).fsTextColor(0x23242A).fsFvid(@"pwdfield");
      factory.button().fsFontSize(15.0).fsImage(@"hidePWD").fsOnClick(seePwdDidClick:).fsFvid(@"seePwdCodeBtn2");


    factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"bl3");
  factory.button().fsFontSize(18.0).fsText(@"登录").fsTextColor(0xFFFFFF).fsOnClick(loginDidClick:).fsBackGroundImage(@"btn_bg").fsFvid(@"loginBtn");
    factory.button().fsFontSize(14.0).fsText(@"快速注册").fsTextColor(0x23242A).fsOnClick(quickRegister).fsClip(YES).fsRadius(3.3).fsFvid(@"quickRegister");

    factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"verticalLine");
    factory.button().fsFontSize(14.0).fsText(@"忘记密码").fsTextColor(0x23242A).fsOnClick(forgetPWD).fsClip(YES).fsRadius(3.3).fsFvid(@"forgetPWD");
    factory.button().fsOnClick(back).fsImage(@"close").fsFvid(@"fClose");
    factory.button().fsOnClick(back).fsFvid(@"fClose2");

  });
    // backgroundImg
  [fQuery(self.view, @"bg_img") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.equalTo(self.view.mas_left);
    make.centerX.equalTo(self.view.mas_centerX);
    make.centerY.equalTo(self.view.mas_centerY);
  }];
    // log
  [fQuery(self.view, @"topLog") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(self.view.mas_top).with.offset(SCREEN_HEIGHT/6);
    make.left.equalTo(self.view).with.offset(92/750.0 * SCREEN_WIDTH);
    make.height.equalTo(@(80/750.0 * SCREEN_WIDTH));
  }];
    //第一行
  [fQuery(self.view, @"bl1") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(fQuery(self.view, @"topLog").mas_bottom).with.offset(235/750.0 * SCREEN_WIDTH);
    make.left.equalTo(self.view).with.offset(92/750.0 * SCREEN_WIDTH);
    make.right.equalTo(self.view).with.offset(-92/750.0 * SCREEN_WIDTH);
    make.height.equalTo(@1);
  }];
  [fQuery(self.view, @"phoneField") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(fQuery(self.view, @"bl1").mas_top).with.offset(8);
    make.height.equalTo(@48);
    make.left.equalTo(fQuery(self.view, @"bl1"));
    make.right.equalTo(fQuery(self.view, @"bl1"));
  }];
  [fQuery(self.view, @"phoneLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(fQuery(self.view, @"phoneField").mas_top).with.offset(4);
    make.height.equalTo(@25);
    make.left.equalTo(fQuery(self.view, @"bl1"));
  }];

  //第二行
  [fQuery(self.view, @"bl3") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(fQuery(self.view, @"bl1").mas_bottom).with.offset(lineSpace);
    make.left.equalTo(fQuery(self.view, @"bl1"));
    make.width.equalTo(fQuery(self.view, @"bl1"));
    make.height.equalTo(@1);
  }];
  [fQuery(self.view, @"pwdfield") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(fQuery(self.view, @"bl3").mas_top).with.offset(8);
    make.height.equalTo(@48);
    make.left.equalTo(fQuery(self.view, @"bl3"));
    make.right.equalTo(fQuery(self.view, @"bl3")).with.offset(-30);
  }];

    [fQuery(self.view, @"seePwdCodeBtn2") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.view, @"pwdfield"));
        make.right.equalTo(fQuery(self.view, @"pwdfield")).with.offset(30);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];
  [fQuery(self.view, @"pwdLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.bottom.equalTo(fQuery(self.view, @"pwdfield").mas_top).with.offset(4);
    make.height.equalTo(@25);
    make.left.equalTo(fQuery(self.view, @"bl3"));
  }];
  [fQuery(self.view, @"loginBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(fQuery(self.view, @"bl3").mas_bottom).with.offset(60);
    make.left.equalTo(fQuery(self.view, @"bl3"));
    make.width.equalTo(fQuery(self.view, @"bl3"));
    make.height.equalTo(@48);
  }];

  [fQuery(self.view, @"quickRegister") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(fQuery(self.view, @"loginBtn").mas_bottom).with.offset(12);
    make.right.equalTo(fQuery(self.view, @"verticalLine").mas_left).with.offset(-20);
    make.width.equalTo(@100);
    make.height.equalTo(@18);
  }];

  [fQuery(self.view, @"verticalLine") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.centerX.equalTo(self.view);
    make.centerY.equalTo(fQuery(self.view, @"quickRegister"));
    make.width.equalTo(@1);
    make.height.equalTo(@15);
  }];

  [fQuery(self.view, @"forgetPWD") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(fQuery(self.view, @"quickRegister"));
    make.left.equalTo(fQuery(self.view, @"verticalLine").mas_right).with.offset(20);
    make.width.equalTo(@100);
    make.height.equalTo(@18);
  }];

  [fQuery(self.view, @"fClose") mas_makeConstraints:^(MASConstraintMaker *make) {
    make.top.equalTo(self.view.mas_top).with.offset(10 + statusBarHeight);
    make.left.equalTo(self.view.mas_left).with.offset(20);
    make.width.equalTo(@30);
    make.height.equalTo(@30);
  }];
    [fQuery(self.view, @"fClose2") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.width.equalTo(@100);
        make.height.equalTo(@100);
    }];

    self.txtPhone = fQueryField(self.view, @"phoneField");
    self.txtPwd = fQueryField(self.view, @"pwdfield");
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViewForSignin];
    [self hideKeyboardTap];
    self.txtPwd.secureTextEntry = YES;
//    [[LocationManager sharedInstance] setNextTime:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationBarBgView.hidden = YES;
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    fQueryBtn(self.view, @"fback").frame = CGRectMake(0, CGRectGetMaxY([UIApplication sharedApplication].statusBarFrame), 44, 44);
}

- (void)seePwdDidClick:(UIButton *)sender {
  sender.selected = !sender.selected;
  if (sender.selected) {
    [sender setImage: [UIImage imageNamed:@"seePWD.png"] forState:UIControlStateNormal];
    self.txtPwd.secureTextEntry = NO;
  }else{
    [sender setImage: [UIImage imageNamed:@"hidePWD.png"]forState:UIControlStateNormal];
    self.txtPwd.secureTextEntry = YES;
  }
}

- (void)userProvicyClick:(id)sender{
    PDWebViewController * vc = [[PDWebViewController alloc] init];
    vc.webUrl =  HTTPAPIWITH(@"/web/index/privacy");;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loginDidClick:(id)sender {
    NSString *phone = self.txtPhone.text;
    NSString *pwd = self.txtPwd.text;
    //可以填写5位的测试号码-----特殊处理
    if (phone.length == 11) {
        NSMutableDictionary * param = [NSMutableDictionary dictionary];
        [param setObject:phone forKey:@"phone"];
        [param setObject:pwd forKey:@"pwd"];
        [self HUDshowWithStatus:@""];
        [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_login) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
            [self HUDdismiss];
            if (!error) {
                UserInfo * user = [[UserInfo alloc] init];
                user.token = responseObject[@"data"];
                user.phone = phone;
                [[UserLocal sharedInstance] setCurrentUserInfo:user];
//                [[WebSocketManager shareInstance] setSocketOpen];
                if (self.isRemoteLogin) {
                    [getDelegate().masterNavigationController popToRootViewControllerAnimated:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    UINavigationController *naviController = getDelegate().masterNavigationController;
                    [naviController.viewControllers[0] setSelectedIndex:0];
                }else{
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGINSUCCESS object:nil];
                NSMutableDictionary * paramSocket = [NSMutableDictionary dictionary];
                paramSocket[@"macId"] = [SystemDeviceTool getUUID];
                paramSocket[@"userId"] = phone;
                paramSocket[@"flag"] = @(0);
                [[PLRequestManage sharedInstance] httpRequestWithUrl:WEBSOCKETWITH(url_loginWebsocket) Type:RequestPost parameters:paramSocket completeHandle:^(id  _Nullable responseObject, NSError *error) {
                    if (!error) {
                    }else{
                        
                    }
                }];
                [LocationManager sharedInstance];
            } else {
                [self HUDshowErrorWithStatus:error.localizedDescription];
            }
        }];
    }else{
        return [self HUDshowErrorWithStatus:@"填写正确手机号"];
    }
}
- (void)back{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

# pragma mark ===== 立即注册 ======
- (void) quickRegister {
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

# pragma mark ====== 忘记密码 ========
- (void) forgetPWD {
    ForgetViewController *forgetVC = [[ForgetViewController alloc] init];
    [self.navigationController pushViewController:forgetVC animated:YES];
}

@end
