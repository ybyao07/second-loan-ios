//
//  RegisterViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "RegisterViewController.h"
#import "UIView+JXTapEvent.h"
#import "NSString+Tools.h"
#import "PDWebViewController.h"
#import "UIImage+Stretch.h"

@interface RegisterViewController ()<UITextFieldDelegate>

@property (nonatomic, assign) CGFloat lastSendcodeTime;
@property (strong, nonatomic) UITextField *txtPhone;
@property (strong, nonatomic) UITextField *txtCode;
@property (strong, nonatomic) UITextField *txtPwd;
@property (strong, nonatomic) UITextField *txtRepeatNewPwd;
@property (strong, nonatomic) UITextField *txtCheckCode;
@property (strong, nonatomic) UIImageView *imgCheckCode;

@property (strong, nonatomic) UITextField *recPhone;

@property (strong, nonatomic) UIButton *loginBtn;
@property (strong, nonatomic) UIButton *checkBtn;

@property (strong, nonatomic) UIScrollView *scrollView;

@end

static CGFloat const lineSpace = 82.0f;

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViewForSignup];
    self.txtPwd.secureTextEntry = YES;
    self.txtRepeatNewPwd.secureTextEntry = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationBarBgView.hidden = YES;
    [self loadCheckCode];
}

- (void)loadCheckCode{
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_check_code) Type:RequestPost parameters:[NSMutableDictionary dictionary] completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        if (!error) {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:responseObject[@"data"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
            [self.imgCheckCode setImage:[UIImage imageWithData:decodedData]];
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

- (void)checkcodeDidClick:(UIButton *)sender {
    NSString * phone = [self.txtPhone text];
    if (phone.length != 11) return [self HUDshowErrorWithStatus:@"请填写正确手机号"];
    [self HUDshowWithStatus:@""];
    NSString *urlStr = [NSString stringWithFormat:@"%@?phone=%@",HTTPAPIWITH(url_send_code),phone];
    sender.enabled = NO;
    [[PLRequestManage sharedInstance] httpRequestWithUrl:urlStr Type:RequestGet parameters:[NSMutableDictionary dictionary] completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        sender.enabled = YES;
        if (!error) {
            [self waitNextSend];
            [self HUDshowSuccessWithStatus:@"验证码已发送"];
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

- (void)waitNextSend{
    self.lastSendcodeTime = [[NSDate date] timeIntervalSince1970];
    fQueryBtn(self.view, @"checkCodeBtn").enabled = NO;
    JLAsyncRun(^{
        while (YES) {
            CGFloat time = [[NSDate date] timeIntervalSince1970];
            if (time - self.lastSendcodeTime < 120) {
                JLAsyncRunInMain(^{
                    fQueryBtn(self.view, @"checkCodeBtn").fsText([NSString stringWithFormat:@"%@s",@((NSInteger)(120+self.lastSendcodeTime - time))]);
                });
            } else {
                JLAsyncRunInMain(^{
                    fQueryBtn(self.view, @"checkCodeBtn").enabled = YES;
                    fQueryBtn(self.view, @"checkCodeBtn").fsText(@"发送验证码");
                });
                break;
            }
            [NSThread sleepForTimeInterval:1];
        }
    });
}

- (void)setupViewForSignup
{
    self.view.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
        factory.imageView().fsImage(@"loginbg").fsImgContentMode(UIViewContentModeScaleAspectFill).fsFvid(@"bg_img");
        factory.scrollView().develop(^(FFactory * factory){
            factory.imageView().fsImage(@"top_log").fsFvid(@"topLog");
            factory.label().fsFontSize(15.0).fsText(@"手机号").fsTextColor(0x23242A).fsFvid(@"phoneLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"请输入手机号").fsLimit(@"11").fsFontSize(15.0).fsTextColor(0x23242A).fsKeyboard(UIKeyboardTypePhonePad).fsFvid(@"phoneField");
            factory.button().fsFontSize(15.0).fsText(@"获取验证码").fsTextColor(0xff4c2b).fsAlign(NSTextAlignmentRight).fsOnClick(checkcodeDidClick:).fsClip(YES).fsFvid(@"checkCodeBtn");
            
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine1");  factory.label().fsFontSize(15.0).fsText(@"手机验证码").fsTextColor(0x23242A).fsFvid(@"yanzhengLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"请输入验证码").fsLimit(@"6").fsFontSize(15.0).fsTextColor(0x23242A).fsKeyboard(UIKeyboardTypeNumberPad).fsFvid(@"cfield");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine2");
            //第三行
            factory.label().fsFontSize(15.0).fsText(@"密码").fsTextColor(0x23242A).fsFvid(@"pwdLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"8-16位数字字母组合密码").fsFontSize(15.0).fsTextColor(0x23242A).fsKeyboard(UIKeyboardTypeASCIICapable).fsFvid(@"pwdfield");
            factory.button().fsFontSize(15.0).fsImage(@"hidePWD").fsOnClick(seePwdDidClick:).fsFvid(@"seePwdCodeBtn");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine3");
            
            //第四行
            factory.label().fsFontSize(15.0).fsText(@"确认密码").fsTextColor(0x23242A).fsFvid(@"pwdRepeatLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"8-16位数字字母组合密码").fsFontSize(15.0).fsTextColor(0x23242A).fsFvid(@"pwdRepeatField");
            factory.button().fsImage(@"hidePWD").fsOnClick(seeRepeatPwdDidClick:).fsClip(YES).fsFvid(@"seeRepeatPwdCodeBtn");
            
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine4");
            
            //第五行
            factory.label().fsFontSize(15.0).fsText(@"图像验证码").fsTextColor(0x23242A).fsFvid(@"imagiLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"请输入图像验证码").fsLimit(@"4").fsFontSize(15.0).fsTextColor(0x23242A).develop(^(FFactory * factory){
            }).fsFvid(@"imgfield");
            factory.imageView().fsFvid(@"imagyz");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine5");
            
            //第6行
            factory.label().fsFontSize(15.0).fsText(@"推荐人手机号").fsTextColor(0x23242A).fsFvid(@"recLabel");
            factory.textField().fsClearButtonMode(UITextFieldViewModeWhileEditing).fsPlaceholder(@"请输入推荐人手机号").fsLimit(@"11").fsFontSize(15.0).fsTextColor(0x23242A).develop(^(FFactory * factory){
            }).fsFvid(@"recPhonefield");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"seperLine6");
            
            factory.button().fsFvid(@"btnCheck");
            factory.button().fsOnClick(checkClick).fsFvid(@"btnCheckBg");
            
            factory.label().fsFontSize(12.0).fsText(@"我已阅读并同意").fsFvid(@"hint");
            factory.button().fsFontSize(12.0).fsText(@"《用户使用协议》").fsTextColor(0x4E8CF3).fsOnClick(oneRuleAction).fsFvid(@"oneRule");
            factory.button().fsFontSize(12.0).fsText(@"《惠枣庄平台隐私政策》").fsTextColor(0x4E8CF3).fsOnClick(twoRuleAction).fsFvid(@"twoRule");
            factory.button().fsFontSize(18.0).fsText(@"注册").fsTextColor(0xFFFFFF).fsOnClick(registerClick:).fsBackGroundImage(@"btn_bg").fsClip(YES).fsRadius(3.3).fsFvid(@"loginBtn");
        }).fsFvid(@"scrollView");
          factory.button().fsOnClick(closeAction:).fsFontSize(15.0).fsImage(@"back-black").fsFvid(@"fClose");
        factory.button().fsOnClick(closeAction:).fsFontSize(15.0).fsFvid(@"fClose2");
    });
    [fQuery(self.view, @"scrollView") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    self.scrollView = fQueryScroll(self.view, @"scrollView");
    // backgroundImg
    [fQuery(self.view, @"bg_img") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    // log
    [fQuery(self.scrollView, @"topLog") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView.mas_top).with.offset(KStatusBarAndNavigationBarHeight + 30);
        make.left.equalTo(self.scrollView).with.offset(92/750.0 * SCREEN_WIDTH);
        make.height.equalTo(@(80/750.0 * SCREEN_WIDTH));
    }];
    //第一行
    [fQuery(self.scrollView, @"seperLine1") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"topLog").mas_bottom).with.offset(90);
        make.left.equalTo(self.scrollView).with.offset(92/750.0 * SCREEN_WIDTH);
//        make.right.equalTo(self.scrollView).with.offset(-92/750.0 * SCREEN_WIDTH);
        make.width.equalTo(@(SCREEN_WIDTH - 2 * 92/750.0 * SCREEN_WIDTH));
        make.height.equalTo(@1);
    }];
    [fQuery(self.scrollView, @"phoneField") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine1").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine1"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine1")).with.offset(-85);
    }];
    [fQuery(self.scrollView, @"checkCodeBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"phoneField"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine1"));
        make.width.equalTo(@90);
        make.height.equalTo(@22);
    }];
    [fQuery(self.scrollView, @"phoneLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"phoneField").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine1"));
    }];
    [fQuery(self.scrollView, @"seperLine2") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine1").mas_bottom).with.offset(lineSpace);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine1"));
        make.width.equalTo(fQuery(self.scrollView, @"seperLine1"));
        make.height.equalTo(@1);
    }];
    [fQuery(self.scrollView, @"cfield") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine2").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine2"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine2"));
    }];
    [fQuery(self.scrollView, @"yanzhengLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"cfield").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine2"));
    }];
    [fQuery(self.scrollView, @"seperLine3") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine2").mas_bottom).with.offset(lineSpace);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine2"));
        make.width.equalTo(fQuery(self.scrollView, @"seperLine2"));
        make.height.equalTo(@1);
    }];
    [fQuery(self.scrollView, @"pwdfield") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine3").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine3"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine3")).with.offset(-30);;
    }];

    [fQuery(self.scrollView, @"seePwdCodeBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"pwdfield"));
        make.right.equalTo(fQuery(self.scrollView, @"pwdfield")).with.offset(30);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];
    [fQuery(self.scrollView, @"pwdLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"pwdfield").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine3"));
    }];
    [fQuery(self.scrollView, @"seperLine4") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine3").mas_bottom).with.offset(lineSpace);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine3"));
        make.width.equalTo(fQuery(self.scrollView, @"seperLine3"));
        make.height.equalTo(@1);
    }];
    // 第四行
    [fQuery(self.scrollView, @"pwdRepeatField") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine4").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine4"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine4")).with.offset(-30);;
    }];
    
    [fQuery(self.scrollView, @"seeRepeatPwdCodeBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"pwdRepeatField"));
        make.right.equalTo(fQuery(self.scrollView, @"pwdRepeatField")).with.offset(30);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];
    [fQuery(self.scrollView, @"pwdRepeatLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"pwdRepeatField").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine4"));
    }];
    
    [fQuery(self.scrollView, @"seperLine5") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine4").mas_bottom).with.offset(lineSpace);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine4"));
        make.width.equalTo(fQuery(self.scrollView, @"seperLine4"));
        make.height.equalTo(@1);
    }];
    
    [fQuery(self.scrollView, @"imgfield") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine5").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine5"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine5")).with.offset(-100);
    }];
    [fQuery(self.scrollView, @"imagyz") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"imgfield"));
        make.right.equalTo(fQuery(self.scrollView, @"imgfield")).with.offset(100);
        make.width.equalTo(@80);
        make.height.equalTo(@22);
    }];
    [fQuery(self.scrollView, @"imagiLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"imgfield").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine5"));
    }];
    
    [fQuery(self.scrollView, @"seperLine6") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine5").mas_bottom).with.offset(lineSpace);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine5"));
        make.width.equalTo(fQuery(self.scrollView, @"seperLine5"));
        make.height.equalTo(@1);
    }];
    
    [fQuery(self.scrollView, @"recPhonefield") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"seperLine6").mas_top).with.offset(8);
        make.height.equalTo(@48);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine6"));
        make.right.equalTo(fQuery(self.scrollView, @"seperLine6")).with.offset(-100);
    }];
    [fQuery(self.scrollView, @"recLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(fQuery(self.scrollView, @"recPhonefield").mas_top).with.offset(4);
        make.height.equalTo(@25);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine6"));
    }];
    

    [fQuery(self.scrollView, @"btnCheck") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine6").mas_bottom).with.offset(20);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine6")).with.offset(-2);
        make.width.equalTo(@12);
        make.height.equalTo(@12);
    }];
    
    [fQuery(self.scrollView, @"btnCheckBg") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.scrollView, @"seperLine6").mas_bottom).with.offset(10);
        make.left.equalTo(fQuery(self.scrollView, @"seperLine6")).with.offset(-12);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    [fQuery(self.scrollView, @"hint") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"btnCheck"));
        make.left.equalTo(fQuery(self.scrollView, @"btnCheck").mas_right).with.offset(2);
    }];
    [fQuery(self.scrollView, @"oneRule") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"btnCheck"));
        make.left.equalTo(fQuery(self.scrollView, @"hint").mas_right).with.offset(-6);
    }];
    [fQuery(self.scrollView, @"twoRule") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self.scrollView, @"btnCheck"));
        make.left.equalTo(fQuery(self.scrollView, @"oneRule").mas_right).with.offset(-6);
    }];
    [fQuery(self.scrollView, @"loginBtn") mas_makeConstraints:^(MASConstraintMaker *make) {
      make.top.equalTo(fQuery(self.scrollView, @"btnCheck").mas_bottom).with.offset(20);
      make.left.equalTo(fQuery(self.scrollView, @"btnCheck"));
      make.width.equalTo(fQuery(self.scrollView, @"seperLine6"));
      make.height.equalTo(@48);
    }];
    [fQuery(self.view, @"fClose") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(10+statusBarHeight);
        make.left.equalTo(self.view.mas_left).with.offset(20);
        make.width.equalTo(@16);
        make.height.equalTo(@28);
    }];
    [fQuery(self.view, @"fClose2") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.width.equalTo(@100);
        make.height.equalTo(@100);
    }];
    self.txtPhone = fQueryField(self.view, @"phoneField");
    self.txtCode = fQueryField(self.view, @"cfield");
    self.txtPwd = fQueryField(self.view, @"pwdfield");
    self.txtRepeatNewPwd = fQueryField(self.view, @"pwdRepeatField");
    self.txtRepeatNewPwd.delegate = self;
    [self.txtPwd setValue:@"16" forKey:@"limit"];
    [self.txtRepeatNewPwd setValue:@"16" forKey:@"limit"];

    self.txtCheckCode = fQueryField(self.view, @"imgfield");
    self.imgCheckCode = fQueryImgv(self.view, @"imagyz");
    
    self.recPhone = fQueryField(self.view, @"recPhonefield");
    
    self.checkBtn = fQueryBtn(self.view, @"btnCheck");
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 780);
    self.scrollView.bounces = NO;
    [self.imgCheckCode addTapOneTarget:self action:@selector(loadCheckCode)];
    [self.checkBtn setImage:[UIImage imageNamed:@"icon_check_red"] forState:UIControlStateSelected];
    [self.checkBtn setImage:[UIImage imageNamed:@"icon_uncheck_gray"] forState:UIControlStateNormal];
    self.loginBtn = fQueryBtn(self.view, @"loginBtn");
    [self.loginBtn setBackgroundImage:[UIImage createImageWithColor:COLOR_Gary_F3] forState:UIControlStateDisabled];
    self.loginBtn.enabled = NO;
}


- (void)checkClick{
     self.checkBtn.selected = !self.checkBtn.selected;
    if (self.checkBtn.isSelected) {
        [self.loginBtn setBackgroundImage:[UIImage imageNamed:@"btn_bg"] forState:UIControlStateDisabled];
        self.loginBtn.enabled = YES;
    }else{
        [self.loginBtn setBackgroundImage:[UIImage createImageWithColor:COLOR_Gary_F3] forState:UIControlStateDisabled];
        self.loginBtn.enabled = NO;
    }
}

- (void)oneRuleAction{
    PDWebViewController * vc = [[PDWebViewController alloc] init];
    vc.webUrl =  url_private_protocal;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)twoRuleAction{
    PDWebViewController * vc = [[PDWebViewController alloc] init];
    vc.webUrl =  url_private_polity;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)seeRepeatPwdDidClick:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
        [sender setImage: [UIImage imageNamed:@"seePWD.png"]forState:UIControlStateNormal];
        self.txtRepeatNewPwd.secureTextEntry = NO;
    }else{
        [sender setImage: [UIImage imageNamed:@"hidePWD.png"]forState:UIControlStateNormal];
        self.txtRepeatNewPwd.secureTextEntry = YES;
    }
}

- (void)seePwdDidClick:(UIButton *)sender {
  sender.selected = !sender.selected;
  if (sender.selected) {
    [sender setImage: [UIImage imageNamed:@"seePWD.png"]forState:UIControlStateNormal];
    self.txtPwd.secureTextEntry = NO;
  }else{
    [sender setImage: [UIImage imageNamed:@"hidePWD.png"]forState:UIControlStateNormal];
    self.txtPwd.secureTextEntry = YES;
  }
}
- (void)closeAction:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)registerClick:(UIButton *)sender{
    if (isEmptyString(self.txtCode.text)) {
        [self HUDshowErrorWithStatus:MsgHintVerifyCodeEmpty];
        return;
    }
    if (isEmptyString(self.txtPwd.text)) {
        [self HUDshowErrorWithStatus:MsgHintPasswordNeed];
        return;
    }
    if (isEmptyString(self.txtRepeatNewPwd.text)) {
        [self HUDshowErrorWithStatus:MsgHintPasswordSureNeed];
        return;
    }
    if (isEmptyString(self.txtCheckCode.text)) {
        [self HUDshowErrorWithStatus:MsgHintVerifyImgCodeEmpty];
        return;
    }
    if (!isEmptyString(self.recPhone.text)) {
        if (self.recPhone.text.length != 11) return [self HUDshowErrorWithStatus:@"请正确填写推荐人手机号"];
    }
    NSString *pwd = self.txtPwd.text;
    NSString *pwdRepeat = self.txtRepeatNewPwd.text;
    //检验密码是否符合规则
    if (self.txtPwd.text.length > 0) { //
        if(![self.txtPwd.text judgePassWordLegal]) {
            [self HUDshowErrorWithStatus:MsgHintPasswordValid];
            return;
        }
        if (![pwd isEqualToString:pwdRepeat]) {
            [self HUDshowErrorWithStatus:@"两次新密码不相同"];
            return;
        }
    }
    if (!self.checkBtn.isSelected) {
        [self HUDshowErrorWithStatus:@"请阅读并接受贷款协议"];
        return;
    }
   //调用接口 -- 如果注册成功直接调用登录接口进行登录 -- 登录成功后 回到我的首页
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:self.txtPhone.text forKey:@"phone"];
    [param setObject:self.txtPwd.text forKey:@"pwd"];
    [param setObject:self.txtCode.text forKey:@"messagecode"];
    [param setObject:self.txtCheckCode.text forKey:@"imgcode"];
    [param setObject:self.recPhone.text forKey:@"telPhone"];
    sender.enabled = NO;
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_register) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        sender.enabled = YES;
        [self HUDdismiss];
        if (!error) {
          [self HUDshowErrorWithStatus:@"注册成功"];
          [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}
#pragma mark === UITextFieldDelegate ====
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(![textField.text isEqualToString:self.txtPwd.text]){
        [self HUDshowErrorWithStatus:@"两次输入密码不一致"];
    }
}

@end
