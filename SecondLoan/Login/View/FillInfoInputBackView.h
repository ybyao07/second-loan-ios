//
//  FillInfoInputBackView.h
//  SuYue
//
//  Created by Win10 on 2018/12/4.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FillInfoInputBackView : UIView

@property (nonatomic,strong) UILabel * label;
@property (nonatomic,strong) UITextField * textField;


- (UITextField * (^)(NSString *))fsTitle;
- (UITextField * (^)(NSString *))fsText;
- (UITextField * (^)(UIKeyboardType))fsKeyboard;
- (UITextField * (^)(NSString*,NSInteger,CGFloat))fsPText;
@end

NS_ASSUME_NONNULL_END
