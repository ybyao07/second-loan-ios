//
//  PHMineHeaderView.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/22.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "PHMineHeaderView.h"
#import "UserInfo.h"
#import "UIView+JXTapEvent.h"
#import "PDWebViewController.h"
#import "NSString+Tools.h"

@interface PHMineHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *eduLabel;  //普惠快贷额度
@property (weak, nonatomic) IBOutlet UILabel *eduRMBLabel;
@property (weak, nonatomic) IBOutlet UILabel *jineLabel; // 7日代还金额
@property (weak, nonatomic) IBOutlet UILabel *jineRMBLbel;
@property (weak, nonatomic) IBOutlet UILabel *yueLabel; // 借款余额
@property (weak, nonatomic) IBOutlet UILabel *yueRMBLabel;
@property (weak, nonatomic) IBOutlet UIView *line1View;
@property (weak, nonatomic) IBOutlet UIView *line2View;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rzImg;
@property (weak, nonatomic) IBOutlet UIImageView *company_log;

@property (weak, nonatomic) IBOutlet UIButton *applyBtn;

@property (strong, nonatomic) NSMutableArray *arrayMoneyViews;

@end

@implementation PHMineHeaderView

+ (instancetype)newView
{
    NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"PHMineHeaderView" owner:self options:nil];
    PHMineHeaderView * infoView = (PHMineHeaderView *)[views firstObjectWithClass:self];
    [infoView.company_log addTapOneTarget:infoView action:@selector(logoTapped)];
    [infoView.avatarImgView addTapOneTarget:infoView action:@selector(avatarClicked)];
    [infoView.rzImg addTapOneTarget:infoView action:@selector(authClicked)];
    infoView.avatarImgView.layer.cornerRadius = CGRectGetWidth(infoView.avatarImgView.bounds)/2.0;
    [infoView setDefault];
    return infoView;
}

- (void)logoTapped{
    [self.delegate logoTapped];
}

- (void)avatarClicked {
    if (![UserLocal isLogin]) { //未登录
        [self.delegate login];
    }else{
        if (self.delegate) {
            [self.delegate goSetAvatar];
        }
    }
}
- (void)authClicked{
    if (self.delegate) {
        [self.delegate showPersonalInfo];
    }
}

- (IBAction)settingClicked:(id)sender {
    if (self.delegate) {
        [self.delegate goSetting];
    }
}

- (IBAction)loginBtnTapped:(id)sender {
    [self.delegate login];
}

- (IBAction)applyBtnTapped:(id)sender {
    [self.delegate apply];
}
- (void)showAccountInfo:(BOOL)show {
    self.nameLabel.hidden = !show;
    if (show) {
        self.nameLabel.text = @"--";
    }
    self.phoneLabel.hidden = !show;
    self.rzImg.hidden = !show;
    self.phoneLabel.text = [[UserLocal sharedInstance] currentUserInfo].phone;
    self.avatarImgView.image = [UIImage imageNamed:@"mine_moren"];
}

- (void)hideApplyBtn {
    self.applyBtn.hidden = YES;
}

- (void)showApplyBtn {
    self.applyBtn.hidden = NO;
}

- (void)hideLoginBtn {
    self.loginOrNameBtn.hidden = YES;
}

- (void)showLoginBtn {
    self.loginOrNameBtn.hidden = NO;
}

- (void)showMoneyInfoView:(BOOL)show {
    self.eduLabel.hidden = !show;
    self.eduRMBLabel.hidden = !show;
    self.jineLabel.hidden = !show;
    self.jineRMBLbel.hidden = !show;
    self.yueLabel.hidden = !show;
    self.yueRMBLabel.hidden = !show;
    self.line1View.hidden = !show;
    self.line2View.hidden = !show;
}

- (void)setDefault {
    [self showMoneyInfoView:YES];
    self.eduRMBLabel.attributedText = [self fromString:@"0.00元"];
    self.jineRMBLbel.attributedText = [self fromString:@"0.00元"];
    self.yueRMBLabel.attributedText = [self fromString:@"0.00元"];
    [self showAccountInfo:NO];
    [self hideApplyBtn];
    [self showLoginBtn];
    _rzImg.hidden = YES;
    _avatarImgView.image = [UIImage imageNamed:@"mine_moren"];
}



- (NSAttributedString *)fromString:(NSString *)str {
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc]initWithString:str];

    NSDictionary * attris = @{NSBaselineOffsetAttributeName:@(-1),

                              NSFontAttributeName:Font18Bold};
    [attributedString setAttributes:attris range:NSMakeRange(0, str.length-1)];

//    NSMutableAttributedString *textFont = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedString addAttribute:NSFontAttributeName
                     value:Font18Bold
                     range:NSMakeRange(0, str.length-1)];
    return attributedString;
}
- (void)setAuthStatus:(BOOL)isAuth
{
    if (isAuth) {
        _rzImg.image = [UIImage imageNamed:@"yirenzheng"] ;
    } else {
        _rzImg.image = [UIImage imageNamed:@"weirenzheng"] ;
    }
}

- (void)setDefaultLogin{
    [self showMoneyInfoView:NO];
    [self showApplyBtn];
    [self showAccountInfo:YES];
    [self hideLoginBtn];
}

- (void)setMineModel:(UserMineInfo *)mineModel{
    _mineModel = mineModel;
    [self hideLoginBtn];
    [self showAccountInfo:YES];
    if (mineModel.quota == 0 && mineModel.returnMoney == 0 && mineModel.borrowBalance == 0) {
        [self showApplyBtn];
        [self showMoneyInfoView:NO];
    }else{
        [self hideApplyBtn];
        [self showMoneyInfoView:YES];
        self.eduRMBLabel.attributedText = [self fromString:[NSString stringWithFormat:@"%@元", [NSString getMoneyStringWithMoneyNumber:mineModel.quota]]];
        self.jineRMBLbel.attributedText = [self fromString:[NSString stringWithFormat:@"%@元", [NSString getMoneyStringWithMoneyNumber:mineModel.returnMoney]]];
        self.yueRMBLabel.attributedText = [self fromString:[NSString stringWithFormat:@"%@元", [NSString getMoneyStringWithMoneyNumber:mineModel.borrowBalance]]];
    }
    self.nameLabel.text = mineModel.username;
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:mineModel.faceImage] placeholderImage:[UIImage imageNamed:@"mine_moren"] options:SDWebImageRefreshCached];
//    [self.avatarImgView setImageURL:[NSURL URLWithString:mineModel.faceImage]];
    _rzImg.hidden = NO;
    _rzImg.image = [self.mineModel isAuthorized]? [UIImage imageNamed:@"yirenzheng"] : [UIImage imageNamed:@"weirenzheng"] ;
}
- (void)setMoneyModel:(MyMoneyRangeModel *)moneyModel{
    _moneyModel = moneyModel;
}



@end
