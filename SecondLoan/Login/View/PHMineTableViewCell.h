//
//  PHMineTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/22.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PHMineTableViewCell : UITableViewCell

- (void)bindData: (NSString *)imgName title: (NSString *)title;

@end

NS_ASSUME_NONNULL_END
