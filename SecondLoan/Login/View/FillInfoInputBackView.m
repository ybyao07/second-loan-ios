//
//  FillInfoInputBackView.m
//  SuYue
//
//  Created by Win10 on 2018/12/4.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "FillInfoInputBackView.h"

@implementation FillInfoInputBackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.develop(^(FFactory * factory){
            factory.label().fsTextColor(0x23242A).fsFontSize(15).fsFvid(@"1312hdks_title");
            factory.textField().fsFontSize(15.0).fsTextColor(0x23242A).fsFvid(@"nickField").fsFvid(@"1312hdks_textField");
            factory.view().fsBackGroundColor(0xE5E5E5).fsFvid(@"1312hdks_bottomline");
        });
    }
    
    self.label = fQueryLbl(self, @"1312hdks_title");
    self.textField = fQueryField(self, @"1312hdks_textField");
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(16);
        make.centerY.equalTo(self);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(96);
        make.right.equalTo(self).with.offset(-16);
        make.centerY.equalTo(self);
    }];
    [fQuery(self, @"1312hdks_bottomline") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(16);
        make.right.equalTo(self).with.offset(-16);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
    return self;
}




- (UITextField * (^)(NSString *))fsTitle {
    return ^id(NSString * content) {
        self.label.text = content;
        return self;
    };
}
- (UITextField * (^)(NSString *))fsText {
    return ^id(NSString * content) {
        self.textField.text = content;
        return self;
    };
}

- (UITextField * (^)(UIKeyboardType))fsKeyboard
{
    return ^id(UIKeyboardType content) {
        self.textField.keyboardType = content;
        return self;
    };
}

- (UITextField * (^)(NSString*,NSInteger,CGFloat))fsPText {
    return ^id(NSString* text, NSInteger hexValue,CGFloat fontsize) {
        self.textField.fsPText(text,hexValue,fontsize);
        return self;
    };
}
@end
