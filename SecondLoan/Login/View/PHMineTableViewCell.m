//
//  PHMineTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/22.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "PHMineTableViewCell.h"

@interface PHMineTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation PHMineTableViewCell

//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//  self = [super initWithCoder:coder];
//  if (self) {
//    [[NSBundle mainBundle] loadNibNamed:@"PHMineTableViewCell" owner:self options:nil];
//  }
//  return self;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)bindData: (NSString *)imgName title: (NSString *)title {
  self.imgView.image = [UIImage imageNamed:imgName];
  self.imgView.contentMode = UIViewContentModeScaleAspectFit;
  self.titleLabel.text = title;
}



@end
