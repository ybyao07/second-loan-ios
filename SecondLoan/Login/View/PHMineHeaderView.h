//
//  PHMineHeaderView.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/22.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserMineInfo.h"
#import "MyMoneyRangeModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PHMineHeaderViewDelegate <NSObject>

- (void)login;
- (void)apply;
- (void)showPersonalInfo;
- (void)goSetting;
- (void)goSetAvatar;
- (void)logoTapped;

@end


@interface PHMineHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *loginOrNameBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImgView;

@property (weak, nonatomic) id<PHMineHeaderViewDelegate>delegate;

@property (strong, nonatomic) UserMineInfo *mineModel;
@property (strong, nonatomic) MyMoneyRangeModel *moneyModel;

- (void)setDefault;
- (void)setDefaultLogin;

- (void)setAuthStatus:(BOOL)isAuth;


+ (instancetype)newView;



@end

NS_ASSUME_NONNULL_END
