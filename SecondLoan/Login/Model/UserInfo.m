//
//  UserInfo.m
//  mobilePD
//
//  Created by wangyan on 2016－05－12.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
//- (NSString *)realname{
//    return [_realname stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//}
@end
@implementation UserLocal
@synthesize  currentUserInfo = _currentUserInfo ;

+ (instancetype)sharedInstance{
    static UserLocal *sharedUserInfo;
    static dispatch_once_t onceAction;
    dispatch_once(&onceAction, ^{
        sharedUserInfo =  [[[self class]alloc]init];
//        [NOTIFICATIONCENTER addObserver:sharedUserInfo selector:@selector(logoutHandle:) name:NOTIFICATION_LOGOUT object:nil];
    });
    return sharedUserInfo;
}
+ (BOOL)isLogin
{
    if ([[self sharedInstance] currentUserInfo] && [[[self sharedInstance] currentUserInfo] token] != nil && ![[[[self sharedInstance] currentUserInfo] token] isEqualToString:@""] ) {
        return YES;
    }
    return NO;
}


+ (NSString *)token{
    NSString *token;
    if ([[self sharedInstance] currentUserInfo]) {
        token =  [[[self sharedInstance] currentUserInfo] token];
    }
    return token!=nil ? token: @"";
}
//+ (NSString *)uId{
//    if ([[self sharedInstance] currentUserInfo]) {
//        return [[[self sharedInstance] currentUserInfo] uid];
//    }
//    return @"";
//}

- (void)setCurrentUserInfo:(UserInfo *)currentUserInfo
{
    _currentUserInfo = currentUserInfo;
    if (currentUserInfo) {
        [USERDEFAULT setObject:[self.currentUserInfo mj_JSONObject] forKey:@"LocalUserInfo"];
    } else {
        [USERDEFAULT removeObjectForKey:@"LocalUserInfo"];
    }
    
    [USERDEFAULT synchronize];
}
- (UserInfo *)currentUserInfo
{
    if (!_currentUserInfo) {
        NSString * us = [USERDEFAULT objectForKey:@"LocalUserInfo"];
        _currentUserInfo = [UserInfo mj_objectWithKeyValues:us];
    }
    return _currentUserInfo;
}

- (void)logoutHandle:(id)object
{
//    [StatisticsTool profileSignOff];
    [self setCurrentUserInfo:nil];
}



@end
