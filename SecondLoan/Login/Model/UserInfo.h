//
//  UserInfo.h
//  mobilePD
//
//  Created by wangyan on 2016－05－12.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserInfo : NSObject

@property (nonatomic,strong) NSString * token;

@property (nonatomic,strong) NSString * uid;
@property (nonatomic,strong) NSString * phone;
//@property (nonatomic,strong) NSString * head;

//@property (nonatomic, copy) NSString *isHaveApplyed; //是否已授权额度
//@property (nonatomic, copy) NSString *isVerified; //是否认证成功了

@end

@interface UserLocal : NSObject

+ (BOOL)isLogin;
+ (NSString *)token;
+ (id)sharedInstance;
//+ (NSString *)uId;

@property (nonatomic,strong) UserInfo * currentUserInfo;

@end
