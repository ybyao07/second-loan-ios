//
//  UserMineInfo.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMineInfo : NSObject
@property (nonatomic, copy) NSString *faceImage;        // 头像
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, assign) NSInteger status;         // 认证状态 1:已认证 2:未认证
@property (nonatomic, assign) NSInteger quota;           //普惠贷贷款额度
@property (nonatomic, assign) NSInteger returnMoney;     //7日待还金额
@property (nonatomic, assign) NSInteger borrowBalance;       //借款余额
//@property (nonatomic, assign) NSInteger quotaStatus;     //1：待审核  2：审核中 3：已放款 4：审批未通过

//- (BOOL) isAppliedSuccess;

- (BOOL) isAuthorized;

@end

NS_ASSUME_NONNULL_END
