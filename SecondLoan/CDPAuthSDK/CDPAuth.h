//
//  CDPAuth.h
//  CDPAuth
//
//  Created by Wee Tom on 2018/3/19.
//  Copyright © 2018年 CDP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "STLivenessEnumType.h"

typedef void (^CDPHandler)(BOOL success, NSError * _Nullable error, NSDictionary * _Nullable result);
typedef void (^CDPInfoHandler)(BOOL success, NSError * _Nullable error, NSDictionary * _Nullable info);
typedef void (^CDPSTDeviceErrorHandler)(STIdDeveiceError errorCode);
typedef void (^CDPSTCancelHandler)(void);
typedef void (^CDPSTSuccessHandler)(NSArray<UIImage *> * _Nonnull images);
typedef void (^CDPSTFailedHandler)(LivefaceErrorType errorType);


@interface CDPAuth : NSObject

/**
 yesOrNo:是否开启测试模式，默认NO为正式服务器，YES为测试模式为测试服务器
 */
+ (void)enableTestMode:(BOOL)yesOrNo;

 /**
  初始化，获取membership和key信息
  membershipKey:认证序列号
  */
+ (void)initializeWithMembershipkey:(NSString *_Nullable)membershipKey
                            handler:(nullable CDPHandler)handler;

/**
 获取初始化时输入的membershipKey
 @return membershipKey
 */
+ (NSString *_Nullable)membershipKey;


/**
 OCR实名认证服务是否可用
 */
+ (BOOL)isProduct1Available;


/**
 手动输入实名认证服务是否可用
 */
+ (BOOL)isProduct2Available;


/**
 @param name 人名
 @param cardNumber 身份证号
 @param product 产品线，1：OCR方案或2：文字方案，不填直接返回失败处理
 */
+ (void)checkName:(nonnull NSString *)name
     idCardNumber:(nonnull NSString *)cardNumber
          product:(int)product
          handler:(nullable CDPHandler)handler;


/**
 调用OCR信息获取服务
 @param imageData 身份证照片
 */
+ (void)ocrIDCardWithImageData:(nonnull NSData *)imageData
                       handler:(nullable CDPInfoHandler)handler;

/**
 调用最终比对服务
 @param name 人名
 @param cardNumber 身份证号
 @param livenssImageData 活体识别照片，使用方法参考：NSString *livenssImageData = [UIImageJPEGRepresentation(image, 1.0)]
 @param product 产品线，1：OCR方案或2：文字方案，不填直接返回失败处理
 */
+ (void)finalLivenessCheckWithName:(nonnull NSString *)name
                      idCardNumber:(nonnull NSString *)cardNumber
                  livenssImageData:(nonnull NSData *)livenssImageData
                           product:(int)product
                           handler:(nullable CDPInfoHandler)handler;

//获取是否开启声音提示接口
+ (BOOL)isLivenessVoiceEnabled;

//设置是否开启活体检测声音提示接口，默认开启
//返回是否配置成功
+ (BOOL)enableVoiceOfLivenessDetect:(BOOL)isVoiceEnabled;

//获取当前活体检测顺序接口
+ (nonnull NSArray *)livenessDetectSeq;

/**
 设置活体检测顺序接口
 @param sequence 顺序数列，默认为以下参考：NSArray *sequence = @[@(LIVE_BLINK), @(LIVE_MOUTH), @(LIVE_NOD), @(LIVE_YAW)]
 @return 返回是否配置成功
 */
+ (BOOL)updateLivenessDectecSequence:(nonnull NSArray *)sequence;

//获取当前活体检测难度接口
+ (LivefaceComplexity)livenessDetectComplexity;

//设置活体检测难度接口，默认为LIVE_COMPLEXITY_NORMAL
//返回是否配置成功
+ (BOOL)updateLivenessDectecComplexity:(LivefaceComplexity)Complexity;

//获取当前活体检测超时
+ (float)livenessTimeout;

//设置活体检测超时时间，默认为10秒
//返回是否配置成功
+ (BOOL)updateLivenessTimeout:(float)timeout;

//获取当前活体检测是否进行眉毛遮挡的检测
+ (BOOL)isLivenessBrowOcclusionEnabled;

//设置活体检测是否进行眉毛遮挡的检测，默认为不检测
//返回是否配置成功
+ (BOOL)enableBrowOcclusionOfLivenessDetect:(BOOL)isBrowOcclusionEnabled;

/**
 调用ST活体验证服务
 @param viewController viewController
 @param cancelHandler 用户取消处理
 @param deviceErrorHandler 设备错误处理，返回一个STIdDeveiceError错误码，无相机权限或设备不再活跃
 @param successHandler 检测成功结果处理，返回一组UIImage照片
 @param failedhHandler 检测失败结果处理，返回一个LivefaceErrorType错误码，错误原因参照错误代码
 */
+ (void)presentLivenessControllerFrom:(nonnull UIViewController *)viewController
                        cancelHandler:(nullable CDPSTCancelHandler)cancelHandler
                   deviceErrorHandler:(nullable CDPSTDeviceErrorHandler)deviceErrorHandler
                        resultHandler:(nullable CDPSTSuccessHandler)successHandler
                        failedHanlder:(nullable CDPSTFailedHandler)failedhHandler;

//终结服务方法
+ (void)finish;
@end

