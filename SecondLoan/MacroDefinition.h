//
//  MacroDefinition.h
//  gaodun
//
//  Created by JET on 15/6/26.
//  Copyright (c) 2015年 &#38472;&#21531;. All rights reserved.
//

#ifndef PLPD_MacroDefinition_h
#define PLPD_MacroDefinition_h

#define OCR_APPKEY @"73b7052e-89a0-426f-8873-0b1f6c1cb866"

#define SCREEN_WIDTH                [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT                [[UIScreen mainScreen] bounds].size.height
#define HEADER_HEIGHT               ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0?64:44)
#define VIEW_BEGINY                 (7.0 > IOS_VERSION ? 0: 20)

// 判断是否是ipad
#define isiPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

#define IS_ABOVE_IPHONE5 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.width > 320.0f)
#define IS_IPHONE_6PLUS ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)
#define IS_IPHONE_6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size)) : NO)
#define IS_IPHONE_5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
// 判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
// 判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
// 判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)

#define W_IPHONE_5 320.0f
#define W_IPHONE_6 375.0f
#define W_IPHONE_PLUS 414.0f
#define H_IPHONE_PLUS 736.0f
#define H_IPHONE_6 667.0f
#define H_IPHONE_5 568.0f

#define SC(n) ((n) * [UIScreen mainScreen].bounds.size.width / W_IPHONE_6)
#define SCH(n) ((n) * [UIScreen mainScreen].bounds.size.height / H_IPHONE_6)


#define IS_BELOW_IOS_7 ([[[UIDevice currentDevice]systemVersion]floatValue] < 7.0)
#define IS_EQUAL_OR_ABOVE_IOS_7 ([[[UIDevice currentDevice]systemVersion]floatValue] >= 7.0)
#define IS_EQUAL_OR_ABOVE_IOS_8 ([[[UIDevice currentDevice]systemVersion]floatValue] >= 8.0)
#define IS_EQUAL_OR_ABOVE_IOS_9 ([[[UIDevice currentDevice]systemVersion]floatValue] >= 9.0)
/**
 *
 *  shortcut marcro define
 *
 */
#define APPDELEGATE                ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define NOTIFICATIONCENTER  ([NSNotificationCenter defaultCenter])
#define USERDEFAULT         ([NSUserDefaults standardUserDefaults])
#define FILEMANAGER         ([NSFileManager defaultManager])

/**
 *
 * 本地沙盒路径
 */
#define DocumentsSubDirectory(dir) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, \
NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:dir]

#define LibraryDirectory(dir) [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, \
NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:dir]

#define DownloadStorePath @"/GD_Tasks"
#define DownloadStoreAllPath DocumentsSubDirectory(DownloadStorePath)

//设备token
#define DeviceTokensCode64 ([USERDEFAULT stringForKey:kDeviceTokens]? :@"")

//时间宏
#define SecondsOfDay (24*60*60)
#define SecondsOfHour (60*60)
#define SecondsOfMinute (60)

#define FormateString(...) [NSString stringWithFormat:__VA_ARGS__]

#ifdef DEBUG
#	define DLog(fmt, ...) NSLog((@"%s #%d " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#	define DLog(...)
#endif

#define S(a) [NSString stringWithFormat:@"%@",a]

#define WeakSelf __weak typeof(self) weakSelf = self;
#define StrongSelf __strong typeof(self) strongSelf = weakSelf;


#define IS_LIUHAISCREEN ([[UIApplication sharedApplication] statusBarFrame].size.height == 44.0f)
#define statusBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height)

#define isEmptyString( str )  (( str == nil || str == NULL || [str isEqualToString:@""] || [[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ) ? YES : NO)

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenHeight ([UIScreen mainScreen].bounds.size.height)
#define K_iPhoneXStyle ((KScreenWidth == 375.f && KScreenHeight == 812.f ? YES : NO) || (KScreenWidth == 414.f && KScreenHeight == 896.f ? YES : NO))
#define KStatusBarAndNavigationBarHeight (K_iPhoneXStyle ? 88.f : 64.f)
#define KStatusBarHeight (K_iPhoneXStyle ? 44.f : 20.f)
#define KTabbarHeight (K_iPhoneXStyle ? 83.f : 49.f)
#define KMagrinBottom (K_iPhoneXStyle ? 34.f : 0.f)

//适配比例
#define KScaleWidth(width) ((width)*(KScreenWidth/375.f))
#define IsIphone6P          SCREEN_WIDTH==414
#define SizeScale           (IsIphone6P ? 1.5 : 1)
#define kFontSize(value)    value*SizeScale
#define kFont(value)        [UIFont systemFontOfSize:value*SizeScale]

#define VCQ(a) fQuery(self.view, a )
#define VCQQ(a,b) fQuery(fQuery(self.view, a ), b )
#define VCQQQ(a,b,c) fQuery(fQuery(fQuery(self.view, a ), b ), c )

#define kInfoEditHeiht 44

#endif
