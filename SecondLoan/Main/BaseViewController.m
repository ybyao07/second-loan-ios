//
//  BaseViewController.m
//  SuYue
//
//  Created by junlei on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "BaseViewController.h"
#import "SuyueCustomHudView.h"
#import "SuyueAutoConfirmHudView.h"

@interface BaseViewController ()

@end

@implementation BaseViewController
- (void)loadView
{
    [super loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.view addSubview:self.navigationBarBgView];
    self.navigationBarBgView.backgroundColor = COLOR_ThemeRed;
}

- (void)setNavigationTitle:(NSString *)title{
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, statusBarHeight, SCREEN_WIDTH - 80, 44)];
    labelTitle.text = title;
    labelTitle.font = [UIFont systemFontOfSize:18];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.textColor = COLOR_White;
    [self.navigationBarBgView addSubview:labelTitle];
}

- (void)setNavigationTitle:(NSString *)title color:(UIColor *)myColor{
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, statusBarHeight, SCREEN_WIDTH - 80, 44)];
    labelTitle.text = title;
    labelTitle.font = [UIFont systemFontOfSize:18];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.textColor = myColor;
    [self.navigationBarBgView addSubview:labelTitle];
}


- (void)setLeftButtonImage:(NSString *)imgUrl
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imgUrl] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    btn.mj_origin = CGPointMake(10, statusBarHeight + (44 - btn.size.height)/2.0);
    [self.navigationBarBgView addSubview:btn];
}

- (void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)navigationBarBgView
{
    if (!_navigationBarBgView) {
        _navigationBarBgView = [[UIView alloc] init];
        _navigationBarBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH,44 + statusBarHeight);
    }
    return _navigationBarBgView;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

#pragma mark - HUD
- (void)setHud:(MBProgressHUD *)hud
{
    if (_hud) {
        [_hud  hideAnimated:YES];
    }
    _hud = hud;
}

- (void)HUDshowWithStatus:(NSString*)string
{
    void (^block)(void) = ^{
        self.hud = [[MBProgressHUD alloc]initWithView:self.view];
        self.hud.removeFromSuperViewOnHide = YES;
        self.hud.label.text = string;
        self.hud.mode = MBProgressHUDModeIndeterminate;
        [self.view addSubview:self.hud];
        [self.hud showAnimated:YES];
    };
    if ([NSThread isMainThread]){
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}
- (void)HUDshowSuccessWithStatus:(NSString*)string
{
//    void (^block)(void) = ^{
//        self.hud = [[MBProgressHUD alloc]initWithView:self.view];
//        self.hud.removeFromSuperViewOnHide = YES;
//        self.hud.label.text = string;
//        self.hud.mode = MBProgressHUDModeText;
//        [self.view addSubview:self.hud];
//        [self.hud showAnimated:YES];
//        [self.hud hideAnimated:YES afterDelay:0.8 ];
//    };
//    if ([NSThread isMainThread]){
//        block();
//    } else {
//        dispatch_async(dispatch_get_main_queue(), block);
//    }
    WKBToastInfo(string);
}
- (void)HUDshowInfoWithStatus:(NSString*)string
{
    void (^block)(void) = ^{
        self.hud = [[MBProgressHUD alloc]initWithView:self.view];
        self.hud.removeFromSuperViewOnHide = YES;
        self.hud.label.text = string;
        self.hud.mode = MBProgressHUDModeText;
        [self.view addSubview:self.hud];
        [self.hud showAnimated:YES];
        [self.hud hideAnimated:YES afterDelay:0.8 ];
    };
    if ([NSThread isMainThread]){
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}
- (void)HUDshowErrorWithStatus:(NSString*)string
{
//    void (^block)(void) = ^{
//        self.hud = [[MBProgressHUD alloc]initWithView:self.view];
//        self.hud.removeFromSuperViewOnHide = YES;
//        self.hud.label.text = string;
//        self.hud.label.textColor = COLOR_White;
//        self.hud.mode = MBProgressHUDModeText;
//        self.hud.contentColor = COLOR_BlackText_33;
//        [self.view addSubview:self.hud];
//        [self.hud showAnimated:YES];
//        [self.hud hideAnimated:YES afterDelay:3 ];
//    };
//    if ([NSThread isMainThread]){
//        block();
//    } else {
//        dispatch_async(dispatch_get_main_queue(), block);
//    }
    WKBToastInfo(string);
}
- (void)HUDshowImage:(UIImage*)image status:(NSString*)string
{
    
}
- (void)HUDdismiss
{
    if ([NSThread isMainThread]){
        [self.hud hideAnimated:YES];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.hud hideAnimated:YES];
        });
    }
    
}
- (void)HUDdismissWithDelay:(NSTimeInterval)delay
{
    [self.hud hideAnimated:YES afterDelay:delay];
}
- (void)HUDshowProgress:(CGFloat)progress
{
    self.hud.mode = MBProgressHUDModeAnnularDeterminate;
    self.hud.progress = progress;
}
- (void)HUDshowProgress:(CGFloat)progress status:(NSString*)status
{
    self.hud.mode = MBProgressHUDModeAnnularDeterminate;
    self.hud.progress = progress;
    self.hud.label.text = status;
}

- (void)HUDshowCustom:(NSString *)title image:(NSString *)image
{
    [self HUDshowCustom:title subTitle:@"" image:image];
}
- (void)HUDshowCustom:(NSString *)title subTitle:(NSString *)subtitle image:(NSString *)image
{
    SuyueCustomHudView * hud = [[SuyueCustomHudView alloc] init];
    hud.label.text = title;
    hud.subLabel.text = subtitle;
    hud.imageView.image = [UIImage imageNamed:image];
    [hud popTo:self.view];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(),^{
        [hud dissmis];
    });
}
- (void)HUDshowConfirm:(NSString *)title
{
    SuyueAutoConfirmHudView * hud = [[SuyueAutoConfirmHudView alloc] init];
    hud.label.text = title;
    hud.label.numberOfLines = 0;
    hud.subLabel.text = @"";
    [hud popTo:nil];
}

- (void)hideKeyboardTap
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    tapGesture.numberOfTapsRequired = 1; //点击次数
    tapGesture.numberOfTouchesRequired = 1; //点击手指数
    [self.view addGestureRecognizer:tapGesture];
}
-(void)tapGesture:(id)sender
{
    [self.view endEditing:YES];
}
@end
