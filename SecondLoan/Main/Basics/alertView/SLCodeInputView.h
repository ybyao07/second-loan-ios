//
//  WKBPayChooseMessageView.h
//  WKCarInsurance
//
//  Created by sunyang on 17/8/16.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+JXExtentRange.h"

@class SLCodeInputView;

@protocol SLCodeInputViewDelegate <NSObject>

- (void)codeButtonClick:(NSString *_Nullable)fieldText completion:(void (^ __nullable)(void))completion;

- (void)codeAlertFirstBtnClick:(NSString *_Nullable)fieldText;

- (void)codeAlertSecondBtnClick:(NSString *_Nullable)fieldText view:(SLCodeInputView *)view completion:(void (^ __nullable)(NSString * message))completion;

@end

@interface SLCodeInputView : UIView

@property(weak, nonatomic) id <SLCodeInputViewDelegate> _Nullable delegate;
//@property (copy, nonatomic) NSString * _Nullable phone;
- (void)waitNextSend;

- (void)dismiss;

- (void)updateCodeAlertViewWithTitle:(NSString *_Nullable)title;

- (void)showShadowView:(BOOL)willShow;

@end
