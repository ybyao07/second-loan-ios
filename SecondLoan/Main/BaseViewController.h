//
//  BaseViewController.h
//  SuYue
//
//  Created by junlei on 2018/11/22.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController
@property (nonatomic, strong) UIView *navigationBarBgView;

- (void)setNavigationTitle:(NSString *)title;
- (void)setLeftButtonImage:(NSString *)imgUrl;
- (void)setNavigationTitle:(NSString *)title color:(UIColor *)myColor;

@property (nonatomic,strong) MBProgressHUD * hud;
- (void)HUDshowWithStatus:(NSString*)string;

- (void)HUDshowSuccessWithStatus:(NSString*)string;
- (void)HUDshowInfoWithStatus:(NSString*)string;
- (void)HUDshowErrorWithStatus:(NSString*)string;
- (void)HUDshowImage:(UIImage*)image status:(NSString*)string;
- (void)HUDdismiss;
- (void)HUDdismissWithDelay:(NSTimeInterval)delay;
- (void)HUDshowProgress:(CGFloat)progress;
- (void)HUDshowProgress:(CGFloat)progress status:(NSString*)status;
- (void)HUDshowCustom:(NSString *)title image:(NSString *)image;
- (void)HUDshowCustom:(NSString *)title subTitle:(NSString *)subtitle image:(NSString *)image;
- (void)HUDshowConfirm:(NSString *)title;

- (void)hideKeyboardTap;
@end

NS_ASSUME_NONNULL_END
