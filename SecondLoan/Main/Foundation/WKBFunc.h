//
//  WKBFunc.h
//  WKCarInsurance
//
//  Created by sunyang on 17/5/9.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <Foundation/Foundation.h>

//toast提示
FOUNDATION_EXPORT void WKBToastInfo(NSString *msg);
FOUNDATION_EXPORT void WKBToastInfoInView(NSString *msg,__kindof UIView *view);

//jump
FOUNDATION_EXPORT void WKBPushViewController(__kindof UIViewController *vc);

FOUNDATION_EXPORT void WKBPopViewController(void);

FOUNDATION_EXPORT void WKBPopToViewController(__kindof UIViewController *vc);

FOUNDATION_EXPORT void WKBPopToRooterViewController(void);

