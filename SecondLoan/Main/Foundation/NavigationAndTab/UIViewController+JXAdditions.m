//
//  UIViewController+JXAdditions.m
//  jiuxian
//
//  Created by WanChangjun on 15/12/28.
//  Copyright © 2015年 jiuxian.com. All rights reserved.
//

#import "UIViewController+JXAdditions.h"
#import "UIView+JXAddition.h"

@implementation UIViewController (JXAdditions)

- (void)pushMasterViewController:(UIViewController *)controller {
    [UIViewController pushMasterViewController:controller];
}

+ (void)pushMasterViewController:(UIViewController *)controller {
    [UIView pushMasterViewController:controller];
}

- (void)popMasterViewController; {
    [UIViewController popMasterViewController];
}

+ (void)popMasterViewController; {
    [UIView popMasterViewController];
}

- (void)popMasterToRootViewController {
    [UIViewController popMasterToRootViewController];
}

+ (void)popMasterToRootViewController {
    [UIView popMasterToRootViewController];
}

- (void)popToRootViewController {
    [UIViewController popToRootViewController];
}

+ (void)popToRootViewController {
    [UIView popToRootViewController];
}

- (void)popViewController {
    [UIView popMasterViewController];
}

- (void)pushViewController:(UIViewController *)controller {
    [UIViewController pushMasterViewController:controller];
}

- (void)popMasterToViewController:(UIViewController *)controller{
    [UIViewController popMasterToViewController:controller];
}

+ (void)popMasterToViewController:(UIViewController *)controller{
    [UIView popMasterToViewController:controller];
}

@end
