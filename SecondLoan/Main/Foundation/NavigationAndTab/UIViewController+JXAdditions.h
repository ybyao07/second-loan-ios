//
//  UIViewController+JXAdditions.h
//  jiuxian
//
//  Created by WanChangjun on 15/12/28.
//  Copyright © 2015年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface UIViewController (JXAdditions)

- (void)pushMasterViewController:(UIViewController *)controller;

- (void)popMasterViewController;

- (void)popViewController;

- (void)pushViewController:(UIViewController *)controller;

- (void)popMasterToViewController:(UIViewController *)controller;
@end
NS_ASSUME_NONNULL_END
