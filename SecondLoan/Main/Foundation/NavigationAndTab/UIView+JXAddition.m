//
//  UIView+JXAddition.m
//  jiuxian
//
//  Created by WanChangjun on 15/12/7.
//  Copyright © 2015年 jiuxian.com. All rights reserved.
//

#import "UIView+JXAddition.h"
#import "AppDelegate.h"

@implementation UIView (JXAddition)

- (UIImage *)snapshotImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

- (UIImage *)snapshotImageAfterScreenUpdates:(BOOL)afterUpdates {
    if (![self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        return [self snapshotImage];
    }
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:afterUpdates];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

- (NSData *)snapshotPDF {
    CGRect bounds = self.bounds;
    NSMutableData *data = [NSMutableData data];
    CGDataConsumerRef consumer = CGDataConsumerCreateWithCFData((__bridge CFMutableDataRef) data);
    CGContextRef context = CGPDFContextCreate(consumer, &bounds, NULL);
    CGDataConsumerRelease(consumer);
    if (!context) return nil;
    CGPDFContextBeginPage(context, NULL);
    CGContextTranslateCTM(context, 0, bounds.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    [self.layer renderInContext:context];
    CGPDFContextEndPage(context);
    CGPDFContextClose(context);
    CGContextRelease(context);
    return data;
}

- (void)setLayerShadow:(UIColor *)color offset:(CGSize)offset radius:(CGFloat)radius {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = radius;
    self.layer.shadowOpacity = 1;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (void)removeAllSubviews {
    //[self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    while (self.subviews.count) {
        [self.subviews.lastObject removeFromSuperview];
    }
}


- (UIViewController *)viewController {
    for (UIView *view = self; view; view = view.superview) {
        UIResponder *nextResponder = [view nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *) nextResponder;
        }
    }
    return nil;
}

- (CGFloat)visibleAlpha {
    if ([self isKindOfClass:[UIWindow class]]) {
        if (self.hidden) return 0;
        return self.alpha;
    }
    if (!self.window) return 0;
    CGFloat alpha = 1;
    UIView *v = self;
    while (v) {
        if (v.hidden) {
            alpha = 0;
            break;
        }
        alpha *= v.alpha;
        v = v.superview;
    }
    return alpha;
}

- (CGPoint)convertPoint:(CGPoint)point toViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *) self) convertPoint:point toWindow:nil];
        } else {
            return [self convertPoint:point toView:nil];
        }
    }

    UIWindow *from = [self isKindOfClass:[UIWindow class]] ? (id) self : self.window;
    UIWindow *to = [view isKindOfClass:[UIWindow class]] ? (id) view : view.window;
    if ((!from || !to) || (from == to)) return [self convertPoint:point toView:view];
    point = [self convertPoint:point toView:from];
    point = [to convertPoint:point fromWindow:from];
    point = [view convertPoint:point fromView:to];
    return point;
}

- (CGPoint)convertPoint:(CGPoint)point fromViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *) self) convertPoint:point fromWindow:nil];
        } else {
            return [self convertPoint:point fromView:nil];
        }
    }

    UIWindow *from = [view isKindOfClass:[UIWindow class]] ? (id) view : view.window;
    UIWindow *to = [self isKindOfClass:[UIWindow class]] ? (id) self : self.window;
    if ((!from || !to) || (from == to)) return [self convertPoint:point fromView:view];
    point = [from convertPoint:point fromView:view];
    point = [to convertPoint:point fromWindow:from];
    point = [self convertPoint:point fromView:to];
    return point;
}

- (CGRect)convertRect:(CGRect)rect toViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *) self) convertRect:rect toWindow:nil];
        } else {
            return [self convertRect:rect toView:nil];
        }
    }

    UIWindow *from = [self isKindOfClass:[UIWindow class]] ? (id) self : self.window;
    UIWindow *to = [view isKindOfClass:[UIWindow class]] ? (id) view : view.window;
    if (!from || !to) return [self convertRect:rect toView:view];
    if (from == to) return [self convertRect:rect toView:view];
    rect = [self convertRect:rect toView:from];
    rect = [to convertRect:rect fromWindow:from];
    rect = [view convertRect:rect fromView:to];
    return rect;
}

- (CGRect)convertRect:(CGRect)rect fromViewOrWindow:(UIView *)view {
    if (!view) {
        if ([self isKindOfClass:[UIWindow class]]) {
            return [((UIWindow *) self) convertRect:rect fromWindow:nil];
        } else {
            return [self convertRect:rect fromView:nil];
        }
    }

    UIWindow *from = [view isKindOfClass:[UIWindow class]] ? (id) view : view.window;
    UIWindow *to = [self isKindOfClass:[UIWindow class]] ? (id) self : self.window;
    if ((!from || !to) || (from == to)) return [self convertRect:rect fromView:view];
    rect = [from convertRect:rect fromView:view];
    rect = [to convertRect:rect fromWindow:from];
    rect = [self convertRect:rect fromView:to];
    return rect;
}

- (void)hideShadow {
    self.layer.shadowColor = [UIColor clearColor].CGColor;
}

- (void)shadowColor:(UIColor *)color shadowOffset:(CGSize)offset shadowRadius:(CGFloat)radius shadowOpacity:(CGFloat)opacity {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = radius;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.layer.bounds].CGPath;
}

- (void)cornerRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)color {
    CALayer *layer = [self layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:radius];
    [layer setBorderWidth:width];
    [layer setBorderColor:color.CGColor];
}

- (void)shadowColor:(UIColor *)shadowColor shadowOffset:(CGSize)offset shadowRadius:(CGFloat)sradius shadowOpacity:(CGFloat)opacity
       cornerRadius:(CGFloat)cradius borderWidth:(CGFloat)width borderColor:(UIColor *)borderColor {
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = sradius;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.layer.bounds].CGPath;
    self.layer.cornerRadius = cradius;
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
    self.layer.masksToBounds = YES;
}

- (void)shake {
    CAKeyframeAnimation *keyAn = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [keyAn setDuration:0.5f];
    NSArray *array = [[NSArray alloc] initWithObjects:
            [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x - 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x + 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x - 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x + 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x - 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x + 5, self.center.y)],
            [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
                    nil];
    [keyAn setValues:array];
    NSArray *times = [[NSArray alloc] initWithObjects:
            [NSNumber numberWithFloat:0.1f],
            [NSNumber numberWithFloat:0.2f],
            [NSNumber numberWithFloat:0.3f],
            [NSNumber numberWithFloat:0.4f],
            [NSNumber numberWithFloat:0.5f],
            [NSNumber numberWithFloat:0.6f],
            [NSNumber numberWithFloat:0.7f],
            [NSNumber numberWithFloat:0.8f],
            [NSNumber numberWithFloat:0.9f],
            [NSNumber numberWithFloat:1.0f],
                    nil];
    [keyAn setKeyTimes:times];
    [self.layer addAnimation:keyAn forKey:@"TextAnim"];
}

- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

- (CGFloat)centerY {
    return self.center.y;
}

- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)pushMasterViewController:(UIViewController *)controller {
    [UIView pushMasterViewController:controller];
}

+ (void)pushMasterViewController:(UIViewController *)controller {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc pushViewController:controller animated:YES];
}

- (void)popMasterViewController {
    [UIView popMasterViewController];
}

+ (void)popMasterViewController {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc popViewControllerAnimated:YES];
}

- (void)popViewController {
    [UIView popViewController];
}

+ (void)popViewController {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc popViewControllerAnimated:YES];
}

- (void)popMasterToViewController:(UIViewController *)controller{
    [UIView popMasterToViewController:controller];
}

+ (void)popMasterToViewController:(UIViewController *)controller{
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc popToViewController:controller animated:YES];
}

+ (void)popToRootViewController {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc popToRootViewControllerAnimated:YES];
}

- (void)pushViewController:(UIViewController *)controller {
    [UIView pushMasterViewController:controller];
}

+ (void)pushViewController:(UIViewController *)controller {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc pushViewController:controller animated:YES];
}

+ (void)popMasterToRootViewController {
    UINavigationController *nvc = getDelegate().masterNavigationController;
    [nvc popToRootViewControllerAnimated:YES];
}

@end
