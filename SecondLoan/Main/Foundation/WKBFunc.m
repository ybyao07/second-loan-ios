//
//  WKBFunc.m
//  WKCarInsurance
//
//  Created by sunyang on 17/5/9.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "WKBFunc.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "JXToast.h"


void WKBToastInfo(NSString *msg){
    
    if (!msg || [msg isEqualToString:@""]) {
        return;
    }
    [JXToast showUIToast:msg];
}

void WKBToastInfoInView(NSString *msg,__kindof UIView *view){
    
    if (!msg || [msg isEqualToString:@""]) {
        return;
    }
    [JXToast showUIToast:msg inView:view];
}

void WKBPushViewController(__kindof UIViewController *vc){
    if (getDelegate()) {
        [getDelegate().masterNavigationController pushMasterViewController:vc];
    }
}

void WKBPopViewController(){
    [UIView popMasterViewController];
}

void WKBPopToViewController(__kindof UIViewController *vc){
    [UIView popMasterToViewController:vc];
}

void WKBPopToRooterViewController(){
    [UIView popMasterToRootViewController];
}
