//
//  SLIndustryChoseView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLSingleChoseView.h"
#import "UIPickerView+FastConfig.h"

@interface SLSingleChoseView()<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,strong) NSArray * dataSource;
//@property (nonatomic,weak) UITableView * tableView;
@property (assign, nonatomic) NSInteger currentSelectedRow;
@property (weak, nonatomic) UIPickerView * pickerView;
@end

@implementation SLSingleChoseView

@synthesize contentView = _contentView;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadActorServices];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame data:(NSArray *)data title: (NSString *)title {
    self = [super initWithFrame:frame];
    if (self) {
        fQueryLbl(self.naviView, @"title").fsText(title);
        self.dataSource = data;
//        [self.tableView reloadData];
        [self.pickerView reloadAllComponents];
    }
    return self;
}
#pragma mark -- getter
- (UIView *)contentView
{
    if (!_contentView) {
//        fQueryLbl(self.naviView, @"title").fsText(@"选择服务");
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT * 0.6);
        [_contentView addSubview:self.naviView];
//        _contentView.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
//            factory.tableView().fsHandler(self).fsRegisterCellClass([UITableViewCell class]).fsSeparator(UITableViewCellSeparatorStyleNone).fsShowIndicator(NO).fsFvid(@"table");
//        });
        _contentView.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
            factory.pickerView().fsHandler(self).fsFvid(@"table");
        });
        self.pickerView = (UIPickerView *)fQuery(_contentView, @"table");
        self.naviView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 44);
        self.pickerView.frame = CGRectMake(0, self.naviView.frame.size.height, SCREEN_WIDTH, _contentView.bounds.size.height - self.naviView.frame.size.height);
//        self.tableView.allowsMultipleSelection = YES;
    }
    return _contentView;
}
- (void)submit
{
    if (self.enterBlock) {
        NSDictionary *dic = self.dataSource[_currentSelectedRow];
        self.enterBlock(dic[@"name"]);
    }
    if (self.enterRowBlock) {
        NSDictionary *dic = self.dataSource[_currentSelectedRow];
        self.enterRowBlock(dic[@"name"], dic[@"id"]);
    }
    if (self.enterWhichRowBlock) {
        NSDictionary *dic = self.dataSource[_currentSelectedRow];
        self.enterWhichRowBlock(dic[@"name"], dic[@"id"], _currentSelectedRow);
    }
    [self dissmis];
}
#pragma mark ---- UIPickerView  ---
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.dataSource.count;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 48;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary * data = self.dataSource[row];
    return data[@"name"];
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view{
//    UILabel * label = [view viewWithTag:1024];
//    if (label == nil) {
//        label = [[UILabel alloc] init];
//        label.fsTextColor(0x60303A).fsFontSize(14);
//        label.tag = 1024;
//        [view addSubview:label];
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.equalTo(view);
//        }];
//    }
//    NSDictionary * data = self.dataSource[row];
//    label.text = data[@"name"];
//    return view;
//}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _currentSelectedRow = row;
//    [self submit];
}

#pragma mark - tableView Delegate
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    [tableView deselectRowAtIndexPath:indexPath animated:NO];
////    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    _currentSelectedRow = indexPath.row;
//    [self submit];
//}
//#pragma mark - tableView DataSource
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return self.dataSource.count;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 48;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
//    UILabel * label = [cell viewWithTag:1024];
//    if (label == nil) {
//        label = [[UILabel alloc] init];
//        label.fsTextColor(0x60303A).fsFontSize(14);
//        label.tag = 1024;
//        [cell addSubview:label];
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.equalTo(cell);
//        }];
//    }
//    NSDictionary * data = self.dataSource[indexPath.row];
//    label.text = data[@"name"];
//    return cell;
//}

- (void)loadActorServices{
    self.dataSource = @[
                        @{
                            @"name":@"IT",
                            @"id":@"1"
                            },
                        @{
                            @"name":@"计算机",
                            @"id":@"2"
                            },
                        @{
                            @"name":@"通信",
                            @"id":@"3"
                            }
                        ];

//    [self.tableView reloadData];
}
@end
