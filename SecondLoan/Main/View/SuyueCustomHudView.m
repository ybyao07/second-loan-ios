//
//  SuyueCustomHudView.m
//  SuYue
//
//  Created by Win10 on 2018/12/13.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "SuyueCustomHudView.h"
@interface SuyueCustomHudView()

@property (nonatomic,assign) NSTimeInterval tapTime;
@property (nonatomic,assign) CGPoint tapPoint;

@property (nonatomic,assign) BOOL isDissmis;
@end

@implementation SuyueCustomHudView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.contentView];
        self.isDissmis = NO;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.width.equalTo(self).multipliedBy(0.8);
            make.height.equalTo(self.contentView.mas_width);
        }];
        
        self.tapAutoRemove = YES;
    }
    return self;
}

#pragma mark -- getter
- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.fsRadius(3.3);
        _contentView.develop(^(FFactory * factory){
            factory.imageView().fsFvid(@"789456432156-icon");
            factory.label().fsFontSize(19).fsTextColor(0x23242A).fsFvid(@"789456432156-label");
            factory.label().fsFontSize(15).fsTextColor(0xAAAAAB).fsFvid(@"789456432156-sublabel");
        });
        self.imageView = fQueryImgv(_contentView, @"789456432156-icon");
        self.label = fQueryLbl(_contentView, @"789456432156-label");
        self.subLabel = fQueryLbl(_contentView, @"789456432156-sublabel");
        WeakSelf;
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakSelf.contentView);
            make.centerY.equalTo(weakSelf.contentView).with.offset(-10);
        }];
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakSelf.contentView);
            make.top.equalTo(weakSelf.imageView.mas_bottom).with.offset(17);
        }];
        [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakSelf.contentView);
            make.top.equalTo(weakSelf.label.mas_bottom).with.offset(7);
        }];
    }
    return _contentView;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    
    if ([self isSubViewEditing]) {
        self.tapTime = 0;
        return;
    }
    self.tapTime = [[NSDate date] timeIntervalSince1970];
    self.tapPoint = point;
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    [self endEditing:YES];
    if ([[NSDate date] timeIntervalSince1970] - self.tapTime < 1)
    {
        if (self.tapAutoRemove && ABS(point.x - self.tapPoint.x) < 10 && ABS(point.y - self.tapPoint.y) < 10) {
            [self dissmis];
        }
    }
    
    
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
    [super touchesCancelled:touches withEvent:event];
}
- (void)dissmis
{
    if (!self.isDissmis) {
        self.isDissmis = YES;
        self.alpha = 1.0;
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}
- (void)popTo:(UIView *)superView
{
    if (superView == nil) {
        superView = [[UIApplication sharedApplication] keyWindow];
    }
    [superView addSubview:self];
    
    self.alpha = 0.0;
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1.0;
    }];
    
    
    
    NSLog(@"%@",NSStringFromCGRect(superView.bounds));
    self.frame = superView.bounds;
}

- (BOOL)isSubViewEditing
{
    return NO;
}
@end
