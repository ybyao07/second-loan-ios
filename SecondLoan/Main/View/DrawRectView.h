//
//  DrawRectView.h
//  SuYue
//
//  Created by junlei on 2019/3/24.
//  Copyright © 2019 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DrawRectView : UIView

@property (nonatomic,strong) void (^drwaRect)(UIView * view);

@end

NS_ASSUME_NONNULL_END
