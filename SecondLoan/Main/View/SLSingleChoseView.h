//
//  SLIndustryChoseView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/27.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYBaseChoseView.h"
NS_ASSUME_NONNULL_BEGIN

@interface SLSingleChoseView :SYBaseChoseView

@property (nonatomic,strong) void (^enterBlock)(NSString * iterm);


@property (nonatomic,strong) void (^enterRowBlock)(NSString * iterm, NSString * rowId);

@property (nonatomic,strong) void (^enterWhichRowBlock)(NSString * iterm, NSString * rowId, NSInteger whichRow);


- (instancetype)initWithFrame:(CGRect)frame data:(NSArray *)data title: (NSString *)title;

@end

NS_ASSUME_NONNULL_END
