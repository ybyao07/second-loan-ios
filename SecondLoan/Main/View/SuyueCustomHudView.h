//
//  SuyueCustomHudView.h
//  SuYue
//
//  Created by Win10 on 2018/12/13.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuyueCustomHudView : UIView

@property (nonatomic,assign) BOOL tapAutoRemove;
@property (nonatomic,strong) UIView * contentView;
@property (nonatomic,weak) UIImageView * imageView;
@property (nonatomic,weak) UILabel * label;
@property (nonatomic,weak) UILabel * subLabel;

- (void)popTo:(UIView *)superView;
- (void)dissmis;

@end

NS_ASSUME_NONNULL_END
