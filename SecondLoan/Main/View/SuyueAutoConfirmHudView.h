//
//  SuyueAutoConfirmHudView.h
//  SuYue
//
//  Created by 姚永波 on 2019/4/4.
//  Copyright © 2019 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuyueAutoConfirmHudView : UIView
@property (nonatomic,assign) BOOL tapAutoRemove;
@property (nonatomic,strong) UIView * contentView;
@property (nonatomic,weak) UIImageView * imageView;
@property (nonatomic,weak) UILabel * label;
@property (nonatomic,weak) UILabel * subLabel;

@property (nonatomic,weak) UIView * submitBtn;
@property (nonatomic,weak) UIView * cancelBtn;

- (void)popTo:(UIView *)superView;
- (void)dissmis;

//@property (nonatomic,strong) void (^enterBlock)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
