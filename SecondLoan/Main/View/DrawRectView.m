//
//  DrawRectView.m
//  SuYue
//
//  Created by junlei on 2019/3/24.
//  Copyright © 2019 Win10. All rights reserved.
//

#import "DrawRectView.h"

@implementation DrawRectView

- (void)setDrwaRect:(void (^)(UIView * _Nonnull))drwaRect
{
    _drwaRect = drwaRect;
    [self setNeedsDisplay];
}

- (BOOL)isOpaque
{
    return NO;
}
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.drwaRect) {
        self.drwaRect(self);
    }
}
@end
