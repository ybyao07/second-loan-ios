//
//  WKBNewGuidepageView.m
//  WKCarInsurance
//
//  Created by sunyang on 17/5/12.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "WKBNewGuidepageView.h"
#import "UIImageView+extension.h"

@interface WKBNewGuidepageView ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) CGFloat viewWidth;
@property (nonatomic, assign) CGFloat viewHeight;
@end

@implementation WKBNewGuidepageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSelfWithFrame:frame];
    }
    return self;

}

- (void)initSelfWithFrame:(CGRect)frame{
    
    //初始化
    _scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [_scrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.delegate = self;
    _scrollView.directionalLockEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.alwaysBounceHorizontal = NO;
    _scrollView.alwaysBounceVertical = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    _viewWidth = CGRectGetWidth(frame);
    _viewHeight = CGRectGetHeight(frame);
    
    NSArray *image5Array = @[@"Iphone6Guide1",@"Iphone6Guide2",@"Iphone6Guide3"];
    NSArray *image6Array = @[@"Iphone6Guide1",@"Iphone6Guide2",@"Iphone6Guide3"];
    NSArray *image6pArray = @[@"Iphone6+Guide1",@"Iphone6+Guide2",@"Iphone6+Guide3"];
    NSArray *imageXrArray = @[@"IphoneXrGuide1",@"IphoneXrGuide2",@"IphoneXrGuide3"];
    NSArray *imageXsArray = @[@"IphoneXsGuide1",@"IphoneXsGuide2",@"IphoneXsGuide3"];
    NSArray *imageXMaxArray = @[@"IphoneXMaxGuide1",@"IphoneXMaxGuide2",@"IphoneXMaxGuide3"];
    NSMutableArray *imageArray = [NSMutableArray array];
    if (IS_IPHONE_5) {
        [imageArray addObjectsFromArray:image5Array];
    }else if (IS_IPHONE_6) {
        [imageArray addObjectsFromArray:image6Array];
    }else if (IS_IPHONE_6PLUS){
        [imageArray addObjectsFromArray:image6pArray];
    }else if(IS_IPHONE_X) {
        [imageArray addObjectsFromArray:imageXsArray];
    }else if (IS_IPHONE_Xr){
        [imageArray addObjectsFromArray:imageXrArray];
    }else if (IS_IPHONE_Xs){
        [imageArray addObjectsFromArray:imageXsArray];
    }else{
        [imageArray addObjectsFromArray:imageXMaxArray];
    }
    for (NSInteger i = 0; i < imageArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*_viewWidth, 0, _viewWidth, _viewHeight)];
        imageView.image = [UIImage imageNamed:[imageArray objectAtIndex:i]];
        [_scrollView addSubview:imageView];
        if (i == (imageArray.count - 1)) {
            [self addGestureToImageView:imageView];
        }
    }
    _scrollView.contentSize = CGSizeMake(imageArray.count *_viewWidth, _viewHeight);
    
}

- (void)addGestureToImageView:(UIImageView *)imageView {
    [imageView enableTouchEvent];
    imageView.pg_block = ^(NSDictionary *param){
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickCloseGuidepageView)]) {
            [self.delegate clickCloseGuidepageView];
        }
    };
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    CGPoint point = _scrollView.contentOffset;
    NSInteger currentPage = point.x / _viewWidth;
    if (currentPage>=2) {
        if (point.x >= _viewWidth) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickCloseGuidepageView)]) {
                [self.delegate clickCloseGuidepageView];
            }
        }
    }
}


@end
