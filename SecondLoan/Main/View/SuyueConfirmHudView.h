//
//  SuyueConfirmHudView.h
//  SuYue
//
//  Created by junlei on 2018/12/13.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuyueConfirmHudView : UIView

@property (nonatomic,assign) BOOL tapAutoRemove;
@property (nonatomic,strong) UIView * contentView;
@property (nonatomic,weak) UILabel * label;

@property (nonatomic,weak) UIView * submitBtn;
@property (nonatomic,weak) UIView * cancelBtn;

- (void)popTo:(UIView *)superView;
- (void)dissmis;

+ (void)popTo:(UIView * _Nullable)superView title:(NSString *)title enterBlock:(void (^)(NSInteger index))enterBlock;


@property (nonatomic,strong) void (^enterBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
