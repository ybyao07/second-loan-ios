//
//  SYBaseChoseView.h
//  SuYue
//
//  Created by junlei on 2019/3/16.
//  Copyright © 2019 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SYBaseChoseView : UIView

@property (nonatomic,assign) BOOL tapAutoRemove;
@property (nonatomic,strong) UIView * naviView;
@property (nonatomic,strong) UIView * contentView;

- (void)popTo:(UIView * _Nullable )superView;
- (void)dissmis;
@end

NS_ASSUME_NONNULL_END
