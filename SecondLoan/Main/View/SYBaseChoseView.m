//
//  SYBaseChoseView.m
//  SuYue
//
//  Created by junlei on 2019/3/16.
//  Copyright © 2019 Win10. All rights reserved.
//

#import "SYBaseChoseView.h"

@interface SYBaseChoseView()

@property (nonatomic,assign) NSTimeInterval tapTime;
@property (nonatomic,assign) CGPoint tapPoint;
@property (nonatomic,assign) BOOL isDissmis;

@end

@implementation SYBaseChoseView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.contentView];
        self.isDissmis = NO;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        self.tapAutoRemove = YES;
    }
    return self;
}

#pragma mark -- getter
- (UIView *)getcontentView
{
    UIView * mainView = [[UIView alloc] init];
    mainView.backgroundColor = [UIColor whiteColor];
    mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT * 0.6);
    return mainView;
}

- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [self getcontentView];
    }
    return _contentView;
}
- (UIView *)naviView
{
    if (!_naviView) {
        _naviView = [[UIView alloc] init];
        _naviView.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
            factory.button().fsFontSize(16).fsText(@"取消").fsTextColor(0x999999).fsOnClick(dissmis).fsFvid(@"cancel");
            factory.label().fsFontSize(18).fsTextColor(0x333333).fsFvid(@"title");
            factory.button().fsFontSize(16).fsText(@"确定").fsTextColor(0xff4c2b).fsOnClick(submit).fsFvid(@"submit");
            factory.view().fsBackGroundColor(0xefefef).fsFvid(@"bottomline");
        });
        __weak UIView * _weaknaviview = _naviView;
        [fQuery(_naviView, @"title") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_weaknaviview);
        }];
        [fQuery(_naviView, @"cancel") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_weaknaviview);
            make.left.equalTo(_weaknaviview).with.offset(16);
        }];
        [fQuery(_naviView, @"submit") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_weaknaviview);
            make.right.equalTo(_weaknaviview).with.offset(-16);
        }];
        [fQuery(_naviView, @"bottomline") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_weaknaviview);
            make.left.equalTo(_weaknaviview);
            make.bottom.equalTo(_weaknaviview);
            make.height.mas_equalTo(1);
        }];
    }
    return _naviView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    
    if ([self isSubViewEditing]) {
        self.tapTime = 0;
        return;
    }
    if (CGRectContainsPoint(self.contentView.frame,point)){
        self.tapTime = 0;
    } else {
        self.tapTime = [[NSDate date] timeIntervalSince1970];
        self.tapPoint = point;
    }
    NSLog(@"%s",__FUNCTION__);
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    NSLog(@"%s",__FUNCTION__);
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    [self endEditing:YES];
    if ([[NSDate date] timeIntervalSince1970] - self.tapTime < 1)
    {
        if (self.tapAutoRemove && ABS(point.x - self.tapPoint.x) < 10 && ABS(point.y - self.tapPoint.y) < 10) {
            [self dissmis];
        }
    }
    NSLog(@"%s",__FUNCTION__);
    
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
    [super touchesCancelled:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    [self endEditing:YES];
    if ([[NSDate date] timeIntervalSince1970] - self.tapTime < 1)
    {
        if (self.tapAutoRemove && ABS(point.x - self.tapPoint.x) < 10 && ABS(point.y - self.tapPoint.y) < 10) {
            [self dissmis];
        }
    }
    NSLog(@"%s",__FUNCTION__);
}
- (void)submit
{
}
- (void)dissmis
{
    if (!self.isDissmis) {
        self.isDissmis = YES;
        self.alpha = 1.0;
        self.contentView.frame = CGRectMake(0, self.frame.size.height - self.contentView.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0.0;
            self.contentView.frame = CGRectMake(0, self.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}
- (void)popTo:(UIView *)superView
{
    if (superView == nil) {
        superView = [[UIApplication sharedApplication] keyWindow];
    }
    [superView addSubview:self];
    self.frame = superView.bounds;
    self.alpha = 0.0;
    self.contentView.frame = CGRectMake(0, self.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1.0;
        self.contentView.frame = CGRectMake(0, self.frame.size.height - self.contentView.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
    }];
    NSLog(@"%@",NSStringFromCGRect(superView.bounds));
}

- (BOOL)isSubViewEditing
{
    return NO;
}

@end
