//
//  SuyueConfirm2HudView.m
//  SuYue
//
//  Created by Win10 on 2019/3/20.
//  Copyright © 2019 Win10. All rights reserved.
//

#import "SuyueConfirm2HudView.h"
@interface SuyueConfirm2HudView()

@property (nonatomic,assign) NSTimeInterval tapTime;
@property (nonatomic,assign) CGPoint tapPoint;

@property (nonatomic,assign) BOOL isDissmis;
@end

@implementation SuyueConfirm2HudView


+ (void)popTo:(UIView *)superView title:(NSString *)title subTitle:(NSString *)subTitle enterBlock:(void (^)(NSInteger index))enterBlock
{
    SuyueConfirm2HudView * v = [[SuyueConfirm2HudView alloc] init];
    v.label.text = title;
    v.subLabel.text = subTitle;
    v.enterBlock = enterBlock;
    [v popTo:superView];
}



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.contentView];
        self.isDissmis = NO;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.width.equalTo(@277);
        }];
        
        self.tapAutoRemove = YES;
    }
    return self;
}
#pragma mark -- getter
- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.fsRadius(3.3).fsClip(YES).develop(^(FFactory * factory){
            factory.label().fsFontSize(17).fsTextColor(0x333333).fsFvid(@"789456432156-label");
            factory.label().fsFontSize(15).fsTextColor(0x333333).fsLines(0).fsAlign(NSTextAlignmentCenter).fsFvid(@"789456432156-sublabel");
            factory.button().fsOnClick(cancelClick).fsText(@"取消").fsTextColor(0xAAAAAB).fsFontSize(15).fsBackGroundColor(0xF7F8FA).fsFvid(@"fasfshkjh_cancelBtn");
            factory.button().fsOnClick(submitClick).fsText(@"确定").fsTextColor(0x23242A).fsFontSize(15).fsBackGroundColor(0xF6C140).fsFvid(@"fasfshkjh_submitBtn");
        });
        self.label = fQueryLbl(_contentView, @"789456432156-label");
        self.subLabel = fQueryLbl(_contentView, @"789456432156-sublabel");
        
        self.cancelBtn = fQueryBtn(_contentView, @"fasfshkjh_cancelBtn");
        self.submitBtn = fQueryBtn(_contentView, @"fasfshkjh_submitBtn");
        WeakSelf;
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakSelf.contentView);
            make.centerY.equalTo(weakSelf.contentView.mas_top).with.offset(29);
        }];
        [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.contentView).with.offset(26);
            make.right.equalTo(weakSelf.contentView).with.offset(-26);
            make.top.equalTo(weakSelf.label.mas_bottom).with.offset(14);
            make.height.mas_greaterThanOrEqualTo(34);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).with.offset(-68);
        }];
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.contentView);
            make.bottom.equalTo(weakSelf.contentView);
            make.height.mas_equalTo(48);
            make.width.equalTo(weakSelf.submitBtn);
        }];
        [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.cancelBtn.mas_right);
            make.right.equalTo(weakSelf.contentView);
            make.bottom.equalTo(weakSelf.contentView);
            make.height.mas_equalTo(48);
        }];
    }
    return _contentView;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    
    if ([self isSubViewEditing]) {
        self.tapTime = 0;
        return;
    }
    //    if (CGRectContainsPoint(self.contentView.frame,point)){
    //        self.tapTime = 0;
    //    } else {
    self.tapTime = [[NSDate date] timeIntervalSince1970];
    self.tapPoint = point;
    //    }
}
- (void)cancelClick
{
    [self dissmis];
    self.enterBlock(0);
}
- (void)submitClick
{
    [self dissmis];
    self.enterBlock(1);
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self];
    [self endEditing:YES];
    if ([[NSDate date] timeIntervalSince1970] - self.tapTime < 1)
    {
        if (self.tapAutoRemove && ABS(point.x - self.tapPoint.x) < 10 && ABS(point.y - self.tapPoint.y) < 10) {
            [self dissmis];
        }
    }
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
    [super touchesCancelled:touches withEvent:event];
}
- (void)dissmis
{
    if (!self.isDissmis) {
        self.isDissmis = YES;
        self.alpha = 1.0;
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}
- (void)popTo:(UIView *)superView
{
    if (superView == nil) {
        superView = [[UIApplication sharedApplication] keyWindow];
    }
    [superView addSubview:self];
    NSLog(@"%@",NSStringFromCGRect(superView.bounds));
    self.frame = superView.bounds;
    self.alpha = 0.0;
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1.0;
    }];
}

- (BOOL)isSubViewEditing
{
    return NO;
}
@end
