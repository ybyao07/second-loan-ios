//
//  WKBNewGuidepageView.h
//  WKCarInsurance
//
//  Created by sunyang on 17/5/12.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WKBNewGuidepageViewDelegate <NSObject>

- (void)clickCloseGuidepageView;

@end

@interface WKBNewGuidepageView : UIView<UIScrollViewDelegate>
@property (weak, nonatomic) id <WKBNewGuidepageViewDelegate> delegate;
@end
