//
//  SuyueConfirm2HudView.h
//  SuYue
//
//  Created by Win10 on 2019/3/20.
//  Copyright © 2019 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuyueConfirm2HudView : UIView

+ (void)popTo:(UIView * _Nullable)superView title:(NSString *)title subTitle:(NSString *)subTitle enterBlock:(void (^)(NSInteger index))enterBlock;

@property (nonatomic,assign) BOOL tapAutoRemove;
@property (nonatomic,strong) UIView * contentView;
@property (nonatomic,weak) UILabel * label;
@property (nonatomic,weak) UILabel * subLabel;

@property (nonatomic,weak) UIView * submitBtn;
@property (nonatomic,weak) UIView * cancelBtn;

- (void)popTo:(UIView * _Nullable)superView;
- (void)dissmis;

@property (nonatomic,strong) void (^enterBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
