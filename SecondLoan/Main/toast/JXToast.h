//
//  JXToast.h
//  jiuxian
//
//  Created by 张正超 on 16/1/6.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//


// 依赖开源项目   CRToast

#import <Foundation/Foundation.h>
#import "CRToastManager.h"

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    JXToastMessageTypeInfo,
    JXToastMessageTypeRight,
    JXToastMessageTypeError
} JXToastMessageType;


@interface JXToast : NSObject

+ (void)showMessage:(NSString *)msg type:(JXToastMessageType)type;

+ (void)showUIToast:(NSString *)msg;

+ (void)showUIToast:(NSString *)msg inView:(__kindof UIView *)view;

@end
NS_ASSUME_NONNULL_END
