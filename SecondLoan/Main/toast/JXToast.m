//
//  JXToast.m
//  jiuxian
//
//  Created by 张正超 on 16/1/6.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import "JXToast.h"
#import "CRToastConfig.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "CommanTool.h"

@implementation JXToast


+ (void)showMessage:(NSString *)msg type:(JXToastMessageType)type{
    
    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar) ,
                                      kCRToastNotificationPresentationTypeKey   :  @(CRToastPresentationTypeCover),
                                      
                                      kCRToastTextKey                           : msg,
                                      kCRToastFontKey                           : [UIFont systemFontOfSize:15],
                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentLeft),
                                      kCRToastBackgroundColorKey                : ColorFromHex(0xfc5a5a),
                                      kCRToastTimeIntervalKey                   : @(2.0f),
                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationOutTypeKey               : @(CRToastAccessoryViewAlignmentLeft),
                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
                                      kCRToastNotificationPreferredPaddingKey   : @(4) } mutableCopy
                                    ];
                                      
    options[kCRToastImageKey] = [UIImage imageNamed:@"jx_toast_ok"];  //jx_toast_ok,fail
    options[kCRToastImageAlignmentKey] =  @(CRToastAccessoryViewAlignmentLeft);
    options[kCRToastImageContentModeKey] = @(UIViewContentModeCenter);
 
    
    [CRToastManager showNotificationWithOptions:[options copy]
                                 apperanceBlock:nil
                                completionBlock:nil];

   
}


//只有登录注册界面的navb不是masterNavigationController，需单独适配
+ (void)showUIToast:(NSString *)msg {
//    [getDelegate().masterNavigationController.view makeToast:msg
//                                     duration:2.0
//                                     position:CSToastPositionCenter
//                                        title:nil
//                                        image:[UIImage imageNamed:@"jx_toast_info"]
//                                        style:nil
//                                   completion:^(BOOL didTap) {
//                                   }];
    [[CommanTool getCurrentViewController].view  makeToast:msg
                                         duration:2.0
                                         position:CSToastPositionCenter
                                            title:nil
                                            image:[UIImage imageNamed:@"jx_toast_info"]
                                            style:nil
                                       completion:^(BOOL didTap) {
    
                                       }];
    
}

+ (void)showUIToast:(NSString *)msg inView:(__kindof UIView *)view {
    [view makeToast:msg
           duration:2.0
           position:CSToastPositionCenter
              title:nil
              image:[UIImage imageNamed:@"jx_toast_info"]
              style:nil
         completion:^(BOOL didTap) {
    
         }];
}

@end
