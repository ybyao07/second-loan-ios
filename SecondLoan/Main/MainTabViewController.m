//
//  XMGMainViewController.m
//  备课-百思不得姐
//
//  Created by MJ Lee on 15/6/15.
//  Copyright © 2015年 小码哥. All rights reserved.
//

#import "MainTabViewController.h"
#import "HomeController.h"
#import "LoginController.h"
#import "LoanMainController.h"
#import "DiscoverViewController.h"
#import "SYNotiAutoPage.h"
#import "PHMineViewController.h"
#import "WKBNewGuidepageView.h"

@interface MainTabViewController ()<CLLocationManagerDelegate,UITabBarControllerDelegate,WKBNewGuidepageViewDelegate>
{
    WKBNewGuidepageView *guideView;
}
@end

@implementation MainTabViewController
//+ (void)initialize
//{
//    UITabBarItem *appearance = [UITabBarItem appearance];
//    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
//    attrs[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
//    [appearance setTitleTextAttributes:attrs forState:UIControlStateSelected];
//
//    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"dian"]];
//    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"dian"]];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"dian"]];
//    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"dian"]];
    // 替换tabbar
    // 初始化所有的子控制器
    self.tabBar.translucent = NO;
    self.delegate = self;
    
    [self setupChildViewControllers];
    [[SYNotiAutoPage shareInstance] setTabbarController:self];
    [LocationManager sharedInstance];
    
    //添加引导图和广告图
    [self guideManage];
}
- (void)guideManage {
    NSString *key = @"CFBundleVersion";
    // 取出沙盒中存储的上次使用软件的版本号
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSString       *lastVersion = [defaults stringForKey:key];
    // 获得当前软件的版本号
    NSString        *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    if (![lastVersion isEqualToString:currentVersion]) {//第一次引导图
        guideView = [[WKBNewGuidepageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        guideView.delegate = self;
        [self.view addSubview:guideView];
        // 存储新版本
        [defaults setObject:currentVersion forKey:key];
        [defaults synchronize];
        
    } else {//广告图
       
    }
}
- (void)clickCloseGuidepageView {
    if (guideView) {
        [UIView animateWithDuration:1.0 animations:^{
            [self->guideView setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self->guideView removeFromSuperview];
        }];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
/**
 * 初始化所有的子控制器
 */
- (void)setupChildViewControllers
{
    HomeController *notice = [[HomeController alloc] init];
    [self setupOneChildViewController:notice title:@"首页" image:@"tab_home_unsel" highlightImage:@"tab_home_sel"];
    
    DiscoverViewController *xiaoxi = [[DiscoverViewController alloc] init];
//    dispatch_async(dispatch_get_main_queue(), ^{
        [xiaoxi.view setNeedsLayout];
//    });
   
    
    LoanMainController *wode = [[LoanMainController alloc] init];
    [self setupOneChildViewController:wode title:@"借款" image:@"tab_loan_unsel" highlightImage:@"tab_loan_sel"];
    
     [self setupOneChildViewController:xiaoxi title:@"发现" image:@"tab_discover_unsel" highlightImage:@"tab_discover_sel"];
    
    PHMineViewController *mine = [[PHMineViewController alloc] init];
    [self setupOneChildViewController:mine title:@"我的" image:@"tab_me_unsel" highlightImage:@"tab_me_sel"];
}

- (void)setupOneChildViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image highlightImage:(NSString *)highlight
{
    [self addChildViewController:vc];
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:highlight];
    NSDictionary *dictHome = [NSDictionary dictionaryWithObject:COLOR_ThemeRed forKey:NSForegroundColorAttributeName];
    [vc.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateSelected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
//    if ([viewController isKindOfClass:[HomeController class]]) {
//        return YES;
//    }else{
//        if ([UserLocal isLogin]) {
//            return YES;
//        } else {
//            MainNavigationController * vc = [[MainNavigationController alloc]initWithRootViewController:[[LoginController alloc] init]];
//            [self presentViewController:vc animated:YES completion:nil];
//            return NO;
//        }
//    }
    return YES;
}
@end
