//
//  SLProductRateTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLProductRateTableViewCell.h"

@interface SLProductRateTableViewCell()

@end

@implementation SLProductRateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    // Configure the view for the selected state
}

- (NSString *)getRate {
    return _expRateField.text;
}

- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate {
    self.expRateField.delegate = delegate;
}


@end
