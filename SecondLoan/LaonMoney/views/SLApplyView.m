//
//  SLApplyView.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLApplyView.h"

@implementation SLApplyView

+ (instancetype)newView
{
  NSArray * views = [[NSBundle mainBundle] loadNibNamed:@"SLApplyView" owner:self options:nil];
  SLApplyView * applyView = (SLApplyView *)[views firstObjectWithClass:self];
  return applyView;
}


@end
