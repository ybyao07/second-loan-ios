//
//  SLProductInfoJineTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLProductInfoJineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *jineTextField;

- (NSInteger)getJine;
- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate;
- (void)setActionDelegate:(id)delegate action:(SEL)action;
- (void)setJineToNil;
@end

NS_ASSUME_NONNULL_END
