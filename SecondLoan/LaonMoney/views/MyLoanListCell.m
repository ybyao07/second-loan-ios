//
//  MyLoanCell.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "MyLoanListCell.h"
#import "CommanTool.h"
#import "CommonModelTypeDIc.h"
#import "NSString+Tools.h"

@implementation MyLoanListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [CommanTool setShadowOnView:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(MyLoanModel *)model
{
    _model = model;
    _lblApplyTime.text = model.loanTime;
    _lblApplyMoney.text = [NSString getMoneyStringWithMoneyNumber:model.loanMoney];
    _lblbBankName.text = model.bankName;
//    if (model.status == CodeMyLoanWait) {
//        [_imgStatus setImage:[UIImage imageNamed:@"loan_state_waiting"]];
//    }else
    if(model.status == CodeMyLoaning){
        [_imgStatus setImage:[UIImage imageNamed:@"apply_status_doing"]];
    }else if(model.status == CodeMyLoanPassed){
        [_imgStatus setImage:[UIImage imageNamed:@"loan_state_passed"]];
    }else{
         [_imgStatus setImage:[UIImage imageNamed:@"apply_status_failed"]];
    }
    _lblType.text = [CommonModelTypeDIc dicPaybackType][@(model.paybackTYPE)];
}


+ (CGFloat)cellHeight
{
    return 130;
}

@end
