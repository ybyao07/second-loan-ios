//
//  SLProductApplyTitleCellTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLProductApplyTitleCellTableViewCell.h"

@implementation SLProductApplyTitleCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    // Configure the view for the selected state
}

@end
