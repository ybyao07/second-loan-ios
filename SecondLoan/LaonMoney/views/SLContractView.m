//
//  SLContractView.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/21.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLContractView.h"

@implementation SLContractView

+ (instancetype)newView
{
  NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"SLContractView" owner:self options:nil];
  SLContractView * contractView = (SLContractView *)[views firstObjectWithClass:self];
  contractView.acceptBtn.selected = NO;
    [contractView.acceptBtn setImage:[UIImage imageNamed:@"icon_check_red"] forState:UIControlStateSelected];
    [contractView.acceptBtn setImage:[UIImage imageNamed:@"icon_uncheck_gray"] forState:UIControlStateNormal];
  return contractView;
}

- (IBAction)contractTapped:(id)sender {
  self.acceptBtn.selected = !self.acceptBtn.selected;
    if (self.delegate) {
        [self.delegate acceptContract:self.acceptBtn];
    }
}
- (IBAction)contractTapped2:(id)sender {
    [self contractTapped:sender];
}

- (IBAction)dataContractTapped:(id)sender {
    if (self.delegate) {
        [self.delegate clickPrivate1Contract];
    }
}

- (IBAction)serviceContractTapped:(id)sender {
    if (self.delegate) {
        [self.delegate clickPrivate2Contract];
    }
}


@end
