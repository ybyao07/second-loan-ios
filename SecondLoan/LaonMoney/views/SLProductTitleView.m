//
//  SLProductTitleView.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLProductTitleView.h"

@implementation SLProductTitleView

+ (instancetype)newView
{
  NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"SLProductTitleView" owner:self options:nil];
  SLProductTitleView * productTitleView = (SLProductTitleView *)[views firstObjectWithClass:self];
  return productTitleView;
}

@end
