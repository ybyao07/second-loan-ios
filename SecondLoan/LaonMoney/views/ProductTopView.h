//
//  ProductTopView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/29.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProductTopView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblMonthRate;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblMount;
@property (weak, nonatomic) IBOutlet UILabel *lblLimit;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *lblHintRate;


+ (instancetype)newView;

@property (strong, nonatomic) ProductDetailModel *model;
@end

NS_ASSUME_NONNULL_END
