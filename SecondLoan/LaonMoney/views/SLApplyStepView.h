//
//  SLApplyStepView.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLApplyStepViewModel : NSObject

@property (strong, nonatomic) NSString *imgName;
@property (nonatomic, strong) NSString *title;

@end

@interface SLApplyStepView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;
//@property (weak, nonatomic) IBOutlet UIImageView *applyImage;
//@property (weak, nonatomic) IBOutlet UILabel *applyTitle;

- (void)bindData:(SLApplyStepViewModel *)viewModel;
@end



NS_ASSUME_NONNULL_END
