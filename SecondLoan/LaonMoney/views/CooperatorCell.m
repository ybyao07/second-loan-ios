//
//  CooperatorCell.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "CooperatorCell.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

@interface CooperatorCell()

@property (strong, nonatomic) NSMutableArray<UILabel *> *arrLabel;
@property (weak, nonatomic) IBOutlet UIView *bgContentView;

@end

@implementation CooperatorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [CommanTool setShadowOnView:self];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ProductModel *)model
{
    _model = model;
    _lblMoney.text = [NSString stringWithFormat:@"￥%@",[NSString getMoneyStringWithMoneyNumber:model.maxQuota]] ;
    _lblDescribe.text = model.productDec;
    _lblProductName.text = model.productName;
    [_imgCompanyLogo sd_setImageWithURL:[NSURL URLWithString: [model.bankUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"place_holder_bank"]];
    _lblBankname.text = model.bankName;
    NSArray *array = [model.productDec componentsSeparatedByString:@","];
    for (UILabel *lbl in self.arrLabel) {
        [lbl removeFromSuperview];
    }
    [self.arrLabel removeAllObjects];
    self.arrLabel = [NSMutableArray new];
    for (NSString *strDes in array) {
        UILabel *lbl = [[UILabel alloc] init];
        lbl.text = strDes;
        lbl.numberOfLines = 0;
        lbl.font = FontThin12;
        lbl.textColor = COLOR_ThemeRed;
        lbl.layer.borderColor = COLOR_ThemeRed.CGColor;
        lbl.layer.borderWidth = 0.6;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 8;
        [self addSubview:lbl];
        [self.arrLabel addObject:lbl];
    }
 
    [self updateDescripConstrain];
}

- (void)updateDescripConstrain{
    NSInteger count = self.arrLabel.count;
    UILabel *lblPre = nil;
    CGFloat lblHeight = 16;
    CGFloat btnMargin = 8;
    // 计算总长度
    CGFloat totalW = 0;
    BOOL is2lineFisrt = YES;
    for (int i = 0 ; i < count ; i++) {
        UILabel *lbl = self.arrLabel[i];
        NSString *text = lbl.text;
        CGSize size = [text sizeWithFont:FontThin12 constrainedToSize:CGSizeMake(SCREEN_WIDTH - 250 , 100)];
        CGFloat width = size.width;
        totalW = totalW + width;
    }
    if (totalW > (SCREEN_WIDTH - 250)) {
        [self.lblDescribe mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.lblProductName.bottom).with.offset(80);
        }];
    }
    
    totalW = 0;
    for (int i = 0 ; i < count ; i++) {
        UILabel *lbl = self.arrLabel[i];
        NSString *text = lbl.text;
        CGSize size = [text sizeWithFont:FontThin12 constrainedToSize:CGSizeMake(SCREEN_WIDTH - 250 , 100)];
        CGFloat width = size.width;
//        CGFloat height = size.height;
//        if (height > lblHeight) {
//            lblHeight = height;
//        }
        totalW = totalW + width;
        if (lblPre != nil) {
            if (totalW < (SCREEN_WIDTH - 250)) {
                [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.lblDescribe.mas_top);
                    make.width.equalTo(@(width + btnMargin));
                    make.height.equalTo(@(lblHeight));
                    make.left.equalTo(lblPre.mas_right).with.offset(4);
                }];
            }else{
                if (is2lineFisrt) {
                    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.lblDescribe.mas_top).mas_offset(20);
                        make.width.equalTo(@(width + btnMargin));
                        make.height.equalTo(@(lblHeight));
                        make.left.equalTo(self.lblDescribe.mas_left);
                    }];
                    is2lineFisrt = NO;
                }else{
                    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.lblDescribe.mas_top).mas_offset(20);
                        make.width.equalTo(@(width + btnMargin));
                        make.height.equalTo(@(lblHeight));
                        make.left.equalTo(lblPre.mas_right).with.offset(4);
                    }];
                }
            }
        }else{
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lblDescribe.mas_top);
                make.width.equalTo(@(width + btnMargin));
                make.height.equalTo(@(lblHeight));
                make.left.equalTo(self.lblDescribe.mas_left);
            }];
        }
//        if (height > lblHeight) {
//            lblHeight = height;
//            break;
//        }
        lblPre = lbl;
    }
}



+ (CGFloat)cellHeight
{
    return 144;
}
@end
