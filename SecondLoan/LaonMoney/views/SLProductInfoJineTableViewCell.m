//
//  SLProductInfoJineTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLProductInfoJineTableViewCell.h"

@interface SLProductInfoJineTableViewCell()
@end

@implementation SLProductInfoJineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    // Configure the view for the selected state
}

- (NSInteger)getJine {
    return [_jineTextField.text integerValue];
}

- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate {
    self.jineTextField.delegate = delegate;
}


- (void)setJineToNil {
    _jineTextField.text = nil;
}

- (void)setActionDelegate:(id)delegate action:(SEL)action {
    [self.jineTextField addTarget:delegate action:action forControlEvents:UIControlEventEditingChanged];
}

@end
