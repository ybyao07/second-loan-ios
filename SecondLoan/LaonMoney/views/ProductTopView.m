//
//  ProductTopView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/29.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "ProductTopView.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

@interface ProductTopView()
@property (weak, nonatomic) IBOutlet UILabel *lblAA;

@property (strong, nonatomic) NSMutableArray<UIButton *> *arrBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation ProductTopView

+ (instancetype)newView
{
    NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"ProductTopView" owner:self options:nil];
    ProductTopView * productTopView = (ProductTopView *)[views firstObjectWithClass:self];
//    productTopView.lblProductName.font = [UIFont fontWithName:@"ZhenyanGB-Regular" size:44];
//    productTopView.topConstraint.constant = KStatusBarAndNavigationBarHeight;
    [CommanTool setShadowOnView1:productTopView.bgView];
    return productTopView;
}
- (void)setModel:(ProductDetailModel *)model{
    _model = model;
    self.lblProductName.text = self.model.productName;
    self.lblMonthRate.text = self.model.rate;
    self.lblMount.text = [NSString stringWithFormat:@"最高%@元",  [NSString getMoneyStringWithMoneyNumber:self.model.maxQuota]];
    self.lblLimit.text = [NSString stringWithFormat:@"最长%@", [self periodLoan:self.model.loanTerm]] ;
     NSArray *array = [model.productDec componentsSeparatedByString:@","]; //字符串按照【分隔成数组
    for (UIButton *btn in self.arrBtn) {
        [btn removeFromSuperview];
    }
    [self.arrBtn removeAllObjects];
    self.arrBtn = [NSMutableArray new];
    for (NSString *strDes in array) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:strDes forState:UIControlStateNormal];
        btn.titleLabel.font = Font15Medium;
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
        [btn setTitleColor:COLOR_White forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"icon_correct_white"] forState:UIControlStateNormal];
        [self addSubview:btn];
        [self.arrBtn addObject:btn];
    }
    [self updateDescripConstrain];
}

- (NSString *)periodLoan:(NSInteger)term
{
    NSString *strTerm = @"";
    if (term == 1) {
        strTerm = @"3个月";
    }else if (term == 2){
        strTerm = @"6个月";
    }else if (term == 3){
        strTerm = @"12个月";
    }else if (term == 4){
        strTerm = @"24个月";
    }else if (term == 5){
        strTerm = @"36个月";
    }
    return strTerm;
}

- (void)updateDescripConstrain{
    NSInteger count = self.arrBtn.count;
    NSInteger center = count/2;
    CGFloat lblHeight = 26;
    CGFloat btnMargin = 8;
    if (count % 2 == 0) { //偶数 -- 只取前2个
        if (count >= 2) {
            UIButton *firstBtn = self.arrBtn[0];
            NSString *text1 = firstBtn.titleLabel.text;
            CGSize size1 = [text1 sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width1 = size1.width + 6;
            [firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lblAA.mas_top);
                make.height.equalTo(@(lblHeight));
                make.width.equalTo(@(width1 + btnMargin));
                make.right.equalTo(self.lblAA.mas_left).with.mas_offset(3);
            }];
            UIButton *secondBtn = self.arrBtn[1];
            NSString *text2 = secondBtn.titleLabel.text;
            CGSize size2 = [text2 sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width2 = size2.width + 6;
            [secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lblAA.mas_top);
                make.height.equalTo(@(lblHeight));
                make.width.equalTo(@(width2 + btnMargin));
                make.left.equalTo(self.lblAA.mas_right).with.mas_offset(3);
            }];
        }else{
            
        }
    }else{// ---- 保证中间的控件居中 ---
        if (count >= 3) { // 取前三
            [self.arrBtn removeObjectsInRange:NSMakeRange(3, self.arrBtn.count-3)];
            count = self.arrBtn.count;
            center = count/2;
        }
        UIButton *centerBtn = self.arrBtn[center];
        NSString *text = centerBtn.titleLabel.text;
        CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
        CGFloat width = size.width + 16;
        [centerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.lblAA.mas_top);
            make.width.equalTo(@(width + btnMargin));
            make.height.equalTo(@(lblHeight));
            make.centerX.equalTo(self.lblAA);
        }];
        UIButton *btnPre = centerBtn;
        for (int i = center - 1 ; i >= 0  ; i--) {
            UIButton *lbl = self.arrBtn[i];
            NSString *text = lbl.titleLabel.text;
            CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width = size.width + 16;
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lblAA.mas_top);
                make.width.equalTo(@(width + btnMargin));
                make.height.equalTo(@(lblHeight));
                make.right.equalTo(btnPre.mas_left).with.offset(-4);
            }];
            btnPre = lbl;
        }
        btnPre = centerBtn;
        for (int i = center + 1 ; i < count  ; i++) {
            UIButton *lbl = self.arrBtn[i];
            NSString *text = lbl.titleLabel.text;
            CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width = size.width + 16;
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lblAA.mas_top);
                make.width.equalTo(@(width + btnMargin));
                make.height.equalTo(@(lblHeight));
                make.left.equalTo(btnPre.mas_right).with.offset(4);
            }];
            btnPre = lbl;
        }
    }
}




@end
