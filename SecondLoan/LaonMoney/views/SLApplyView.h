//
//  SLApplyView.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLApplyStepView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLApplyView : UIView
@property (weak, nonatomic) IBOutlet SLApplyStepView *s1;
@property (weak, nonatomic) IBOutlet SLApplyStepView *s2;
@property (weak, nonatomic) IBOutlet SLApplyStepView *s3;
@property (weak, nonatomic) IBOutlet SLApplyStepView *s4;

+ (instancetype)newView;

@end

NS_ASSUME_NONNULL_END
