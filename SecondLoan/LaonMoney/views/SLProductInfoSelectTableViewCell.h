//
//  SLProductInfoSelectTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, SelectCellType) {
    SelectCellTypeTerm,
    SelectCellTypeMethod
};

@class SLProductInfoSelectTableViewCell;

@protocol SLProductInfoSelectTableViewCellDelegate <NSObject>

- (void)showOptionsOnView: (SLProductInfoSelectTableViewCell *)targetCell;

@end

@interface SLProductInfoSelectTableViewCell : UITableViewCell

@property (nonatomic, weak) id<SLProductInfoSelectTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSString *selectedValue;
@property (nonatomic, assign) SelectCellType type;

- (void)setTitle: (NSString *)title;
- (void)setItem: (NSString *)item;
@end

NS_ASSUME_NONNULL_END
