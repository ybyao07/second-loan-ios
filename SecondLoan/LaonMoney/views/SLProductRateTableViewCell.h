//
//  SLProductRateTableViewCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLProductRateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *expRateField;

- (NSString *)getRate;
- (void)setTextFieldDelegate:(id <UITextFieldDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
