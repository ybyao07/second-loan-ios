//
//  SLProductInfoSelectTableViewCell.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLProductInfoSelectTableViewCell.h"
#import "UIView+JXTapEvent.h"


@interface SLProductInfoSelectTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UILabel *seleltItem;


@end

@implementation SLProductInfoSelectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (void)setTitle: (NSString *)title {
    self.titleLabel.text = title;
}

- (IBAction)selectDidTap:(id)sender {
    if (self.delegate) {
        [self.delegate showOptionsOnView:self];
    }
}

- (void)setItem: (NSString *)item {
    self.seleltItem.text = item;
}

@end
