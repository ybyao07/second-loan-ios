//
//  SLApplyStepView.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLApplyStepView.h"

@implementation SLApplyStepViewModel

@end

@implementation SLApplyStepView

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super initWithCoder:coder];
  if (self) {
    [self setup];
  }
  return self;
}

- (void)setup {
  [[NSBundle mainBundle] loadNibNamed:@"SLApplyStepView" owner:self options:nil];
  [self addSubview: self.view];
}


- (void)bindData:(SLApplyStepViewModel *)viewModel {
//  self.applyTitle.text = viewModel.title;
}
@end
