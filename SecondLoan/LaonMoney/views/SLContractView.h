//
//  SLContractView.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/21.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SLContractViewDelegate <NSObject>

@optional

/** 点击协议回调 */
- (void)clickOnDataContract:(NSString *)contract;
- (void)acceptContract:(UIButton *)btn;
- (void)clickPrivate1Contract;
- (void)clickPrivate2Contract;


@end

@interface SLContractView : UIView
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *qiluContract;
@property (weak, nonatomic) IBOutlet UIButton *dataContract;
//@property (nonatomic, assign) BOOL checked;
@property (nonatomic, weak) id<SLContractViewDelegate> delegate;

+ (instancetype)newView;

@end

NS_ASSUME_NONNULL_END
