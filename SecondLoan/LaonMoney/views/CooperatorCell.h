//
//  CooperatorCell.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CooperatorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblMoney;
@property (weak, nonatomic) IBOutlet UILabel *lblDescribe;
@property (weak, nonatomic) IBOutlet UIImageView *imgCompanyLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
//@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblBankname;


+ (CGFloat)cellHeight;

@property (nonatomic, strong) ProductModel *model;
@end

NS_ASSUME_NONNULL_END
