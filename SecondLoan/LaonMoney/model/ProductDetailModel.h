//
//  ProductDeailModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/1.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailModel : NSObject

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productLogo;
@property (nonatomic, copy) NSString *productPic;
@property (nonatomic, copy) NSString *bankName;
@property (nonatomic, copy) NSString *bankUrl;      // 银行logo
@property (nonatomic, assign) NSInteger maxQuota;         //最高额度
@property (nonatomic, copy) NSString *productDec;      //产品宣传语
@property (nonatomic, copy) NSString *applyProcess;   // 申请流程图
@property (nonatomic, copy) NSString *rate;         // 月利率
@property (nonatomic, copy) NSString *condition;        //申请条件
@property (nonatomic, copy) NSString *preInfo;      //准备材料
@property (assign, nonatomic) NSInteger loanTerm;   // 贷款期限 (1：3个月 2：6个月 3：12个月 4：24个月 5：36个月)
//@property (assign, nonatomic) NSInteger guarantyType;    // 担保方式
@property (nonatomic, copy) NSString *productExplain;      //产品特点说明


@end

NS_ASSUME_NONNULL_END
