//
//  SLProductInfoViewController.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/20.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLProductInfoViewController : BaseViewController

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, assign) BOOL isPuhui;

@end

NS_ASSUME_NONNULL_END
