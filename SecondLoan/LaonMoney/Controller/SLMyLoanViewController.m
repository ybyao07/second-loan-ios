//
//  SLApplyViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMyLoanViewController.h"
#import "MyLoanListCell.h"
#import "MyLoanModel.h"

@interface SLMyLoanViewController ()

@property (assign,nonatomic) NSInteger cPage;

@end

@implementation SLMyLoanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = @"我的贷款";
    self.view.backgroundColor = UIColorHex(0xf3f3f3);
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(6, 0, 0, 0);
    [self.tableView registerNib:[UINib nibWithNibName:@"MyLoanListCell" bundle:nil] forCellReuseIdentifier:@"MyLoanListCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    [self loadData];
//    [self addTestData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)addTestData{
    self.dataSource = [MyLoanModel mj_objectArrayWithKeyValuesArray:@[
                                                                           @{
                                                                               @"collateralMortgage":@(1),
                                                                               @"bankName":@"中国银行",
                                                                               @"loanTime":@"2015-08-18",
                                                                               @"loanMoney":@"100",
                                                                               @"status":@(1)
                                                                               },
                                                                           @{
                                                                               @"collateralMortgage":@(2),
                                                                               @"bankName":@"建设银行",
                                                                               @"loanTime":@"2015-08-18",
                                                                               @"loanMoney":@"100",
                                                                               @"status":@(2)
                                                                               },
                                                                           @{
                                                                               @"collateralMortgage":@(4),
                                                                               @"bankName":@"中国银行",
                                                                               @"loanTime":@"2019-08-18",
                                                                               @"loanMoney":@"100",
                                                                               @"status":@(3)
                                                                               },
                                                                           ]];
    [self.tableView reloadData];
}


- (void)loadData
{
    self.cPage = 1;
    [self loadCurrentPageData];
}
- (void)loadMoreData
{
    [self loadCurrentPageData];
}
- (void)loadCurrentPageData
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:@(self.cPage) forKey:@"pageNum"];
    [param setObject:@(20) forKey:@"pageSize"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_myloan_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        if(error){
            UITableView * tableView = self.tableView;
            [tableView.mj_header endRefreshing];
            [tableView.mj_footer endRefreshing];
            [self HUDshowErrorWithStatus:error.localizedDescription];
            return;
        }
        NSDictionary *resultData = responseObject[@"data"];
        UITableView * tableView = self.tableView;
        AUTO_TABLE_PLACEHOLDER(tableView,self.dataSource,resultData,error)
        if (!error) {
            NSArray * list = resultData[@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            for (int i = 0; i< list.count; i ++) {
                MyLoanModel * data = [MyLoanModel mj_objectWithKeyValues:list[i]];
                [mlist addObject:data];
            }
            if (self.cPage == 1){
                self.dataSource = mlist;
            } else {
                self.dataSource =  [self.dataSource arrayByAddingObjectsFromArray:mlist];
            }
            self.cPage++;
            tableView.mj_footer.hidden = (self.dataSource.count >= [resultData[@"total"] integerValue]);
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        [tableView reloadData];
    }];
}
#pragma mark ====== UItableview ======
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [MyLoanListCell cellHeight];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyLoanListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyLoanListCell"];
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


@end
