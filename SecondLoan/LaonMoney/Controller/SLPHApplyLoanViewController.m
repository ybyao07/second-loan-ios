//
//  SLPHApplyLoanViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/11.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLPHApplyLoanViewController.h"
#import "KMQRCode.h"

@interface SLPHApplyLoanViewController ()

@end

@implementation SLPHApplyLoanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"立即借款";
    self.view.backgroundColor = COLOR_White;
    [self addViews];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)addViews{
    UIImageView *QRImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 180)/2, KStatusBarAndNavigationBarHeight + 60, 180, 180)];
//    QRImageView.image = [self QRCodeView];
    QRImageView.image = [UIImage imageNamed:@"icon_qr"];
    [self.view addSubview:QRImageView];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(QRImageView.frame) + 30, CGRectGetWidth(QRImageView.frame) + 60, 40)];
    title.font = Font14;
    title.textColor = COLOR_Gary_66;
    title.text = @"您好，你需要办理银行卡，并安装银行的手机银行，进行在线提款";
    title.numberOfLines = 2;
    title.textAlignment = NSTextAlignmentCenter;
    title.centerX = QRImageView.centerX;
    [self.view addSubview:title];
}

- (UIImage *)QRCodeView {
//    NSString *source = self.productId;
    NSString *source = @"https://mbc.sdrcu.com/mbcper/download.html";
    //使用iOS 7后的CIFilter对象操作，生成二维码图片imgQRCode（会拉伸图片，比较模糊，效果不佳）
    CIImage *imgQRCode = [KMQRCode createQRCodeImage:source];
    //使用核心绘图框架CG（Core Graphics）对象操作，进一步针对大小生成二维码图片imgAdaptiveQRCode（图片大小适合，清晰，效果好）
    UIImage *imgAdaptiveQRCode = [KMQRCode resizeQRCodeImage:imgQRCode
                                                    withSize:150];
    //使用核心绘图框架CG（Core Graphics）对象操作，创建带圆角效果的图片
    UIImage *imgIcon = [UIImage imageNamed:@"icon-share"];
    //使用核心绘图框架CG（Core Graphics）对象操作，合并二维码图片和用于中间显示的图标图片
    imgAdaptiveQRCode = [KMQRCode addIconToQRCodeImage:imgAdaptiveQRCode
                                              withIcon:imgIcon
                                          withIconSize:imgIcon.size];
    
    //    qrImgView.image = imgAdaptiveQRCode;
    return imgAdaptiveQRCode;
}



@end
