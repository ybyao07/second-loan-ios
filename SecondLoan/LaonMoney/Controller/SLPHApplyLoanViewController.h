//
//  SLPHApplyLoanViewController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/11.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SLPHApplyLoanViewController : BaseViewController

@property (nonatomic, strong) NSString *productId;

@end

NS_ASSUME_NONNULL_END
