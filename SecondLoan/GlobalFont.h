//
//  GlobalFont.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#ifndef GlobalFont_h
#define GlobalFont_h

#define SET_DEFAULT_FONT(n) [UIFont systemFontOfSize:n]
#define DEFAULT_FONT SET_DEFAULT_FONT(15)

#define Font18 [UIFont fontWithName:@"PingFangSC-Regular" size:18]
#define Font18Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:18]
#define Font18Medium [UIFont fontWithName:@"PingFangSC-Medium" size:18]
#define Font16 [UIFont fontWithName:@"PingFangSC-Regular" size:16]
#define Font16Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:16]
#define Font16Medium [UIFont fontWithName:@"PingFangSC-Medium" size:16]
#define Font15 [UIFont fontWithName:@"PingFangSC-Regular" size:15]
#define Font15Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:15]
#define Font15Medium [UIFont fontWithName:@"PingFangSC-Medium" size:15]
#define Font14 [UIFont fontWithName:@"PingFangSC-Regular" size:14]
#define Font14Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:14]
#define Font14Medium [UIFont fontWithName:@"PingFangSC-Medium" size:14]
#define Font13 [UIFont fontWithName:@"PingFangSC-Regular" size:13]
#define Font13Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:13]
#define Font13Medium [UIFont fontWithName:@"PingFangSC-Medium" size:13]
#define Font12 [UIFont fontWithName:@"PingFangSC-Regular" size:12]
#define FontThin12 [UIFont fontWithName:@"PingFangSC-Thin" size:12]

#define Font12Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:12]
#define Font12Medium [UIFont fontWithName:@"PingFangSC-Medium" size:12]
#define Font11 [UIFont fontWithName:@"PingFangSC-Regular" size:11]
#define Font11Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:11]
#define Font11Medium [UIFont fontWithName:@"PingFangSC-Medium" size:11]
#define Font10 [UIFont fontWithName:@"PingFangSC-Regular" size:10]
#define Font10Bold [UIFont fontWithName:@"PingFangSC-SemiBold" size:10]
#define Font10Medium [UIFont fontWithName:@"PingFangSC-Medium" size:10]

#endif /* GlobalFont_h */
