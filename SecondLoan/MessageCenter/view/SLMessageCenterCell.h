//
//  SLMessageCenterCell.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLLoanMessageModel.h"

NS_ASSUME_NONNULL_BEGIN
@class SLMessageCenterCell;
@protocol SLMessageCenterCellDelegate <NSObject>

- (void)showDetailOf:(SLMessageCenterCell *)cell;

@end

@interface SLMessageCenterCell : UITableViewCell
@property (nonatomic, strong) SLLoanMessageModel *model;
@property (nonatomic, weak) id<SLMessageCenterCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END


