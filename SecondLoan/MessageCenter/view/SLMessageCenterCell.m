    //
    //  SLMessageCenterCell.m
    //  SecondLoan
    //
    //  Created by Echo Zhangjie on 2019/8/31.
    //  Copyright © 2019 姚永波. All rights reserved.
    //

#import "SLMessageCenterCell.h"

@interface SLMessageCenterCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, assign) NSInteger messageType;
@property (nonatomic, assign) NSInteger messageStatus;
@property (weak, nonatomic) IBOutlet UILabel *indicatorLabel;

@property (weak, nonatomic) IBOutlet UIImageView *messageIcon;


@end

@implementation SLMessageCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
        // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

        // Configure the view for the selected state
}

- (IBAction)tapDetailButton:(id)sender {
    [self.delegate showDetailOf:self];
}


- (void)setModel:(SLLoanMessageModel *)model
{
    _model = model;
    self.messageBodyLabel.text = model.content;
    self.messageTitleLabel.text = model.title;
    self.timeLabel.text = model.updateTime;
    self.messageType = model.type;
    self.messageStatus = model.status;
    NSString *imgName = self.messageType == 1?   @"notice_loan" : @"notice_activity";
    self.messageIcon.image = [UIImage imageNamed:imgName];
}

- (void)setMessageStatus:(NSInteger)messageStatus {
    self.indicatorLabel.hidden = !(messageStatus == 1);
}

@end


