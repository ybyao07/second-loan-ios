//
//  SLLoanMessageModel.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLLoanMessageModel : NSObject
@property (strong, nonatomic) NSString *mId;
@property (nonatomic, copy) NSString *title;          //标题
@property (nonatomic, copy) NSString *content;          //消息主体
@property (nonatomic, copy) NSString *updateTime;          
@property (nonatomic, copy) NSString *picUrl;          //消息图片
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger status;


@end

NS_ASSUME_NONNULL_END
