//
//  SLMessageDetailViewController.m
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMessageDetailViewController.h"
//#import "UIView"

@interface SLMessageDetailViewController ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *messageImg;

@property (nonatomic, strong) SLLoanMessageModel *model;


@end

@implementation SLMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息详情";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupView];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)setMessageDetailModel:(SLLoanMessageModel *)model{
    self.model = model;
}

- (void)setupView {
    self.view.fsBackGroundColor(0xffffff).develop(^(FFactory * factory){
        factory.label().fsText(self.model.title).fsFont(Font16).fsTextColor(0x333333).fsLines(0).fsAlign(NSTextAlignmentCenter).fsFvid(@"titleLabel");
        factory.label().fsText(self.model.updateTime).fsFont(Font12).fsTextColor(0x666666).fsLines(0).fsAlign(NSTextAlignmentCenter).fsFvid(@"timeLabel");
            factory.imageView().fsImgContentMode(UIViewContentModeScaleAspectFit).fsFvid(@"messageImg");
        factory.label().fsText(self.model.content).fsFont(Font14).fsTextColor(0x666666).fsLines(0).fsAlign(NSTextAlignmentLeft).fsFvid(@"contentLabel");
    });

    self.titleLabel = fQueryLbl(self.view, @"titleLabel");
    self.timeLabel = fQueryLbl(self.view, @"timeLabel");
    self.contentLabel = fQueryLbl(self.view, @"contentLabel");
    self.messageImg = fQueryImgv(self.view, @"messageImg");

    [fQuery(self.view, @"titleLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(KStatusBarAndNavigationBarHeight);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-16);
    }];
    [fQuery(self.view, @"timeLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.view, @"titleLabel").mas_bottom).with.offset(8);
        make.left.equalTo(self.view).with.offset(16);
        make.right.equalTo(self.view).with.offset(-16);
    }];

    [fQuery(self.view, @"messageImg") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self.view, @"timeLabel").mas_bottom).with.offset(8);
        make.left.equalTo(self.view).with.offset(16);
        make.right.equalTo(self.view).with.offset(-16);
        make.height.equalTo(@(160));
    }];

    if (self.model.picUrl == nil || isEmptyString(self.model.picUrl)) {
        self.messageImg.hidden = YES;
        [fQuery(self.view, @"contentLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(fQuery(self.view, @"timeLabel").mas_bottom).with.offset(8);
            make.left.equalTo(self.view).with.offset(16);
            make.right.equalTo(self.view).with.offset(-16);
        }];
    } else {
        [self.messageImg sd_setImageWithURL:[NSURL URLWithString:_model.picUrl]  placeholderImage:[UIImage imageNamed:@""]];
        [fQuery(self.view, @"contentLabel") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(fQuery(self.view, @"messageImg").mas_bottom).with.offset(8);
            make.left.equalTo(self.view).with.offset(16);
            make.right.equalTo(self.view).with.offset(-16);
        }];
    }
}

@end
