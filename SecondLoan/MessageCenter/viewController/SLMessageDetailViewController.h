//
//  SLMessageDetailViewController.h
//  SecondLoan
//
//  Created by Echo Zhangjie on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "BaseViewController.h"
#import "SLLoanMessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLMessageDetailViewController : BaseViewController

- (void)setMessageDetailModel:(SLLoanMessageModel *)model;

@end

NS_ASSUME_NONNULL_END
