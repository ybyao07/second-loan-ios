//
//  SLMessageListController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMessageListController.h"
#import "SLLoanMessageModel.h"
#import "SLMessageCenterCell.h"
#import "SLMessageDetailViewController.h"

@interface SLMessageListController ()<UITableViewDelegate, SLMessageCenterCellDelegate>
@property (assign,nonatomic) NSInteger cPage;
//@property (nonatomic, copy) NSString *navTitle;

@end

@implementation SLMessageListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = @"消息中心";
    self.view.backgroundColor = COLOR_White;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(6, 0, 0, 0);
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"SLMessageCenterCell" bundle:nil] forCellReuseIdentifier:@"SLMessageCenterCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    [self loadData];
}


- (void)loadData
{
    self.cPage = 1;
    [self loadCurrentPageData];
}
- (void)loadMoreData
{
    [self loadCurrentPageData];
}
- (void)loadCurrentPageData
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:@(self.cPage) forKey:@"pageNum"];
    [param setObject:@(20) forKey:@"pageSize"];

    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_message_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        UITableView * tableView = self.tableView;
        NSDictionary *resultData = responseObject[@"data"];
        AUTO_TABLE_PLACEHOLDER(tableView,self.dataSource,resultData,error)
        if (!error) {
            NSArray * list = resultData[@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            for (int i = 0; i< list.count; i ++) {
                SLLoanMessageModel * data = [SLLoanMessageModel mj_objectWithKeyValues:list[i]];
                [mlist addObject:data];
            }
            if (self.cPage == 1){
                self.dataSource = mlist;
            } else {
                self.dataSource =  [self.dataSource arrayByAddingObjectsFromArray:mlist];
            }
            self.cPage++;
            tableView.mj_footer.hidden = (self.dataSource.count >= [resultData[@"total"] integerValue]);
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        [tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
#pragma mark ============= UITableViewDelegate ==========

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLMessageCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SLMessageCenterCell"];
    cell.model = self.dataSource[indexPath.row];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    SLMessageCenterCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    [self showDetailOf:cell];
}

- (void)showDetailOf:(SLMessageCenterCell *)cell {
    SLMessageDetailViewController *detailVC = [SLMessageDetailViewController new];
    [detailVC setMessageDetailModel:cell.model];
    [self.navigationController pushViewController:detailVC animated:YES];
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:cell.model.mId forKey:@"messageId"];
    WeakSelf
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_update_status) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        StrongSelf
        if (!error) {
            cell.model.status = -1;
            [strongSelf.tableView reloadData];
        }
    }];

}


@end
