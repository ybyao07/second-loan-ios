//
//  SLMessageListController.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLMessageListController : MainTableViewController
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
