//
//  SLMessageViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLMessageViewController.h"
#import "MessageTotalCell.h"
#import "SLMessageListController.h"
#import "MessageTotalModel.h"

@interface SLMessageViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) MessageTotalModel *modelLoan;
@property (strong, nonatomic) MessageTotalModel *modelActivity;
@property (strong, nonatomic) MessageTotalModel *modelAboutUs;

@property (nonatomic, strong) NSMutableArray *arrColleDatas;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SLMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息中心";
    [self.view addSubview:self.tableView];
    
    self.modelLoan = [MessageTotalModel createModelWithTitle:@"贷款通知" icon:@"notice_loan" content:@""];
    self.modelActivity = [MessageTotalModel createModelWithTitle:@"活动公告" icon:@"notice_activity" content:@""];
    [self.arrColleDatas addObject:self.modelLoan];
    [self.arrColleDatas addObject:self.modelActivity];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
#pragma mark ============= UITableViewDelegate ==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrColleDatas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [MessageTotalCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageTotalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTotalCell"];
    MessageTotalModel *model = self.arrColleDatas[indexPath.row];
    cell.lblTitle.text = model.title;
    [cell.imgIcon setImage:[UIImage imageNamed:model.icon]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessageTotalModel *model = self.arrColleDatas[indexPath.row];
    SLMessageListController *detailVC = [[SLMessageListController alloc] init];
    detailVC.navTitle = model.title;

    if (indexPath.row == 0){//贷款通知
        detailVC.type = 1;
    }else if (indexPath.row == 1){ //活动公告
        detailVC.type = 2;
    }
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark ============ set && get 方法 ==========
- (NSMutableArray *)arrColleDatas
{
    if (!_arrColleDatas) {
        _arrColleDatas = [NSMutableArray new];
    }
    return _arrColleDatas;
}
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.scrollEnabled = YES;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = COLOR_White;
        [_tableView registerNib:[UINib nibWithNibName:@"MessageTotalCell" bundle:nil] forCellReuseIdentifier:@"MessageTotalCell"];
    }
    return  _tableView;
}

@end
