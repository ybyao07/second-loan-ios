//
//  DiscoverViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/18.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "DiscoverViewController.h"
#import <WebKit/WebKit.h>

@interface DiscoverViewController ()<WKNavigationDelegate,WKUIDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic,strong) NSString * webUrl;
@property (nonatomic,strong) NSDictionary * webParams;
@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = COLOR_White;
    [self.view addSubview:self.webView];
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, KStatusBarHeight)];
    topView.backgroundColor = COLOR_White;
    self.view.backgroundColor = COLOR_White;
    [self.view addSubview:topView];
    [self addConstraints];
    self.webUrl = url_discover_home;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationBarBgView.backgroundColor = COLOR_ThemeRed;
    self.navigationBarBgView.hidden = YES;
//    [self.view bringSubviewToFront:self.webView];
    if (self.webUrl) {
        [self loadData];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)addConstraints
{
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view).with.mas_offset(KStatusBarHeight);
    }];
}

- (void)loadData
{
    if (_webUrl.length) {
        [self HUDshowWithStatus:@""];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_webUrl]]];
    }
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [self HUDdismiss];
}

- (WKWebView *)webView
{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height - KStatusBarHeight )];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        _webView.scrollView.bounces = NO;
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    }
    return _webView;
}

- (void)dealloc
{
    if (_webView) {
        [self.webView setNavigationDelegate:nil];
        [self.webView setUIDelegate:nil];
    }
}

@end
