//
//  CommonTools.h
//  jz_ios
//
//  Created by junlei on 2017/2/18.
//  Copyright © 2017年 hppower. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface CommonTools : NSObject
+ (NSString *)timeDescrip:(NSTimeInterval)time;
+ (UIImage *)limitImageFrom:(UIImage *)image size:(CGSize)limitsize;
+ (UIImage *)fixOrientation:(UIImage *)aImage;
+ (NSString *)transNumToMoneyFormate:(CGFloat)money;

+ (NSURL *)transQiniuKeyToURL:(NSString *)string;
+ (NSString *)toStr:(id )what;

+ (void)setStorage:(NSString *)key value:(id)value;
+ (id)getStorage:(NSString *)key;


+(NSString *)getConstellation:(NSInteger)timestamp;
+(NSInteger)getUserAge:(NSInteger)timestamp;

+ (NSMutableAttributedString *)textString:(NSString *)text WithChangeText:(NSString *)changeText WithColor:(UIColor *)textColor;
+ (NSMutableAttributedString *)textString:(NSString *)text WithChangeText:(NSString *)changeText WithColor:(UIColor *)textColor fontSize:(CGFloat)fontSize;
+ (UIImage *)createNonInterpolatedUIIamgeFormCIImage:(CIImage *)image withSize:(CGFloat)size;
@end
