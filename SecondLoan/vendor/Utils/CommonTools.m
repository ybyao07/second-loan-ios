//
//  CommonTools.m
//  jz_ios
//
//  Created by junlei on 2017/2/18.
//  Copyright © 2017年 hppower. All rights reserved.
//

#import "CommonTools.h"
#import "UIImage+JLExtension.h"

@implementation CommonTools
+ (NSString *)timeDescrip:(NSTimeInterval)time
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDate * cDate = [NSDate date];
    CGFloat  offtime = cDate.timeIntervalSince1970 - date.timeIntervalSince1970;
    if (offtime < 60) {
        return @"刚刚";
    }
    if (offtime < 60 * 60) {
        return [NSString stringWithFormat:@"%@分钟前",@((int)(offtime/60))];
    }
    if (offtime < 60 * 60 * 24 ) {
        return [NSString stringWithFormat:@"%@小时前",@((int)(offtime/(60*60)))];
    }
    if (offtime < 60 * 60 * 24 * 7 ) {
        return [NSString stringWithFormat:@"%@天前",@((int)(offtime/(60*60* 24)))];
    }
    return [NSString stringWithFormat:@"%@",[date stringWithFormat:@"MM-dd HH:mm:ss"]];
}

+ (UIImage *)limitImageFrom:(UIImage *)image size:(CGSize)limitsize
{
    if (image.size.width > limitsize.width) {
        image = [image toScale:limitsize.width/image.size.width];
    }
    if (image.size.height > limitsize.height) {
        image = [image toScale:limitsize.height/image.size.height];
    }
    
    return image;
}

+ (UIImage *)fixOrientation:(UIImage *)aImage {
    
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+ (NSString *)transNumToMoneyFormate:(CGFloat)money
{
    //    CGFloat tmoney = money * 100;
    //    CGFloat lastmoney = tmoney - ((int)tmoney);
    //    if (lastmoney >= 0.5) {
    //        tmoney += 1;
    //    }
    //    money = ((CGFloat)((int)tmoney))/100;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###,##0.00;"];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:money]];
    return formattedNumberString;
    
}


+ (NSURL *)transQiniuKeyToURL:(NSString *)string
{
    NSString * urlstr = [string containsString:@"http"]?string:[NSString stringWithFormat:@"%@%@",QiNiuBaseUrl,string];

    return [NSURL URLWithString:urlstr];
}

+ (NSString *)toStr:(id )what
{
    return [NSString stringWithFormat:@"%@",what];
}



+ (void)setStorage:(NSString *)key value:(NSString *)value
{
    if (key) {
        [USERDEFAULT setObject:value forKey:key];
    } else {
        [USERDEFAULT removeObjectForKey:key];
    }
    [USERDEFAULT synchronize];
    return;
}

+ (NSString *)getStorage:(NSString *)key
{
    return [USERDEFAULT objectForKey:key];
}


+(NSString *)getConstellation:(NSInteger)timestamp {
    NSDate *birthDate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *birthcompomemts = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay fromDate:birthDate];
    NSInteger birthYear = birthcompomemts.year;
    NSInteger birthMonth = birthcompomemts.month;
    NSInteger birthDay = birthcompomemts.day;
    //计算月份
    NSString *retStr=@"";
    
    NSString *astroString = @"魔羯水瓶双鱼白羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯";
    NSString *astroFormat = @"102123444543";
    
    retStr=[NSString stringWithFormat:@"%@",[astroString substringWithRange:NSMakeRange(birthMonth*2-(birthDay < [[astroFormat substringWithRange:NSMakeRange((birthMonth-1), 1)] intValue] - (-19))*2,2)]];
    
    return [NSString stringWithFormat:@"%@座",retStr];
}
+(NSInteger)getUserAge:(NSInteger)timestamp{
    NSDate *nowDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *compomemts = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay fromDate:nowDate];
    NSInteger nowYear = compomemts.year;
    NSInteger nowMonth = compomemts.month;
    NSInteger nowDay = compomemts.day;
    
    NSDate *birthDate = [NSDate dateWithTimeIntervalSince1970:timestamp];//
    NSDateComponents *birthcompomemts = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay fromDate:birthDate];
    NSInteger birthYear = birthcompomemts.year;
    NSInteger birthMonth = birthcompomemts.month;
    NSInteger birthDay = birthcompomemts.day;

    // 计算年龄
    NSInteger userAge = nowYear - birthYear - 1;
    if ((nowMonth > birthMonth) || (nowMonth == birthMonth && nowDay >= birthDay)) {
        userAge++;
    }
    return userAge;
}

+ (NSMutableAttributedString *)textString:(NSString *)text WithChangeText:(NSString *)changeText WithColor:(UIColor *)textColor{
    NSMutableAttributedString *textStr = [[NSMutableAttributedString alloc] initWithString:text];
    [textStr addAttribute:NSForegroundColorAttributeName value:textColor range:[text rangeOfString:changeText]];
    return textStr;
}
+ (NSMutableAttributedString *)textString:(NSString *)text WithChangeText:(NSString *)changeText WithColor:(UIColor *)textColor fontSize:(CGFloat)fontSize{
    NSMutableAttributedString *textStr = [[NSMutableAttributedString alloc] initWithString:text];
    [textStr addAttribute:NSForegroundColorAttributeName value:textColor range:[text rangeOfString:changeText]];
    [textStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:[text rangeOfString:changeText]];
    return textStr;
}
/**
 *  根据CIImage生成指定大小的UIImage 生成清晰的二维码
 *
 *  @param image CIImage
 *  @param size  图片宽度
 */
+ (UIImage *)createNonInterpolatedUIIamgeFormCIImage:(CIImage *)image withSize:(CGFloat)size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size / CGRectGetWidth(extent), size / CGRectGetHeight(extent));
    
    // 1.创建bitmap
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceCMYK();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}
@end
