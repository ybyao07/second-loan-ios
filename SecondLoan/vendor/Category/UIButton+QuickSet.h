//
//  UIButton+QuickSet.h
//  gaodun
//
//  Created by CHEN on 15/1/21.
//  Copyright (c) 2015年 陈君. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (QuickSet)

+ (id)buttonWithImageName:(NSString *)imageName;

+ (id)buttonWithBackgroundImageName:(NSString *)imageName;

+ (UIButton *)buttonWithtitle:(NSString*)str titleColor:(UIColor *)tcolor titleSize:(CGFloat)fontsize backColor:(UIColor *)bcolor cornerRadius:(CGFloat)cornerRadius target:(id)targ action:(SEL)action;

- (void)setTitle:(nullable NSString *)title color:(nullable UIColor *)color;
- (void)setTitle:(nullable NSString *)title autoColor:(nullable UIColor *)color;
- (void)setTitle:(nullable NSString *)title size:(CGFloat)size color:(nullable UIColor *)color;
@end
