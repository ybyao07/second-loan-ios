//
//  NSString+extention.h
//  GaoDunSchool2.0
//
//  Created by 陈君 on 13-9-29.
//  Copyright (c) 2013年 陈君. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (extention)
- (NSData *)base64Decode;

-(NSString *)URLString:(NSString *)urlString;

-(NSNumber *)integerNumber;

-(NSString *)md5;

-(NSString *)sha1;

-(NSString *)hmacSha1:(NSString *)key;

-(NSString *)md5ForSQLServerUTF16;
-(BOOL)myContainsString:(NSString*)other;

/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;


/**
 *  字体所占size大小
 *
 *  @param font          字体大小
 *  @param constrainSize 最大长宽
 *
 *  @return size
 */
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)constrainSize;

- (NSString *)appendStrings:(id)firstObj, ... NS_REQUIRES_NIL_TERMINATION;

- (BOOL)isNull;

@end
