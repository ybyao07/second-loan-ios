//
//  UITableView+Placeholder.m
//  SuYue
//
//  Created by junlei on 2018/12/20.
//  Copyright © 2018 Win10. All rights reserved.
//

#import "UIScrollView+Placeholder.h"

@implementation UIScrollView(Placeholder)

- (void)showNoDataView:(BOOL)isshow
{
    if (isshow) {
        if (fQuery(self, @"UITableView-Placeholder-nodata")) {
            return;
        }
        UIView * view = [[UIView alloc] initWithFrame:UIScreen.mainScreen.bounds];
        view.fsBackGroundColor(0xffffff).fsFvid(@"UITableView-Placeholder-nodata").develop(^(FFactory * factory){
            factory.imageView().fsImage(@"nodetaicon").fsFvid(@"uiotuioweurow-icon");
            factory.label().fsText(@"暂无内容").fsFontSize(15).fsTextColor(0xAAAAAB).fsFvid(@"uiotuioweurow-label");
        });
        [self addSubview:view];
        [fQuery(view, @"uiotuioweurow-label") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.centerY.equalTo(view).with.offset(-KStatusBarAndNavigationBarHeight);
        }];

        [fQuery(view, @"uiotuioweurow-icon") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.bottom.equalTo(fQuery(view, @"uiotuioweurow-label").mas_top).with.offset(-16);

        }];
    } else {
        [fQuery(self, @"UITableView-Placeholder-nodata") removeFromSuperview];
    }
}
- (void)showErrorView:(BOOL)isshow
{
    if (isshow) {
        if (fQuery(self, @"UITableView-Placeholder-errordata")) {
            return;
        }
        UIView * view = [[UIView alloc] init];
        view.fsBackGroundColor(0xffffff).fsFvid(@"UITableView-Placeholder-errordata").develop(^(FFactory * factory){
            factory.imageView().fsImage(@"dataerror").fsFvid(@"fdsfserrwetw-icon");
            factory.label().fsText(@"哎呀！出错了！").fsFontSize(15).fsTextColor(0x23242A).fsFvid(@"fdsfserrwetw-label1");
            factory.label().fsText(@"可能原因").fsFontSize(15).fsTextColor(0xAAAAAB).fsFvid(@"fdsfserrwetw-label2");
            factory.label().fsText(@"暂网络连接失败\n无网络\n网络异常").fsLines(0).fsFontSize(13).fsTextColor(0xAAAAAB).fsFvid(@"fdsfserrwetw-label3");
        });
        [fQuery(view, @"fdsfserrwetw-icon") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.centerY.equalTo(view).multipliedBy(0.8);
        }];
        [fQuery(view, @"fdsfserrwetw-label1") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.top.equalTo(fQuery(view, @"fdsfserrwetw-icon").mas_bottom).with.offset(16);
        }];
        [fQuery(view, @"fdsfserrwetw-label2") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.top.equalTo(fQuery(view, @"fdsfserrwetw-label1").mas_bottom).with.offset(33);
        }];
        [fQuery(view, @"fdsfserrwetw-label3") mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view);
            make.top.equalTo(fQuery(view, @"fdsfserrwetw-label2").mas_bottom).with.offset(16);
        }];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.width.equalTo(self);
            make.height.equalTo(self);
        }];
    } else {
        [fQuery(self, @"UITableView-Placeholder-errordata") removeFromSuperview];
    }
}
@end
