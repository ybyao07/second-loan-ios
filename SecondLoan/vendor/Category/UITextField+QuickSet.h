//
//  UITextField+QuickSet.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/9/8.
//  Copyright (c) 2015年 JET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField(QuickSet)
- (void)initWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor;
- (void)initWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor borderColor:(UIColor *)borderColor radius:(CGFloat)radius;

+ (UITextField *)textFieldWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor;
+ (UITextField *)textFieldWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor borderColor:(UIColor *)borderColor radius:(CGFloat)radius;
- (void)clearText;
@end
