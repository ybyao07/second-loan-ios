//
//  NSArray+Keng.m
//  mobilePD
//
//  Created by wangyan on 2016－06－13.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "NSArray+Extension.h"

@implementation NSArray(Extension)

- (NSArray *)GroupUsingBlock:(BOOL (^)(id obj1,id obj2))block
{
    NSMutableArray * mainArray = [NSMutableArray array];
    //初始化
    
    for (int index = 0; index < self.count; index++ ) {
        BOOL flag = NO;
        id currentObject = self[index];
        for (int jndex = 0; jndex < mainArray.count; jndex++) {
            NSMutableArray * mArr = mainArray[jndex];
            if (block([mArr lastObject],currentObject)) {
                [mArr addObject:currentObject];
                flag = YES;
                break;
            }
        }
        if (!flag) {
            [mainArray addObject:[NSMutableArray arrayWithObject:currentObject]];
        }
    }
    return mainArray;
    
}

- (UIView *)findViewWithTag:(NSInteger)tag
{
    UIView * view = nil;
    for (int i = 0; i < self.count; i ++) {
        view = self[i];
        if (view.tag == tag) {
            break;
        }
    }
    return view;
}
- (NSObject *)firstObjectWithClass:(Class)aclass
{
    id object = nil;
    for (int i = 0; i < self.count; i++) {
        NSObject * cobj = self[i];
        if ([cobj.class isSubclassOfClass:aclass]) {
            object = cobj;
            break;
        }
    }
    return object;
}

- (NSDictionary *)numDictionary
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]initWithCapacity:self.count];
    for (int i =0; i < self.count; i++) {
        [dic setObject:self[i] forKey:[@(i) stringValue]];
    }
    return [dic copy];
}
@end
@implementation NSMutableArray(Extension)
- (void)addNullableObject:(id)obj
{
    if (obj) {
        [self addObject:obj];
    }
}
@end
