//
//  UIVIew+Extention.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/9/9.
//  Copyright (c) 2015年 JET. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView(Extention)
@property (nonatomic,assign) CGSize GDsize;

@property (nonatomic,assign) CGFloat GDx;
@property (nonatomic,assign) CGFloat GDy;

@property (nonatomic,assign,readonly) CGFloat GDRightx;
@property (nonatomic,assign,readonly) CGFloat GDBottomy;

@property (nonatomic,assign) CGFloat GDwidth;
@property (nonatomic,assign) CGFloat GDheight;

@property (nonatomic,assign) CGFloat GDcenterX;
@property (nonatomic,assign) CGFloat GDcenterY;


- (void)GDDaddSubviews:(id)firstObj, ...NS_REQUIRES_NIL_TERMINATION;
- (void)GDaddSubviews:(NSArray *)objects;

- (void)removeAllSubviews;
@end
