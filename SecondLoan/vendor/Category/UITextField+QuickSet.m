//
//  UITextField+QuickSet.m
//  easyride_kuaiji
//
//  Created by gaodun on 15/9/8.
//  Copyright (c) 2015年 JET. All rights reserved.
//

#import "UITextField+QuickSet.h"
#import <objc/runtime.h>

@implementation UITextField(QuickSet)
- (void)initWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor
{
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: fontColor,NSFontAttributeName:font                                                                                                     }];
    self.textColor = fontColor;
    self.font = font;
    self.backgroundColor = backColor;
    
    self.keyboardType = keyboard;
    
    float imgWidth = 40.0;
    UIImage * leftImage = nil;
    if (!imgStr) {
        imgWidth = 15.0;
    } else {
        leftImage = [UIImage imageNamed:imgStr];
    }
    self.leftView = [[UIImageView alloc]initWithImage:leftImage];
    self.leftView.contentMode = UIViewContentModeCenter;
    self.leftView.frame = CGRectMake(0, 0, imgWidth, 30);
    self.leftViewMode = UITextFieldViewModeAlways;
    
    
    self.rightViewMode = UITextFieldViewModeNever;
    UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    clearButton.frame = CGRectMake(0, 0, 50, 20);
    clearButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [clearButton setImage:[UIImage imageNamed:@"clearButton"] forState:UIControlStateNormal];
    
    [clearButton addTarget:self action:@selector(clearText) forControlEvents:UIControlEventTouchUpInside];
    self.rightView = clearButton;
    
    self.layer.cornerRadius = 6.0;
}
- (void)initWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor borderColor:(UIColor *)borderColor radius:(CGFloat)radius
{
                               
    [self initWithplaceholder:str font:font textColor:fontColor leftImageName:imgStr keyboardType:keyboard backgroundColor:backColor];
    self.layer.borderColor = borderColor.CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = radius;
}





+ (UITextField *)textFieldWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor
{
    UITextField * textField = [[UITextField alloc]init];
    [textField initWithplaceholder:str font:font textColor:fontColor leftImageName:imgStr keyboardType:keyboard backgroundColor:backColor];
    //[textField clearText];
    return textField;
}
+ (UITextField *)textFieldWithplaceholder:(NSString *)str font:(UIFont *)font textColor:(UIColor *)fontColor leftImageName:(NSString *)imgStr keyboardType:(UIKeyboardType)keyboard backgroundColor:(UIColor *)backColor borderColor:(UIColor *)borderColor radius:(CGFloat)radius
{
    UITextField * textField = [UITextField textFieldWithplaceholder:str font:font textColor:fontColor leftImageName:imgStr keyboardType:keyboard backgroundColor:backColor];
    textField.layer.borderColor = borderColor.CGColor;
    textField.layer.borderWidth = 1.0;
    textField.layer.cornerRadius = radius;
    return textField;
}

- (void)clearText
{
    self.text = @"";
}


@end
