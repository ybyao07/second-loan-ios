//
//  NSString+Base64.h
//  yrtx
//
//  Created by Win10 on 2018/2/8.
//  Copyright © 2018年 mac mini. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (Base64)
/**
 *  转换为Base64编码
 */
- (NSString *)base64EncodedString;
/**
 *  将Base64编码还原
 */
- (NSString *)base64DecodedString;


- (NSString *)defaultStr:(NSString *)dft;
@end

