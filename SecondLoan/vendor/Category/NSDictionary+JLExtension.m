//
//  NSDictionary+Throw.m
//  easyride_kuaiji
//
//  Created by gaodun on 15/11/4.
//  Copyright © 2015年 JET. All rights reserved.
//

#import "NSDictionary+JLExtension.h"

@implementation NSMutableDictionary(JLExtension)
- (void)throwSetObject:(id)item forKey:(NSString*)key
{
    if (item == nil) {
        NSLog(@"%@ 的值非法<字典>：%@",key,item);
        return;
    }
    [self setObject:item forKey:key];

}
- (void)key:(NSString*)key object:(id)item
{
    [self throwSetObject:item forKey:key];
}
- (void)addNullableDictionary:(NSDictionary *)dictionary
{
    if (dictionary) {
        [self addEntriesFromDictionary:dictionary];
    }
}

+ (NSMutableArray *)dicToArr:(NSDictionary *)dic{
    NSMutableArray *dataArr = [NSMutableArray array];
    NSArray *keyArr = [dic allKeys];
    for (NSString *keyStr in keyArr) {
        NSMutableArray *itemArr = [NSMutableArray array];
        NSDictionary *keyDic = [dic objectForKey:keyStr];
        NSArray *arr1 = [keyDic allKeys];
        for (NSString *key1 in arr1) {
            [itemArr addObject:[NSMutableDictionary dictionaryWithObject:key1 forKey:[keyDic objectForKey:key1]]];
        }
        [dataArr addObject:[NSMutableDictionary dictionaryWithObject:itemArr forKey:keyStr]];
    }
    return dataArr;
}
@end

