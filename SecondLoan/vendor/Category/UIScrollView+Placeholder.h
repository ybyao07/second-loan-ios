//
//  UITableView+Placeholder.h
//  SuYue
//
//  Created by junlei on 2018/12/20.
//  Copyright © 2018 Win10. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define  AUTO_TABLE_PLACEHOLDER(tableview,dataSorce,data,error)       if (!error) {\
                    [(tableview) showNoDataView:([(data)[@"total"] integerValue] == 0)];\
                }\
                [(tableview) showErrorView:((error) != nil && (dataSorce).count == 0)];

@interface UIScrollView(Placeholder)

- (void)showNoDataView:(BOOL)isshow;

- (void)showErrorView:(BOOL)isshow;
@end

NS_ASSUME_NONNULL_END
