//
//  NSDictionary+Throw.h
//  easyride_kuaiji
//
//  Created by gaodun on 15/11/4.
//  Copyright © 2015年 JET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary(Throw)

- (void)key:(NSString*)key object:(id)item;
- (void)addNullableDictionary:(NSDictionary *)dictionary;
+ (NSMutableArray *)dicToArr:(NSDictionary *)dic;
@end
