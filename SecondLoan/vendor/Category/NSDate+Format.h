//
//  NSDate+Format.h
//  ZZSS
//
//  Created by Holly Lee on 14-2-8.
//  Copyright (c) 2014年 Holly Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Format)

-(NSString *)stringByFormat:(NSString *)format;

+(NSDate *)dateFromString:(NSString *)str format:(NSString *)format;

+ (NSTimeInterval)todayZero;
- (NSTimeInterval)dayOfZero;
- (NSDate *)yestoday;
+ (BOOL)isSameDay:(NSInteger)day1 withDay:(NSInteger)day2;
- (BOOL)isToday;
@end
@interface NSDate (Contacts)
+(NSString *)contactsStringWithTimeInterval:(NSTimeInterval )setTime;
@end