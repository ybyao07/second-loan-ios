//
//  UILabel+Fast.m
//  yrtx
//
//  Created by mac mini on 2016/11/22.
//  Copyright © 2016年 mac mini. All rights reserved.
//

#import "UILabel+Fast.h"

@implementation UILabel(Fast)

+ (instancetype)labelWithText:(NSString*)text
{
    UILabel * label = [[UILabel alloc]init];
    label.text = text;
    return label;
}

@end
