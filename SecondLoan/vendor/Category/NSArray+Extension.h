//
//  NSArray+Keng.h
//  mobilePD
//
//  Created by wangyan on 2016－06－13.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(Extension)
- (NSArray *)GroupUsingBlock:(BOOL (^)(id obj1,id obj2))block;
- (UIView *)findViewWithTag:(NSInteger)tag;
- (NSObject *)firstObjectWithClass:(Class)aclass;
- (NSDictionary *)numDictionary;
@end
@interface NSMutableArray(Extension)
- (void)addNullableObject:(id)obj;
@end
