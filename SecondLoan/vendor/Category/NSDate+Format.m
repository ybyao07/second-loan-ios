//
//  NSDate+Format.m
//  ZZSS
//
//  Created by Holly Lee on 14-2-8.
//  Copyright (c) 2014年 Holly Lee. All rights reserved.
//
#define TimeZong 8

#define SecondsOfDay (60*60*24)
#define SecondsOfHour (60*60)

#import "NSDate+Format.h"

@implementation NSDate (Format)

-(NSString *) stringByFormat:(NSString *)format
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSString * s = [formatter stringFromDate:self];
    return s;
}

+(NSDate *)dateFromString:(NSString *)str format:(NSString *)format
{
    NSDate * date = [NSDate date];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    date = [formatter dateFromString:str];
    return date;
}

+ (NSTimeInterval)todayZero
{
    return (NSTimeInterval)((( (int)([[NSDate date] timeIntervalSince1970] + TimeZong * SecondsOfHour) / SecondsOfDay)*SecondsOfDay) - TimeZong * SecondsOfHour);
}
- (NSTimeInterval)dayOfZero
{
    return (NSTimeInterval)((( (int)([self timeIntervalSince1970] + TimeZong * SecondsOfHour) / SecondsOfDay)*SecondsOfDay) - TimeZong * SecondsOfHour);
}
+ (BOOL)isSameDay:(NSInteger)day1 withDay:(NSInteger)day2
{
    NSInteger time = ((( (int)(day1 + TimeZong * SecondsOfHour) / SecondsOfDay)*SecondsOfDay) - TimeZong * SecondsOfHour);
    if (day2 >= time && day2 < (time + 3600 * 24)) {
        return YES;
    }
    return NO;
}
-(NSDate *)yestoday
{
    return [NSDate dateWithTimeIntervalSince1970:(self.timeIntervalSince1970 - 3600*24)];
}
- (BOOL)isToday
{
    NSInteger timeToday = (NSInteger)[NSDate todayZero];
    if (self.timeIntervalSince1970 >= timeToday && self.timeIntervalSince1970 < (timeToday + 3600 * 24)) {
        return YES;
    }
    return NO;
}
@end
@implementation NSDate(Contacts)
+(NSString *)contactsStringWithTimeInterval:(NSTimeInterval )setTime
{
    NSInteger standTime = (NSInteger)((( (int)(((float)setTime) + TimeZong * SecondsOfHour) / SecondsOfDay)*SecondsOfDay) - TimeZong * SecondsOfHour);
    
    NSInteger todayTime = (NSInteger)((( (int)([[NSDate date] timeIntervalSince1970] + TimeZong * SecondsOfHour) / SecondsOfDay)*SecondsOfDay) - TimeZong * SecondsOfHour);
    
    if (standTime == todayTime) {
        NSDate * modifyDate = [NSDate dateWithTimeIntervalSince1970:setTime];
        return [modifyDate stringByFormat:@"hh:mm"];
    }
    if (standTime == (todayTime - SecondsOfDay)) {
        return @"昨天";
    }
    
    NSDate * modifyDate = [NSDate dateWithTimeIntervalSince1970:setTime];
    return [modifyDate stringByFormat:@"MM-dd"];
}
@end
