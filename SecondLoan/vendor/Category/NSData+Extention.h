//
//  NSData+Extention.h
//  JLiOSBase
//
//  Created by mac mini on 2017/1/6.
//  Copyright © 2017年 JL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(Extention)
- (NSString *)base64Encode;
- (NSString *)hexString;
@end
