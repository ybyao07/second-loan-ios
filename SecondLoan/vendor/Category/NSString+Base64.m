//
//  NSString+Base64.m
//  yrtx
//
//  Created by Win10 on 2018/2/8.
//  Copyright © 2018年 mac mini. All rights reserved.
//

#import "NSString+Base64.h"
@implementation NSString(Base64)

- (NSString *)base64EncodedString;
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

- (NSString *)base64DecodedString
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:self options:0];
    return [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
}

- (NSString *)defaultStr:(NSString *)dft
{
    if (self == nil) {
        return dft;
    }
    return [NSString stringWithFormat:@"%@",self];
}
@end

