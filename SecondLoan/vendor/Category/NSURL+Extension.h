//
//  NSURL+Extension.h
//  mobilePD
//
//  Created by wangyan on 2016－06－07.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL(Extension)
+ (NSURL *)URLWithStringAutoEncode:(NSString *)URLString;
@end
