//
//  UITableView+extension.h
//  mobilePD
//
//  Created by wangyan on 2016－06－03.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView(extension)

- (void)jl_scrollToBottom;
- (void)jl_scrollWithOffsetAuto:(CGFloat)offset;

- (void)refreshCurrentCells;
@end
