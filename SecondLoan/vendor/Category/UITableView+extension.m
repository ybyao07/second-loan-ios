//
//  UITableView+extension.m
//  mobilePD
//
//  Created by wangyan on 2016－06－03.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "UITableView+extension.h"
#import "UIVIew+Extention.h"

@implementation UITableView(extension)
- (void)jl_scrollWithOffsetAuto:(CGFloat)offset
{
    CGSize contentSize = self.contentSize;
    CGFloat offsetY = self.contentOffset.y;
    
    CGFloat realY = contentSize.height - offsetY;
    
    if (realY < self.GDheight) {
        if ((self.GDheight - realY) < offset) {
            [self setContentOffset:CGPointMake(0, offsetY + (realY + offset) - self.GDheight) animated:YES];
        }
    } else {
//        [self setContentOffset:CGPointMake(0, offsetY + offset) animated:YES];
    }
    
    
}
- (void)jl_scrollToBottom
{
    [self setContentOffset:CGPointMake(0, self.contentSize.height) animated:YES];
}

- (void)refreshCurrentCells
{
    NSArray * visibleRows = [self indexPathsForVisibleRows];
    [self reloadRowsAtIndexPaths:visibleRows withRowAnimation:UITableViewRowAnimationNone];
}
@end
