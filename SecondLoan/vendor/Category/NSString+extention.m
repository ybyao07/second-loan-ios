//
//  NSString+URL.m
//  GaoDunSchool2.0
//
//  Created by 陈君 on 13-9-29.
//  Copyright (c) 2013年 陈君. All rights reserved.
//

#import "NSString+extention.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation NSString (extention)

- (NSData *)base64Decode
{
    return [[NSData alloc] initWithBase64EncodedString:self options:0];
}

-(NSString *)URLString:(NSString *)urlString{
    return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(NSNumber *)integerNumber{
    return [NSNumber numberWithInteger:self.integerValue];
}

- (NSString *)md5 {
    const char *cString = [self UTF8String];
    unsigned char results[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cString, strlen(cString), results);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", results[i]];
    }
    
    return ret;
}

- (NSString *)sha1 {
    const char *cString = [self UTF8String];
    unsigned char results[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(cString, strlen(cString), results);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH*2];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", results[i]];
    }
    
    return ret;
}

- (NSString *)hmacSha1:(NSString *)key {
    const char *cString = [self UTF8String];
    const char *kString = [key UTF8String];
    unsigned char results[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, kString, strlen(kString), cString, strlen(cString), results);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH*2];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", results[i]];
    }
    
    return ret;
}

- (NSString *)md5ForSQLServerUTF16 {
    NSData *temp = [self dataUsingEncoding:NSUTF16LittleEndianStringEncoding];
    
    UInt8 *cStr = (UInt8 *)[temp bytes];
    
    unsigned char results[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, [temp length], results);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH*2];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", results[i]];
    }
    
    return ret;
}

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize
{
    CGSize contentSize;
   
    NSDictionary *attrs = @{NSFontAttributeName : font};
    contentSize = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
  
    return contentSize;
}
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)constrainSize{
    CGSize size = CGSizeZero;
    CGRect rect = [self boundingRectWithSize:constrainSize//限制最大的宽度和高度
                                     options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin//采用换行模式
                                  attributes:@{NSFontAttributeName : font}//传人的字体字典
                                     context:nil];
    size = rect.size;
    return size;
}
/**
 *
 *
 *  @param other targetStr
 *
 *  @return yes or no
 */
- (BOOL)myContainsString:(NSString*)other {
    NSRange range = [self rangeOfString:other];
    return range.length != 0;
}

- (NSString *)appendStrings:(id)firstObj, ... NS_REQUIRES_NIL_TERMINATION
{
    NSMutableString * string = [NSMutableString stringWithString:self];
    if (firstObj == nil) {
        firstObj = @"";
    }
    [string appendString:firstObj];

    
    va_list argumentList;
    va_start(argumentList, firstObj);
    id object;
    while(1){
        object = va_arg(argumentList,id);
        if (object == nil)
            break;
        [string appendString:object];
    }
    va_end(argumentList);
    return string;
}

- (BOOL)isNull{
    if ([self isKindOfClass:[NSNull class]] || [self isEqual:[NSNull null]] || self == nil) {
        return YES;
    }else {
        return NO;
    }
}

@end
