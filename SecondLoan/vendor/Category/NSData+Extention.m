//
//  NSData+Extention.m
//  JLiOSBase
//
//  Created by mac mini on 2017/1/6.
//  Copyright © 2017年 JL. All rights reserved.
//

#import "NSData+Extention.h"

@implementation NSData(Extention)
- (NSString *)base64Encode
{
    return [self base64EncodedStringWithOptions:0];;
}
- (NSString *)hexString
{
    if (!self || [self length] == 0) {
        return @"";
    }
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[self length]];
    [self enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    return string;
}
@end
