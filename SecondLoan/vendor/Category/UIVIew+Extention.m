//
//  UIVIew+Extention.m
//  easyride_kuaiji
//
//  Created by gaodun on 15/9/9.
//  Copyright (c) 2015年 JET. All rights reserved.
//

#import "UIView+Extention.h"

@implementation UIView(Extention)


#pragma mark - Getter && Setter
- (void)setGDsize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size.height = size.height;
    frame.size.width = size.width;
    self.frame = frame;
}
-(CGSize)GDsize
{
    return self.frame.size;
}
- (CGFloat)GDx
{
    return self.frame.origin.x;
}
- (void)setGDx:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}
- (CGFloat)GDy
{
    return self.frame.origin.y;
}
- (void)setGDy:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)GDRightx
{
    return self.GDx + self.GDwidth;
}

- (CGFloat)GDBottomy
{
    return self.GDy + self.GDheight;
}

- (CGFloat)GDwidth
{
    return self.frame.size.width;
}
- (void)setGDwidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)GDheight
{
    return self.frame.size.height;
}
- (void)setGDheight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)GDcenterX
{
    return self.center.x;
}
- (void)setGDcenterX:(CGFloat)centerX
{
    CGPoint point = self.center;
    point.x = centerX;
    self.center = point;
}

- (CGFloat)GDcenterY
{
    return self.center.y;
}
- (void)setGDcenterY:(CGFloat)centerY
{
    CGPoint point = self.center;
    point.y = centerY;
    self.center = point;
}



#pragma mark - Public Method
- (void)GDDaddSubviews:(id)firstObj, ... NS_REQUIRES_NIL_TERMINATION
{
    [self addSubview:firstObj];
    
    va_list argumentList;
    va_start(argumentList, firstObj);
    id object;
    while(1){
        object = va_arg(argumentList,id);
        if (object == nil)
            break;
        [self addSubview:object];
    }
    va_end(argumentList);
}
- (void)GDaddSubviews:(NSArray *)objects
{
    if (objects == nil && objects.count == 0) {
        return;
    }
    for (int i = 0; i< objects.count; i ++) {
        [self addSubview:objects[i]];
    }
}

- (void)removeAllSubviews
{
    NSArray * array = self.subviews;
    for(int i = 0;i<[array count];i++){
        [[ array objectAtIndex:i] removeFromSuperview];
    }
}


@end
