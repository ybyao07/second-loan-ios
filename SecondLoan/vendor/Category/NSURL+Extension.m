//
//  NSURL+Extension.m
//  mobilePD
//
//  Created by wangyan on 2016－06－07.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "NSURL+Extension.h"

@implementation NSURL(Extension)
+ (NSURL *)URLWithStringAutoEncode:(NSString *)URLString
{
    NSString* encodedString = [URLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"`#%^{}\"[]|\\<> "].invertedSet];
    return [self URLWithString:encodedString];
}
@end
