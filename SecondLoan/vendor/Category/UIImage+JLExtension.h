//
//  UIImage+JLExtension.h
//  JLiOSBase
//
//  Created by mac mini on 2017/1/16.
//  Copyright © 2017年 JL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(JLExtension)
- (UIImage *)toScale:(float)scaleSize;
@end
