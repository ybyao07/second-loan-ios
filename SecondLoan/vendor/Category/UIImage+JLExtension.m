//
//  UIImage+JLExtension.m
//  JLiOSBase
//
//  Created by mac mini on 2017/1/16.
//  Copyright © 2017年 JL. All rights reserved.
//

#import "UIImage+JLExtension.h"

@implementation UIImage(JLExtension)
- (UIImage *)toScale:(float)scaleSize

{
    UIGraphicsBeginImageContext(CGSizeMake(self.size.width * scaleSize, self.size.height * scaleSize));
    [self drawInRect:CGRectMake(0, 0, self.size.width * scaleSize, self.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
@end
