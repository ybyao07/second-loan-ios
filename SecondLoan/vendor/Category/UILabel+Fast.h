//
//  UILabel+Fast.h
//  yrtx
//
//  Created by mac mini on 2016/11/22.
//  Copyright © 2016年 mac mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel(Fast)
+ (instancetype)labelWithText:(NSString*)test;
@end
