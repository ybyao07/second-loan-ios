//
//  UIButton+QuickSet.m
//  gaodun
//
//  Created by CHEN on 15/1/21.
//  Copyright (c) 2015年 陈君. All rights reserved.
//

#import "UIButton+QuickSet.h"

@implementation UIButton (QuickSet)

+ (id)buttonWithImageName:(NSString *)imageName {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:imageName];
    UIImage *highlightImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed",imageName]];
    
    // make resizable image
    CGFloat resizbleMargin = 0;
    if ((NSInteger)image.size.width % 2 == 0){
        resizbleMargin = floorf(image.size.width / 2) - 1;
    }
    else {
        resizbleMargin = floor(image.size.width / 2);
    }
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, resizbleMargin, 0, resizbleMargin)];
    highlightImage = [highlightImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, resizbleMargin, 0, resizbleMargin)];
    
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:highlightImage forState:UIControlStateHighlighted];
    [button setImage:highlightImage forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    
    return button;
}

+ (id)buttonWithBackgroundImageName:(NSString *)imageName{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:imageName];
    UIImage *highlightImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed",imageName]];
    UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_select",imageName]];
    
    // make resizable image
    CGFloat resizbleMargin = 0;
    if ((NSInteger)image.size.width % 2 == 0) {
        resizbleMargin = floorf(image.size.width / 2) - 1;
    }
    else {
        resizbleMargin = floorf(image.size.width / 2);
    }
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, resizbleMargin, 0, resizbleMargin)];
    highlightImage = [highlightImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, resizbleMargin, 0, resizbleMargin)];
    selectedImage = [selectedImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, resizbleMargin, 0, resizbleMargin)];
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:selectedImage forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    
    return button;
}
/**
 *  快速建立button junlei
 *
 *  @param str          <#str description#>
 *  @param tcolor       <#tcolor description#>
 *  @param fontsize     <#fontsize description#>
 *  @param bcolor       <#bcolor description#>
 *  @param cornerRadius <#cornerRadius description#>
 *  @param targ         <#targ description#>
 *  @param action       <#action description#>
 */
+ (UIButton *)buttonWithtitle:(NSString*)str titleColor:(UIColor *)tcolor titleSize:(CGFloat)fontsize backColor:(UIColor *)bcolor cornerRadius:(CGFloat)cornerRadius target:(id)targ action:(SEL)action
{
    UIButton * button = [[UIButton alloc]init];
    button.backgroundColor = bcolor;
    button.titleLabel.font = [UIFont systemFontOfSize:fontsize];
    [button setTitle:str forState:UIControlStateNormal];
    [button setTitleColor:tcolor forState:UIControlStateNormal];
    [button setTitleColor:[tcolor colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    button.layer.cornerRadius = cornerRadius;
    button.layer.masksToBounds = YES;
    [button addTarget:targ action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)setTitle:(nullable NSString *)title color:(nullable UIColor *)color
{
    if (title) {
        [self setTitle:title forState:UIControlStateNormal];
    }
    if (color) {
        [self setTitleColor:color forState:UIControlStateNormal];
    }
}
- (void)setTitle:(nullable NSString *)title size:(CGFloat)size color:(nullable UIColor *)color
{
    self.titleLabel.font = [UIFont systemFontOfSize:size];
    if (title) {
        [self setTitle:title forState:UIControlStateNormal];
    }
    if (color) {
        [self setTitleColor:color forState:UIControlStateNormal];
    }
}
- (void)setTitle:(nullable NSString *)title autoColor:(nullable UIColor *)color
{
    if (title) {
        [self setTitle:title forState:UIControlStateNormal];
    }
    if (color) {
        [self setTitleColor:color forState:UIControlStateNormal];
//        [self setTitleColor:[UIColor colorWithRed:color.red/2.0 green:color.green/2.0 blue:color.blue/2.0 alpha:1.0] forState:UIControlStateHighlighted];
    }
}
@end
