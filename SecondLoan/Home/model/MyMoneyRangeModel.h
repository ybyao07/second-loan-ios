//
//  MyMoneyRangeModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/31.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyMoneyRangeModel : NSObject
//@property (assign, nonatomic) NSInteger borrowMoney;    // 借款
@property (assign, nonatomic) NSInteger viewQuota;          // 借款额度
@property (assign, nonatomic) NSInteger surplusMoney;       // 待还金额
@property (assign, nonatomic) NSInteger status;      // 额度审批状态 1：审核中 2：：审批成功   3：审批失败
@end

NS_ASSUME_NONNULL_END
