//
//  MessageTotalModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTotalModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *content;

+ (MessageTotalModel *)createModelWithTitle:(NSString *)title icon:(NSString *)icon content:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
