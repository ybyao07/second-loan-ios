//
//  ProductModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/25.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductModel : NSObject

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *productName; //名称
@property (nonatomic, copy) NSString *productLogo;  //产品LOGO
@property (nonatomic, copy) NSString *productPic;  //产品宣传图
@property (nonatomic, copy) NSString *bankName;
@property (nonatomic, copy) NSString *bankUrl;      //银行LOGO
@property (nonatomic, assign) NSInteger maxQuota;     //最高额度
@property (nonatomic, copy) NSString *applyProcess;  //申请流程图
@property (nonatomic, copy) NSString *rate;         //月利率
@property (nonatomic, copy) NSString *condition;        //申请条件
@property (nonatomic, copy) NSString *preInfo;        //准备资料
@property (nonatomic, copy) NSString *loanTerm;     //贷款期限
@property (nonatomic, copy) NSString *productDec;   //产品宣传语
@property (nonatomic, copy) NSString *productExplain;  // 产品特点说明

@end

NS_ASSUME_NONNULL_END
