//
//  HomeBannerModel.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeBannerModel : NSObject

@property (nonatomic, copy) NSString *picture;
@property (nonatomic, copy) NSString *jumpTitle;
@property (nonatomic, copy) NSString *jumpType;  //跳转类型
@property (nonatomic, copy) NSString *jumpURL;

@end

NS_ASSUME_NONNULL_END
