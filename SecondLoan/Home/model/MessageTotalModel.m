//
//  MessageTotalModel.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/26.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "MessageTotalModel.h"

@implementation MessageTotalModel

+ (MessageTotalModel *)createModelWithTitle:(NSString *)title icon:(NSString *)icon content:(NSString *)content
{
    MessageTotalModel *model = [[MessageTotalModel alloc] init];
    model.title = title;
    model.icon = icon;
    model.content = content;
    return model;
}


@end
