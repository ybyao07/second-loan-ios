//
//  SLVerifySubmitViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLVerifySubmitViewController.h"
#import "HomeController.h"
@interface SLVerifySubmitViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblHint;

@end

@implementation SLVerifySubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"提交审批";
    if (_isPuhui) {
        _lblHint.text = @"您的申请信息已提交\n您可返回首页立即查看额度审批结果";
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (IBAction)backHomePage:(id)sender {
    [self.navigationController popToRootViewControllerAnimated: YES];
    UINavigationController *naviController = getDelegate().masterNavigationController;
    [naviController.viewControllers[0] setSelectedIndex:0];
}

@end
