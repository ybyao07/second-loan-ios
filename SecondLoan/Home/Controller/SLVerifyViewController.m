//
//  SLVerifyViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLVerifyViewController.h"

@interface SLVerifyViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblMoney;


@end

@implementation SLVerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"审核结果";
    _lblMoney.text = [NSString stringWithFormat:@"%@元", self.money];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (IBAction)goHomeAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}




@end
