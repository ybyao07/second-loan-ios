//
//  PDWebViewController.m
//  mobilePD
//
//  Created by wangyan on 2016－05－30.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "PDWebViewController.h"
#import <SafariServices/SafariServices.h>

@interface PDWebViewController ()<UIWebViewDelegate>

@end

@implementation PDWebViewController
- (void)loadView
{
    [super loadView];
//    self.navigationBar.barTintColor = COLOR_Theme;
    [self.view addSubview:self.webView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addConstraints];
    if (self.webUrl) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:self.webUrl]];
        [request setHTTPMethod:@"GET"];
        [self HUDshowWithStatus:nil];
        [self.webView loadRequest:request];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_isAboutUS) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"back_red"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        button.bounds = CGRectMake(0, 0, 20, 30);
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        self.navigationItem.leftBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:button];
        [self.navigationController setNavigationBarHidden:NO animated:animated];
        self.navigationBarBgView.backgroundColor = [UIColor whiteColor];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
}
- (void)goBack{
    debugLog(@"back");
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (_isAboutUS) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addConstraints
{
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
    }];
}
#pragma mark - getter
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc]init];
        _webView.delegate = self;
        _webView.scrollView.bounces = NO;
    }
    return _webView;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"%@:%@ body{%@}",request.HTTPMethod,request,request.HTTPBody.utf8String);
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self HUDshowErrorWithStatus:error.localizedDescription];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *theTitle=[webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if (_isAboutUS) {
    }else{
        self.title = theTitle;
    }
    [self HUDdismiss];
    if (self.didFinishLoad) {
        self.didFinishLoad(webView);
    }
}
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
//{
//    
//}
#pragma mark -- method
- (void)refresh
{
    [self HUDshowWithStatus:nil];
    [self.webView reload];
}
@end
