//
//  HomeController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/18.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "HomeController.h"
#import "SDCycleScrollView.h"
#import "HomeBannerModel.h"
#import "PDWebViewController.h"
#import "HomeTopViewNoLogin.h"
#import "HomeHeaderView.h"
#import "HomeTableCell.h"
#import "CollectionImageCell.h"
#import "LoginController.h"
#import "SLProductInfoViewController.h"
#import "PLRequestManage.h"
#import "SLMessageViewController.h"
#import "ProductModel.h"
#import "SLVerifyViewController.h"
#import "SLVerifyFailedViewController.h"
#import "MyMoneyRangeModel.h"
#import "SLPHApplyLoanViewController.h"
#import "UserMineInfo.h"
#import "SLBorrowRecordController.h"
#import "SLVerifySubmitViewController.h"
#import "SLVerifyFailedViewController.h"
#import "WKBGlobalData.h"
#import "SLMessageListController.h"
#import "UIView+JXTapEvent.h"
#import "CommonModelTypeDIc.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

static CGFloat const tableSectionHeaderHeight = 50.0f;
static CGFloat const tableCellHeightRatio = 200.0/750;

@interface HomeController ()<UITableViewDelegate, UITableViewDataSource, SDCycleScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) HomeHeaderView *headerView;
@property (strong, nonatomic) NSArray *bannerList;//banner列表
@property (nonatomic, strong) NSMutableArray *arrColleDatas;
@property (strong, nonatomic) HomeTableCell *productCell;
@property (strong, nonatomic) ProductModel *productPuhui;
@property (strong, nonatomic) MyMoneyRangeModel *myMoneyRange; // 我的额度
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) UILabel *indicatorLabel;
@property (assign, nonatomic) UIStatusBarStyle    statusBarStyle;  /**< 状态栏样式 */

@end

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.bannerList = [HomeBannerModel mj_objectArrayWithKeyValuesArray:@[
    @{
        @"picture":@"product_detail_top1.jpg",
        @"jumpURL":@"",
    },
    @{
      @"picture":@"product_detail_top2.jpg",
      @"jumpURL":@""
    },
    @{
        @"picture":@"product_detail_top3.jpg",
        @"jumpURL":@""
        }]];
    [self setupTopScrollViewWithImageURLArray];

    UIView *leftTop = [[UIView alloc] initWithFrame:CGRectMake(0, statusBarHeight, 350, 44)];
    UIImageView *imgLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_company"]];
    [imgLogo addTapOneTarget:self action:@selector(goAboutUs)];
    imgLogo.contentMode = UIViewContentModeScaleAspectFit;
    imgLogo.mj_origin = CGPointMake(10, (CGRectGetHeight(leftTop.frame) - imgLogo.bounds.size.height)/2.0);
    [leftTop addSubview:imgLogo];
    [self.navigationBarBgView addSubview:leftTop];

    UIButton *rightMegBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rightMegBtn setImage:[UIImage imageNamed:@"icon_nav_white_message"] forState:UIControlStateNormal];
    [rightMegBtn setBackgroundImage:[UIImage imageNamed:@"icon_nav_white_message"] forState:UIControlStateNormal];
    rightMegBtn.frame = CGRectMake(SCREEN_WIDTH - 36, statusBarHeight + 7, 18, 20);
    [rightMegBtn addTarget:self action:@selector(onMessageAction:) forControlEvents:UIControlEventTouchUpInside];

    UILabel *indicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(rightMegBtn.bounds.size.width - 4, 0, 6, 6)];
    [rightMegBtn addSubview: indicatorLabel];
    indicatorLabel.backgroundColor = [UIColor whiteColor];
    indicatorLabel.layer.cornerRadius = 3;
    indicatorLabel.layer.masksToBounds = YES;
    self.indicatorLabel = indicatorLabel;
    self.indicatorLabel.hidden = YES;

    [self.navigationBarBgView addSubview:rightMegBtn];
    [self loadSelectList];
    [self setDefautTopView];
}

- (void)setDefautTopView{
    if ([WKBGlobalData shareManager].homeMoneyModel) {
        self.myMoneyRange = [WKBGlobalData shareManager].homeMoneyModel;
        self.productPuhui = [WKBGlobalData shareManager].homeProduct;
        if (self.myMoneyRange && self.myMoneyRange.status == CodeCreditPassed) { //
            self.headerView.headTopCreditView.model = self.myMoneyRange;
            [self.headerView.headTopCreditView.imgBank sd_setImageWithURL:[NSURL URLWithString:self.productPuhui.bankUrl]];
            self.headerView.headTopCreditView.lblName.text = self.productPuhui.bankName;
            [self.headerView.headTopCreditView setProductName:self.productPuhui.productName];
            self.headerView.headTopCreditView.hidden = NO;
            self.headerView.headTopView.hidden = YES;
        }else{
            self.headerView.headTopView.model = self.productPuhui;
            self.headerView.headTopCreditView.hidden = YES;
            self.headerView.headTopView.hidden = NO;
        }
    }
}

- (void) goAboutUs{
    PDWebViewController * vc = [[PDWebViewController alloc] init];
    vc.webUrl =  url_about_us;
    vc.isAboutUS = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getNewMessage {
    if([UserLocal isLogin]){
        NSMutableDictionary * param = [NSMutableDictionary dictionary];
        [param setObject:[UserLocal token] forKey:@"token"];
        WeakSelf
        [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_get_new_message) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
            StrongSelf
            if (!error) {
                [strongSelf setNewMessage:responseObject[@"data"]];
            }
        }];
    }
}

- (void)setNewMessage:(NSNumber *)newMessage {
    self.indicatorLabel.hidden = !([newMessage integerValue] == 1);
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self addClearNavBarStyle];
    [self loadData];
    [self getNewMessage];
}
- (void)loadData{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:@(1) forKey:@"pageNum"];
    [param setObject:@(10) forKey:@"pageSize"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_home_product_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error){
        if (!error) {
            NSArray * list = responseObject[@"data"][@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            if ([list count]<4) {
                for (int i = 0; i< [list count]; i++) {
                    ProductModel * data = [ProductModel mj_objectWithKeyValues:list[i]];
                    [mlist addObject:data];
                }
            }else{
                for (int i = 0; i< 4; i++) {
                    ProductModel * data = [ProductModel mj_objectWithKeyValues:list[i]];
                    [mlist addObject:data];
                }
            }
            self.arrColleDatas = mlist;
            [self.productCell.collectionView reloadData];
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [self.tableView.mj_header endRefreshing];
    } offlineCache:url_home_product_list];
    [self loadProductPuhui];
}

- (void)loadSelectList{
    NSArray *arr =     @[@"marStatusMap",@"highEduMap",@"addressTypeMap",@"companyScaleMap",@"workPostMap",@"salaryMap",@"contactsRelationMap",@"collateralMortgageMap", @"occupationMap",@"applyTermMap",@"paybackTYPEMap",@"loanPurposeMap",@"ifHaveChildMap"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_select_list) Type:RequestPost parameters:arr completeHandle:^(id  _Nullable responseObject, NSError *error){
        if (!error) {
            NSDictionary *result = responseObject[@"data"];
//            [CommonModelTypeDIc shareManager].arrayMarriageType = [((NSDictionary *)result[@"marStatusMap"]).allValues sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            
            [CommonModelTypeDIc shareManager].arrayMarriageType = [CommanTool sortedWithDict:result[@"marStatusMap"]];
            [CommonModelTypeDIc shareManager].arrayHighEduType = [CommanTool sortedWithDict:result[@"highEduMap"]];
            [CommonModelTypeDIc shareManager].arrayAddressType = [CommanTool sortedWithDict:result[@"addressTypeMap"]];
            [CommonModelTypeDIc shareManager].arrayCompanyScale = [CommanTool sortedWithDict:result[@"companyScaleMap"]];
            [CommonModelTypeDIc shareManager].arrayWorkPost = [CommanTool sortedWithDict:result[@"workPostMap"]];
            [CommonModelTypeDIc shareManager].arraySalaryType = [CommanTool sortedWithDict:result[@"salaryMap"]];
            [CommonModelTypeDIc shareManager].arrayRelationType = [CommanTool sortedWithDict:result[@"contactsRelationMap"]];
            [CommonModelTypeDIc shareManager].arrayMortgage = [CommanTool sortedWithDict:result[@"collateralMortgageMap"]];
            [CommonModelTypeDIc shareManager].arrayWorkOccupation = [CommanTool sortedWithDict:result[@"occupationMap"]];
            [CommonModelTypeDIc shareManager].arrayLoanTerm = [CommanTool sortedWithDict:result[@"applyTermMap"]];
            [CommonModelTypeDIc shareManager].arrayPaybackType = [CommanTool sortedWithDict:result[@"paybackTYPEMap"]];
            [CommonModelTypeDIc shareManager].arrayLoanPurpos = [CommanTool sortedWithDict:result[@"loanPurposeMap"]];
            [CommonModelTypeDIc shareManager].arrayHavaChild = [CommanTool sortedWithDict:result[@"ifHaveChildMap"]];
        }
    } offlineCache:url_select_list];
    NSMutableDictionary *param = [NSMutableDictionary new];
    param[@"fatherid"] =  @"STAT";
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_select_industry) Type:RequestGet parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error){
        if (!error) {
            [CommonModelTypeDIc shareManager].arrayWorkType = responseObject[@"data"];
        }
    } offlineCache:url_select_industry];
    
    
//    [self loadProductPuhui];
}


- (void)loadProductPuhui{
    self.myMoneyRange = nil;
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_product_puhui) Type:RequestPost parameters:[NSDictionary new] completeHandle:^(id  _Nullable responseObject, NSError *error) {
        if (!error) {
            self.productPuhui = [ProductModel mj_objectWithKeyValues:responseObject[@"data"]];
            [WKBGlobalData shareManager].productIdPuhui = self.productPuhui.productId;
            [WKBGlobalData shareManager].homeProduct = self.productPuhui;
            if([UserLocal isLogin]){ // 如果登录，则调用是否有额度接口获取我的额度
                NSMutableDictionary * param = [NSMutableDictionary dictionary];
                [param setObject:[UserLocal token] forKey:@"token"];
                [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_my_quota) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
                    [self HUDdismiss];
                    if (!error) {
                        self.myMoneyRange = [MyMoneyRangeModel mj_objectWithKeyValues:responseObject[@"data"]];
                        [WKBGlobalData shareManager].homeMoneyModel = self.myMoneyRange;
                        if (self.myMoneyRange && self.myMoneyRange.status == CodeCreditPassed) { //
                            self.headerView.headTopCreditView.model = self.myMoneyRange;
                            [self.headerView.headTopCreditView.imgBank sd_setImageWithURL:[NSURL URLWithString:self.productPuhui.bankUrl]];
                             self.headerView.headTopCreditView.lblName.text = self.productPuhui.bankName;
                            [self.headerView.headTopCreditView setProductName:self.productPuhui.productName];
                            self.headerView.headTopCreditView.hidden = NO;
                            self.headerView.headTopView.hidden = YES;
                        }else{
                            self.headerView.headTopView.model = self.productPuhui;
                            self.headerView.headTopCreditView.hidden = YES;
                            self.headerView.headTopView.hidden = NO;
                        }
                    }else{
                        self.headerView.headTopView.model = self.productPuhui;
                        self.headerView.headTopCreditView.hidden = YES;
                        self.headerView.headTopView.hidden = NO;
                    }
                }];
            }else{
                self.headerView.headTopView.model = self.productPuhui;
                self.headerView.headTopCreditView.hidden = YES;
                self.headerView.headTopView.hidden = NO;
            }
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
    }];
}

#pragma mark ============= UITableViewDelegate ==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return tableSectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//  return SCREEN_HEIGHT - KStatusBarAndNavigationBarHeight - KTabbarHeight - tableSectionHeaderHeight - [HomeHeaderView height];
  return tableCellHeightRatio*SCREEN_WIDTH * 2 + KTabbarHeight + 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, tableSectionHeaderHeight)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_table_sec_hint"]];
    imgView.frame = CGRectMake(10, 15, 7, tableSectionHeaderHeight - 2*15);
    [view addSubview:imgView];
    UILabel *lblRecommend = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame) + 8, 0, 120, tableSectionHeaderHeight)];
    lblRecommend.text = @"优选推荐";
    lblRecommend.textColor = COLOR_BlackText_33;
    lblRecommend.font = Font14Bold;    
    [view addSubview:lblRecommend];
    
    UIButton *btnMore = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMore addTarget:self action:@selector(goMoreProductAction) forControlEvents:UIControlEventTouchUpInside];
    [btnMore setTitle:@"更多产品" forState:UIControlStateNormal];
    btnMore.titleLabel.textAlignment = NSTextAlignmentRight;
    [btnMore setFrame:CGRectMake(SCREEN_WIDTH - 96 , 0, 90, tableSectionHeaderHeight)];
    [btnMore setTitleColor:COLOR_Gary_99 forState:UIControlStateNormal];
    btnMore.titleLabel.font =  Font14;
    [view addSubview:btnMore];
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_right"]];
    CGFloat arrowHeight = arrow.bounds.size.height;
    CGFloat arrowWidth = arrow.bounds.size.width;
    arrow.frame = CGRectMake(SCREEN_WIDTH - arrow.bounds.size.width - 10, (tableSectionHeaderHeight - arrowHeight)/2.0, arrowWidth, arrowHeight);
    [view addSubview:arrow];
    view.backgroundColor = COLOR_Gary_FA;
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeTableCell"];
    cell.collectionView.delegate = self;
    cell.collectionView.dataSource = self;
    self.productCell = cell;
    return cell;
}

#pragma mark UICollectionViewDelegate
//每个section的item个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrColleDatas.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionImageCell" forIndexPath:indexPath];
    cell.model = self.arrColleDatas[indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 2 * kColletionHorMargin - 20) /2.0, tableCellHeightRatio*SCREEN_WIDTH);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (![UserLocal isLogin]) {
//        MainNavigationController * vc = [[MainNavigationController alloc]initWithRootViewController:[[LoginController alloc] init]];
//        [self presentViewController:vc animated:YES completion:nil];
//        return;
//    }else{ // 进入第三方产品详情
        SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
        ProductModel *model = self.arrColleDatas[indexPath.row];
        productInfo.productId = model.productId;
        [self.navigationController pushViewController:productInfo animated:YES];
//    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 16.0;
}
//
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 14.0;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, kColletionHorMargin, 0, kColletionHorMargin);
}


#pragma mark SLHomeCardsViewDelegate
- (void)clickCardsView:(id)view atRow:(NSInteger)row{
    if (![UserLocal isLogin]) {
        MainNavigationController * vc = [[MainNavigationController alloc]initWithRootViewController:[[LoginController alloc] init]];
        [self presentViewController:vc animated:YES completion:nil];
        return;
    }else{ // 进入第三方产品详情
        SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
        ProductModel *model = self.arrColleDatas[row];
        productInfo.productId = model.productId;
        [self.navigationController pushViewController:productInfo animated:YES];
    }
}
# pragma mark ----- action ---------------

- (void)goMoreProductAction{
    UINavigationController *naviController = getDelegate().masterNavigationController;
    [naviController.viewControllers[0] setSelectedIndex:1];
}

- (void)onMessageAction:(UIButton *)btn{
    if (![UserLocal isLogin]) {
        MainNavigationController * vc = [[MainNavigationController alloc]initWithRootViewController:[[LoginController alloc] init]];
        [self presentViewController:vc animated:YES completion:nil];
        return;
    }else{
      SLMessageListController *detailVC = [[SLMessageListController alloc] init];
      detailVC.type = 1;
      [self.navigationController pushViewController:detailVC animated:YES];
    };
}
#pragma mark - ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ 循环滚动 ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
- (void)setupTopScrollViewWithImageURLArray {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //        _homeScrollView.placeholderImage = [UIImage imageNamed:@"default_fang"];
        NSMutableArray *imgBanners = [NSMutableArray new];
        [self.bannerList enumerateObjectsUsingBlock:^(HomeBannerModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [imgBanners addObject:obj.picture];
//            [imgBanners addObject:@"product_detail_top"];
        }];
         self.headerView.homeScrollView.imageURLStringsGroup = imgBanners;
    });
    self.headerView.homeScrollView.delegate = self;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    HomeBannerModel *banner = self.bannerList[index];
    if (banner.jumpURL.length) {
            PDWebViewController * vc = [[PDWebViewController alloc] init];
            vc.webUrl =  banner.jumpURL;
            [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ============ set && get 方法 ==========
- (NSMutableArray *)arrColleDatas
{
    if (!_arrColleDatas) {
        _arrColleDatas = [NSMutableArray new];
    }
    return _arrColleDatas;
}
- (UITableView *)tableView{
    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KStatusBarAndNavigationBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT - KStatusBarAndNavigationBarHeight) style:UITableViewStylePlain];
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, -KStatusBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.tableHeaderView = self.headerView;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorFromHex(0xF3F3F3);
        [_tableView registerNib:[UINib nibWithNibName:@"HomeTableCell" bundle:nil] forCellReuseIdentifier:@"HomeTableCell"];
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self loadData];
        }];
    }
    return  _tableView;
}

- (HomeHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [[HomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, [HomeHeaderView height])];
        WeakSelf
        _headerView.aboutClickBlock = ^(){
            PDWebViewController * vc = [[PDWebViewController alloc] init];
            vc.webUrl =  url_about_us;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        // 查看额度
        _headerView.checkMyCreditBlock = ^(){
//            UserInfo *info = [[UserLocal sharedInstance] currentUserInfo];
            if ([UserLocal isLogin]) { // 登录 --
                //重新调用获取额度接口 ------- 如果成果则跳转到审核通过的界面 ----- SLVerifyViewController
                NSMutableDictionary * param = [NSMutableDictionary dictionary];
                [param setObject:[UserLocal token] forKey:@"token"];
                [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_my_quota) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
                    if (!error) {
                        weakSelf.myMoneyRange = [MyMoneyRangeModel mj_objectWithKeyValues:responseObject[@"data"]];
                        [WKBGlobalData shareManager].homeMoneyModel = weakSelf.myMoneyRange;
                        if (weakSelf.myMoneyRange) {
                            if (weakSelf.myMoneyRange.status == CodeCreditDoing) {
                                SLVerifySubmitViewController *doingVC = [[SLVerifySubmitViewController alloc] initWithNibName:@"SLVerifySubmitViewController" bundle:nil];
                                doingVC.isPuhui = YES;
                                [weakSelf.navigationController pushViewController:doingVC animated:YES];
                            }else if (weakSelf.myMoneyRange.status == CodeCreditNotPassed){
                                SLVerifyFailedViewController *failedVC = [[SLVerifyFailedViewController alloc] initWithNibName:@"SLVerifyFailedViewController" bundle:nil];
                                [weakSelf.navigationController pushViewController:failedVC animated:YES];
                            }else if (weakSelf.myMoneyRange.status == CodeCreditPassed ){
                                SLVerifyViewController *vcController = [[SLVerifyViewController alloc] init];
                                vcController.money = [NSString stringWithFormat:@"%@", [NSString getMoneyStringWithMoneyNumber:weakSelf.myMoneyRange.viewQuota]];
                                [weakSelf.navigationController pushViewController:vcController animated:YES];
                            }
                        }else{
                            if (weakSelf.productPuhui) {
                                SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
                                productInfo.productId = weakSelf.productPuhui.productId;
                                productInfo.isPuhui = YES;
                                [weakSelf.navigationController pushViewController:productInfo animated:YES];
                            }
                        }
                    }else{
                        if (weakSelf.productPuhui) {
                            SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
                            productInfo.productId = weakSelf.productPuhui.productId;
                            productInfo.isPuhui = YES;
                            [weakSelf.navigationController pushViewController:productInfo animated:YES];
                        }
                    }
                }];
//                if (weakSelf.myMoneyRange) {
//                    if (weakSelf.myMoneyRange.status == CodeCreditDoing) {
//                        SLVerifySubmitViewController *doingVC = [[SLVerifySubmitViewController alloc] initWithNibName:@"SLVerifySubmitViewController" bundle:nil];
//                        [weakSelf.navigationController pushViewController:doingVC animated:YES];
//                    }else if (weakSelf.myMoneyRange.status == CodeCreditNotPassed){
//                        SLVerifyFailedViewController *failedVC = [[SLVerifyFailedViewController alloc] initWithNibName:@"SLVerifyFailedViewController" bundle:nil];
//                        [weakSelf.navigationController pushViewController:failedVC animated:YES];
//                    }
//                }else{
//                    if (weakSelf.productPuhui) {
//                        SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
//                        productInfo.productId = weakSelf.productPuhui.productId;
//                        productInfo.isPuhui = YES;
//                        [weakSelf.navigationController pushViewController:productInfo animated:YES];
//                    }
//                }
            }else{
                if (weakSelf.productPuhui) {
                    SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
                    productInfo.productId = weakSelf.productPuhui.productId;
                    productInfo.isPuhui = YES;
                    [weakSelf.navigationController pushViewController:productInfo animated:YES];
                }
            }
        };
        // 立即借款
        _headerView.goBorrowBlock = ^(){
//            SLPHApplyLoanViewController *applyVC = [[SLPHApplyLoanViewController alloc] init];
//            applyVC.productId = weakSelf.productPuhui.productId;
//            [weakSelf.navigationController pushViewController:applyVC animated:YES];
            PDWebViewController * vc = [[PDWebViewController alloc] init];
            vc.webUrl = url_download;
            [weakSelf.navigationController.navigationController pushViewController:vc animated:YES];
        };
        // 查看借款历史
        _headerView.checkMyBorrowBlock = ^(){
            SLBorrowRecordController *finishVC = [[SLBorrowRecordController alloc] init];
            [weakSelf.navigationController pushViewController:finishVC animated:YES];
        };
    }
    return _headerView;
}
#pragma mark 添加首页导航条渐变原始样式
- (void)addClearNavBarStyle {
    self.navigationBarBgView.backgroundColor = [UIColor clearColor];
    [self scrollToChangeNavBarBgColor:_tableView.contentOffset.y];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self scrollToChangeNavBarBgColor:scrollView.contentOffset.y];
}

#pragma mark 根据滚动对导航条进行颜色渐变及隐藏
- (void)scrollToChangeNavBarBgColor:(CGFloat)offsetY {
    self.navigationBarBgView.frame = CGRectMake(0,  - offsetY - KStatusBarHeight, SCREEN_WIDTH, KStatusBarAndNavigationBarHeight);
    if (offsetY >= -KStatusBarHeight) {
        self.navigationBarBgView.backgroundColor = COLOR_ThemeRed;
        self.navigationBarBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, KStatusBarAndNavigationBarHeight);
        self.statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        self.navigationBarBgView.backgroundColor = [UIColor clearColor];
        self.statusBarStyle = UIStatusBarStyleLightContent;
    }
    [self setNeedsStatusBarAppearanceUpdate];
//    [self setAlpha:alpha];
}

- (void)setAlpha:(CGFloat)alpha {
    UIColor *color = [UIColor colorWithWhite:1 alpha:alpha];
    self.navigationBarBgView.backgroundColor = color;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.statusBarStyle;
}



@end
