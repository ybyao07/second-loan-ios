//
//  SLVerifyFailedViewController.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/28.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLVerifyFailedViewController.h"
#import "CooperatorCell.h"
#import "SLProductInfoViewController.h"
#import "ProductModel.h"

static CGFloat const tableSectionHeaderHeight = 50.0f;
@interface SLVerifyFailedViewController()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation SLVerifyFailedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"审核结果";
     [_tableView registerNib:[UINib nibWithNibName:@"CooperatorCell" bundle:nil] forCellReuseIdentifier:@"CooperatorCell"];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self loadCurrentPageData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)loadCurrentPageData
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[UserLocal token] forKey:@"token"];
    [param setObject:@(1) forKey:@"pageNum"];
    [param setObject:@(20) forKey:@"pageSize"];
    [[PLRequestManage sharedInstance] httpRequestWithUrl:HTTPAPIWITH(url_home_product_list) Type:RequestPost parameters:param completeHandle:^(id  _Nullable responseObject, NSError *error) {
        [self HUDdismiss];
        UITableView * tableView = self.tableView;
        NSDictionary *resultData = responseObject[@"data"];
        AUTO_TABLE_PLACEHOLDER(tableView,self.dataSource,resultData,error)
        if (!error) {
            NSArray * list = resultData[@"rows"];
            NSMutableArray * mlist = [NSMutableArray array];
            for (int i = 0; i< list.count; i ++) {
                ProductModel * data = [ProductModel mj_objectWithKeyValues:list[i]];
                [mlist addObject:data];
            }
            self.dataSource = mlist;
        } else {
            [self HUDshowErrorWithStatus:error.localizedDescription];
        }
        [tableView reloadData];
    }];
}

#pragma mark ============= UITableViewDelegate ==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return tableSectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CooperatorCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, tableSectionHeaderHeight)];
    view.backgroundColor = COLOR_White;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_table_sec_hint"]];
    imgView.frame = CGRectMake(10, 15, 7, tableSectionHeaderHeight - 2*15);
    [view addSubview:imgView];
    UILabel *lblRecommend = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imgView.frame) + 8, 0, 120, tableSectionHeaderHeight)];
    lblRecommend.text = @"贷款产品";
    lblRecommend.font = Font14Bold;
    [view addSubview:lblRecommend];
//    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CooperatorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CooperatorCell"];
    if (cell == nil) {
        cell = [[CooperatorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CooperatorCell"];
    }
    cell.model = self.dataSource[indexPath.row];
    return cell;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if (![UserLocal isLogin]) {
//        MainNavigationController * vc = [[MainNavigationController alloc]initWithRootViewController:[[LoginController alloc] init]];
//        [self presentViewController:vc animated:YES completion:nil];
//        return;
//    }else{ // 进入第三方产品详情
        SLProductInfoViewController *productInfo = [[SLProductInfoViewController alloc] init];
        ProductModel *model = self.dataSource[indexPath.row];
        productInfo.productId = model.productId;
        [self.navigationController pushViewController:productInfo animated:YES];
//    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat sectionHeaderHeight = 40;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
//}

@end
