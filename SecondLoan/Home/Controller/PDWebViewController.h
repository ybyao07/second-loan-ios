//
//  PDWebViewController.h
//  mobilePD
//
//  Created by wangyan on 2016－05－30.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import "BaseViewController.h"


@interface PDWebViewController : BaseViewController
@property (nonatomic,strong) NSString * webUrl;
//@property (nonatomic,strong) NSString * params;
@property (nonatomic,strong) NSDictionary * webParams;
@property (nonatomic,strong) UIWebView * webView;
@property (nonatomic, assign) BOOL isAboutUS;
@property (nonatomic,copy) void (^ didFinishLoad)(UIWebView * webView);
@end
