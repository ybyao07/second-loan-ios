//
//  HomeTopViewNoLogin.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeTopViewNoLogin : UIView

+ (instancetype)newView;
@property (weak, nonatomic) IBOutlet UILabel *lblMoney;
@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
@property (weak, nonatomic) IBOutlet UIImageView *imgBank;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthRate;
@property (weak, nonatomic) IBOutlet UILabel *productDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgNameBg;

@property (strong, nonatomic) ProductModel *model;

@end

NS_ASSUME_NONNULL_END
