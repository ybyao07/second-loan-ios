//
//  HomeHeaderView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "HomeTopViewNoLogin.h"
#import "UserInfo.h"
#import "HomeTopViewLoginCredit.h"
#import "UserMineInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeHeaderView : UIView

@property (nonatomic, strong) UILabel *lblLogo;
@property (nonatomic, strong) UIButton *btnAboutUs;

@property (nonatomic, strong) SDCycleScrollView *homeScrollView;    //轮播图
@property (nonatomic, strong) HomeTopViewNoLogin *headTopView;
@property (nonatomic, strong) HomeTopViewLoginCredit *headTopCreditView;
@property (nonatomic, strong) void (^aboutClickBlock)(void);  //关于我们
@property (nonatomic, strong) void (^checkMyCreditBlock)(void); // 查看余额
@property (nonatomic, strong) void (^checkMyBorrowBlock)(void); //查看借款明细
@property (nonatomic, strong) void (^goBorrowBlock)(void);  //立即借款


+ (CGFloat)height;

@end

NS_ASSUME_NONNULL_END
