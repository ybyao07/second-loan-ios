//
//  HomeTopViewNoLogin.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "HomeTopViewNoLogin.h"
#import "CommanTool.h"
#import "NSString+Tools.h"
@interface HomeTopViewNoLogin()

@property (strong, nonatomic) NSMutableArray<UIButton *> *arrBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblBankname;

@end


@implementation HomeTopViewNoLogin

+ (instancetype)newView
{
    NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"HomeViews" owner:self options:nil];
    HomeTopViewNoLogin * infoView = (HomeTopViewNoLogin *)[views firstObjectWithClass:self];
    [CommanTool setShadowOnView:infoView];
    return infoView;
}

- (void)setModel:(ProductModel *)model{
    _model = model;
    _lblMonthRate.text = [NSString stringWithFormat:@"月利率%@",model.rate];
    _lblMoney.text = [NSString getMoneyStringWithMoneyNumber:model.maxQuota];
    _lblName.text = model.productName;
    NSArray *array = [model.productDec componentsSeparatedByString:@","]; //字符串按照【分隔成数组
    for (UIButton *btn in self.arrBtn) {
        [btn removeFromSuperview];
    }
    [self.arrBtn removeAllObjects];
    self.arrBtn = [NSMutableArray new];
    for (NSString *strDes in array) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:strDes forState:UIControlStateNormal];
        btn.titleLabel.font = Font13;
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
        [btn setTitleColor:COLOR_Gary_99 forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"icon_correct"] forState:UIControlStateNormal];
        [self addSubview:btn];
        [self.arrBtn addObject:btn];
    }
    [_imgBank sd_setImageWithURL:[NSURL URLWithString:[model.bankUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  placeholderImage:[UIImage imageNamed:@""]];
    _lblBankname.text = _model.bankName;
    [self updateDescripConstrain];
}




- (void)updateDescripConstrain{
    CGSize sizeName = [self.model.productName sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 110)];
    [self.imgNameBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self).with.mas_offset(12);
        make.height.equalTo(@(sizeName.height + 28));
        make.width.equalTo(@(sizeName.width + 14));
    }];
    NSInteger count = self.arrBtn.count;
    NSInteger center = count/2;
    CGFloat lblHeight = 26;
    CGFloat btnMargin = 8;
    if (count % 2 == 0) { //偶数 -- 只取前2个
        if (count >= 2) {
            UIButton *firstBtn = self.arrBtn[0];
            NSString *text1 = firstBtn.titleLabel.text;
            CGSize size1 = [text1 sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width1 = size1.width + 6;
            [firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.productDescLabel.mas_top);
                make.height.equalTo(@(lblHeight));
                make.width.equalTo(@(width1 + btnMargin));
                make.right.equalTo(self.productDescLabel.mas_left).with.mas_offset(3);
            }];
            UIButton *secondBtn = self.arrBtn[1];
            NSString *text2 = secondBtn.titleLabel.text;
            CGSize size2 = [text2 sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width2 = size2.width + 6;
            [secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.productDescLabel.mas_top);
                make.height.equalTo(@(lblHeight));
                make.width.equalTo(@(width2 + btnMargin));
                make.left.equalTo(self.productDescLabel.mas_right).with.mas_offset(3);
            }];
        }else{
            
        }
    }else{// ---- 保证中间的控件居中 ---
        UIButton *centerBtn = self.arrBtn[center];
        NSString *text = centerBtn.titleLabel.text;
        CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
        CGFloat width = size.width + 16;
        [centerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.productDescLabel.mas_top);
            make.width.equalTo(@(width + btnMargin));
            make.height.equalTo(@(lblHeight));
            make.centerX.equalTo(self.productDescLabel);
        }];
        UIButton *btnPre = centerBtn;
        for (int i = center - 1 ; i >= 0  ; i--) {
            UIButton *lbl = self.arrBtn[i];
            NSString *text = lbl.titleLabel.text;
            CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width = size.width + 16;
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.productDescLabel.mas_top);
                make.width.equalTo(@(width + btnMargin));
                make.height.equalTo(@(lblHeight));
                make.right.equalTo(btnPre.mas_left).with.offset(-4);
            }];
            btnPre = lbl;
        }
        btnPre = centerBtn;
        for (int i = center + 1 ; i < count  ; i++) {
            UIButton *lbl = self.arrBtn[i];
            NSString *text = lbl.titleLabel.text;
            CGSize size = [text sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 100)];
            CGFloat width = size.width + 16;
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.productDescLabel.mas_top);
                make.width.equalTo(@(width + btnMargin));
                make.height.equalTo(@(lblHeight));
                make.left.equalTo(btnPre.mas_right).with.offset(4);
            }];
            btnPre = lbl;
        }
    }
}

@end
