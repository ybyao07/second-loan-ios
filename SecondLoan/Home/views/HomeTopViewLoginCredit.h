//
//  HomeTopViewLoginCredit.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMoneyRangeModel.h"
#import "UserMineInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeTopViewLoginCredit : UIView

+ (instancetype)newView;
@property (weak, nonatomic) IBOutlet UILabel *lblMoney;
@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBorrow;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitTo;
@property (weak, nonatomic) IBOutlet UIImageView *imgBank;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgNameBg;

@property (strong, nonatomic) MyMoneyRangeModel *model;

- (void)setProductName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
