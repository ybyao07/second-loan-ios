//
//  CollectionImageCell.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
NS_ASSUME_NONNULL_BEGIN



@interface CollectionImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBankLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblBankName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductDes;

@property (nonatomic, strong) ProductModel *model;
@end

NS_ASSUME_NONNULL_END
