//
//  HomeTopViewLoginCredit.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/24.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "HomeTopViewLoginCredit.h"
#import "CommanTool.h"
#import "NSString+Tools.h"

@implementation HomeTopViewLoginCredit

+ (instancetype)newView
{
    NSArray * views =[[NSBundle mainBundle] loadNibNamed:@"HomeViews" owner:self options:nil];
    HomeTopViewLoginCredit * infoView = (HomeTopViewLoginCredit *)[views firstObjectWithClass:self];
    [CommanTool setShadowOnView:infoView];
    return infoView;
}

- (void)setModel:(MyMoneyRangeModel *)model
{
    _model = model;
    _lblMoney.text = [NSString getMoneyStringWithMoneyNumber:model.viewQuota];
    _lblWaitTo.text = [NSString stringWithFormat:@"当前待还金额:%@元",[NSString getMoneyStringWithMoneyNumber:model.surplusMoney]];
}

- (void)setProductName:(NSString *)name
{
    self.lblName.text = name;
    CGSize sizeName = [name sizeWithFont:Font15Medium constrainedToSize:CGSizeMake(SCREEN_WIDTH, 110)];
    [self.imgNameBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self).with.mas_offset(12);
        make.height.equalTo(@(sizeName.height + 28));
        make.width.equalTo(@(sizeName.width + 14));
    }];
}

@end
