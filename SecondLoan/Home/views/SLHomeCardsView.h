//
//  SLHomeCardsView.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

NS_ASSUME_NONNULL_BEGIN
@class SLHomeCardsView;
@protocol SLHomeCardsViewDelegate <NSObject>
- (void)clickCardsView:(SLHomeCardsView *)view atRow:(NSInteger)row;
@end

@interface SLHomeCardsView : UIView

@property(nonatomic, strong) NSArray *dataSource;

@property (nonatomic, weak) id<SLHomeCardsViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
