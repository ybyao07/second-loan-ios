//
//  SLHomeCardsView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/9/8.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "SLHomeCardsView.h"
#import "CollectionImageCell.h"
#import "SLProductInfoViewController.h"

NSString *const CellIdentifier = @"CollectionImageCell";
///CGFloat const HorizontalMargin = SCREEN_WIDTH/3 - 22;
//#define HorizontalMargin SCREEN_WIDTH /3 - 22
#define HorizontalMargin 10
CGFloat const ItemMargin = 20.0;

@interface SLHomeCardsView()<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) UIScrollView *panScrollView;

@property(nonatomic, assign, getter=isMultiplePages) BOOL multiplePage;
@property(nonatomic, strong) NSTimer *timer;
//@property (strong, nonatomic) UIView *lineView;
//@property (strong, nonatomic) UIView *line1;
//@property (strong, nonatomic) UIView *line2;
//@property (strong, nonatomic) UIView *line3;
//@property (strong, nonatomic) UIView *line4;

@end

@implementation SLHomeCardsView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self setupView];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
//        [self setupView];
    }
    return self;
}
- (void)awakeFromNib{
    [super awakeFromNib];
//    [self setupView];
}

- (void)setupView {
    CGFloat collectionViewWidth = self.frame.size.width;
    CGFloat collectionViewHeight = self.frame.size.height - 10;
    CGFloat itemWidth = (collectionViewWidth - ItemMargin * 2 - 20) / 2.0;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(itemWidth, collectionViewHeight);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = ItemMargin;
    layout.sectionInset = UIEdgeInsetsMake(0, HorizontalMargin, 0, HorizontalMargin);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, collectionViewWidth, collectionViewHeight) collectionViewLayout:layout];
    [self addSubview:collectionView];
    _collectionView = collectionView;
    collectionView.backgroundColor = [UIColor whiteColor];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.alwaysBounceHorizontal = YES;
    collectionView.clipsToBounds = NO;
    collectionView.dataSource = self;
    collectionView.delegate = self;
    [collectionView registerNib:[UINib nibWithNibName:@"CollectionImageCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionImageCell"];
    CGFloat pageScrollWidth = itemWidth + ItemMargin;
    _panScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake((collectionView.frame.size.width - pageScrollWidth)/2 - ItemMargin/2.0, collectionViewHeight , pageScrollWidth, 20)];
    [self addSubview:_panScrollView];
//    _panScrollView.backgroundColor = [UIColor blueColor];
    _panScrollView.hidden = NO;
    _panScrollView.showsHorizontalScrollIndicator = NO;
    _panScrollView.alwaysBounceHorizontal = YES;
    _panScrollView.pagingEnabled = YES;
    _panScrollView.delegate = self;
    [_collectionView addGestureRecognizer:_panScrollView.panGestureRecognizer];
    _collectionView.panGestureRecognizer.enabled = NO;
    
//    _lineView = [[UIView alloc] initWithFrame:CGRectMake(collectionView.frame.size.width/3 , collectionViewHeight , collectionView.frame.size.width/3, 20)];
//    _line1 = [[UIView alloc] init];
//    _line1.backgroundColor = COLOR_ThemeRed;
//    [_lineView addSubview:_line1];
//
//    _line2 = [[UIView alloc] init];
//    _line2.backgroundColor = CCellSeperLineColor;
//    [_lineView addSubview:_line2];
//
//    _line3 = [[UIView alloc] init];
//    _line3.backgroundColor = CCellSeperLineColor;
//    [_lineView addSubview:_line3];
//
//    _line4 = [[UIView alloc] init];
//    _line4.backgroundColor = CCellSeperLineColor;
//    [_lineView addSubview:_line4];
//
//    [self addSubview:_lineView];
}

- (void)setDataSource:(NSArray *)dataSource{
    _dataSource = dataSource;
    [self updateView];
}

- (void)updateView {
//    [self updateLineIndicator:0];
    [_collectionView reloadData];
    [self addTimer];
}

- (void)addTimer {
    if (self.timer.isValid) {
        [self.timer invalidate];
    }
    self.timer = [NSTimer timerWithTimeInterval:5.0f target:self selector:@selector(autoScroll) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)autoScroll {
    if (self.dataSource.count <= 1) {
        return;
    }
    // 滚到最后一页的时候，回到第一页
    if (_panScrollView.contentOffset.x >= _panScrollView.frame.size.width * (_dataSource.count - 1)) {
//        [self updateLineIndicator:0];
        [_panScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else {
        CGFloat pageWidth = _panScrollView.frame.size.width;
        [_panScrollView setContentOffset:CGPointMake(_panScrollView.contentOffset.x + _panScrollView.frame.size.width, 0) animated:YES];
//        int currentPage = floor((_panScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//        [self updateLineIndicator:currentPage+1];
    }
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    _panScrollView.contentSize = CGSizeMake(_panScrollView.frame.size.width * _dataSource.count, 0);
    return _dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.item < _dataSource.count) {
        cell.model = self.dataSource[indexPath.item];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate) {
        [self.delegate clickCardsView:self atRow:indexPath.row];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _panScrollView) {
        _collectionView.contentOffset = _panScrollView.contentOffset;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    CGFloat pageWidth = _panScrollView.frame.size.width;
//    int currentPage = floor((_panScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//    [self updateLineIndicator:currentPage];
}

//- (void)updateLineIndicator:(NSInteger)page{
//
//    CGFloat width = CGRectGetWidth(self.lineView.frame);
//    CGFloat marginHor = 16.0f;
//    CGFloat widthLineGray = (width - 3*marginHor)/5.0;
//    CGFloat widthLineRed = widthLineGray * 2;
//    switch (page) {
//        case 0:
//        {
////            [UIView animateWithDuration:0.5 animations:^{
//                self.line1.frame = CGRectMake(0, 8, widthLineRed, 2);
//                self.line2.frame = CGRectMake(widthLineRed + marginHor, 8, widthLineGray, 2);
//                self.line3.frame = CGRectMake(CGRectGetMaxX(self.line2.frame) +  marginHor, 8, widthLineGray, 2);
//                self.line4.frame = CGRectMake(CGRectGetMaxX(self.line3.frame) +  marginHor, 8, widthLineGray, 2);
////            } completion:^(BOOL finished) {
////
////            }];
//        }
//            break;
//        case 1:
//        {
////            [UIView animateWithDuration:0.5 animations:^{
//                self.line2.frame = CGRectMake(0, 8, widthLineGray, 2);
//                self.line1.frame = CGRectMake(CGRectGetMaxX(self.line2.frame) +  marginHor, 8, widthLineRed, 2);
//                self.line3.frame = CGRectMake(CGRectGetMaxX(self.line1.frame) +  marginHor, 8, widthLineGray, 2);
//                self.line4.frame = CGRectMake(CGRectGetMaxX(self.line3.frame) +  marginHor, 8, widthLineGray, 2);
////            } completion:^(BOOL finished) {
////
////            }];
//        }
//            break;
//        case 2:
//        {
////            [UIView animateWithDuration:0.5 animations:^{
//                self.line2.frame = CGRectMake(0, 8, widthLineGray, 2);
//                self.line3.frame = CGRectMake(CGRectGetMaxX(self.line2.frame) +  marginHor, 8, widthLineGray, 2);
//                self.line1.frame = CGRectMake(CGRectGetMaxX(self.line3.frame) +  marginHor, 8, widthLineRed, 2);
//                self.line4.frame = CGRectMake(CGRectGetMaxX(self.line1.frame) +  marginHor, 8, widthLineGray, 2);
////            } completion:^(BOOL finished) {
////
////            }];
//        }
//            break;
//        case 3:
//        {
////            [UIView animateWithDuration:0.5 animations:^{
//                self.line2.frame = CGRectMake(0, 8, widthLineGray, 2);
//                self.line3.frame = CGRectMake(CGRectGetMaxX(self.line2.frame) +  marginHor, 8, widthLineGray, 2);
//                self.line4.frame = CGRectMake(CGRectGetMaxX(self.line3.frame) +  marginHor, 8, widthLineGray, 2);
//                self.line1.frame = CGRectMake(CGRectGetMaxX(self.line4.frame) +  marginHor, 8, widthLineRed, 2);
////            } completion:^(BOOL finished) {
////
////            }];
//        }
//            break;
//        default:
//            break;
//    }
//}
@end
