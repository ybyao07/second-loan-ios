//
//  HomeHeaderView.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "HomeHeaderView.h"

#define BannerHeight  ((SCREEN_WIDTH) * (110 / 375.0))
#define kRatio 1
#define kTopLabelHeight 21
#define kCenterViewTop 10
#define kCneterViewHeight 270
#define kVerticalScrollViewTop 20

@implementation HomeHeaderView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.develop(^(FFactory * factory){
            factory.imageView().fsImage(@"home_top_bg").fsImgContentMode(UIViewContentModeScaleToFill).fsFvid(@"topBg");
            factory.label().fsText(@"秉承普惠金融理念 服务齐鲁大地").fsFont(Font14).fsTextColor(0xffffff).fsFvid(@"lbllogo");
            factory.button().fsText(@"了解我们").fsFont(Font14).fsTextColor(0xffffff).fsOnClick(aboutUsClick:).fsRadius(10.0).fsFvid(@"aboutUs");
            factory.cView([HomeTopViewNoLogin newView]).fsFvid(@"homeNoLogin");
            factory.cView([HomeTopViewLoginCredit newView]).fsFvid(@"homeLoginCredit");
            factory.cView(self.homeScrollView);
        }).fsBackGroundColor(0xFAFAFA);
    }
    self.headTopView = (HomeTopViewNoLogin *)fQuery(self, @"homeNoLogin");
    self.headTopCreditView = (HomeTopViewLoginCredit *)fQuery(self, @"homeLoginCredit");
    self.lblLogo = (UILabel *)fQuery(self, @"lbllogo");
    self.btnAboutUs = (UIButton *)fQuery(self, @"aboutUs");
    self.btnAboutUs.hidden = YES;
    [fQuery(self, @"topBg") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo(338 * kRatio);
    }];
    [fQuery(self, @"lbllogo") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(KStatusBarAndNavigationBarHeight);
        make.left.equalTo(self).with.offset(10);
        make.height.mas_equalTo(kTopLabelHeight);
    }];
    [fQuery(self, @"aboutUs") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fQuery(self, @"lbllogo"));
        make.right.equalTo(self).with.offset(-10);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(70);
    }];
    
    [fQuery(self, @"homeNoLogin") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self, @"lbllogo").mas_bottom).with.offset(kCenterViewTop);
        make.centerX.equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH-20);
        make.height.mas_equalTo(kCneterViewHeight);
    }];
    [fQuery(self, @"homeLoginCredit") mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fQuery(self, @"lbllogo").mas_bottom).with.offset(kCenterViewTop);
        make.centerX.equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH-20);
        make.height.mas_equalTo(kCneterViewHeight);
    }];
    
    [self.homeScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headTopView.mas_bottom).with.offset(kVerticalScrollViewTop);
        make.left.equalTo(self).with.offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH-20);
        make.height.mas_equalTo(BannerHeight-20);
    }];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"了解我们"];
    NSRange strRange = {0,[str length]};
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [((UIButton *)fQuery(self, @"aboutUs")) setAttributedTitle:str forState:UIControlStateNormal];

    [self.headTopView.btnCredit addTarget:self action:@selector(checkClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headTopCreditView.btnCredit addTarget:self action:@selector(goBorrowClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headTopCreditView.btnCheckBorrow addTarget:self action:@selector(checkBorrowClick:) forControlEvents:UIControlEventTouchUpInside];
    return self;
}

- (void)aboutUsClick:(UIButton *)btn{
    if (self.aboutClickBlock) {
        self.aboutClickBlock();
    }
}

- (void)checkClick:(UIButton *)sender{
    sender.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.enabled = YES;
    });
    if (self.checkMyCreditBlock) {
        self.checkMyCreditBlock();
    }
}

- (void)checkBorrowClick:(UIButton *)sender{
    sender.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.enabled = YES;
    });
    if (self.checkMyBorrowBlock) {
        self.checkMyBorrowBlock();
    }
}

- (void)goBorrowClick:(UIButton *)sender{
    sender.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.enabled = YES;
    });
    if (self.goBorrowBlock) {
        self.goBorrowBlock();
    }
}

- (SDCycleScrollView *)homeScrollView
{
    if (!_homeScrollView) {
        _homeScrollView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, BannerHeight)];
        [_homeScrollView setBannerImageViewContentMode:UIViewContentModeScaleToFill];
        _homeScrollView.pageControlDotSize = CGSizeMake(8, 8);
        _homeScrollView.autoScrollTimeInterval = 3.0;
        _homeScrollView.backgroundColor = [UIColor clearColor];
        _homeScrollView.pageDotColor = COLOR_Gary_99;
        _homeScrollView.currentPageDotColor = COLOR_ThemeRed;
        _homeScrollView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
    }
    return _homeScrollView;
}

+ (CGFloat)height
{
    return KStatusBarAndNavigationBarHeight + kTopLabelHeight + kCenterViewTop + kCneterViewHeight + kVerticalScrollViewTop + BannerHeight - 10;
}
@end
