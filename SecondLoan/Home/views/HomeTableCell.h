//
//  HomeTableCell.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLHomeCardsView.h"
NS_ASSUME_NONNULL_BEGIN

#define kColletionHorMargin 10
@interface HomeTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

//@property (weak, nonatomic) IBOutlet SLHomeCardsView *homeCardView;


@end

NS_ASSUME_NONNULL_END
