//
//  CollectionImageCell.m
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/19.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import "CollectionImageCell.h"
#import "CommanTool.h"
@implementation CollectionImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [CommanTool setShadowOnView:self];
}


- (void)setModel:(ProductModel *)model
{
    _model = model;
    _imgView.backgroundColor = COLOR_White_FD;
    [_imgBankLogo sd_setImageWithURL:[NSURL URLWithString:[model.bankUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"place_holder_bank"]];
    _lblBankName.text = model.bankName;
    _lblProductName.text = model.productName;
    _lblProductDes.text  = model.productExplain;
}

@end
