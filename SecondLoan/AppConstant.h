//
//  AppConstant.h
//  mobilePD
//
//  Created by wangyan on 2016－05－12.
//  Copyright © 2016年 Goddess. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef  AppConstant_h
#define  AppConstant_h
/**
 *  Configure AppInfo
 */

//NS_ENUM，定义状态等普通枚举
typedef NS_ENUM(NSUInteger, HTTPCodeState) {
    HTTPCodeOK = 200,
    HTTPCodeLogout = 1024,
};

typedef NS_ENUM(NSUInteger, VerifyCode) {
    CodeVerifyNotStart = 2,   // 未认证
    CodeVerifyDoing = 0,       // 认证中
//    CodeVerifyNeedMaterial = 1, // 需要完善资料
    CodeVerifyPassed = 1,       // 认证通过
};

// 我的额度状态 -- 有个未提交的状态 -------
typedef NS_ENUM(NSUInteger, CreditStateCode) {
//    CodeCreditNotStart = 1,  // 待审核 --- 表示已提交
    CodeCreditDoing = 1,     // 额度审批中
    CodeCreditPassed = 2,     // 开通额度成功 ----- 可以获取额度
    CodeCreditNotPassed = 3,   // 开通额度失败
//    CodeCreditNotEdit = 5, // 未编辑信息
};

typedef NS_ENUM(NSUInteger, PuhuiLoanStateCode) { //1：待审核  2：审核中 3：已放款  4：审批未通过
    CodePuhuiLoanVerify = 1,     // 请先实名认证
    CodePuhuiLoanNeedInfo = 2,     // 请先完善信息
    CodePuhuiLoanPassed = 3,    // 您已经提交了额度申请，请查看额度
    CodePuhuiLoanFailed = 4,
    CodePuhuiLoanNotTime = 5
};

typedef NS_ENUM(NSUInteger, PuhuiQuotaStatusCode) { //1：待审核  2：审核中 3：已放款  4：审批未通过
    CodeQuotaNotStart = 0,   //已认证，未提交
//    CodeQuotaWait = 1,     // 待审核
    CodeQuotaDoing = 1,     // 审核中
    CodeQuotaPassed  = 2,    // 已放款
    CodeQuotaFailed = 3 // 审批未通过
};

// 我的贷款状态
typedef NS_ENUM(NSUInteger, MyLoanStateCode) {
//    CodeMyLoanWait = 2,  // 待审核
    CodeMyLoaning = 1,   // 审核中
    CodeMyLoanPassed = 2, // 已放款
    CodeMyLoanFailed = 3 // 审核失败
};

// 贷款申请记录
typedef NS_ENUM(NSUInteger, ApplyStateCode) {
//    CodeApplyWait = 1,  // 待审核
    CodeApplying = 1,   // 审核中
    CodeApplyPassed = 2, // 审批通过
    CodeApplyFailed = 3 // 失败
};

// 借款申请记录
typedef NS_ENUM(NSUInteger, BorrowStateCode) {
    CodeBorrowing = 1,  // 借款中
    CodeBorrowNotFinish = 2,   // 未结清
    CodeForrowFinished = 3 // 已结清
};

#define ColorFromHex(hexValue) \
[UIColor colorWithRed:((float)(((hexValue) & 0xFF0000) >> 16))/255.0 \
green:((float)(((hexValue) & 0xFF00  ) >> 8 ))/255.0 \
blue:((float)( (hexValue) & 0xFF    ))       /255.0 \
alpha:1.0]

// 登录成功
extern NSString *const NOTIFICATION_LOGINSUCCESS;
// 退出登录
extern NSString *const NOTIFICATION_LOGOUT;
// 登录过期
extern NSString *const NOTIFICATION_LOGIN_EXPIRED;
// 弹出登录提示
extern NSString *const NOTIFICATION_NEED_ALERT_LOGINVIEWCONTROLLER;
// 只在wifi情况下下载
extern NSString * const NOTIFICATION_WIFI_ONLY;

extern NSString * const NOTIFICATION_GROUP_LIST_NEW;
// MESSAGE
extern NSString * const NOTIFICATION_MESSAGE;

extern NSString * const NOTIFICATION_LOAD_BADGE;

extern NSString * const HomeCityCode;
extern NSString * const HomeCityName;

extern NSString * const kDeviceTokens;

extern NSString * const kNoNetworkMsg;
extern NSString * const kNetworkTimeoutMsg;
typedef void (^JLRun)(void);

//提示
extern NSString * const MsgEmpty;
extern NSString * const MsgAddAddressCityEmpty;
extern NSString * const MsgAddAddressEmpty;
extern NSString * const MsgHintPasswordNeed;
extern NSString * const MsgHintNewPasswordNeed;
extern NSString * const MsgHintNewEqualOld;
extern NSString * const MsgHintPasswordValid;
extern NSString * const MsgHintPhoneEmpty;
extern NSString * const MsgHintPhoneNoCorrect;
extern NSString * const MsgHintContactPhoneNoCorrect;
extern NSString * const MsgHintCompanyPhoneNoCorrect;
extern NSString * const MsgHintSpousePhoneNoCorrect;
extern NSString * const MsgHintErrorEMail;
extern NSString * const MsgHintErrorWX;
extern NSString * const MsgHintNameEmpty;
extern NSString * const MsgHintNameNoCorrect;
extern NSString * const MsgHintIDCardEmpty;
extern NSString * const MsgHintIDCardNoCorrect;
extern NSString * const MsgHintVerifyCodeNoCorrect;
extern NSString * const MsgHintVerifyCodeEmpty;
extern NSString * const MsgHintVerifyImgCodeEmpty;
extern NSString * const MsgHintPasswordSureNeed;
extern NSString * const MsgHintNewPasswordSureNeed;
extern NSString * const MsgHintNoFaceData;
extern NSString * const MsgHintFaceFailureData;
extern NSString * const kMsgHintBaiduyunNotFull;
extern NSString * const kBaiduyunNoInfo;


void JLAsyncRun(JLRun run);

void JLAsyncRunInMain(JLRun run);

void JLAsyncRunInMainDelayed(CGFloat afterTime,JLRun run);
#endif
