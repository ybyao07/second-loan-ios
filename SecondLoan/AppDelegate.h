//
//  AppDelegate.h
//  SecondLoan
//
//  Created by 姚永波 on 2019/8/18.
//  Copyright © 2019 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLLoanMessageModel.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

AppDelegate* getDelegate(void);

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic ,assign) BOOL isNetworkConnenct;
@property (strong,nonatomic) UINavigationController *masterNavigationController;

- (void)addLocalNotice:(SLLoanMessageModel *)model;


@end

